import 'bootstrap/dist/css/bootstrap.min.css';
import 'material-icons/css/material-icons.min.css';
import 'react-select/dist/react-select.css';
import 'react-notifications/lib/notifications.css';
import 'react-quill/dist/quill.snow.css';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-datetime/css/react-datetime.css';
