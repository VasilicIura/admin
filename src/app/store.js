import { applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import { router5Middleware } from 'redux-router5';
import { router } from './route.component';
import rootReducer from './root-reducer';
import { loaderMiddleware } from './loader';

const middleware = applyMiddleware(
  loaderMiddleware,
  promise(),
  thunk,
  router5Middleware(router),
);

const initialState = {};

const enhancers = [];
const { devToolsExtension } = window;

if (typeof devToolsExtension === 'function') {
  enhancers.push(devToolsExtension());
}

const composedEnhancers = compose(
  middleware,
  ...enhancers,
);

export const store = createStore(rootReducer, initialState, composedEnhancers);
