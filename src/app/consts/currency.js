export const CURRENCIES = {
  EUR: '€',
  GBP: '£',
  USD: '$',
};
