import { createSelector } from 'reselect';

const eventChainsSelector = state => state.chainsWorkflow;

const filteredEvents = createSelector(
  eventChainsSelector,
  event => event.get('filteredEvents').toJS(),
);

const filterCriteriasSelector = createSelector(
  eventChainsSelector,
  criterias => criterias.get('criteria').toJS(),
);

const eventsListSelector = createSelector(
  eventChainsSelector,
  (events) => {
    const list = events.get('filteredEvents').toJS();
    return list && list.content;
  },
);

export {
  eventsListSelector,
  filteredEvents,
  filterCriteriasSelector,
};
