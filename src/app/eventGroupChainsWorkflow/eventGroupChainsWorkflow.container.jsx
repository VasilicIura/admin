import React from 'react';
import { connect } from 'react-redux';
import { func, object } from 'prop-types';
import { bindActionCreators } from 'redux';
import { Col, Row } from 'react-bootstrap';
import {
  COLUMNS,
  filteredEvents,
  searchEvents,
  fetchEventStatuses,
  changePagination,
  changeSorting,
} from 'app/events';
import Table from '../common/table';
import {
  AddEventGroupChains,
  SwitchCustomCell,
  EventsSwitchFilterCell,
} from './components';


class EventGroupChainsWorkflow extends Table {
  static defaultProps = {
    events: null,
  };

  static propTypes = {
    fetchData: func.isRequired,
    changePagination: func.isRequired,
    events: object,
  };

  componentDidMount = () => this.props.fetchEventStatuses();

  getCustomCell = cell => <SwitchCustomCell {...cell} />;
  switchFilter = grid => <EventsSwitchFilterCell {...grid} />;

  render = () => (
    <div className="events-container">
      <Row>
        <Col xs={12}>
          {this.renderTable(
            COLUMNS(this.getCustomCell),
            this.props.events,
            false,
            this.switchFilter,
            'Events Group Chains',
          )}
          <div className="add-event">
            <AddEventGroupChains />
          </div>
        </Col>
      </Row>
    </div>
  );
}

const mapStateToProp = state => ({
  events: filteredEvents(state),
});

const mapDispatchToProp = dispatch => (bindActionCreators({
  changePagination,
  changeSorting,
  fetchData: () => searchEvents(),
  fetchEventStatuses,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(EventGroupChainsWorkflow);
