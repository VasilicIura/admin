import { createAction } from 'redux-actions';
import { filterSelector, queryFilters } from 'fe-shared';
import { ACTIONS, FILTER_TYPES } from './consts';
import { filterCriteriasSelector } from './selectors';
import { tradingApi } from '../api';

const filterEvents = createAction(
  ACTIONS.FILTER_CHAINS_WORKFLOW_EVENTS,
  (criteria = []) => tradingApi.post('events/search', { ...criteria }),
);

const setFilterCriterias = createAction(
  ACTIONS.SET_CHAINS_WORKFLOW_FILTERS,
  values => values,
);

const setPagination = createAction(
  ACTIONS.CHANGE_CHAINS_WORKFLOW_PAGINATION,
  (pagination = {}) => pagination,
);

const searchEvents = () =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'eventGroupChainsFilters' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    dispatch(filterEvents(filterCriteriasSelector(newState)));
  };

export {
  setPagination,
  searchEvents,
};
