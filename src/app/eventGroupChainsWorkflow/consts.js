import keyMirror from 'keymirror';

export const EVENT_RGOUP_PERIODS = [
  'M - S',
  'S - F',
  ' Weekends Events',
  '1 Week',
  '2 Weeks',
  '3 Weeks',
];

export const ACTIONS = keyMirror({
  FILTER_CHAINS_WORKFLOW_EVENTS: null,
  SET_CHAINS_WORKFLOW_FILTERS: null,
  CHANGE_CHAINS_WORKFLOW_PAGINATION: null,
});

export const FILTER_TYPES = {
  IN: [
    'eventStatusPk',
    'leagueId',
    'participants',
  ],
  LIKE: [
    'name',
    'location',
  ],
};
