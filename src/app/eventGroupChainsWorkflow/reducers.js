import { handleActions } from 'redux-actions';
import { Map, List } from 'immutable';
import { ACTIONS } from './consts';

const initialState = Map({
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  filteredEvents: Map({}),
});

export const reducer = handleActions({
  [`${ACTIONS.FILTER_CHAINS_WORKFLOW_EVENTS}_FULFILLED`]:
    (state, action) => state.set('filteredEvents', Map(action.payload)),
  [ACTIONS.SET_CHAINS_WORKFLOW_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
  [ACTIONS.CHANGE_CHAINS_WORKFLOW_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
}, initialState);
