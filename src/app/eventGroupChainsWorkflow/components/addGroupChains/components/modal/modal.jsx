import React, { Component } from 'react';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { CreateEventGroupChainsForm } from '../form';

export class AddGroupChainsModal extends Component {
  state = {
    showModal: false,
  };

  toggle = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>add event group chains</span>
      </Tooltip>
    );

    return (
      <div style={{ float: 'left', marginRight: '10px' }}>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="add-button"
            onClick={this.toggle}
          >
            <i className="mi mi-add" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggle}
          className="event-group-chains"
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Add Chains of Event Groups</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal &&
              <CreateEventGroupChainsForm />
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default AddGroupChainsModal;
