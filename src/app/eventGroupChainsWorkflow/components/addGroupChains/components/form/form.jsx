import React, { Component } from 'react';
import { Form, Col, Row } from 'react-bootstrap';
import { reduxForm } from 'redux-form';
import { ChainNameField, EventGroupPeriod, ChainEventsList, EventGroupChainPreview } from './components';

class CreateEventGroupChainsForm extends Component {
  render = () => (
    <Row>
      <Col>
        <Form>
          <div>
            <ChainNameField />
            <EventGroupPeriod />
            <div className="clearfix">
              <Col md="6">
                <ChainEventsList />
              </Col>
              <Col md="6">
                <EventGroupChainPreview />
              </Col>
            </div>
          </div>
        </Form>
      </Col>
    </Row>
  );
}

export default (reduxForm({
  form: 'eventGroupChains',
}))(CreateEventGroupChainsForm);

