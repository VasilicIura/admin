import React from 'react';
import { func, array } from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { searchEvents, autocompleteEvents } from 'app/events';
import {
  reduxFilter,
  FilterField,
  AutoCompleteFilter,
} from 'fe-shared';

class EventsNameFilters extends React.PureComponent {
  static defaultProps = {
    statuses: null,
    participants: null,
  };

  static propTypes = {
    autocompleteLeagues: func.isRequired,
    autocompleteParticipants: func.isRequired,
    autocompleteEvents: func.isRequired,
    searchEvents: func.isRequired,
    statuses: array,
    participants: array,
  };

  render = () => (
    <div style={{ margin: '10px 0' }}>
      <FilterField
        name="name"
        component={AutoCompleteFilter}
        autoCompleteAction={this.props.autocompleteEvents}
        valueKey="name"
        {...this.props}
      />
    </div>
  );
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  searchEvents,
  autocompleteEvents,
}, dispatch));

export default compose(
  connect(null, mapDispatchToProps),
  reduxFilter({
    filter: 'eventGroupChainsFilters',
  }),
)(EventsNameFilters);
