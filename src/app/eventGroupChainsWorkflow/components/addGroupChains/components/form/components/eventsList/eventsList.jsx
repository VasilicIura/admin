import React, { Component } from 'react';
import { array, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ListGroupItem, ListGroup, Row, Col } from 'react-bootstrap';
import { eventsListSelector, searchEvents, setPagination } from 'app/eventGroupChainsWorkflow';
import { EventsFiltersPreview, EventsNameFilters } from '../filter-events';

class ChainEventsList extends Component {
  static defaultProps = {
    events: [],
  };

  static propTypes = {
    events: array,
    searchEvents: func.isRequired,
    setPagination: func.isRequired,
  };

  componentDidMount = () => {
    this.props.setPagination({
      page: 0,
      size: 100,
    });

    this.props.searchEvents();
  };

  render = () => (
    <div>
      <h3>Select Events For Your Groups</h3>
      <div>
        <Row>
          <Col md="10">
            <EventsNameFilters />
          </Col>
          <Col md="2" className="dropDownChains">
            <EventsFiltersPreview />
          </Col>
        </Row>
        <ListGroup className="events-list-container">
          {
            !!this.props.events.length && this.props.events.map(event => (
              <ListGroupItem>
                {event.name}
              </ListGroupItem>
            ))
          }
        </ListGroup>
      </div>
    </div>
  );
}

const mapStateToProps = state => ({
  events: eventsListSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  searchEvents,
  setPagination,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(ChainEventsList);
