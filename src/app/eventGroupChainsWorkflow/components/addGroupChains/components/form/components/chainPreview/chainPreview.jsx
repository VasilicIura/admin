import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { ListGroupItem, ListGroup } from 'react-bootstrap';
import { filteredEvents } from 'app/events';

const EventGroupChainPreview = (props) => {
  const isEventsFetched = props.events && !!props.events.content;

  return (
    <div>
      <h3>Your Event Group Chain Preview</h3>
      <div>
        <ListGroup>
          {
            isEventsFetched && props.events.content.map(event => (
              <ListGroupItem>
                {event.name}
              </ListGroupItem>
            ))
          }
        </ListGroup>
      </div>
    </div>
  );
};

EventGroupChainPreview.defaultProps = {
  events: [],
};

EventGroupChainPreview.propTypes = {
  events: propTypes.array,
};

const mapStateToProps = state => ({
  events: filteredEvents(state),
});

export default connect(mapStateToProps)(EventGroupChainPreview);
