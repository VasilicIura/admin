import React from 'react';
import propTypes from 'prop-types';
import { Dropdown, Button } from 'react-bootstrap';
import { EventsFilter } from './components';

const ToggleFilters = (props) => {
  const handleClick = (e) => {
    e.preventDefault();

    props.onClick(e);
  };

  return (
    <Button
      className="event-chains-filter-button"
      onClick={handleClick}
    >
      <i className="mi mi-filter-list" />
    </Button>
  );
};

ToggleFilters.propTypes = {
  onClick: propTypes.func.isRequired,
};

class CustomMenu extends React.Component {
  render() {
    const { children } = this.props;

    return (
      <div className="dropdown-menu" style={{ padding: '' }}>
        <ul className="list-unstyled">
          {children}
        </ul>
      </div>
    );
  }
}


const EventsFiltersPreview = () => (
  <Dropdown bsSize="large">
    <ToggleFilters bsRole="toggle" />
    <CustomMenu bsRole="menu">
      <EventsFilter />
    </CustomMenu>
  </Dropdown>
);

export default EventsFiltersPreview;
