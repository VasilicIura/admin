import React from 'react';
import { Field } from 'redux-form';
import { Col } from 'react-bootstrap';
import { RadioControl } from 'fe-shared';
import { EVENT_RGOUP_PERIODS } from 'app/eventGroupChainsWorkflow';

const EventGroupPeriod = () => (
  <Col md="12">
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
      }}
    >
      {
      EVENT_RGOUP_PERIODS.map(item => (
        <Field
          name="groupPeriod"
          component={RadioControl}
          label={item}
        />
      ))
    }
    </div>
  </Col>
);

export default EventGroupPeriod;
