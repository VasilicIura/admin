import React from 'react';
import { func, array } from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { autocompleteLeagues } from 'app/leagues';
import { autocompleteParticipants } from 'app/participants';
import { eventStatusesSelector } from 'app/events';
import { searchEvents } from 'app/eventGroupChainsWorkflow';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  SelectFilter,
  AutoCompleteFilter,
  toSelectArray,
} from 'fe-shared';

class EventsFilter extends React.PureComponent {
  static defaultProps = {
    statuses: null,
    participants: null,
  };

  static propTypes = {
    autocompleteLeagues: func.isRequired,
    autocompleteParticipants: func.isRequired,
    searchEvents: func.isRequired,
    statuses: array,
    participants: array,
  };

  filterEvents = () => this.props.searchEvents();

  render = () => {
    const { statuses } = this.props;
    const statusesOptions = statuses && statuses.length;

    return (
      <Row style={{ width: '330px' }}>
        <Col md="12">
          <li style={{ margin: '10px' }}>
            <FilterField
              name="location"
              component={FilterInputControl}
              {...this.props}
            />
          </li>
          <li style={{ margin: '10px' }}>
            <FilterField
              name="leagueId"
              component={AutoCompleteFilter}
              autoCompleteAction={this.props.autocompleteLeagues}
              {...this.props}
            />
          </li>
          <li style={{ margin: '10px' }}>
            <FilterField
              name="eventStatusPk"
              component={SelectFilter}
              options={statusesOptions && toSelectArray(statuses)}
              {...this.props}
            />
          </li>
          <li className="submit-modal-btn">
            <Button
              type="button"
              bsStyle="success"
              onClick={this.filterEvents}
            >
              Filter
            </Button>
          </li>
        </Col>
      </Row>
    );
  };
}

const mapStateToProps = state => ({
  statuses: eventStatusesSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  searchEvents,
  autocompleteLeagues,
  autocompleteParticipants,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxFilter({
    filter: 'eventGroupChainsFilters',
  }),
)(EventsFilter);
