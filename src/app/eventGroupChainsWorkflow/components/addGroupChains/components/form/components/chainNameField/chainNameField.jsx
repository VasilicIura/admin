import React from 'react';
import { Field } from 'redux-form';
import { Col } from 'react-bootstrap';
import { ControlInput } from 'fe-shared';

const ChainNameField = () => (
  <Col md="12">
    <Field
      name="name"
      placeholder="Name"
      type="text"
      component={ControlInput}
    />
  </Col>
);

export default ChainNameField;
