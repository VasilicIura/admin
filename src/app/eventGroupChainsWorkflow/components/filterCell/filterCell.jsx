import React from 'react';
import { func, object, array } from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchLeagues, autocompleteLeagues } from 'app/leagues';
import { autocompleteParticipants } from 'app/participants';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  SelectFilter,
  toSelectArray,
  AutoCompleteFilter,
} from 'fe-shared';
import {
  fetchEventStatuses,
  fetchParticipants,
  eventStatusesSelector,
} from 'app/events';

class EventsSwitchFilterCell extends React.PureComponent {
  static defaultProps = {
    leagues: null,
    statuses: null,
    participants: null,
    column: null,
  };

  static propTypes = {
    fetchLeagues: func.isRequired,
    fetchEventStatuses: func.isRequired,
    fetchParticipants: func.isRequired,
    autocompleteLeagues: func.isRequired,
    autocompleteParticipants: func.isRequired,
    leagues: object,
    statuses: array,
    participants: array,
    column: object,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
      statuses,
    } = this.props;

    const statusesOptions = statuses;
    switch (name) {
      case 'name': return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
      case 'location': return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
      case 'participants': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteParticipants}
          {...this.props}
        />
      );
      case 'league': return (
        <FilterField
          name="leagueId"
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteLeagues}
          {...this.props}
        />
      );
      case 'eventStatusPk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={statusesOptions && toSelectArray(statuses)}
          {...this.props}
        />
      );
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapStateToProps = state => ({
  statuses: eventStatusesSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchLeagues,
  fetchEventStatuses,
  fetchParticipants,
  autocompleteLeagues,
  autocompleteParticipants,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxFilter({
    filter: 'eventsFilter',
  }),
)(EventsSwitchFilterCell);
