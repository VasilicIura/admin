import React from 'react';
import propTypes from 'prop-types';
import { EventLeague } from '../league';

const LeagueCellTemplate = ({ league }) => (
  <EventLeague
    league={league}
  />
);

LeagueCellTemplate.propTypes = {
  league: propTypes.object.isRequired,
};

export default LeagueCellTemplate;
