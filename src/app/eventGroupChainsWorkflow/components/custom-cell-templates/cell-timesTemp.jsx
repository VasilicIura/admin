import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const TimesTempCellTemplate = ({ value }) => (
  <em>{moment(value).format('YYYY-MM-DD')}</em>
);

TimesTempCellTemplate.propTypes = {
  value: PropTypes.string.isRequired,
};

export default TimesTempCellTemplate;
