import React from 'react';
import { object, any } from 'prop-types';
import ParticipantsCellTemplate from './cell-participants';
import LeagueCellTemplate from './cell-league';
import TimesTempCellTemplate from './cell-timesTemp';

const SwitchCustomCell = (props) => {
  const cell = props.column.id;
  const { value } = props;

  switch (cell) {
    case 'startTimeStamp':
      return <TimesTempCellTemplate value={value} />;
    case 'league':
      return <LeagueCellTemplate league={value} />;
    case 'participants':
      return <ParticipantsCellTemplate participants={value} />;
    default: return value;
  }
};

SwitchCustomCell.defaultProps = {
  value: null,
};

SwitchCustomCell.propTypes = {
  column: object.isRequired,
  value: any,
};

export default SwitchCustomCell;
