import React from 'react';
import { array } from 'prop-types';
import { EventParticipant } from '../participant';

const ParticipantsCellTemplate = ({ participants }) => (
  <div>
    { Array.isArray(participants) ? participants.map((participant, index) => (index !== 0 ?
          (
            <div key={participant.id}>
              <span>Home: </span>
              <EventParticipant participant={participant} />
            </div>
          ) :
          (
            <div key={participant.id}>
              <span>Away: </span>
              <EventParticipant participant={participant} />
            </div>
          )

      )) : 'No participants' }
  </div>
);

ParticipantsCellTemplate.defaultProps = {
  participants: [],
};

ParticipantsCellTemplate.propTypes = {
  participants: array,
};

export default ParticipantsCellTemplate;
