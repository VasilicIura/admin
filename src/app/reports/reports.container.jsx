import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './actions';
import * as selectors from './selectors';
import { Reports } from './reports';

class ReportsContainer extends Component {
  generateReport = (data) => {
    this.props.getReport(data);
  };

  render = () => (
    <Reports
      {...this.props}
      generateReport={this.generateReport}
    />
  );
}

const mapStateToProp = state => ({
  report: selectors.getReport(state),
});

const mapDispatchToProp = dispatch => (bindActionCreators({
  getReport: actions.getReport,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(ReportsContainer);
