import { createSelector } from 'reselect';

const reportsStateSelector = state => state.reports;

export const getReport = createSelector(
  reportsStateSelector,
  state => state.get('report').toJS(),
);
