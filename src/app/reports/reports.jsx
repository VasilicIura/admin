import React from 'react';
import { FilterForm } from './components';
import './reports.scss';

export const Reports = props => (
  <div>
    <h3>Reports</h3>
    <FilterForm onSubmit={props.generateReport} />
    <table className="table table-condensed table-striped">
      <thead>
        <tr>
          <th>Title</th>
          <th>Before</th>
          <th>Debit</th>
          <th>Credit</th>
          <th>Difference</th>
          <th>Current</th>
        </tr>
      </thead>
      <tbody>
        {props.report.map((line, index) => (
          <tr key={index}>
            <td>{line.title}</td>
            <td><b>{line.before}</b> {line.currency}</td>
            <td><b>{line.debit}</b> {line.currency}</td>
            <td><b>{line.credit}</b> {line.currency}</td>
            <td><b>{line.diff}</b> {line.currency}</td>
            <td><b>{line.current}</b> {line.currency}</td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);
