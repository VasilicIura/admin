import { createAction } from 'redux-actions';
import { walletApi } from 'app/api';
import * as ACTIONS from './actionTypes';

const setReport = createAction(
  ACTIONS.SET,
  option => option,
);

export function getReport(data) {
  return (dispatch) => {
    walletApi.post(`report/${data.currency}`, {
      criteria: [{
        field: 'timeStamp',
        from: data.from,
        to: data.to,
      }],
    })
      .then(response => dispatch(setReport(response)));
  };
}
