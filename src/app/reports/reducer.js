import { Map, List, fromJS } from 'immutable';
import { handleActions } from 'redux-actions';
import * as ACTIONS from './actionTypes';

const initialState = Map({
  report: List(),
});

export const reducer = handleActions({
  [ACTIONS.SET]: (state, action) =>
    state.set('report', fromJS(action.payload)),
}, initialState);
