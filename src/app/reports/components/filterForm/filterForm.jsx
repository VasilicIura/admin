import React, { Component } from 'react';
import { Alert, Form, Button } from 'react-bootstrap';
import { any, func } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { ControlSelect, toSelectArray, ControlDatePicker } from 'fe-shared';
import { CURRENCIES } from 'app/consts';
import { validate } from './validate';

class FilterForm extends Component {
  static propTypes = {
    handleSubmit: func.isRequired,
    submitting: any.isRequired,
  };

  render() {
    const {
      handleSubmit,
      submitting,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="currency"
          placeholder="Currency"
          component={ControlSelect}
          options={toSelectArray(Object.keys(CURRENCIES))}
          label="Currency:"
        />
        <Field
          name="from"
          placeholder="From"
          label="From:"
          component={ControlDatePicker}
          from
        />
        <Field
          name="to"
          placeholder="To"
          label="To:"
          component={ControlDatePicker}
          to
        />
        {this.props.error && <Alert bsStyle="danger">{this.props.error}</Alert>}
        <Button
          type="submit"
          disabled={submitting}
          bsStyle="success"
          style={{ marginBottom: '10px' }}
        >
          Generate Report
        </Button>
      </Form>
    );
  }
}

export default
reduxForm({
  form: 'filterForm',
  validate,
})(FilterForm);
