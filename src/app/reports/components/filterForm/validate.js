export const validate = (values) => {
  const errors = {};

  if (!values.currency) {
    errors.currency = 'Required';
  }
  if (!values.from) {
    errors.from = 'Required';
  }
  if (!values.to) {
    errors.to = 'Required';
  }

  return errors;
};
