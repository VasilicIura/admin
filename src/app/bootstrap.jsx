import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'react-bootstrap/dist/react-bootstrap.min';
import App from './app';
import { store } from './store';
import AppRoutes, { router } from './route.component';

/* eslint-disable import/first */
import 'styles/index.scss';
/* eslint-enable import/first */

function run() {
  const container = document.querySelector('#app-root');
  if (!container) {
    throw new Error('Can\'t find root node');
  }

  router.start(() => {
    ReactDOM.render(
      <Provider store={store}>
        <App>
          <AppRoutes />
        </App>
      </Provider>,
      container,
    );
  });
}

document.addEventListener('DOMContentLoaded', run);
