import React, { Component } from 'react';
import createRouter from 'router5';
import browserPlugin from 'router5/plugins/browser';
import { connect } from 'react-redux';
import { shape, func, string } from 'prop-types';
import { routes } from './routes';
import { store } from './store';
import { isAuthenticatedSelector } from './auth/selectors';

const authMiddleware = () => (toState, fromState, done) => {
  if (toState.name !== 'login' && !isAuthenticatedSelector(store.getState())) return done({ redirect: { name: 'login' } });
  return done();
};

const NotFound = () => <div>Not found</div>;

class Root extends Component {
  static propTypes = {
    route: shape({ name: string, component: func }).isRequired,
  };

  render() {
    const { route } = this.props;
    const segment = route ? route.name.split('.')[0] : undefined;
    const config = routes
      .find(routeItem => routeItem.name === segment) || { component: NotFound };
    const RouteComponent = config.component;
    return (<RouteComponent route={route} key={route.meta.id} {...route.params} />);
  }
}

export default connect(state => state.router)(Root);

export const router = createRouter(routes, { defaultRoute: 'participants' })
  .usePlugin(browserPlugin({ useHash: false }))
  .useMiddleware(authMiddleware);
