import { Map, List } from 'immutable';
import { handleActions } from 'redux-actions';
import * as ACTIONS from './actionTypes';
import users from '../userManagement/components/users';

const initialState = Map({
  transactions: Map({}),
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
});

export const reducer = handleActions({
  [ACTIONS.SET_TRANSACTIONS_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
  [ACTIONS.CHANGE_TRANSACTION_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [ACTIONS.CHANGE_TRANSACTION_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
  [`${ACTIONS.FETCH_TRANSACTIONS}_FULFILLED`]: (state, action) =>
    state.set('transactions', Map(action.payload)),
  [ACTIONS.CLEAR_TRANSACTIONS]: () => initialState,
  [users.actionTypes.SELECT_USER]: () => initialState,
}, initialState);
