import { createAction } from 'redux-actions';
import { queryFilters, filterSelector } from 'fe-shared';
import { walletApi, commonApi } from 'app/api';
import * as ACTIONS from './actionTypes';
import { FILTER_TYPES } from './consts';
import { filterCriteriasSelector } from './selectors';

const setFilterCriterias = createAction(
  ACTIONS.SET_TRANSACTIONS_FILTERS,
  values => values,
);

const fetch = createAction(
  ACTIONS.FETCH_TRANSACTIONS,
  (criteria = []) => walletApi.post('transactions', { ...criteria }),
);

const fetchById = createAction(
  ACTIONS.FETCH_TRANSACTIONS,
  (criteria = [], id) => walletApi.post(`transactions/${id}`, { ...criteria }),
);

const clear = createAction(ACTIONS.CLEAR_TRANSACTIONS);

function clearTransactions() {
  return (dispatch) => {
    dispatch(clear());
  };
}

function fetchTransactions(id) {
  return (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'transactions' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    if (id === undefined) {
      dispatch(fetch(filterCriteriasSelector(newState)));
      return;
    }
    dispatch(fetchById(filterCriteriasSelector(newState), id));
  };
}

const fetchTransactionTypes = createAction(
  ACTIONS.FETCH_TRANSACTION_TYPES,
  () => commonApi.get('enums/types/entity/Transaction'),
);

function fetchTransactionStatuses() {
  return () => {
    let statuses = [];

    return commonApi.get('enums/statuses/entity/Transaction%20-%20External%20User%20Deposit')
      .then((response) => {
        statuses = statuses.concat(response);
        return commonApi.get('enums/statuses/entity/Transaction%20-%20External%20User%20Withdrawal');
      })
      .then((response) => {
        statuses = statuses.concat(response);
        return commonApi.get('enums/statuses/entity/Transaction%20-%20External%20User%20Refund');
      })
      .then(response => statuses.concat(response));
  };
}

const refundTransaction = createAction(
  ACTIONS.REFUND_TRANSACTION,
  data => walletApi.post(`transactions/refund/${data.id}`, { amount: data.amount }),
);

const changePagination = createAction(
  ACTIONS.CHANGE_TRANSACTION_PAGINATION,
  (pagination = {}) => pagination,
);

const changeSorting = createAction(
  ACTIONS.CHANGE_TRANSACTION_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

export {
  fetchTransactions,
  refundTransaction,
  fetchTransactionTypes,
  fetchTransactionStatuses,
  changePagination,
  changeSorting,
  clearTransactions,
};
