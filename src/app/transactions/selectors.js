import { createSelector } from 'reselect';

const transactionsStateSelector = state => state.transactions;

export const getTransactions = createSelector(
  transactionsStateSelector,
  state => state.get('transactions').toJS(),
);

export const filterCriteriasSelector = createSelector(
  transactionsStateSelector,
  criterias => criterias.get('criteria').toJS(),
);
