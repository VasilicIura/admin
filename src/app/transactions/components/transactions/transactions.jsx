import React from 'react';
import { func, object, any, bool } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SubmissionError } from 'redux-form';
import {
  fetchTransactions,
  refundTransaction,
  fetchTransactionTypes,
  fetchTransactionStatuses,
  changePagination,
  changeSorting,
  clearTransactions,
} from '../../actions';
import * as selectors from '../../selectors';
import { RefundModal, FilterCell, CustomCell } from './components';
import Table from '../../../common/table';

class Transactions extends Table {
  static propTypes = {
    fetchData: func.isRequired,
    fetchTransactionTypes: func.isRequired,
    fetchTransactionStatuses: func.isRequired,
    refundTransaction: func.isRequired,
    data: any,
    loading: bool,
    changePagination: func.isRequired,
    changeSorting: func.isRequired,
    route: object,
  };

  static defaultProps = {
    route: null,
    data: [],
    loading: false,
  };

  state = {
    refundTransaction: null,
  };

  componentDidMount = () => {
    this.props.fetchData();
    const { id } = this.props.route.params;
    this.setState({ id });
    this.props.fetchTransactionTypes()
      .then(result => this.setState({
        transactionTypes: result.value.map(
          type => ({ value: type, label: type }),
        ),
      }));
    this.props.fetchTransactionStatuses()
      .then(response => this.setState({
        transactionStatuses: [...new Set(response)]
          .map(status => ({ value: status, label: status })),
      }));
  };
  componentWillUnmount() {
    this.props.clearTransactions();
  }
  transactionsColumns = [
    { Header: 'Amount', accessor: 'amount' },
    { Header: 'User', accessor: 'user' },
    { Header: 'Type', accessor: 'theType' },
    { Header: 'Status', accessor: 'etTransactionStatusPk' },
    {
      Header: 'Last Updated',
      accessor: 'lastUpdated',
      Cell: row => this.useCustomCell(row),
    },
    {
      Header: 'Refund',
      accessor: 'refund',
      Filter: ({ onChange }) => (
        <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
      ),
      Cell: row => this.useCustomCell(row),
    },
  ];

  userTransactionsColumns = [
    { Header: 'Amount', accessor: 'amount' },
    { Header: 'Type', accessor: 'theType' },
    { Header: 'Status', accessor: 'transactionStatusPk' },
    { Header: 'Last Updated', accessor: 'lastUpdated', Cell: row => <div>{this.useCustomCell(row)}</div> },
    {
      Header: 'Refund',
      accessor: 'refund',
      Filter: ({ onChange }) => (
        <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
      ),
      Cell: row => <div>{this.useCustomCell(row)}</div>,
    },
  ];

  switchFilters = cellProps =>
    (<FilterCell
      {...cellProps}
      {...this.props}
      transactionTypes={this.state.transactionTypes}
      transactionStatuses={this.state.transactionStatuses}
    />);

  useCustomCell = grid => <CustomCell onRefund={this.refund} {...grid} />;

  onRefund = (data) => {
    try {
      return this.props.refundTransaction(data)
        .then((response) => {
          const { value: responseData } = response;
          const successStatus = 'Pending Payment-Provider Transaction Pending Settlement';

          if (responseData.status === successStatus) {
            this.setState({ refundTransaction: null });
          }

          return Promise.reject(new SubmissionError({
            _error: responseData.message || 'Refund failed.',
          }));
        });
    } catch (err) {
      return Promise.reject(new SubmissionError({
        _error: 'Refund failed.',
      }));
    }
  };

  refund = transaction => this.setState({
    refundTransaction: transaction,
  });

  closeRefund = () => this.setState({ refundTransaction: null });

  render() {
    const { data, loading, id } = this.props;
    return (!data) ? (
      <div>Loading</div>
    ) : (
      <div>
        {this.renderTable(id ? this.userTransactionsColumns : this.transactionsColumns,
          data, loading, this.switchFilters, id ? 'User Transactions' : 'Transactions')}
        {this.state.refundTransaction &&
          (<RefundModal
            onSubmit={this.onRefund}
            transaction={this.state.refundTransaction}
            onClose={this.closeRefund}
          />)
        }
      </div>
    );
  }
}

const mapStateToProp = state => ({
  data: selectors.getTransactions(state),
});

const mapDispatchToProp = (dispatch, props) => (bindActionCreators({
  fetchData: () => fetchTransactions(props.route.params.id),
  refundTransaction,
  fetchTransactionTypes,
  fetchTransactionStatuses,
  changePagination,
  changeSorting,
  clearTransactions,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(Transactions);
