import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';

const CustomCell = (props) => {
  switch (props.column.id) {
    case 'refund': return (
      <div>
        {(props.original.etTransactionStatusPk === 'Settled')
          ? (<a onClick={() => props.onRefund(props.original)}>Refund</a>)
          : 'Transactions not yet settled, cannot refund'
        }
      </div>
    );
    case 'lastUpdated':
      return props.value && moment(props.value).format('MMM D, YYYY h:mm A');
    default:
      return props.value;
  }
};

CustomCell.defaultProps = {
  original: null,
  value: null,
};

CustomCell.propTypes = {
  onRefund: propTypes.func.isRequired,
  column: propTypes.object.isRequired,
  original: propTypes.object,
  value: propTypes.any,
};

export default CustomCell;
