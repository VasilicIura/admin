import React from 'react';
import propTypes from 'prop-types';
import { Modal } from 'react-bootstrap';
import { RefundForm } from '../refundForm';

const RefundModal = props => (
  <Modal.Dialog>
    <Modal.Header>
      <Modal.Title>Refund</Modal.Title>
    </Modal.Header>

    <Modal.Body>
      <RefundForm
        onSubmit={props.onSubmit}
        initialValues={props.transaction}
        onClose={props.onClose}
      />
    </Modal.Body>
  </Modal.Dialog>
);

RefundModal.defaultProps = {
  transaction: null,
};

RefundModal.propTypes = {
  onSubmit: propTypes.func.isRequired,
  onClose: propTypes.func.isRequired,
  transaction: propTypes.object,
};

export default RefundModal;
