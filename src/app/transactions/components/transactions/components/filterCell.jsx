import React from 'react';
import { object, array } from 'prop-types';
import { compose } from 'recompose';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  SelectFilter,
} from 'fe-shared';

class FilterCell extends React.PureComponent {
  static defaultProps = {
    transactionTypes: [],
    transactionStatuses: [],
    column: null,
  };

  static propTypes = {
    transactionTypes: array,
    transactionStatuses: array,
    column: object,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
    } = this.props;

    switch (name) {
      case 'user': return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
      case 'name': return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
      case 'theType': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={this.props.transactionTypes}
          {...this.props}
        />
      );
      case 'transactionStatusPk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={this.props.transactionStatuses}
          {...this.props}
        />
      );
      default: return (
        <div />
      );
    }
  };

  render = () => this.switchTemplates();
}

export default compose(
  reduxFilter({
    filter: 'transactions',
  }),
)(FilterCell);
