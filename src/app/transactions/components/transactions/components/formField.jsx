import React from 'react';
import { FormGroup, FormControl, HelpBlock, ControlLabel } from 'react-bootstrap';

export default class FormField extends React.Component {
  static defaultProps = {
    doValidate: true,
  };

  // the field content
  content() {
    const { label } = this.props;
    return (
      <div>
        {label ? <ControlLabel>{label}</ControlLabel> : null}
        {this.field()}
        <FormControl.Feedback />
      </div>
    );
  }

  // the field itself
  field() {
    const {
      input,
      componentClass,
      type,
      placeholder,
      children,
    } = this.props;

    return (
      <FormControl {...input} componentClass={componentClass} type={type} placeholder={placeholder}>
        {children}
      </FormControl>
    );
  }

  render() {
    const { className, doValidate, meta } = this.props;
    if (doValidate) {
      let validationState = null;
      if (meta.touched) {
        validationState = meta.error ? 'error' : 'success';
      }

      return (
        <FormGroup
          className={className}
          validationState={validationState}
        >
          {this.content()}
          <HelpBlock>
            {meta.touched && meta.error ? meta.error : null}
          </HelpBlock>
        </FormGroup>
      );
    }

    return (
      <FormGroup className={className}>
        {this.content()}
      </FormGroup>
    );
  }
}
