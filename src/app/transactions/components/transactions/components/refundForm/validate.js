export const validate = (values) => {
  const errors = {};

  if (!values.amount) errors.amount = 'Required';

  return errors;
};
