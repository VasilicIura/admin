import React, { Component } from 'react';
import { Alert, Form, Button } from 'react-bootstrap';
import { any, func } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { FormField } from '../';
import { validate } from './validate';

class RefundForm extends Component {
  static propTypes = {
    handleSubmit: func.isRequired,
    submitting: any.isRequired,
  };

  render() {
    const {
      handleSubmit,
      submitting,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="amount"
          placeholder="Amount"
          component={FormField}
          label="Amount"
          doValidate
        />
        {this.props.error && <Alert bsStyle="danger">{this.props.error}</Alert>}
        <Button
          type="submit"
          disabled={submitting}
          bsStyle="success"
        >
          Refund
        </Button>
        <Button
          type="button"
          disabled={submitting}
          bsStyle="danger"
          onClick={this.props.onClose}
        >
          Cancel
        </Button>
      </Form>
    );
  }
}

export default
reduxForm({
  form: 'refund',
  validate,
})(RefundForm);
