import { Map, List, fromJS } from 'immutable';
import { handleActions } from 'redux-actions';
import * as ACTIONS from './actionTypes';

const initialState = Map({
  balance: List(),
  balanceControls: List(),
  balanceRecords: List(),
});

export const reducer = handleActions({
  [ACTIONS.SET]: (state, action) =>
    state.set('balance', fromJS(action.payload)),
  [ACTIONS.UPDATE]: (state, action) => {
    const { amount, currency } = action.payload;
    const balance = state.get('balance').toJS();
    const balancePerCurrency = balance.find(b => b.currency === currency);

    if (balancePerCurrency) {
      balancePerCurrency.amount = amount;
      return state.set('balance', fromJS(balance));
    }

    return state;
  },
  [ACTIONS.SET_BALANCE_CONTROLS]: (state, action) =>
    state.set('balanceControls', fromJS(action.payload)),
  [ACTIONS.SET_BALANCE_RECORDS]: (state, action) =>
    state.set('balanceRecords', fromJS(action.payload)),
}, initialState);
