export const SET = 'balance/SET';
export const UPDATE = 'balance/UPDATE';
export const SET_BALANCE_CONTROLS = 'balance/SET_BALANCE_CONTROLS';
export const SET_BALANCE_RECORDS = 'balance/SET_BALANCE_RECORDS';
