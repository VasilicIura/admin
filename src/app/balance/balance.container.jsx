import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NotificationManager } from 'react-notifications';
import * as actions from './actions';
import * as selectors from './selectors';
import { Balance } from './balance';

class BalanceContainer extends Component {
  state = {
    showDeposit: false,
    showWithdraw: false,
  };
  componentDidMount() {
    this.props.getBalance();
  }


  onDepositSubmit = data =>
    this.props.onDeposit(data)
      .then(() => {
        this.toggleDeposit();
        NotificationManager.success('Deposit Successful');
        this.props.getBalanceControls();
      });

  onWithdrawSubmit = data =>
    this.props.onWithdraw(data)
      .then(() => {
        this.toggleWithdraw();
        NotificationManager.success('Withdraw Successful');
        this.props.getBalanceControls();
      });

  toggleDeposit = () => this.setState({ showDeposit: !this.state.showDeposit });
  toggleWithdraw = () => this.setState({ showWithdraw: !this.state.showWithdraw });


  render = () => (
    <Balance
      {...this.props}
      onDeposit={this.onDepositSubmit}
      onWithdraw={this.onWithdrawSubmit}
      showDeposit={this.state.showDeposit}
      showWithdraw={this.state.showWithdraw}
      toggleDeposit={this.toggleDeposit}
      toggleWithdraw={this.toggleWithdraw}
    />
  );
}

const mapStateToProp = state => ({
  balance: selectors.getBalance(state),
});

const mapDispatchToProp = dispatch => (bindActionCreators({
  getBalanceControls: actions.getBalanceControls,
  getBalance: actions.getBalance,
  onDeposit: actions.deposit,
  onWithdraw: actions.withdraw,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(BalanceContainer);
