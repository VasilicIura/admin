import { createSelector } from 'reselect';

const balanceStateSelector = state => state.balance;

export const getBalance = createSelector(
  balanceStateSelector,
  state => state.get('balance').toJS(),
);

export const getBalanceControls = createSelector(
  balanceStateSelector,
  state => state.get('balanceControls').toJS(),
);

export const getBalanceRecords = createSelector(
  balanceStateSelector,
  state => state.get('balanceRecords').toJS(),
);
