export const validate = (values) => {
  const errors = {};

  if (!values.amount) {
    errors.amount = 'Required';
  } else {
    const amount = +values.amount;
    if (!amount) errors.amount = 'Invalid amount given';
  }

  if (!values.currency) errors.currency = 'Required';

  return errors;
};
