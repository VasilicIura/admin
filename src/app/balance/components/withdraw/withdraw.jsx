import React from 'react';
import { Modal } from 'react-bootstrap';
import WithdrawForm from './form';

export const Withdraw = props => (
  <Modal
    show={props.show}
    onHide={props.onHide}
  >
    <Modal.Header closeButton>
      <Modal.Title className="modal-title">Withdraw</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <WithdrawForm
        onSubmit={props.onSubmit}
      />
    </Modal.Body>
  </Modal>
);
