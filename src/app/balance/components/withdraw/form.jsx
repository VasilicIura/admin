import React, { Component } from 'react';
import { Alert, Form, Button } from 'react-bootstrap';
import { any, func } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { ControlInput, ControlSelect, toSelectArray } from 'fe-shared';
import { CURRENCIES } from 'app/consts';
import { validate } from './validate';

class DepositForm extends Component {
  static propTypes = {
    handleSubmit: func.isRequired,
    submitting: any.isRequired,
  };

  render() {
    const {
      handleSubmit,
      submitting,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="amount"
          placeholder="Amount"
          component={ControlInput}
          label="Amount:"
          doValidate
        />
        <Field
          name="currency"
          placeholder="Currency"
          component={ControlSelect}
          options={toSelectArray(Object.keys(CURRENCIES))}
          label="Currency:"
        />
        {this.props.error && <Alert bsStyle="danger">{this.props.error}</Alert>}
        <Button
          type="submit"
          disabled={submitting}
          bsStyle="success"
        >
          Deposit
        </Button>
      </Form>
    );
  }
}

export default
reduxForm({
  form: 'deposit',
  validate,
})(DepositForm);
