import React from 'react';
import propTypes from 'prop-types';
import { Modal } from 'react-bootstrap';
import DepositForm from './form';

export const Deposit = props => (
  <Modal
    show={props.show}
    onHide={props.onHide}
  >
    <Modal.Header closeButton>
      <Modal.Title className="modal-title">Deposit</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <DepositForm
        onSubmit={props.onSubmit}
      />
    </Modal.Body>
  </Modal>
);

Deposit.defaultProps = {
  show: null,
  onHide: null,
  onSubmit: null,
};

Deposit.propTypes = {
  show: propTypes.any,
  onHide: propTypes.func,
  onSubmit: propTypes.any,
};
