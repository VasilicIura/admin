import React, { Component } from 'react';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import * as selectors from '../../selectors';
import { BalanceControls } from './balanceControls';

class BalanceContainer extends Component {
  static propTypes = {
    getBalanceControls: func.isRequired,
    getBalanceRecords: func.isRequired,
  };
  state = {
    selectedBalanceControl: null,
  };

  componentDidMount() {
    this.props.getBalanceControls();
  }

  onShowBalanceRecords = (balanceControl) => {
    this.setState({ selectedBalanceControl: balanceControl });
    this.props.getBalanceRecords(balanceControl);
  };

  onPageChange = (page, pageSize) => {
    this.props.getBalanceRecords(this.state.selectedBalanceControl, page, pageSize);
  };

  toggleBalanceRecords = () =>
    this.setState({ selectedBalanceControl: null });

  render = () => (
    <BalanceControls
      {...this.props}
      showBalanceRecords={this.onShowBalanceRecords}
      onCloseBalanceRecords={this.toggleBalanceRecords}
      isBalanceRecordsVisible={this.state.selectedBalanceControl}
      onPageChange={this.onPageChange}
    />
  );
}

const mapStateToProp = state => ({
  balanceControls: selectors.getBalanceControls(state),
  balanceRecords: selectors.getBalanceRecords(state),
});

const mapDispatchToProp = dispatch => (bindActionCreators({
  getBalanceControls: actions.getBalanceControls,
  getBalanceRecords: actions.getBalanceRecords,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(BalanceContainer);
