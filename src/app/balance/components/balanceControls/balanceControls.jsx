import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'react-bootstrap';
import BalanceRecordsTable from 'app/common/balanceRecordsTable';
import './balanceControls.scss';

export const BalanceControls = (props) => {
  BalanceControls.propTypes = {
    onPageChange: PropTypes.func.isRequired,
    balanceControls: PropTypes.any.isRequired,
    isBalanceRecordsVisible: PropTypes.any,
    onCloseBalanceRecords: PropTypes.any,
    balanceRecords: PropTypes.any,
  };

  BalanceControls.defaultProps = {
    isBalanceRecordsVisible: null,
    onCloseBalanceRecords: null,
    balanceRecords: null,
  };

  return (
    <div>
      <table className="table table-condensed table-striped controlsTable">
        <thead>
          <tr>
            <th>Title</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          { props.balanceControls.map((bc, index) => (
            <tr key={index} onClick={() => props.showBalanceRecords(bc)}>
              <td>{bc.title}</td>
              <td>{bc.amount} <b>{bc.currency}</b></td>
            </tr>
          ))}
        </tbody>
      </table>
      <Modal
        show={props.isBalanceRecordsVisible !== null}
        onHide={props.onCloseBalanceRecords}
      >
        <Modal.Header closeButton>
          <Modal.Title className="modal-title">Balance Records</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {props.balanceRecords &&
            <BalanceRecordsTable
              data={props.balanceRecords}
              onPageChange={props.onPageChange}
            />
          }
        </Modal.Body>
      </Modal>
    </div>
  );
};
