import React from 'react';
import propTypes from 'prop-types';
import { Button, Glyphicon } from 'react-bootstrap';
import { Deposit, Withdraw, BalanceControls } from './components';
import './balance.scss';

export const Balance = props => (
  <div>
    <h3>Balance</h3>
    <ul>
      {props.balance.map(amount => (
        <li key={amount.currency}>
          <b>{amount.currency}:</b>{amount.amount}
        </li>
      ))}
    </ul>

    <Button
      bsStyle="primary"
      style={{ marginRight: '10px' }}
      onClick={props.toggleDeposit}
    >
      <Glyphicon glyph="download" /> Deposit
    </Button>
    <Button bsStyle="primary" onClick={props.toggleWithdraw}>
      <Glyphicon glyph="upload" /> Withdraw
    </Button>

    <Deposit
      show={props.showDeposit}
      onSubmit={props.onDeposit}
      onHide={props.toggleDeposit}
    />
    <Withdraw
      show={props.showWithdraw}
      onSubmit={props.onWithdraw}
      onHide={props.toggleWithdraw}
    />

    <h4 style={{ textAlign: 'center' }}>Balance Controls</h4>
    <BalanceControls />
  </div>
);

Balance.propTypes = {
  balance: propTypes.array.isRequired,
  toggleDeposit: propTypes.func.isRequired,
  toggleWithdraw: propTypes.func.isRequired,
  onDeposit: propTypes.func.isRequired,
  showDeposit: propTypes.bool.isRequired,
  onWithdraw: propTypes.func.isRequired,
  showWithdraw: propTypes.bool.isRequired,
};
