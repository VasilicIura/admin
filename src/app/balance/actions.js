import { createAction } from 'redux-actions';
import { SubmissionError } from 'redux-form';
import { walletApi } from 'app/api';
import * as ACTIONS from './actionTypes';

const setBalance = createAction(
  ACTIONS.SET,
  option => option,
);

const updateBalance = createAction(
  ACTIONS.UPDATE,
  data => data,
);

const setBalanceControls = createAction(
  ACTIONS.SET_BALANCE_CONTROLS,
  option => option,
);

const setBalanceRecords = createAction(
  ACTIONS.SET_BALANCE_RECORDS,
  option => option,
);

export function getBalance() {
  return (dispatch) => {
    walletApi.get('admin/balance')
      .then(response => dispatch(setBalance(response)));
  };
}

export function getBalanceControls() {
  return (dispatch) => {
    walletApi.get('admin/balance/controls')
      .then(response => dispatch(setBalanceControls(response)));
  };
}

export function getBalanceRecords(balanceControl, requiredPage, pageSize) {
  return (dispatch) => {
    const page = requiredPage || 0;
    const size = pageSize || 10;

    const data = {
      pageable: {
        size,
        page,
      },
    };
    walletApi.post(`report/balanceRecords/${balanceControl.title}/${balanceControl.currency}`, data)
      .then(response => dispatch(setBalanceRecords(response)));
  };
}

export function deposit(data) {
  return dispatch =>
    walletApi.post('admin/deposit', data)
      .then((response) => {
        const updateData = {
          amount: response.amount,
          currency: data.currency,
        };
        dispatch(updateBalance(updateData));
      })
      .catch((err) => {
        if (err.response && err.response.data && err.response.data.message) {
          return Promise.reject(new SubmissionError({ _error: err.response.data.message }));
        }

        return Promise.reject(new SubmissionError({ _error: 'Deposit failed' }));
      });
}

export function withdraw(data) {
  return dispatch =>
    walletApi.post('admin/withdraw', data)
      .then((response) => {
        const updateData = {
          amount: response.amount,
          currency: data.currency,
        };
        dispatch(updateBalance(updateData));
      })
      .catch((err) => {
        if (err.response && err.response.data && err.response.data.message) {
          return Promise.reject(new SubmissionError({ _error: err.response.data.message }));
        }

        return Promise.reject(new SubmissionError({ _error: 'Withdraw failed' }));
      });
}
