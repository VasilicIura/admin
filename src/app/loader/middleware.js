import { showLoader, hideLoader } from './actions';

const isPromise = (value) => {
  if (value !== null && typeof value === 'object') {
    return value && typeof value.then === 'function';
  }

  return false;
};

export default store => next => (action) => {
  let promise;
  let chain = [];
  if (action.payload) {
    const PAYLOAD = action.payload;

    // Step 1.1: Is the promise implicitly defined?
    if (isPromise(PAYLOAD)) {
      promise = PAYLOAD;
    } else if (isPromise(PAYLOAD.promise)) {
      promise = PAYLOAD.promise; // eslint-disable-line
    } else if (
      typeof PAYLOAD === 'function' ||
      typeof PAYLOAD.promise === 'function'
    ) {
      promise = PAYLOAD.promise ? PAYLOAD.promise() : PAYLOAD();
    }
  }

  if (promise) {
    chain.push(promise);
    store.dispatch(showLoader());

    const onFinally = () => {
      chain = chain.filter(p => p !== promise);
      if (!chain.length) {
        setTimeout(() => store.dispatch(hideLoader()), 300);
      }
    };
    promise.then(onFinally).catch(onFinally);
  }

  return next(action);
};
