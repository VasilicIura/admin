import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loaderSelectors } from './selectors';

class Loader extends Component {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
  };

  render() {
    if (!this.props.loading) {
      return <div>{this.props.children}</div>;
    }
    return (
      <div className="browser-screen-loading-content">
        <div className="loading-dots dark-gray">
          <i />
          <i />
          <i />
          <i />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loading: loaderSelectors.getLoaderState(state),
});

export default connect(mapStateToProps)(Loader);
