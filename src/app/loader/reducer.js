/* eslint-disable prefer-const */
import { handleActions } from 'redux-actions';
import { Map } from 'immutable';
import Const from './constants';

const initialState = Map({
  default: false,
});

export const reducer = handleActions({
  [Const.SHOW_LOADER]: (state, action) => state.update(action.payload.id.toString(), () => true),
  [Const.HIDE_LOADER]: (state, action) => state.update(action.payload.id.toString(), () => false),
}, initialState);
