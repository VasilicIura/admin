import keyMirror from 'keymirror';

const Const = keyMirror({
  SHOW_LOADER: null,
  HIDE_LOADER: null,
});

export default Const;
