import { createAction } from 'redux-actions';
import Const from './constants';

const showLoader = createAction(
  Const.SHOW_LOADER,
  (id = 'default') => ({ id }),
);

const hideLoader = createAction(
  Const.HIDE_LOADER,
  (id = 'default') => ({ id }),
);

export {
  showLoader, hideLoader,
};
