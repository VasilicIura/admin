import { createSelector } from 'reselect';

const getLoaderState = createSelector(
  state => state.loader,
  (state, loaderId = 'default') => loaderId,
  (loader, loaderId) => !!loader.get(loaderId.toString()),
);

export const loaderSelectors = {
  getLoaderState,
};
