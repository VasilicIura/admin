import { createAction } from 'redux-actions';
import { queryFilters, filterSelector } from 'fe-shared';
import { tradingApi, commonApi } from '../api';
import { ACTIONS, EVENT_GROUP_STATUS_ENTITY, FILTER_TYPES } from './consts';
import { filterCriteriasSelector } from './selectors';

const fetchEventGroups = createAction(
  ACTIONS.FETCH_EVENT_GROUPS,
  (paginationQueries = '') => tradingApi.get(`event-groups${paginationQueries}`),
);

const fetchEventGroupById = createAction(
  ACTIONS.FETCH_EVENT_GROUP_BY_ID,
  id => tradingApi.get(`event-groups/${id}`),
);

const addEventGroup = createAction(
  ACTIONS.FETCH_EVENT_GROUPS,
  ({ events, eventLeague, ...eventGroup }) => tradingApi.post('event-groups', {
    ...eventGroup,
    events: events.map(({ value }) => value),
    leagueId: eventLeague.value,
  }),
);

const updateEventGroup = createAction(
  ACTIONS.UPDATE_EVENT_GROUPS,
  ({
    id,
    events,
    eventLeague,
    ...rest
  }) => (tradingApi.patch(`event-groups/${id}`, {
    ...rest,
    events: events.map(item => item.value || item),
    leagueId: eventLeague.value || eventLeague,
  })),
);

const fetchEventGroupStatuses = createAction(
  ACTIONS.FETCH_EVENT_GROUP_STATUSES,
  () => commonApi.get(`enums/statuses/entity/${EVENT_GROUP_STATUS_ENTITY}`),
);

const fetchEventGroupLeagues = createAction(
  ACTIONS.EVENT_GROUPS_LEAGUES,
  (paginationQueries = '') => tradingApi.get(`leagues${paginationQueries}`),
);

const fetchEventGroupsAvailableStatuses = createAction(
  ACTIONS.EVENT_GROUPS_AVAILABLE_STATUSES,
  (currentStatus, entity) =>
    commonApi.get(`enums/statuses/entity/${entity}/from-status/${currentStatus}`),
);

const filterEventGroups = createAction(
  ACTIONS.SEARCH_EVENT_GROUPS,
  (criteria = []) => tradingApi.post('event-groups/search', { ...criteria }),
);

const fetchEvents = createAction(
  ACTIONS.SEARCH_EVENTS,
  (criteria = []) => tradingApi.post('events/search', { ...criteria }),
);

const changePagination = createAction(
  ACTIONS.CHANGE_PAGINATION,
  (pagination = {}) => pagination,
);

const setFilterCriterias = createAction(
  ACTIONS.SET_EVENT_GROUP_FILTERS,
  values => values,
);

const autoCompleteEventGroups = createAction(
  ACTIONS.AUTOCOMPLETE_EVENT_GROUPS,
  (criteria = []) => tradingApi.post('event-groups/search', { ...criteria }),
);

const fetchLeagueById = createAction(
  ACTIONS.FETCH_EVENT_GROUP_LEAGUE_BY_ID,
  id => tradingApi.get(`leagues/${id}`),
);

const changeSorting = createAction(
  ACTIONS.CHANGE_EVENT_GROUP_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

const searchEventGroups = () =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'eventGroupsFilter' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    dispatch(filterEventGroups(filterCriteriasSelector(newState)));
  };

export {
  fetchEventGroups,
  addEventGroup,
  fetchEventGroupStatuses,
  fetchEventGroupById,
  updateEventGroup,
  fetchEvents,
  searchEventGroups,
  changePagination,
  fetchEventGroupsAvailableStatuses,
  fetchEventGroupLeagues,
  autoCompleteEventGroups,
  fetchLeagueById,
  changeSorting,
};
