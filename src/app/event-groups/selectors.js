import { createSelector } from 'reselect';
import { toSelectArray, toSelectArrayFromJSON } from 'fe-shared';
import { DEFAULT_STATUS } from './consts';

const eventGroupStateSelector = state => state.eventGroup;

const eventGroupSelector = createSelector(
  eventGroupStateSelector,
  eventGroups => eventGroups.get('eventGroups').toJS(),
);

const eventGroupStatuses = createSelector(
  eventGroupStateSelector,
  eventStatuses => toSelectArray(eventStatuses.get('eventGroupStatuses').toJS()),
);

const eventsSelector = createSelector(
  eventGroupStateSelector,
  eventStatuses => toSelectArrayFromJSON(eventStatuses.get('events').toJS(), 'id', 'name'),
);

const filteredEventGroups = createSelector(
  eventGroupStateSelector,
  eventGroups => eventGroups.get('filteredEventGroups').toJS(),
);

const filterCriteriasSelector = createSelector(
  eventGroupStateSelector,
  criterias => criterias.get('criteria').toJS(),
);

const eventGroupsListSelector = createSelector(
  eventGroupStateSelector,
  (eventGroups) => {
    const list = eventGroups.get('filteredEventGroups').toJS();
    return list && list.content;
  },
);

const eventGroupAvailableStatuses = createSelector(
  eventGroupStateSelector,
  (state, currentStatus) => currentStatus || null,
  (statuses, currentStatus) => {
    const appendStatus = currentStatus || DEFAULT_STATUS;
    const availableStatuses = statuses.get('eventGroupAvailableStatuses').toJS();
    availableStatuses.unshift(appendStatus);
    return toSelectArray(availableStatuses);
  },
);

const leaguesSelector = createSelector(
  eventGroupStateSelector,
  leagues => toSelectArrayFromJSON(leagues.get('leagues').toJS(), 'id', 'name'),
);

const leagueByIdSelector = createSelector(
  eventGroupStateSelector,
  leagues => leagues.get('league').toJS(),
);

const eventsSortingSelector = createSelector(
  filterCriteriasSelector,
  ({ pageable }) => {
    if (pageable.hasOwnProperty('orders')) { //eslint-disable-line
      return [
        {
          columnName: pageable.orders[0].field,
          direction: pageable.orders[0].direction.toLowerCase(),
        },
      ];
    }

    return null;
  },
);

export {
  eventGroupSelector,
  eventGroupStatuses,
  eventsSelector,
  filteredEventGroups,
  filterCriteriasSelector,
  eventGroupsListSelector,
  eventGroupAvailableStatuses,
  leaguesSelector,
  leagueByIdSelector,
  eventsSortingSelector,
};
