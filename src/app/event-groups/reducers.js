import { Map, List } from 'immutable';
import { handleActions } from 'redux-actions';
import { ACTIONS } from './consts';

const initialState = Map({
  eventGroups: List([]),
  eventGroupStatuses: List([]),
  events: List([]),
  filteredEventGroups: Map({}),
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  eventGroupAvailableStatuses: List([]),
  leagues: List([]),
  league: Map({}),
});

export const reducer = handleActions({
  [`${ACTIONS.FETCH_EVENT_GROUPS}_FULFILLED`]:
    (state, action) => state.set('eventGroups', List(action.payload.content)),
  [`${ACTIONS.FETCH_EVENT_GROUP_STATUSES}_FULFILLED`]:
    (state, action) => state.set('eventGroupStatuses', List(action.payload)),
  [`${ACTIONS.SEARCH_EVENTS}_FULFILLED`]:
    (state, action) => state.set('events', List(action.payload.content)),
  [`${ACTIONS.SEARCH_EVENT_GROUPS}_FULFILLED`]:
    (state, action) => state.set('filteredEventGroups', Map(action.payload)),
  [ACTIONS.CHANGE_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [ACTIONS.SET_EVENT_GROUP_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
  [`${ACTIONS.EVENT_GROUPS_AVAILABLE_STATUSES}_FULFILLED`]:
    (state, action) => state.set('eventGroupAvailableStatuses', List(action.payload)),
  [`${ACTIONS.EVENT_GROUPS_LEAGUES}_FULFILLED`]:
    (state, action) => state.set('leagues', List(action.payload.content)),
  [ACTIONS.CHANGE_EVENT_GROUP_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
  [`${ACTIONS.FETCH_EVENT_GROUP_LEAGUE_BY_ID}_FULFILLED`]:
    (state, action) => state.set('league', Map(action.payload)),
}, initialState);
