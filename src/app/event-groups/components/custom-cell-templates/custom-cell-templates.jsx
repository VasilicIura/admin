import React from 'react';
import propTypes from 'prop-types';
import { EventGroupDetail, EditEventGroup } from '../event-group';

const CustomCellTemplate = (props) => {
  switch (props.column.id) {
    case 'events':
      return (<span>Assigned events number {`(${props.value.length})`} </span>);
    case 'actions':
      return (
        <div>
          <EventGroupDetail id={props.original.id} />
          <EditEventGroup eventGroup={props.original} />
        </div>
      );
    default: return props.value;
  }
};

CustomCellTemplate.defaultProps = {
  value: null,
};

CustomCellTemplate.propTypes = {
  column: propTypes.object.isRequired,
  value: propTypes.any,
  original: propTypes.object.isRequired,
};

export default CustomCellTemplate;
