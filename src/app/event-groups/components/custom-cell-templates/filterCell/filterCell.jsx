import React, { Component } from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  SelectFilter,
  AutoCompleteFilter,
} from 'fe-shared';
import { autocompleteEvents } from 'app/events';
import { eventGroupStatuses } from '../../../selectors';
import { autoCompleteEventGroups } from '../../../actions';

class EventGroupsSwitchFilterCell extends Component {
  static defaultProps = {
    statuses: [],
    column: null,
  };

  static propTypes = {
    autoCompleteEventGroups: propTypes.func.isRequired,
    autocompleteEvents: propTypes.func.isRequired,
    statuses: propTypes.array,
    column: propTypes.object,
  };


  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
      statuses,
    } = this.props;

    switch (name) {
      case 'name': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autoCompleteEventGroups}
          valueKey="name"
          {...this.props}
        />
      );

      case 'events': return (
        <FilterField
          name="eventId"
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteEvents}
          {...this.props}
        />
      );
      case 'eventGroupStatusPk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={statuses}
          {...this.props}
        />
      );
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  autoCompleteEventGroups,
  autocompleteEvents,
}, dispatch));

const mapStateToProps = state => ({
  statuses: eventGroupStatuses(state),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxFilter({
    filter: 'eventGroupsFilter',
  }),
)(EventGroupsSwitchFilterCell);
