import React, { Component } from 'react';
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { func, object } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchAvailableStatuses } from 'app/eventGroupChains';
import { NotificationManager } from 'react-notifications';
import { EventGroupForm } from '../form';
import { updateEventGroup, searchEventGroups } from '../../../actions';

export class EditEventGroup extends Component {
  static propTypes = {
    searchEventGroups: func.isRequired,
    updateEventGroup: func.isRequired,
    eventGroup: object.isRequired,
  };

  state = {
    showModal: false,
  };

  handleSubmit = (value) => {
    const { id } = this.props.eventGroup;
    this.props.updateEventGroup({
      ...value,
      id,
    })
      .then(this.props.searchEventGroups)
      .then(() => {
        NotificationManager.info('Updated', `Event group was updated ${value.name}`);
        this.setState({ showModal: !this.state.showModal });
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Edit Event Group</span>
      </Tooltip>
    );
    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.toggleModal}
          >
            <i className="mi mi-edit" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggleModal}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Edit Event Group</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal && (
                <EventGroupForm
                  onSubmit={this.handleSubmit}
                  eventGroup={this.props.eventGroup}
                />
              )
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  updateEventGroup,
  fetchAvailableStatuses,
  searchEventGroups,
}, dispatch));

export default connect(null, mapDispatchToProps)(EditEventGroup);
