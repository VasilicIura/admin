import React from 'react';
import { object } from 'prop-types';
import { Modal, Label, ListGroup, ListGroupItem } from 'react-bootstrap';

const EventGroupDetailTemplate = ({ eventGroup }) => (
  <Modal.Body>
    <div>
      <div className="detail-view">
        <h5 style={{ lineHeight: '44px' }}>Name:  {eventGroup.name}</h5>
        <h5 style={{ float: 'right' }}>
          Current status:
          <Label className="status-label">{eventGroup.eventGroupStatusPk}</Label>
        </h5>
      </div>
      <div>
        <h5>Description:</h5><br />
        {eventGroup.description}
      </div>
      <div style={{ marginTop: '3em' }}>
        <h3>Assigned events</h3>
        <ListGroup>
          {
            eventGroup.events
              .map(e => (<ListGroupItem key={e.id}>{e.name}</ListGroupItem>))
          }
        </ListGroup>
      </div>
    </div>
  </Modal.Body>
);

EventGroupDetailTemplate.defaultProps = {
  eventGroup: null,
};

EventGroupDetailTemplate.propTypes = {
  eventGroup: object,
};

export default EventGroupDetailTemplate;
