import React, { Component } from 'react';
import { number, func, bool, object } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { fetchEventGroupById } from '../../../actions';
import EventGroupDetailTemplate from './detail-template';

export class EventGroupDetail extends Component {
  static defaultProps = {
    eventGroup: {},
    showCreated: true,
    id: null,
  };

  static propTypes = {
    id: number,
    fetchEventGroupById: func.isRequired,
    showCreated: bool,
    eventGroup: object,
  };

  state = {
    showModal: false,
  };

  componentWillReceiveProps(newProps) {
    if (newProps.showCreated !== this.props.showCreated) {
      this.showModal();
    }
  }

  showModal = () => {
    if (this.props.id) {
      this.props.fetchEventGroupById(this.props.id)
        .then(({ value }) => this.setState({
          eventGroup: value,
          showModal: !this.state.showModal,
        }));
    } else {
      this.setState({
        eventGroup: this.props.eventGroup,
        showModal: !this.state.showModal,
      });
    }
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>View Event Group details</span>
      </Tooltip>
    );
    return (
      <div>
        {
          this.props.id ? (
            <OverlayTrigger placement="top" overlay={tooltip}>
              <Button
                className="circle-button"
                onClick={this.showModal}
                style={{ float: 'left', marginRight: '15px' }}
              >
                <i className="mi mi-visibility" />
              </Button>
            </OverlayTrigger>

          ) : null
        }
        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({ showModal: !this.state.showModal })}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Event Group details</Modal.Title>
          </Modal.Header>
          <EventGroupDetailTemplate eventGroup={this.state.eventGroup} />
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchEventGroupById,
}, dispatch));

export default connect(null, mapDispatchToProps)(EventGroupDetail);
