import React, { Component } from 'react';
import { func, string } from 'prop-types';
import { Modal, Button, Col, Row } from 'react-bootstrap';
import { Field } from 'redux-form';
import { ControlDatePicker } from 'fe-shared';

class EventsFilter extends Component {
  static defaultProps = {
    periodFrom: null,
    periodTo: null,
  };

  static propTypes = {
    setPeriodFilter: func.isRequired,
    periodFrom: string,
    periodTo: string,
  };

  state = {
    openModal: false,
  };

  setPeriodFilter = () => {
    const { periodFrom, periodTo } = this.props;

    this.props.setPeriodFilter({
      periodFrom,
      periodTo,
    });

    this.toggleFilter();
  };

  toggleFilter = () => this.setState({ openModal: !this.state.openModal });

  render = () => (
    <div>
      <Button
        className="events-filter"
        onClick={this.toggleFilter}
      >
        <i className="mi mi-filter-list" />
      </Button>

      <Modal show={this.state.openModal} onHide={this.toggleFilter}>
        <Modal.Header closeButton>
          <Modal.Title className="modal-title">Filter event by selected period</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col md={12} >
              <Col md={6}>
                <Field
                  name="from"
                  placeholder="Choose start period"
                  component={ControlDatePicker}
                  label="Choose period"
                  from
                />
              </Col>
              <Col md={6}>
                <Field
                  name="to"
                  placeholder="Choose end period"
                  component={ControlDatePicker}
                  label="Choose period"
                  to
                />
              </Col>
              <div className="submit-modal-btn">
                <Button
                  type="button"
                  bsStyle="success"
                  onClick={this.setPeriodFilter}
                >
                    Filter
                </Button>
              </div>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default EventsFilter;
