import React from 'react';
import { Form, Button, Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { array, any, func, string, number, object } from 'prop-types';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { autocompleteLeagues } from 'app/leagues';
import { ControlInput, ControlSelect, toSelectArrayFromJSON, ControlAutoCompleteSelect } from 'fe-shared';
import { fetchEvents, fetchEventGroupsAvailableStatuses, fetchLeagueById } from '../../../actions';
import { filterSize, DEFAULT_STATUS, CURRENT_ENTITY } from '../../../consts';
import { eventsSelector, eventGroupAvailableStatuses, leagueByIdSelector } from '../../../selectors';
import { EventsFilter } from '../eventsFilter';
import { validate } from './validate';

class EventGroupForm extends React.PureComponent {
  static defaultProps = {
    statuses: [],
    eventGroup: null,
    periodFrom: null,
    periodTo: null,
    eventLeague: null,
    league: null,
    error: null,
  };

  static propTypes = {
    handleSubmit: func.isRequired,
    fetchEvents: func.isRequired,
    autocompleteLeagues: func.isRequired,
    fetchLeagueById: func.isRequired,
    fetchEventGroupsAvailableStatuses: func.isRequired,
    submitting: any.isRequired,
    statuses: array,
    events: array.isRequired,
    eventGroup: any,
    league: object,
    periodFrom: string,
    periodTo: string,
    eventLeague: number,
    error: string,
  };

  constructor(props) {
    super(props);
    const { eventGroup } = props;
    this.state = {
      criterias: {},
      currentStatus: (eventGroup && eventGroup.eventGroupStatusPk) ||
      DEFAULT_STATUS,
    };
  }

  componentDidMount = () => {
    const { currentStatus } = this.state;
    this.getEvents();
    this.props.fetchEventGroupsAvailableStatuses(currentStatus, CURRENT_ENTITY);

    this.props.eventGroup && this.props.fetchLeagueById(this.props.eventGroup.leagueId); // eslint-disable-line
  };

  componentWillReceiveProps = ({ eventLeague }) => (
    (eventLeague !== this.props.eventLeague) && this.setLeagueFilter(eventLeague));

  getEvents = () => {
    const { eventGroup } = this.props;
    (eventGroup && eventGroup.leagueId) ? // eslint-disable-line
    this.setLeagueFilter(eventGroup.leagueId) :
      this.props.fetchEvents();
  };

  setPeriodFilter = ({ periodFrom, periodTo }) => {
    const model = {
      field: 'startTimeStamp',
      type: 'RANGE',
      from: periodFrom,
      to: periodTo,
    };

    this.setCriterias('period', model);
  };

  setLeagueFilter = league => this.setCriterias('league', {
    field: 'leagueId',
    type: 'EQ',
    value: league.value,
  });

  setCriterias = (name, model) => this.setState({
    criterias: {
      ...this.state.criterias,
      [name]: model,
    },
  }, () => {
    this.props.fetchEvents({
      criteria: Object.keys(this.state.criterias)
        .map(criteria => this.state.criterias[criteria]),
      pageable: filterSize,
    });
  });

  render() {
    const {
      handleSubmit,
      submitting,
      eventGroup,
      periodFrom,
      periodTo,
      error,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="name"
          placeholder="Name"
          type="text"
          component={ControlInput}
          label="Name"
          currentValue={eventGroup ? eventGroup.name : ''}
        />
        <Field
          name="description"
          placeholder="Description"
          type="text"
          component={ControlInput}
          label="Description:"
          currentValue={eventGroup ? eventGroup.description : ''}
        />
        <Field
          name="eventGroupStatusPk"
          placeholder="Event group status"
          component={ControlSelect}
          options={this.props.statuses}
          label="Event group status:"
          currentValue={eventGroup ? eventGroup.eventGroupStatusPk : ''}
        />
        <Field
          name="eventLeague"
          placeholder="League"
          component={ControlAutoCompleteSelect}
          autoCompleteAction={this.props.autocompleteLeagues}
          label="Leagues"
          currentValue={eventGroup ? this.props.league.name : ''}
        />
        <div className="clearfix">
          <div className="select-pull-left">
            <Field
              multi
              name="events"
              placeholder="Events"
              component={ControlSelect}
              options={this.props.events}
              label="Events:"
              currentValue={eventGroup ? toSelectArrayFromJSON(eventGroup.events, 'id', 'name') : ''}
            />
          </div>
          <div className="filter-container">
            <EventsFilter
              periodFrom={periodFrom}
              periodTo={periodTo}
              setPeriodFilter={this.setPeriodFilter}
            />
          </div>
        </div>
        {error && <Alert bsStyle="danger">{error}</Alert>}
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

const selector = formValueSelector('eventGroup');
const mapStateToProps = (state, props) => ({
  periodFrom: selector(state, 'from'),
  periodTo: selector(state, 'to'),
  eventLeague: selector(state, 'eventLeague'),
  events: eventsSelector(state),
  statuses: eventGroupAvailableStatuses(
    state,
    props.eventGroup && props.eventGroup.eventGroupStatusPk,
  ),
  league: leagueByIdSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchEvents,
  fetchEventGroupsAvailableStatuses,
  autocompleteLeagues,
  fetchLeagueById,
}, dispatch));

export default compose(
  reduxForm({
    form: 'eventGroup',
    validate,
  }),
  connect(mapStateToProps, mapDispatchToProps),
)(EventGroupForm);
