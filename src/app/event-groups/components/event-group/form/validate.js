export const validate = (values) => {
  const errors = {};
  const {
    name, description, eventGroupStatusPk, events, eventLeague,
  } = values;

  if (!name) {
    errors.name = 'Name field is required';
  }

  if (!description) {
    errors.description = 'Description field is required';
  }

  if (!eventGroupStatusPk) {
    errors.eventGroupStatusPk = 'Status field is required';
  }

  if (!eventLeague) {
    errors.eventLeague = 'League is required';
  }

  if (!events) {
    errors.events = 'Events are required';
  }

  return errors;
};
