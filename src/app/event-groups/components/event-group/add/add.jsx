import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import propTypes from 'prop-types';
import { EventGroupForm } from '../form';

const AddEventGroup = props => (
  <div>
    <Button
      className="add-button"
      onClick={props.toggleModal}
    >
      <i className="mi mi-add" />
    </Button>

    <Modal show={props.showModal} onHide={props.toggleModal}>
      <Modal.Header closeButton>
        <Modal.Title className="modal-title">Add Event Group</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <EventGroupForm onSubmit={props.handleSubmit} />
      </Modal.Body>
    </Modal>
  </div>
);

AddEventGroup.propTypes = {
  toggleModal: propTypes.func.isRequired,
  showModal: propTypes.bool.isRequired,
  handleSubmit: propTypes.func.isRequired,
};

export default AddEventGroup;
