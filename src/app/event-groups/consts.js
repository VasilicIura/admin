import React from 'react';
import keyMirror from 'keymirror';

export const EVENT_GROUP_STATUS_ENTITY = 'Event Group';
export const ACTIONS = keyMirror({
  FETCH_EVENT_GROUPS: null,
  FETCH_EVENTS_LIST: null,
  ADD_EVENT_GROUPS: null,
  SEARCH_EVENTS: null,
  SEARCH_EVENT_GROUPS: null,
  UPDATE_EVENT_GROUPS: null,
  FETCH_EVENT_GROUP_BY_ID: null,
  FETCH_EVENT_GROUP_STATUSES: null,
  CHANGE_PAGINATION: null,
  EVENT_GROUPS_AVAILABLE_STATUSES: null,
  EVENT_GROUPS_LEAGUES: null,
  SET_EVENT_GROUP_FILTERS: null,
  AUTOCOMPLETE_EVENT_GROUPS: null,
  FETCH_EVENT_GROUP_LEAGUE_BY_ID: null,
  CHANGE_EVENT_GROUP_SORTING: null,
});

export const COLUMNS = customCell => [
  {
    Header: 'Name',
    accessor: 'name',
  },
  {
    Header: 'Description',
    accessor: 'description',
    Cell: row => customCell(row),
  },
  {
    Header: 'Status',
    accessor: 'eventGroupStatusPk',
    Cell: row => customCell(row),
  },
  {
    Header: 'Assigned Events',
    accessor: 'events',
    Cell: row => customCell(row),
  },
  {
    Header: '',
    width: 130,
    accessor: 'actions',
    sortable: false,
    Filter: ({ onChange }) => (
      <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
    ),
    Cell: row => customCell(row),
  },
];

export const FILTER_TYPES = {
  IN: [
    'eventGroupStatusPk',
    'eventId',
  ],
  LIKE: [
    'name',
    'description',
  ],
};

export const filterSize = {
  page: 0,
  size: 1000,
};

export const CURRENT_ENTITY = 'Event Group';
export const DEFAULT_STATUS = 'New';
