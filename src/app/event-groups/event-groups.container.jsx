import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SubmissionError } from 'redux-form';
import { ShowLastCreatedNotification } from 'fe-shared';
import { func, object } from 'prop-types';
import { changeSorting, fetchEventGroupStatuses, fetchEvents, addEventGroup, changePagination, searchEventGroups } from './actions';
import { AddEventGroup, EventGroupDetail } from './components';
import { filteredEventGroups } from './selectors';
import { CustomCellTemplate, EventGroupsSwitchFilterCell } from './components/custom-cell-templates';
import { COLUMNS } from './consts';
import Table from '../common/table';

class EventGroups extends Table {
static propTypes = {
  fetchData: func.isRequired,
  changePagination: func.isRequired,
  addEventGroup: func.isRequired,
  eventGroups: object,
};

static defaultProps = {
  eventGroups: null,
};

  state = {
    eventGroup: {},
    showLastCreated: true,
    showModal: false,
  };

  componentDidMount = () => {
    this.props.fetchEventGroupStatuses();
    this.props.fetchEvents();
  };

  showLastCreated = () => this.setState({ showLastCreated: !this.state.showLastCreated });
  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = values =>
    this.props.addEventGroup(values)
      .then(({ value }) => {
        this.setState({ eventGroup: value });
        return this.props.fetchData();
      })
      .then(() => {
        ShowLastCreatedNotification(this.showLastCreated);
        this.toggleModal();
      })
      .catch((err) => {
        const response = err.response.data;
        let message = '';
        if (typeof response.message === 'string') {
          ({ message } = response);
        } else {
          message = JSON.stringify(response.message);
        }
        return Promise.reject(new SubmissionError({ _error: message }));
      });

      switchFilters = grid => <EventGroupsSwitchFilterCell {...grid} />;
      useCustomCell = props => <CustomCellTemplate {...props} />;

      render() {
        const { eventGroups, loading } = this.props;
        return (
          <div className="event-group-container">
            <EventGroupDetail
              eventGroup={this.state.eventGroup}
              showCreated={this.state.showLastCreated}
            />
            <div>
              {this.renderTable(
                COLUMNS(this.useCustomCell),
                eventGroups,
                loading,
                this.switchFilters,
                'Event groups',
                )}
              <div className="add-btn-container">
                <AddEventGroup
                  toggleModal={this.toggleModal}
                  showModal={this.state.showModal}
                  showLastCreated={this.state.showLastCreated}
                  handleSubmit={this.handleSubmit}
                />
              </div>
            </div>
          </div>
        );
      }
}

const mapStateToProps = state => ({
  eventGroups: filteredEventGroups(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchData: () => searchEventGroups(),
  addEventGroup,
  changePagination,
  fetchEventGroupStatuses,
  fetchEvents,
  changeSorting,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(EventGroups);
