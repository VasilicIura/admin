import { createAction } from 'redux-actions';
import { filterSelector, queryFilters } from 'fe-shared';
import { ACTIONS, CURRENT_ENTITY, FILTER_TYPES } from './consts';
import { filterCriteriasSelector } from './selectors';
import { tradingApi, commonApi } from '../api';

const fetchEvents = createAction(
  ACTIONS.FETCH_EVENTS,
  (paginationQueries = '') => tradingApi.get(`events${paginationQueries}`),
);

const addEvent = createAction(
  ACTIONS.ADD_EVENT,
  (event) => {
    const {
      participantAway,
      participantHome,
      leagueId,
      ...values
    } = event;

    const model = {
      ...values,
      leagueId: leagueId.value,
      participants: [participantAway.value, participantHome.value],
    };

    return tradingApi.post('events', model);
  },
);

const fetchEventStatuses = createAction(
  ACTIONS.FETCH_ALL_EVENT_STATUSES,
  () => commonApi.get(`enums/statuses/entity/${CURRENT_ENTITY}`),
);

const fetchEventsAvailableStatuses = createAction(
  ACTIONS.EVENTS_AVAILABLE_STATUSES,
  (currentStatus, entity) =>
    commonApi.get(`enums/statuses/entity/${entity}/from-status/${currentStatus}`),
);

const fetchEventById = createAction(
  ACTIONS.EVENT_BY_ID,
  id => tradingApi.get(`events/${id}`),
);

const updateEvent = createAction(
  ACTIONS.UPDATE_EVENT,
  (event) => {
    const {
      id,
      participantAway,
      participantHome,
      league,
      leagueId,
      ...updatedEvent
    } = event;

    const model = {
      ...updatedEvent,
      leagueId: leagueId.value || leagueId,
      participants: [
        participantAway.value || participantAway,
        participantHome.value || participantHome,
      ],
    };

    return tradingApi.patch(`events/${id}`, model);
  },
);

const fetchLeagues = createAction(
  ACTIONS.FETCH_LEAGUES,
  (paginationQueries = '') => tradingApi.get(`leagues${paginationQueries}`),
);

const fetchParticipants = createAction(
  ACTIONS.FETCH_PARTICIPANTS,
  () => tradingApi.get('participants/type/Team'),
);

const fetchLeagueById = createAction(
  ACTIONS.FETCH_LEAGUE_BY_ID,
  id => tradingApi.get(`leagues/${id}`),
);

const fetchParticipantById = createAction(
  ACTIONS.EVENT_PARTICIPANT,
  id => tradingApi.get(`participants/${id}`),
);

const filterEvents = createAction(
  ACTIONS.FILTER_EVENTS,
  (criteria = []) => tradingApi.post('events/search', { ...criteria }),
);

const changePagination = createAction(
  ACTIONS.CHANGE_EVENTS_PAGINATION,
  (pagination = {}) => pagination,
);

const setFilterCriterias = createAction(
  ACTIONS.SET_EVENTS_FILTERS,
  values => values,
);

const autocompleteEvents = createAction(
  ACTIONS.AUTOCOMPLETE_EVENTS_FILTER,
  (criteria = []) => tradingApi.post('events/search', { ...criteria }),
);

const changeSorting = createAction(
  ACTIONS.CHANGE_EVENTS_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

const searchEvents = () =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'eventsFilter' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    dispatch(filterEvents(filterCriteriasSelector(newState)));
  };

export {
  fetchEvents,
  fetchEventById,
  addEvent,
  fetchLeagues,
  fetchParticipants,
  updateEvent,
  fetchLeagueById,
  fetchParticipantById,
  fetchEventStatuses,
  changePagination,
  searchEvents,
  fetchEventsAvailableStatuses,
  autocompleteEvents,
  changeSorting,
};
