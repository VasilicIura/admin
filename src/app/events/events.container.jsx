import React from 'react';
import { connect } from 'react-redux';
import { func, object } from 'prop-types';
import { bindActionCreators } from 'redux';
import Table from '../common/table';
import { filteredEvents } from './selectors';
import {
  searchEvents,
  changePagination,
  changeSorting,
  fetchEventStatuses,
} from './actions';
import {
  AddEvent,
  SwitchCustomCell,
  EventsSwitchFilterCell,
} from './components';
import { COLUMNS } from './consts';
import './events.scss';

class Events extends Table {
  static defaultProps = {
    events: null,
  };

  static propTypes = {
    fetchData: func.isRequired,
    changePagination: func.isRequired,
    events: object,
  };

  componentDidMount = () => {
    this.props.fetchEventStatuses();
  };


  getCustomCell = cell => <SwitchCustomCell {...cell} />;
  switchFilters = grid => <EventsSwitchFilterCell {...grid} />;

  render() {
    return (
      <div className="events-container">
        <div>
          {this.renderTable(
            COLUMNS(this.getCustomCell),
            this.props.events,
            false,
            this.switchFilters,
            'Events',
          )}
        </div>
        <div className="add-event">
          <AddEvent />
        </div>
      </div>
    );
  }
}

const mapStateToProp = state => ({
  events: filteredEvents(state),
});

const mapDispatchToProp = dispatch => (bindActionCreators({
  changePagination,
  changeSorting,
  fetchData: () => searchEvents(),
  fetchEventStatuses,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(Events);
