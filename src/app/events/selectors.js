import { createSelector } from 'reselect';
import { toSelectArrayFromJSON, toSelectArray } from 'fe-shared';
import { DEFAULT_STATUS } from './consts';

const eventsStateSelector = state => state.events;

const eventsSelector = createSelector(
  eventsStateSelector,
  eventsList => eventsList.get('events').toJS(),
);

const leaguesSelector = createSelector(
  eventsStateSelector,
  leaguesList =>
    toSelectArrayFromJSON(leaguesList.get('leagues').toJS(), 'id', 'name'),
);

const participantsSelector = createSelector(
  eventsStateSelector,
  participantsList => toSelectArrayFromJSON(participantsList.get('participants').toJS(), 'id', 'name'),
);

const eventStatusesSelector = createSelector(
  eventsStateSelector,
  statuses => statuses.get('eventStatuses').toJS(),
);

const filteredEvents = createSelector(
  eventsStateSelector,
  event => event.get('filteredEvents').toJS(),
);

const filterCriteriasSelector = createSelector(
  eventsStateSelector,
  criterias => criterias.get('criteria').toJS(),
);

const eventsListSelector = createSelector(
  eventsStateSelector,
  (events) => {
    const list = events.get('filteredEvents').toJS();
    return list && list.content;
  },
);

const eventsAvailableStatusesSelector = createSelector(
  eventsStateSelector,
  (state, currentStatus) => currentStatus || null,
  (statuses, currentStatus) => {
    const appendStatuses = currentStatus || DEFAULT_STATUS;
    const eventStatuses = statuses.get('eventsAvailableStatuses').toJS();
    eventStatuses.unshift(appendStatuses);

    return toSelectArray(eventStatuses);
  },
);

const eventsSortingSelector = createSelector(
  filterCriteriasSelector,
  ({ pageable }) => {
    if (pageable.hasOwnProperty('orders')) { //eslint-disable-line
      return [
        {
          columnName: pageable.orders[0].field,
          direction: pageable.orders[0].direction.toLowerCase(),
        },
      ];
    }

    return null;
  },
);

export {
  eventsSelector,
  eventsListSelector,
  leaguesSelector,
  participantsSelector,
  eventStatusesSelector,
  filteredEvents,
  filterCriteriasSelector,
  eventsAvailableStatusesSelector,
  eventsSortingSelector,
};
