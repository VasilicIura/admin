import { handleActions } from 'redux-actions';
import { Map, List } from 'immutable';
import { ACTIONS } from './consts';

const initialState = Map({
  events: Map(),
  leagues: List([]),
  participants: List([]),
  league: Map({}),
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  event_participant: Map({}),
  eventStatuses: List(),
  filteredEvents: Map({}),
  eventsAvailableStatuses: List([]),
});

export const reducer = handleActions({
  [`${ACTIONS.FETCH_EVENTS}_FULFILLED`]:
    (state, action) => state.set('events', Map(action.payload)),
  [`${ACTIONS.FETCH_LEAGUES}_FULFILLED`]:
    (state, action) => state.set('leagues', List(action.payload.content)),
  [`${ACTIONS.FETCH_PARTICIPANTS}_FULFILLED`]:
    (state, action) => state.set('participants', List(action.payload.content)),
  [`${ACTIONS.FETCH_LEAGUE_BY_ID}_FULFILLED`]:
    (state, action) => state.set('league', Map(action.payload)),
  [`${ACTIONS.EVENT_PARTICIPANT}_FULFILLED`]:
    (state, action) => state.set('event_participant', Map(action.payload)),
  [`${ACTIONS.EVENTS_AVAILABLE_STATUSES}_FULFILLED`]:
    (state, action) => state.set('eventsAvailableStatuses', List(action.payload)),
  [`${ACTIONS.FILTER_EVENTS}_FULFILLED`]:
    (state, action) => state.set('filteredEvents', Map(action.payload)),
  [ACTIONS.CHANGE_EVENTS_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [ACTIONS.SET_EVENTS_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
  [ACTIONS.CHANGE_EVENTS_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
  [`${ACTIONS.FETCH_ALL_EVENT_STATUSES}_FULFILLED`]:
    (state, action) => state.set('eventStatuses', List(action.payload)),
}, initialState);
