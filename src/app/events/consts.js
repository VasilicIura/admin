import React from 'react';
import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  FETCH_EVENTS: null,
  FETCH_LEAGUES: null,
  FETCH_LEAGUE_BY_ID: null,
  FETCH_ALL_EVENT_STATUSES: null,
  FETCH_PARTICIPANTS: null,
  EVENT_PARTICIPANT: null,
  FILTER_EVENTS: null,
  SET_EVENTS_FILTERS: null,
  CHANGE_EVENTS_PAGINATION: null,
  EVENTS_AVAILABLE_STATUSES: null,
  EVENT_BY_ID: null,
  ADD_EVENT: null,
  UPDATE_EVENT: null,
  CHANGE_EVENTS_SORTING: null,
  EVENTS_TABLE_LEAGUES: null,
  AUTOCOMPLETE_EVENTS_FILTER: null,
});

export const FILTER_TYPES = {
  IN: [
    'eventStatusPk',
    'leagueId',
    'participant',
  ],
  LIKE: [
    'name',
    'location',
    'description',
  ],
  RANGE: [
    'startTimeStamp',
  ],
};

export const COLUMNS = getCustomCell => [
  {
    Header: 'Name',
    accessor: 'name',
  },
  {
    Header: 'Location',
    accessor: 'location',
  },
  {
    Header: 'Status',
    accessor: 'eventStatusPk',
  },
  {
    Header: 'League',
    accessor: 'league',
    Cell: cell => getCustomCell(cell),
  },
  {
    Header: 'Time',
    accessor: 'startTimeStamp',
    Cell: cell => getCustomCell(cell),
  },
  {
    Header: 'Participants',
    accessor: 'participants',
    Cell: cell => getCustomCell(cell),
  },
  {
    Header: 'Description',
    accessor: 'description',
  },
  {
    Header: '',
    accessor: 'actions',
    sortable: false,
    Filter: ({ onChange }) => (
      <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
    ),
    Cell: cell => getCustomCell(cell),
  },
];

export const CURRENT_ENTITY = 'Event';
export const DEFAULT_STATUS = 'New';
