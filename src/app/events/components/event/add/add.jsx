import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, Button } from 'react-bootstrap';
import { ShowLastCreatedNotification } from 'fe-shared';
import { func } from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { EventDetailTemplate } from '../../event';
import { addEvent, searchEvents } from '../../../actions';
import { EventsForm } from '../form';

class AddEvent extends Component {
  static propTypes = {
    addEvent: func.isRequired,
    searchEvents: func.isRequired,
  };

  state = {
    showModal: false,
    showLastCreated: true,
  };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });
  showLastCreated = () => this.setState({ showLastCreated: !this.state.showLastCreated });

  handleSubmit = (event) => {
    this.props.addEvent(event)
      .then(({ value }) => {
        this.setState({ event: value });
        return this.props.searchEvents();
      })
      .then(() => {
        ShowLastCreatedNotification(this.showLastCreated);
        this.toggleModal();
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render = () => (
    <div>
      <EventDetailTemplate
        event={this.state.event}
        showCreated={this.state.showLastCreated}
      />
      <Button
        className="add-button"
        onClick={this.toggleModal}
      >
        <i className="mi mi-add" />
      </Button>

      <Modal show={this.state.showModal} onHide={this.toggleModal}>
        <Modal.Header closeButton>
          <Modal.Title className="modal-title">Add Event</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.state.showModal && <EventsForm onSubmit={this.handleSubmit} />}
        </Modal.Body>
      </Modal>
    </div>
  );
}


const mapDispatchToProp = dispatch => (bindActionCreators({
  addEvent,
  searchEvents,
}, dispatch));

export default connect(null, mapDispatchToProp)(AddEvent);
