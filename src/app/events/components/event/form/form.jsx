import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { Form, Button } from 'react-bootstrap';
import { array, any, func, object } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { autocompleteLeagues } from 'app/leagues';
import { autocompleteParticipants } from 'app/participants';
import { ControlInput, ControlSelect, ControlAutoCompleteSelect } from 'fe-shared';
import { DateTimePicker } from './components';
import { fetchEventsAvailableStatuses } from '../../../actions';
import { eventsAvailableStatusesSelector } from '../../../selectors';
import { DEFAULT_STATUS, CURRENT_ENTITY } from '../../../consts';
import { validate } from './validate';

class EventsForm extends Component {
  static defaultProps = {
    event: null,
  };

  static propTypes = {
    handleSubmit: func.isRequired,
    autocompleteLeagues: func.isRequired,
    autocompleteParticipants: func.isRequired,
    fetchEventsAvailableStatuses: func.isRequired,
    submitting: any.isRequired,
    statuses: array.isRequired,
    event: object,
  };

  state = {
    currentStatus: (this.props.event && this.props.event.eventStatusPk) ||
    DEFAULT_STATUS,
  };

  componentDidMount = () => {
    const { currentStatus } = this.state;

    this.props.fetchEventsAvailableStatuses(currentStatus, CURRENT_ENTITY);
  };

  render() {
    const {
      handleSubmit,
      submitting,
      statuses,
      event,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="name"
          placeholder="Name"
          type="text"
          component={ControlInput}
          label="Name"
          currentValue={event ? event.name : ''}
        />
        <Field
          name="description"
          placeholder="Description"
          type="text"
          component={ControlInput}
          label="Description:"
          currentValue={event ? event.description : ''}
        />
        <Field
          name="location"
          placeholder="Location"
          type="text"
          component={ControlInput}
          label="Location:"
          currentValue={event ? event.location : ''}
        />
        <Field
          name="leagueId"
          placeholder="League"
          component={ControlAutoCompleteSelect}
          autoCompleteAction={this.props.autocompleteLeagues}
          label="League:"
          currentValue={event ? event.league.name : ''}
        />
        <Field
          name="participantAway"
          placeholder="Participant Home"
          component={ControlAutoCompleteSelect}
          autoCompleteAction={this.props.autocompleteParticipants}
          label="Participant Home:"
          currentValue={(event && !!event.participants.length) ? event.participants[0].name : ''}
        />
        <Field
          name="participantHome"
          placeholder="Participant Away"
          component={ControlAutoCompleteSelect}
          autoCompleteAction={this.props.autocompleteParticipants}
          label="Participant Away:"
          currentValue={(event && !!event.participants.length) ? event.participants[1].name : ''}
        />
        <Field
          name="eventStatusPk"
          placeholder="Event Status"
          component={ControlSelect}
          options={statuses}
          label="Event Status:"
          currentValue={event ? event.eventStatusPk : ''}
        />
        <Field
          name="startTimeStamp"
          placeholder="Event State Date"
          component={DateTimePicker}
          label="Event StartDate:"
          currentValue={event ? event.startTimeStamp : ''}
        />
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = (state, props) => ({
  statuses: eventsAvailableStatusesSelector(state, props.event && props.event.eventStatusPk),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchEventsAvailableStatuses,
  autocompleteLeagues,
  autocompleteParticipants,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'events',
    validate,
  }),
)(EventsForm);
