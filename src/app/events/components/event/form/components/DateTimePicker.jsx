import React, { Component } from 'react';
import { any } from 'prop-types';
import moment from 'moment';
import DateTime from 'react-datetime';
import { FormGroup, ControlLabel } from 'react-bootstrap';

class ControlDateTimePicker extends Component {
  static propTypes = {
    input: any.isRequired,
    label: any.isRequired,
    meta: any.isRequired,
    currentValue: any.isRequired,
  };

  state = {
    value: moment(this.props.currentValue),
  };

  componentDidMount() {
    this.handleChange(this.state.value);
  }

  handleChange = (receivedValue) => {
    let value = receivedValue;
    this.setState({ value });

    if (value && value.format) {
      if (this.props.from) value = value.startOf('day');
      if (this.props.to) value = value.endOf('day');
      this.props.input.onChange(value.format());
    } else {
      this.props.input.onChange(null);
    }
  };

  clearDateField = () => {
    (this.state.value !== '') && this.handleChange(null); // eslint-disable-line
  };

  render() {
    const {
      label,
      meta: { error, touched },
    } = this.props;

    let message;
    const validationState = touched && (error && 'error'); // eslint-disable-line

    if (touched && error) {
      message = <span className="error-message">{error}</span>;
    }

    return (
      <FormGroup validationState={validationState || null}>
        <ControlLabel>{label}</ControlLabel>
        {/* <span className="close" onClick={this.clearDateField}>×</span> */}
        <DateTime
          value={this.state.value}
          onChange={this.handleChange}
        />
        { message }
      </FormGroup>
    );
  }
}

export default ControlDateTimePicker;
