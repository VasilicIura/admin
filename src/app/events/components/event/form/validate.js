export const validate = (values) => {
  const errors = {};
  const {
    name,
    description,
    location,
    leagueId,
    participantAway,
    participantHome,
    eventStatusPk,
    startTimeStamp,
  } = values;

  if (!name) {
    errors.name = 'Name field is required';
  }

  if (!description) {
    errors.description = 'Description field is required';
  }

  if (!location) {
    errors.location = 'Location field is required';
  }

  if (!leagueId) {
    errors.leagueId = 'League field is required';
  }

  if (!participantAway) {
    errors.participantAway = 'Participant Away field is required';
  }

  if (!participantHome) {
    errors.participantHome = 'Participant Home field is required';
  }

  if (!eventStatusPk) {
    errors.eventStatusPk = 'Event status field is required';
  }

  if (!startTimeStamp) {
    errors.startTimeStamp = 'Start date field is required';
  }

  return errors;
};
