import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { func, object } from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { updateEvent, searchEvents } from '../../../actions';
import { EventsForm } from '../form';

class EditEvent extends Component {
  static defaultProps = {
    event: null,
  };

  static propTypes = {
    event: object,
    updateEvent: func.isRequired,
    searchEvents: func.isRequired,
  };

  state = {
    showModal: false,
  };

  handleSubmit = (values) => {
    this.props.updateEvent(Object.assign({}, this.props.event, values))
      .then(this.props.searchEvents)
      .then(() => {
        NotificationManager.info('Updated', `Event was updated ${values.name}`);
        this.toggleModal();
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Update event</span>
      </Tooltip>
    );
    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            style={{ display: 'inline-block' }}
            onClick={this.toggleModal}
          >
            <i className="mi mi-create" />
          </Button>
        </OverlayTrigger>

        <Modal
          show={this.state.showModal}
          onHide={this.toggleModal}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Update Event</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal && this.props.event && (
                <EventsForm
                  onSubmit={this.handleSubmit}
                  event={this.props.event}
                />
              )
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProp = dispatch => (bindActionCreators({
  updateEvent,
  searchEvents,
}, dispatch));

export default connect(null, mapDispatchToProp)(EditEvent);
