import React from 'react';
import { object } from 'prop-types';
import moment from 'moment';
import { Modal, Label, ListGroup, ListGroupItem } from 'react-bootstrap';
import { EventParticipant, EventLeague } from '../../';

const EventDetailTemplate = ({ event }) => (
  <Modal.Body>
    <div>
      <div className="detail-view">
        <h5 style={{ lineHeight: '32px' }}>Name:  {event.name}</h5>
        <h5 style={{ float: 'right' }}>
          Current status:
          <Label className="status-label">{event.eventStatusPk}</Label>
        </h5>
      </div>
      <div className="detail-view">
        <h5 style={{ lineHeight: '44px' }}>Location:  {event.location}</h5>
        <h5 style={{ float: 'right' }}>
          Created at: <em>{moment(event.startTimeStamp).format('YYYY-MM-DD')}</em>
        </h5>
      </div>
      <div>
        <h5>Description:</h5><br />
        {event.description}
      </div>
      <div style={{ marginTop: '3em' }}>
        <h3>League</h3>
        <ListGroup>
          <ListGroupItem>
            <EventLeague league={event.league} />
          </ListGroupItem>
        </ListGroup>
      </div>
      <div style={{ marginTop: '1em' }}>
        <h3>Assigned Participants</h3>
        <ListGroup>
          {
            event.participants.map((e, index) => (
              <ListGroupItem key={e} >
                { index === 0 ? 'Home: ' : 'Away: '}
                <EventParticipant participant={e} />
              </ListGroupItem>
            ))
          }
        </ListGroup>
      </div>
    </div>
  </Modal.Body>
);

EventDetailTemplate.defaultProps = {
  event: null,
};

EventDetailTemplate.propTypes = {
  event: object,
};

export default EventDetailTemplate;
