import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { number, func, bool, object } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { fetchEventById } from '../../../actions';
import EventGroupDetailTemplate from './detail-template';

export class EventDetail extends Component {
  static defaultProps = {
    id: null,
    event: null,
    showCreated: null,
  };

  static propTypes = {
    id: number,
    fetchEventById: func.isRequired,
    showCreated: bool,
    event: object,
  };

  state = {
    showModal: false,
  };

  componentWillReceiveProps(newProps) {
    if (newProps.showCreated !== this.props.showCreated) {
      this.showModal();
    }
  }

  showModal = () => {
    if (this.props.id) {
      this.props.fetchEventById(this.props.id)
        .then(({ value }) => this.setState({
          event: value,
          showModal: !this.state.showModal,
        }));
    } else {
      this.setState({
        event: this.props.event,
        showModal: !this.state.showModal,
      });
    }
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>View Event details</span>
      </Tooltip>
    );
    return (
      <div>
        {
          this.props.id ? (
            <OverlayTrigger placement="top" overlay={tooltip}>
              <Button
                className="circle-button"
                onClick={this.showModal}
                style={{ float: 'left', marginRight: '15px' }}
              >
                <i className="mi mi-visibility" />
              </Button>
            </OverlayTrigger>
          ) : null
        }
        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({ showModal: !this.state.showModal })}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Event detail</Modal.Title>
          </Modal.Header>
          <EventGroupDetailTemplate event={this.state.event} />
        </Modal>
      </div>
    );
  }
}


const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchEventById,
}, dispatch));

export default connect(null, mapDispatchToProps)(EventDetail);
