import React from 'react';
import { func, object, array } from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { autocompleteLeagues } from 'app/leagues';
import { autocompleteParticipants } from 'app/participants';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  SelectFilter,
  AutoCompleteFilter,
  DateRangeFilter,
  toSelectArray,
} from 'fe-shared';
import { autocompleteEvents } from '../../actions';
import { eventStatusesSelector, participantsSelector } from '../../selectors';

class EventsSwitchFilterCell extends React.PureComponent {
  static defaultProps = {
    statuses: null,
    participants: null,
    column: null,
  };

  static propTypes = {
    autocompleteEvents: func.isRequired,
    autocompleteLeagues: func.isRequired,
    autocompleteParticipants: func.isRequired,
    statuses: array,
    participants: array,
    column: object,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
      statuses,
    } = this.props;

    const statusesOptions = statuses;
    switch (name) {
      case 'name': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteEvents}
          valueKey="name"
          {...this.props}
        />
      );
      case 'location': return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
      case 'participants': return (
        <FilterField
          name="participant"
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteParticipants}
          {...this.props}
        />
      );
      case 'league': return (
        <FilterField
          name="leagueId"
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteLeagues}
          {...this.props}
        />
      );
      case 'eventStatusPk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={statusesOptions && toSelectArray(statuses)}
          {...this.props}
        />
      );
      case 'startTimeStamp': return (
        <FilterField
          name={name}
          component={DateRangeFilter}
          {...this.props}
        />
      );
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapStateToProps = state => ({
  statuses: eventStatusesSelector(state),
  participants: participantsSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  autocompleteLeagues,
  autocompleteEvents,
  autocompleteParticipants,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxFilter({
    filter: 'eventsFilter',
  }),
)(EventsSwitchFilterCell);
