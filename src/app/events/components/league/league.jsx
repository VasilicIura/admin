import React from 'react';
import propTypes from 'prop-types';

const EventLeague = ({ league }) => (<div>{league && league.name}</div>);
EventLeague.propTypes = {
  league: propTypes.object.isRequired,
};

export default EventLeague;
