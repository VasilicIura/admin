import React from 'react';
import propTypes from 'prop-types';

const EventParticipant = ({ participant }) => (
  <div style={{ display: 'inline-block', marginRight: '10px' }}>{participant.name}</div>
);

EventParticipant.propTypes = {
  participant: propTypes.object.isRequired,
};

export default EventParticipant;
