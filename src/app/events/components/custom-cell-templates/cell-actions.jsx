import React from 'react';
import PropTypes from 'prop-types';
import { EditEvent, EventDetailTemplate } from '../event';

const ActionsCellTemplate = ({ event }) => (
  <div>
    <EventDetailTemplate id={event.id} />
    <EditEvent event={event} />
  </div>
);

ActionsCellTemplate.propTypes = {
  event: PropTypes.object.isRequired,
};

export default ActionsCellTemplate;
