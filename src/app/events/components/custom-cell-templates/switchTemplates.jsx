import React from 'react';
import { object, any } from 'prop-types';
import ActionsCellTemplate from './cell-actions';
import ParticipantsCellTemplate from './cell-participants';
import LeagueCellTemplate from './cell-league';
import TimesTempCellTemplate from './cell-timesTemp';

const SwitchCustomCell = (props) => {
  const cell = props.column.id;
  const { value } = props;
  const event = props.original;

  switch (cell) {
    case 'startTimeStamp':
      return <TimesTempCellTemplate value={value} />;
    case 'league':
      return <LeagueCellTemplate league={value} />;
    case 'participants':
      return <ParticipantsCellTemplate participants={value} />;
    case 'actions':
      return <ActionsCellTemplate event={event} />;
    default: return <div>{props.value}</div>;
  }
};

SwitchCustomCell.defaultProps = {
  value: null,
};

SwitchCustomCell.propTypes = {
  column: object.isRequired,
  original: object.isRequired,
  value: any,
};

export default SwitchCustomCell;
