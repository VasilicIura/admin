import { Login } from './auth';
import { Participants } from './participants';
import { Leagues } from './leagues';
import { Events } from './events';
import { Contests } from './contests';
import { Markets, OddsHistory } from './markets';
import { OutComes } from './outcomes';
import { EventGroups } from './event-groups';
import { EventGroupChains } from './eventGroupChains';
import { About } from './about';
import { MarketTypes } from './marketTypes';
import { Selections } from './selections';
import { RecuringContests } from './recuring-contests';
import { StaticPages } from './static-pages';
import { EventGroupChainsWorkflow } from './eventGroupChainsWorkflow';
import { mails } from './mails';
import { UserManagement } from './userManagement';
import { Roles } from './roles';
import { Configuration } from './configuration';
import { Balance } from './balance';
import { Reports } from './reports';
import { Bets } from './user-bets';
import { ContestBets } from './contest-bets';
import { Transactions } from './transactions';

export const routes = [
  {
    name: 'login',
    path: '/login',
    component: Login,
  },
  {
    name: 'participants',
    path: '/participants',
    component: Participants,
  },
  {
    name: 'leagues',
    path: '/leagues',
    component: Leagues,
  },
  {
    name: 'events',
    path: '/events',
    component: Events,
  },
  {
    name: 'markets',
    path: '/markets',
    component: Markets,
  },
  {
    name: 'odds-history',
    path: '/markets/selections/:id',
    component: OddsHistory,
  },
  {
    name: 'outcomes',
    path: '/outcomes',
    component: OutComes,
  },
  {
    name: 'event-groups',
    path: '/event-groups',
    component: EventGroups,
  },
  {
    name: 'contests',
    path: '/contests',
    component: Contests,
  },
  {
    name: 'market-types',
    path: '/market-types',
    component: MarketTypes,
  },
  {
    name: 'event-chains',
    path: '/event-chains',
    component: EventGroupChains,
  },
  {
    name: 'selections',
    path: '/selections',
    component: Selections,
  },
  {
    name: 'about',
    path: '/about',
    component: About,
  },
  {
    name: 'event-group-chains',
    path: '/event-group-chains',
    component: EventGroupChainsWorkflow,
  },
  {
    name: 'recuring-contests',
    path: '/recuring-contests',
    component: RecuringContests,
  },
  {
    name: 'content',
    path: '/content',
    component: StaticPages,
  },
  {
    name: 'configuration',
    path: '/configuration',
    component: Configuration,
  },
  {
    name: 'mails',
    path: '/mails',
    component: mails,
  },
  {
    name: 'users',
    path: '/users',
    component: UserManagement,
  },
  {
    name: 'balance',
    path: '/balance',
    component: Balance,
  },
  {
    name: 'reports',
    path: '/reports',
    component: Reports,
  },
  {
    name: 'transactions',
    path: '/transactions',
    component: Transactions,
  },
  {
    name: 'userTransactions',
    path: '/userTransactions/:id',
    component: Transactions,
  },
  {
    name: 'userBets',
    path: '/bets/:id',
    component: Bets,
  },
  {
    name: 'roles',
    path: '/roles',
    component: Roles,
  },
  {
    name: 'contest-bets',
    path: '/contest-bets/:id',
    component: ContestBets,
  },
];
