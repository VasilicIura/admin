import { createAction } from 'redux-actions';
import { SubmissionError } from 'redux-form';
import { accountApi } from '../api';
import { router } from '../route.component';
import { ACTIONS } from './consts';

function requestLogin(creds) {
  return {
    type: ACTIONS.LOGIN_REQUEST,
    creds,
  };
}

function receiveLogin(token) {
  return {
    type: ACTIONS.LOGIN_SUCCESS,
    token,
  };
}

function loginError(message) {
  return {
    type: ACTIONS.LOGIN_FAILURE,
    message,
  };
}

const requestLogout = createAction(ACTIONS.LOGOUT_REQUEST);

export const requestUsername = createAction(
  ACTIONS.USERNAME_REQUEST,
  email => accountApi.get(`sessions/forgot-login/${email}`),
);

export const requestPassword = createAction(
  ACTIONS.PASSWORD_REQUEST,
  username => accountApi.get(`sessions/forgot-password/${username}`),
);

export const loginUser = creds => ((dispatch) => {
  dispatch(requestLogin(creds));

  return accountApi.post('sessions/admin/login', creds)
    .then((response) => {
      localStorage.setItem('token', response.sessionId);
      dispatch(receiveLogin(response.sessionId));
      router.navigate('participants');
    })
    .catch((err) => {
      dispatch(loginError('failed'));

      if (err.response && err.response.status === 401) {
        throw (new SubmissionError({
          _error: 'Wrong Username or Password.',
        }));
      }

      throw (new SubmissionError({
        _error: 'Login failed! ContactSupport',
      }));
    });
});

export function logoutUser() {
  return (dispatch) => {
    localStorage.removeItem('token');
    router.navigate('login');
    dispatch(requestLogout());
  };
}
