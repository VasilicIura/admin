import { handleActions } from 'redux-actions';
import { Map } from 'immutable';
import { ACTIONS } from './consts';

const initialState = Map({
  isFetching: false,
  isAuthenticated: !!localStorage.getItem('token'),
  error: '',
});

export const reducer = handleActions({
  [ACTIONS.LOGIN_REQUEST]: state => state
    .set('isFetching', true)
    .set('isAuthenticated', false),
  [ACTIONS.LOGIN_SUCCESS]: state => state
    .set('isFetching', false)
    .set('isAuthenticated', true)
    .set('error', ''),
  [ACTIONS.LOGIN_FAILURE]: (state, action) => state
    .set('isFetching', false)
    .set('isAuthenticated', false)
    .set('error', action.message),
  [ACTIONS.LOGOUT_REQUEST]: state => state
    .set('isFetching', false)
    .set('isAuthenticated', false)
    .set('error', ''),
}, initialState);
