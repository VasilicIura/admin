import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  LOGIN_REQUEST: null,
  LOGIN_SUCCESS: null,
  LOGIN_FAILURE: null,
  LOGOUT_REQUEST: null,
  USERNAME_REQUEST: null,
  PASSWORD_REQUEST: null,
});
