import { createSelector } from 'reselect';

const authStateSelector = state => state.auth;

export const isAuthenticatedSelector = createSelector(
  authStateSelector,
  auth => auth.get('isAuthenticated'),
);
