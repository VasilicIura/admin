import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { isAuthenticatedSelector } from '../../selectors';
import { loginUser, logoutUser } from '../../actions';
import LoginForm from './form';
import './login.scss';

const Login = props => (
  <div className="login-container">
    <LoginForm onSubmit={props.onSubmit} />
  </div>
);

Login.propTypes = {
  onSubmit: propTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isAuthenticated: isAuthenticatedSelector(state),
});

const mapDispatchToProps = dispatch => ({
  onSubmit: creds => dispatch(loginUser(creds)),
  onLogout: () => dispatch(logoutUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
