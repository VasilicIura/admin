import React from 'react';
import propTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { Form, Button, Alert } from 'react-bootstrap';
import { ControlInput } from 'fe-shared';

const LoginForm = ({ handleSubmit, submitting, error }) => (
  <Form onSubmit={handleSubmit}>
    <Field
      name="username"
      placeholder="Username"
      type="text"
      component={ControlInput}
      label="Username"
    />
    <Field
      name="password"
      placeholder="Password"
      type="password"
      component={ControlInput}
      label="Password"
    />

    {error &&
      <Alert bsStyle="danger">
        {error}
      </Alert>
    }

    <Button type="submit" disabled={submitting} bsStyle="success">
      {submitting ? 'Sending...' : 'Login'}
    </Button>
  </Form>
);

LoginForm.defaultProps = {
  submitting: null,
  error: null,
};

LoginForm.propTypes = {
  handleSubmit: propTypes.func.isRequired,
  submitting: propTypes.bool,
  error: propTypes.object,
};

export default reduxForm({
  form: 'login',
})(LoginForm);

