import { createSelector } from 'reselect';
import { toSelectArray } from 'fe-shared';

const leaguesStateSelector = state => state.leagues;

const leaguesSelector = createSelector(
  leaguesStateSelector,
  leagues => leagues.get('leagues').toJS(),
);

const countriesSelector = createSelector(
  leaguesStateSelector,
  leagues => toSelectArray(leagues.get('countries').toJS()),
);

const sportTypesSelector = createSelector(
  leaguesStateSelector,
  leagues => toSelectArray(leagues.get('sportTypes').toJS()),
);

const filteredLeagues = createSelector(
  leaguesStateSelector,
  eventGroups => eventGroups.get('filteredLeagues').toJS(),
);

const filterCriteriasSelector = createSelector(
  leaguesStateSelector,
  criterias => criterias.get('criteria').toJS(),
);

const leaguesListSelector = createSelector(
  leaguesStateSelector,
  (eventGroups) => {
    const list = eventGroups.get('filteredLeagues').toJS();
    return list && list.content;
  },
);

const leagueSortingFilter = createSelector(
  filterCriteriasSelector,
  ({ pageable }) => {
    if (pageable.hasOwnProperty('orders')) { //eslint-disable-line
      return [
        {
          columnName: pageable.orders[0].field,
          direction: pageable.orders[0].direction.toLowerCase(),
        },
      ];
    }

    return null;
  },
);

export {
  leaguesSelector,
  countriesSelector,
  sportTypesSelector,
  filteredLeagues,
  filterCriteriasSelector,
  leaguesListSelector,
  leagueSortingFilter,
};
