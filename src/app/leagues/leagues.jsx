import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Col, Row } from 'react-bootstrap';
import { NotificationManager } from 'react-notifications';
import { ShowLastCreatedNotification } from 'fe-shared';
import { func, bool, any } from 'prop-types';
import { searchLeagues, fetchCountries, fetchSportTypes, addLeague, changePagination, changeSorting } from './actions';
import { filteredLeagues } from './selectors';
import { AddLeague, LeagueDetail } from './components';
import { COLUMNS } from './consts';
import { CustomCellTemplate, LeaguesSwitchFilterCell } from './components/grid-custom-cell';
import Table from '../common/table';
import './leagues.scss';

class Leagues extends Table {
  static propTypes = {
    fetchData: func.isRequired,
    fetchCountries: func.isRequired,
    fetchSportTypes: func.isRequired,
    changePagination: func.isRequired,
    addLeague: func.isRequired,
    loading: bool,
    leagues: any,
  };

  static defaultProps = {
    leagues: [],
    loading: false,
  };

  state = {
    league: {},
    showLastCreated: true,
    showModal: false,
  };

  componentDidMount = () => {
    this.props.fetchCountries();
    this.props.fetchSportTypes();
  };
  setCurrentActiveColumn = activeSortColumn => this.setState({ activeSortColumn });

  showLastCreated = () => this.setState({ showLastCreated: !this.state.showLastCreated });

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = (values) => {
    this.props.addLeague(values)
      .then(({ value }) => {
        this.setState({ league: value });
        return this.props.fetchData();
      })
      .then(() => {
        ShowLastCreatedNotification(this.showLastCreated);
        this.toggleModal();
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  switchFilters = grid => <LeaguesSwitchFilterCell {...grid} />;
  useCustomCell = props => <CustomCellTemplate {...props} />;

  render() {
    const { leagues, loading } = this.props;
    return (
      <div className="leagues-container">
        <Row>
          <Col lg={10} lgOffset={1} md={12}>
            <LeagueDetail league={this.state.league} showCreated={this.state.showLastCreated} />
            {this.renderTable(
              COLUMNS(this.useCustomCell),
              leagues,
              loading,
              this.switchFilters,
              'Leagues',
            )}
          </Col>
        </Row>
        <div className="add-league">
          <AddLeague
            toggleModal={this.toggleModal}
            showModal={this.state.showModal}
            showLastCreated={this.state.showLastCreated}
            handleSubmit={this.handleSubmit}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  leagues: filteredLeagues(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchData: () => searchLeagues(),
  fetchCountries,
  fetchSportTypes,
  addLeague,
  changePagination,
  changeSorting,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(Leagues);
