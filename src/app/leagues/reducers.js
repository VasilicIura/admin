import { Map, List } from 'immutable';
import { handleActions } from 'redux-actions';
import { LEAGUE_ACTIONS } from './consts';

const initialState = Map({
  leagues: Map(),
  sportTypes: List(),
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  filteredLeagues: Map({}),
  countries: List([]),
});

export const reducer = handleActions({
  [`${LEAGUE_ACTIONS.FETCH_LEAGUES}_FULFILLED`]: (state, action) =>
    state.set('leagues', Map(action.payload)),
  [`${LEAGUE_ACTIONS.FETCH_COUNTRIES}_FULFILLED`]: (state, action) =>
    state.set('countries', List(action.payload)),
  [`${LEAGUE_ACTIONS.FETCH_SPORT_TYPES}_FULFILLED`]: (state, action) =>
    state.set('sportTypes', List(action.payload)),
  [`${LEAGUE_ACTIONS.FILTER_LEAGUES}_FULFILLED`]:
    (state, action) => state.set('filteredLeagues', Map(action.payload)),
  [LEAGUE_ACTIONS.CHANGE_LEAGUES_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [LEAGUE_ACTIONS.CHANGE_LEAGUES_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
  [LEAGUE_ACTIONS.SET_LEAGUES_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
}, initialState);
