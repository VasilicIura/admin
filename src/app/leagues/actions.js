import { createAction } from 'redux-actions';
import { queryFilters, filterSelector } from 'fe-shared';
import { LEAGUE_ACTIONS, FILTER_TYPES } from './consts';
import { filterCriteriasSelector } from './selectors';
import { tradingApi, commonApi } from '../api';

const fetchLeagues = createAction(
  LEAGUE_ACTIONS.FETCH_LEAGUES,
  (paginationQueries = '') => tradingApi.get(`leagues${paginationQueries}`),
);

const fetchCountries = createAction(
  LEAGUE_ACTIONS.FETCH_COUNTRIES,
  () => commonApi.get('enums/countries'),
);

const fetchLeagueById = createAction(
  LEAGUE_ACTIONS.FETCH_LEAGUE_BY_ID,
  id => tradingApi.get(`leagues/${id}`),
);

const updateLeague = createAction(
  LEAGUE_ACTIONS.UPDATE_LEAGUE,
  ({ id, ...league }) => {
    const countries = league.countries.map(e => e.value || e);
    return tradingApi.patch(`leagues/${id}`, { ...league, countries });
  },
);

const fetchSportTypes = createAction(
  LEAGUE_ACTIONS.FETCH_SPORT_TYPES,
  () => commonApi.get('enums/sports'),
);

const addLeague = createAction(
  LEAGUE_ACTIONS.CREATE_LEAGUE,
  (league) => {
    const countries = league.countries.map(e => e.value);
    return tradingApi.post('leagues', { ...league, countries });
  },
);

const filterLeagues = createAction(
  LEAGUE_ACTIONS.FILTER_LEAGUES,
  (criteria = []) => tradingApi.post('leagues/search', { ...criteria }),
);

const changePagination = createAction(
  LEAGUE_ACTIONS.CHANGE_LEAGUES_PAGINATION,
  (pagination = {}) => pagination,
);

const changeSorting = createAction(
  LEAGUE_ACTIONS.CHANGE_LEAGUES_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

const setFilterCriterias = createAction(
  LEAGUE_ACTIONS.SET_LEAGUES_FILTERS,
  values => values,
);

const autocompleteLeagues = createAction(
  LEAGUE_ACTIONS.AUTOCOMPLETE_LEAGUES,
  (criteria = []) => tradingApi.post('leagues/search', { ...criteria }),
);

const searchLeagues = () =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'leaguesFilter' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    dispatch(filterLeagues(filterCriteriasSelector(newState)));
  };

export {
  fetchLeagues,
  updateLeague,
  addLeague,
  fetchCountries,
  fetchLeagueById,
  fetchSportTypes,
  filterLeagues,
  changePagination,
  autocompleteLeagues,
  searchLeagues,
  changeSorting,
};
