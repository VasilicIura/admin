import React from 'react';
import keyMirror from 'keymirror';

export const LEAGUE_ACTIONS = keyMirror({
  FETCH_LEAGUES: null,
  UPDATE_LEAGUE: null,
  CREATE_LEAGUE: null,
  FILTER_LEAGUES: null,
  CHANGE_LEAGUES_PAGINATION: null,
  CHANGE_LEAGUES_SORTING: null,
  SET_LEAGUES_FILTERS: null,
  FETCH_LEAGUE_BY_ID: null,
  FETCH_SPORT_TYPES: null,
  FETCH_COUNTRIES: null,
  AUTOCOMPLETE_LEAGUES: null,
});

export const FILTER_TYPES = {
  IN: [
    'sportPk',
    'country',
  ],
  LIKE: [
    'name',
  ],
};

export const COLUMNS = customCell => [
  {
    Header: 'Name',
    accessor: 'name',
  },
  {
    Header: 'Sport type',
    accessor: 'sportPk',
    Cell: row => customCell(row),
  },
  {
    Header: 'Countries',
    accessor: 'countries',
    Cell: row => customCell(row),
  },
  {
    Header: '',
    width: 130,
    accessor: 'actions',
    sortable: false,
    Filter: ({ onChange }) => (
      <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
    ),
    Cell: row => customCell(row),
  },
];

export const LEAGUES_SORTING_FILTERS = [
  { columnName: 'name', direction: 'asc' },
];
