import React from 'react';
import { func, object, array } from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  SelectFilter,
  AutoCompleteFilter,
} from 'fe-shared';
import { autocompleteLeagues } from '../../../actions';
import { countriesSelector, sportTypesSelector } from '../../../selectors';

class LeaguesSwitchFilterCell extends React.PureComponent {
  static defaultProps = {
    sportTypes: [],
    countries: [],
    column: null,
  };

  static propTypes = {
    autocompleteLeagues: func.isRequired,
    sportTypes: array,
    countries: array,
    column: object,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
      sportTypes,
      countries,
    } = this.props;

    switch (name) {
      case 'name': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteLeagues}
          valueKey="name"
          {...this.props}
        />
      );

      case 'countries': return (
        <FilterField
          name="countries"
          component={SelectFilter}
          options={countries}
          {...this.props}
        />
      );
      case 'sportPk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={sportTypes}
          {...this.props}
        />
      );
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapStateToProps = state => ({
  countries: countriesSelector(state),
  sportTypes: sportTypesSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  autocompleteLeagues,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxFilter({
    filter: 'leaguesFilter',
  }),
)(LeaguesSwitchFilterCell);
