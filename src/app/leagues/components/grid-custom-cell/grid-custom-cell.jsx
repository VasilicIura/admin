import React from 'react';
import { LeagueDetail, EditLeague } from '../../components';

const CustomCellTemplate = (props) => {
  if (props.column.id === 'countries') {
    return (<div>Number of countries assigned: {props.original.countries.length}</div>);
  } else if (props.column.id === 'actions') {
    return (
      <div>
        <LeagueDetail id={props.original.id} />
        <EditLeague league={props.original} />
      </div>
    );
  }

  return <div>{props.value}</div>; // eslint-disable-line
};

export default CustomCellTemplate;
