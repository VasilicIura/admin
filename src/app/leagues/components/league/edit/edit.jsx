import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NotificationManager } from 'react-notifications';
import { func, object } from 'prop-types';
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { LeaguesForm } from '../form';
import { updateLeague, searchLeagues } from '../../../actions';

class EditLeague extends Component {
  static defaultProps = {
    league: null,
  };

  static propTypes = {
    updateLeague: func.isRequired,
    searchLeagues: func.isRequired,
    league: object,
  };

  state = {
    showModal: false,
  };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = (value) => {
    this.props.updateLeague(Object.assign({}, this.props.league, value))
      .then(this.props.searchLeagues)
      .then(() => {
        NotificationManager.info('Updated', `League was updated ${value.name}`);
        this.toggleModal();
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Edit League</span>
      </Tooltip>
    );
    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.toggleModal}
          >
            <i className="mi mi-create" />
          </Button>
        </OverlayTrigger>

        <Modal show={this.state.showModal} onHide={this.toggleModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Edit League</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal &&
              <LeaguesForm
                onSubmit={this.handleSubmit}
                league={this.props.league}
              />
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  updateLeague,
  searchLeagues,
}, dispatch));

export default connect(null, mapDispatchToProps)(EditLeague);
