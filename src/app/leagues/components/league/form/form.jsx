import React, { Component } from 'react';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Form, Button } from 'react-bootstrap';
import { array, any, func, object } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { ControlInput, ControlSelect } from 'fe-shared';
import { countriesSelector, sportTypesSelector } from '../../../selectors';
import { fetchSportTypes, fetchCountries } from '../../../actions';
import { validate } from './validate';

class LeaguesForm extends Component {
  static defaultProps = {
    league: null,
  };

  static propTypes = {
    handleSubmit: func.isRequired,
    submitting: any.isRequired,
    fetchCountries: any.isRequired,
    fetchSportTypes: any.isRequired,
    countries: array.isRequired,
    sports: array.isRequired,
    league: object,
  };

  componentDidMount = () => {
    this.props.fetchCountries();
    this.props.fetchSportTypes();
  };

  componentDidMount = () => {
    this.props.fetchCountries();
    this.props.fetchSportTypes();
  };

  render() {
    const {
      handleSubmit,
      submitting,
      countries,
      sports,
      league,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="name"
          placeholder="Name"
          type="text"
          component={ControlInput}
          label="Name"
          currentValue={league ? league.name : ''}
        />
        <Field
          multi
          name="countries"
          placeholder="Countries"
          component={ControlSelect}
          options={countries}
          label="Countries:"
          currentValue={league ? league.countries : ''}
        />
        <Field
          name="sportPk"
          placeholder="Sport type"
          component={ControlSelect}
          options={sports}
          label="Sport type:"
          currentValue={league ? league.sportPk : ''}
        />
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  sports: sportTypesSelector(state),
  countries: countriesSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchCountries,
  fetchSportTypes,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'leagues',
    validate,
  }),
)(LeaguesForm);
