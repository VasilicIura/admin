export const validate = (values) => {
  const errors = {};
  const {
    name, countries, sportPk,
  } = values;

  if (!name) {
    errors.name = 'Name field is required';
  }

  if (!countries) {
    errors.countries = 'Countries field is required';
  }

  if (!sportPk) {
    errors.sportPk = 'Sport type field is required';
  }

  return errors;
};
