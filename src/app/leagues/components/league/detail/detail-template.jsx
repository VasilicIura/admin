import React from 'react';
import { object } from 'prop-types';
import { Modal, Label, ListGroup, ListGroupItem } from 'react-bootstrap';

const LeagueDetailTemplate = ({ league }) => (
  <Modal.Body>
    <div>
      <div className="detail-view">
        <h5 style={{ lineHeight: '32px' }}>Name:  {league.name}</h5>
        <h5 style={{ float: 'right' }}>
          Sport type:
          <Label className="status-label">{league.sportPk}</Label>
        </h5>
      </div>
      <div style={{ marginTop: '1em' }}>
        <h3>Assigned for countries</h3>
        <ListGroup>
          {
            league.countries.map(country =>
              (<ListGroupItem key={country}>{country}</ListGroupItem>))
          }
        </ListGroup>
      </div>
    </div>
  </Modal.Body>
);

LeagueDetailTemplate.defaultProps = {
  league: null,
};

LeagueDetailTemplate.propTypes = {
  league: object,
};

export default LeagueDetailTemplate;
