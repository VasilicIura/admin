import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { number, func, bool, object } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { fetchLeagueById } from '../../../actions';
import LeagueDetailTemplate from './detail-template';

export class LeagueDetail extends React.PureComponent {
  static defaultProps = {
    id: null,
    league: null,
    showCreated: null,
  };

  static propTypes = {
    id: number,
    fetchLeagueById: func.isRequired,
    showCreated: bool,
    league: object,
  };

  state = {
    showModal: false,
  };

  componentWillReceiveProps(newProps) {
    if (newProps.showCreated !== this.props.showCreated) {
      this.showModal();
    }
  }

  showModal = () => {
    if (this.props.id) {
      this.props.fetchLeagueById(this.props.id)
        .then(({ value }) => this.setState({
          league: value,
          showModal: !this.state.showModal,
        }));
    } else {
      this.setState({
        league: this.props.league,
        showModal: !this.state.showModal,
      });
    }
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>League details</span>
      </Tooltip>
    );
    return (
      <div>
        {
          this.props.id ? (
            <OverlayTrigger placement="top" overlay={tooltip}>
              <Button
                className="circle-button"
                onClick={this.showModal}
                style={{ float: 'left', marginRight: '15px' }}
              >
                <i className="mi mi-visibility" />
              </Button>
            </OverlayTrigger>
          ) : null
        }
        <Modal
          show={this.state.showModal}
          onHide={() => {
            this.setState({ showModal: !this.state.showModal });
          }}
          backdrop="static"
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">League details</Modal.Title>
          </Modal.Header>
          <LeagueDetailTemplate league={this.state.league} />
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchLeagueById,
}, dispatch));

export default connect(null, mapDispatchToProps)(LeagueDetail);
