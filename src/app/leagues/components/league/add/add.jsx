import React from 'react';
import propTypes from 'prop-types';
import { Modal, Button } from 'react-bootstrap';
import { LeaguesForm } from '../form';

const AddLeague = props => (
  <div>
    <Button
      className="add-button"
      onClick={props.toggleModal}
    >
      <i className="mi mi-add" />
    </Button>

    <Modal show={props.showModal} onHide={props.toggleModal}>
      <Modal.Header closeButton>
        <Modal.Title className="modal-title">Add League</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {props.toggleModal && <LeaguesForm onSubmit={props.handleSubmit} />}
      </Modal.Body>
    </Modal>
  </div>
);

AddLeague.propTypes = {
  handleSubmit: propTypes.func.isRequired,
  showModal: propTypes.any.isRequired,
  toggleModal: propTypes.func.isRequired,
};

export default AddLeague;
