import { createAction } from 'redux-actions';
import { filterSelector, queryFilters } from 'fe-shared';
import { SELECTIONS_ACTIONS, FILTER_TYPES } from './consts';
import { filterCriteriasSelector } from './selectors';
import { tradingApi, commonApi } from '../api';

const fetchSelections = createAction(
  SELECTIONS_ACTIONS.SEARCH_SELECTIONS,
  (criteria = []) => tradingApi
    .post('selections/search', { ...criteria }),
);

const setFilterCriterias = createAction(
  SELECTIONS_ACTIONS.SET_SELECTIONS_FILTERS,
  values => values,
);

const autocompleteSelections = createAction(
  SELECTIONS_ACTIONS.AUTOCOMPLETE_SELECTIONS_FILTER,
  (criteria = []) => tradingApi.post('selections/search', { ...criteria }),
);

const searchSelections = () =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'selectionsFilter' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    dispatch(fetchSelections(filterCriteriasSelector(newState)));
  };


const editSelections = createAction(
  SELECTIONS_ACTIONS.EDIT_SELECTIONS,
  ({ oddsNumerator, oddsDenominator, ...selection }) => tradingApi.put('selections', [{
    ...selection,
    oddsNumerator: parseInt(oddsNumerator, 10),
    oddsDenominator: parseInt(oddsDenominator, 10),
  }]),
);

const changePagination = createAction(
  SELECTIONS_ACTIONS.CHANGE_SELECTIONS_PAGINATION,
  (pagination = {}) => pagination,
);

const changeSorting = createAction(
  SELECTIONS_ACTIONS.CHANGE_SELECTIONS_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

const fetchSelectionsTypes = createAction(
  SELECTIONS_ACTIONS.FETCH_SELECTIONS_TYPE,
  () => commonApi.get('/enums/types/entity/Selection'),
);

export {
  fetchSelections,
  editSelections,
  autocompleteSelections,
  changePagination,
  changeSorting,
  searchSelections,
  fetchSelectionsTypes,
};
