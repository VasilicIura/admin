import React from 'react';
import { func, any } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { searchSelections, fetchSelectionsTypes, changeSorting, changePagination } from './actions';
import { filteredSelections } from './selectors';
import Table from '../common/table';
import { COLUMNS } from './consts';
import { CustomCellTemplate } from './components/custom-cell-template';
import { SelectionsSwitchFilterCell } from './components/filterCell';
import './selections.scss';

class Selections extends Table {
  static defaultProps = {
    selections: [],
  };

  static propTypes = {
    fetchData: func.isRequired,
    changePagination: func.isRequired,
    selections: any,
  };

  componentDidMount = () => this.props.fetchSelectionsTypes();

  switchFilters = grid => <SelectionsSwitchFilterCell {...grid} />;
  useCustomCell = props => <CustomCellTemplate {...props} />;

  render = () => (
    <div className="events-container">
      {this.renderTable(
        COLUMNS(this.useCustomCell),
        this.props.selections,
        false,
        this.switchFilters,
        'Selections',
      )}
    </div>
  );
}

const mapStateToProps = state => ({
  selections: filteredSelections(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchData: () => searchSelections(),
  fetchSelectionsTypes,
  changeSorting,
  changePagination,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(Selections);

