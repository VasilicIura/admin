import { Map, List } from 'immutable';
import { handleActions } from 'redux-actions';
import { SELECTIONS_ACTIONS } from './consts';

const initialState = Map({
  selections: Map({}),
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  filteredSelections: Map({}),
  selectionTypes: List([]),
});

export const reducer = handleActions({
  [`${SELECTIONS_ACTIONS.SEARCH_SELECTIONS}_FULFILLED`]: (state, action) =>
    state.set('selections', Map(action.payload)),
  [`${SELECTIONS_ACTIONS.SEARCH_SELECTIONS}_FULFILLED`]:
    (state, action) => state.set('filteredSelections', Map(action.payload)),
  [SELECTIONS_ACTIONS.CHANGE_SELECTIONS_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [SELECTIONS_ACTIONS.SET_SELECTIONS_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
  [SELECTIONS_ACTIONS.CHANGE_SELECTIONS_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
  [`${SELECTIONS_ACTIONS.FETCH_SELECTIONS_TYPE}_FULFILLED`]: (state, action) =>
    state.set('selectionTypes', List(action.payload)),
}, initialState);
