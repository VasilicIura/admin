import React from 'react';
import propTypes from 'prop-types';
import { ViewSelection } from '../selection/viewMode';
import { EditSelection } from '../selection/edit';

const CustomCellTemplate = (props) => {
  switch (props.column.id) {
    case 'actions':
      return (
        <div>
          <EditSelection selection={props.original} />
          <ViewSelection selection={props.original} />
        </div>
      );
    default: return props.value;
  }
};

CustomCellTemplate.defaultProps = {
  original: null,
  value: '',
};

CustomCellTemplate.propTypes = {
  column: propTypes.object.isRequired,
  value: propTypes.any,
  original: propTypes.object,
};

export default CustomCellTemplate;
