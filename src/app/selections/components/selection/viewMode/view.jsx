import React from 'react';
import { Button, Modal, OverlayTrigger, Tooltip, ListGroup, ListGroupItem } from 'react-bootstrap';
import { object } from 'prop-types';

export class ViewSelection extends React.Component {
  static propTypes = {
    selection: object.isRequired,
  };

  state = {
    showModal: false,
  };

  showModal = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const view = this.props.selection;
    const tooltip = (
      <Tooltip id="tooltip">
        <span>View Selection</span>
      </Tooltip>
    );
    return (
      <div className="outcome-view">
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.showModal}
          >
            <i className="mi mi-visibility" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={() => this.showModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">View Selection</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <ListGroup>
              <ListGroupItem>
                <strong>Selection Target</strong> {view.appliesToCdn}
              </ListGroupItem>
              <ListGroupItem>
                <strong>Selection Types</strong> {view.selectionTypePk}
              </ListGroupItem>
              <ListGroupItem>
                <strong>Numerator</strong> {view.oddsNumerator}
              </ListGroupItem>
              <ListGroupItem>
                <strong>Denominator</strong> {view.oddsDenominator}
              </ListGroupItem>
            </ListGroup>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default ViewSelection;
