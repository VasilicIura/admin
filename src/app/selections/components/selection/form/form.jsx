import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import { any, func, object } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { ControlInput } from 'fe-shared';

class FormSelection extends Component {
  static propTypes = {
    handleSubmit: func.isRequired,
    submitting: any.isRequired,
    selections: object.isRequired,
  };

  render() {
    const {
      handleSubmit,
      submitting,
      selections,
    } = this.props;
    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="oddsNumerator"
          placeholder="Numerator"
          type="number"
          pattern="[0-9]*"
          component={ControlInput}
          label="Numerator:"
          currentValue={selections ? `${selections.oddsNumerator}` : ''}
        />
        <Field
          name="oddsDenominator"
          placeholder="Denominator"
          type="number"
          pattern="[0-9]*"
          component={ControlInput}
          label="Denominator:"
          currentValue={selections ? `${selections.oddsDenominator}` : ''}
        />
        <Field
          name="appliesToCdn"
          placeholder="Selection Target"
          type="text"
          component={ControlInput}
          label="Selection Target:"
          currentValue={selections ? selections.appliesToCdn : ''}
        />
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'selections',
})(FormSelection);
