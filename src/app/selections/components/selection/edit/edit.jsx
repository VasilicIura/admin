import React from 'react';
import { connect } from 'react-redux';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { func, object } from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { bindActionCreators } from 'redux';
import { FormSelection } from './../form';
import { fetchSelections, editSelections } from '../../../actions';

export class EditSelection extends React.Component {
  static propTypes = {
    selection: object.isRequired,
    editSelections: func.isRequired,
    fetchSelections: func.isRequired,
  };

  state = {
    showModal: false,
  };

  showModal = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = (value) => {
    this.props.editSelections(Object.assign({}, this.props.selection, value))
      .then(() => {
        this.props.fetchSelections();
        this.showModal();
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Edit Selection</span>
      </Tooltip>
    );
    return (
      <div className="outcome-view">
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.showModal}
          >
            <i className="mi mi-create" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({ showModal: !this.state.showModal })}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Edit Selection</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <FormSelection
                onSubmit={this.handleSubmit}
                selections={this.props.selection}
              />
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  editSelections,
  fetchSelections,
}, dispatch));

export default connect(null, mapDispatchToProps)(EditSelection);
