import React from 'react';
import { func, object, array } from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  AutoCompleteFilter,
  SelectFilter,
} from 'fe-shared';
import { autocompleteSelections, fetchSelectionsTypes } from '../../actions';
import { selectionTypes } from '../../selectors';

class SelectionsSwitchFilterCell extends React.PureComponent {
  static defaultProps = {
    column: null,
    types: [],
  };

  static propTypes = {
    autocompleteSelections: func.isRequired,
    types: array,
    column: object,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
    } = this.props;

    switch (name) {
      case 'selectionTypePk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={this.props.types}
          {...this.props}
        />
      );
      case 'appliesToCdn': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autocompleteKey="appliesToCdn"
          valueKey="appliesToCdn"
          labelKey="appliesToCdn"
          autoCompleteAction={this.props.autocompleteSelections}
          {...this.props}
        />
      );
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapStateToProps = state => ({
  types: selectionTypes(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  autocompleteSelections,
  fetchSelectionsTypes,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxFilter({
    filter: 'selectionsFilter',
  }),
)(SelectionsSwitchFilterCell);
