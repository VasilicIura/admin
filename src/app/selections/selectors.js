import { createSelector } from 'reselect';
import { toSelectArray } from 'fe-shared';

const selectionsStateSelector = state => state.selections;

const selectionsSelector = createSelector(
  selectionsStateSelector,
  selections => selections.get('selections').toJS(),
);


const filteredSelections = createSelector(
  selectionsStateSelector,
  selections => selections.get('filteredSelections').toJS(),
);

const filterCriteriasSelector = createSelector(
  selectionsStateSelector,
  criterias => criterias.get('criteria').toJS(),
);

const selectionsListSelector = createSelector(
  selectionsStateSelector,
  (selections) => {
    const list = selections.get('filteredSelections').toJS();
    return list && list.content;
  },
);

const selectionsSortingSelector = createSelector(
  filterCriteriasSelector,
  ({ pageable }) => {
    if (pageable.hasOwnProperty('orders')) { //eslint-disable-line
      return [
        {
          columnName: pageable.orders[0].field,
          direction: pageable.orders[0].direction.toLowerCase(),
        },
      ];
    }

    return null;
  },
);

const selectionTypes = createSelector(
  selectionsStateSelector,
  selection => toSelectArray(selection.get('selectionTypes').toJS()),
);

export {
  selectionsSelector,
  selectionsListSelector,
  selectionsSortingSelector,
  filteredSelections,
  filterCriteriasSelector,
  selectionTypes,
};
