import React from 'react';
import keyMirror from 'keymirror';

export const SELECTIONS_ACTIONS = keyMirror({
  SEARCH_SELECTIONS: null,
  CHANGE_SELECTIONS_SORTING: null,
  SET_SELECTIONS_FILTERS: null,
  CHANGE_SELECTIONS_PAGINATION: null,
  AUTOCOMPLETE_SELECTIONS_FILTER: null,
  FETCH_SELECTIONS_TYPE: null,
  EDIT_SELECTIONS: null,
});

export const COLUMNS = customCell => [
  {
    Header: 'Numerator',
    accessor: 'oddsNumerator',
  },
  {
    Header: 'Denominator',
    accessor: 'oddsDenominator',
  },
  {
    Header: 'Selection Types',
    accessor: 'selectionTypePk',
  },
  {
    Header: 'Selection Target',
    accessor: 'appliesToCdn',
  },
  {
    Header: '',
    width: 130,
    accessor: 'actions',
    sortable: false,
    Filter: ({ onChange }) => (
      <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
    ),
    Cell: row => customCell(row),
  },
];

export const FILTER_TYPES = {
  IN: [
    'appliesToCdn',
    'selectionTypePk',
  ],
  EQ: [
    'oddsNumerator',
    'oddsDenominator',
  ],
};
