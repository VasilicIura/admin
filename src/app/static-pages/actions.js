import { createAction } from 'redux-actions';
import { STATIC_PAGES_ACTIONS } from './consts';
import { commonApi } from '../api';

const fetchPageKeys = createAction(
  STATIC_PAGES_ACTIONS.FETCH_KEYS,
  () => commonApi.get('config/domain/Static content/groupname/Static content keys'),
);

const fetchPageContent = createAction(
  STATIC_PAGES_ACTIONS.FETCH_CONTENT,
  key => commonApi.get(`content/${key}`),
);

const savePageContent = createAction(
  STATIC_PAGES_ACTIONS.SAVE_PAGE,
  (key, content) => commonApi.patch(`content/${key}`, { thekey: key, theContent: content }),
);

export {
  fetchPageKeys,
  fetchPageContent,
  savePageContent,
};
