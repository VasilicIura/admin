/* eslint-disable */
export const EDITOR_MODULES = {
  toolbar: [
    [{ 'header': [1, 2, false] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{ 'color': [] }, { 'background': [] }],
    [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
    ['link', 'image'],
    ['clean'],
  ],
};

export const EDITOR_FORMATS = [
  // tag header 
  'header',
  // text formatting 
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  // color formatting
  'color', 'background',
  // lists and indentation
  'list', 'bullet', 'indent',
  // image formatting
  'link', 'image',
];
/* eslint-enable */
