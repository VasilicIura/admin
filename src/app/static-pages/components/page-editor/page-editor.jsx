import React, { Component } from 'react';
import ReactQuill from 'react-quill';
import { Button } from 'react-bootstrap';
import { PropTypes } from 'prop-types';
import { EDITOR_MODULES, EDITOR_FORMATS } from './consts';
import './page-editor.scss';

class PageEditor extends Component {
  static propTypes = {
    content: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    onSave: PropTypes.func.isRequired,
  };

  state = { text: '' };

  componentWillReceiveProps(nextProps) {
    if (nextProps.content !== this.state.text) {
      this.setState({ text: nextProps.content });
    }
  }

  handleChange = (text) => {
    this.setState({ text });
  };

  render() {
    return (
      <div className="page-editor">
        <h1>{this.props.title}</h1>

        <ReactQuill
          className="bg-white"
          value={this.state.text}
          onChange={this.handleChange}
          modules={EDITOR_MODULES}
          formats={EDITOR_FORMATS}
        />

        <Button onClick={() => this.props.onSave(this.state.text)} > Save </Button>
      </div>
    );
  }
}

export default PageEditor;
