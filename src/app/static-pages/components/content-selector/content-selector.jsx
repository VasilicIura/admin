import React from 'react';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import { string, arrayOf } from 'prop-types';

const ContentSelector = (props) => {
  const keys = props.contentKeys.map(key => (
    <MenuItem
      eventKey={key}
      key={key}
      active={key === props.selectedKey}
      onClick={() => props.onSelect(key)}
    >
      {key}
    </MenuItem>
  ));

  return (
    <DropdownButton
      bsStyle="primary"
      title={props.selectedKey ? props.selectedKey : 'Select content to edit'}
      id="content-select"
    >
      {keys}
    </DropdownButton>
  );
};

ContentSelector.propTypes = {
  selectedKey: string,
  contentKeys: arrayOf(string).isRequired,
};

ContentSelector.defaultProps = {
  selectedKey: null,
};

export default ContentSelector;
