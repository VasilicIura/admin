import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { func, string, any } from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { PageEditor, ContentSelector } from './components';
import {
  fetchPageKeys,
  fetchPageContent,
  savePageContent,
} from './actions';
import { keysSelector, contentSelector } from './selectors';

class StaticPages extends Component {
  static propTypes = {
    fetchPageKeys: func.isRequired,
    fetchPageContent: func.isRequired,
    savePageContent: func.isRequired,
    contentKeys: any.isRequired,
    content: string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedKey: null,
    };
  }

  componentDidMount() {
    this.props.fetchPageKeys();
  }

  getContentByKey = (key) => {
    this.setState({ selectedKey: key });
    this.props.fetchPageContent(key);
  };

  save = (text) => {
    this.props.savePageContent(this.state.selectedKey, text)
      .then(() => NotificationManager.success('Content saved.', 'Done.', 3000))
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render() {
    return (
      <div>
        <h3>Content Editor</h3>
        <ContentSelector
          onSelect={this.getContentByKey}
          contentKeys={this.props.contentKeys}
          selectedKey={this.state.selectedKey}
        />
        {this.state.selectedKey &&
          <PageEditor
            content={this.props.content}
            title={this.state.selectedKey}
            onSave={this.save}
          />
        }
      </div>
    );
  }
}

const mapStateToProp = state => ({
  contentKeys: keysSelector(state),
  content: contentSelector(state),
});

const mapDispatchToProp = dispatch => (bindActionCreators({
  fetchPageKeys,
  fetchPageContent,
  savePageContent,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(StaticPages);
