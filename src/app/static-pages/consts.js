import keyMirror from 'keymirror';

export const STATIC_PAGES_ACTIONS = keyMirror({
  FETCH_KEYS: null,
  FETCH_CONTENT: null,
  SAVE_PAGE: null,
});
