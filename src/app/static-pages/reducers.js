import { Map, List } from 'immutable';
import { handleActions } from 'redux-actions';
import { STATIC_PAGES_ACTIONS } from './consts';

const initialState = Map({
  pageKeys: List([]),
  content: '',
});

export const reducer = handleActions({
  [`${STATIC_PAGES_ACTIONS.FETCH_KEYS}_FULFILLED`]: (state, action) =>
    state.set('pageKeys', List(action.payload.values)),
  [STATIC_PAGES_ACTIONS.FETCH_CONTENT]: state => state.set('content', ''),
  [`${STATIC_PAGES_ACTIONS.FETCH_CONTENT}_FULFILLED`]: (state, action) =>
    state.set('content', action.payload.theContent),
}, initialState);
