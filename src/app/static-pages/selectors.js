import { createSelector } from 'reselect';

const StaticPagesStateSelector = state => state.staticPages;

const contentSelector = createSelector(
  StaticPagesStateSelector,
  pages => pages.get('content'),
);

const keysSelector = createSelector(
  StaticPagesStateSelector,
  pages => pages.get('pageKeys').toJS(),
);

export {
  keysSelector,
  contentSelector,
};
