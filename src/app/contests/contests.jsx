import React from 'react';
import { Col, Row, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { func, bool, any } from 'prop-types';
import { bindActionCreators } from 'redux';
import 'react-table/react-table.css';
import { fetchMarketTypes } from 'app/markets';
import { AddContest } from './components';
import { searchContests, changePagination, fetchSportTypes, fetchCurrencies, changeSorting, fetchEntityStatuses } from './actions';
import { filteredContests } from './selectors';
import { CustomCellTemplate } from './components/custom-cell-template';
import { ContestSwitchFilters } from './components/filterCell';
import { ColumnChooser } from './components/column-chooser/column-chooser';
import Table from '../common/table';
import './contests.scss';

class Contests extends Table {
  static propTypes = {
    fetchData: func.isRequired,
    changePagination: func.isRequired,
    changeSorting: func.isRequired,
    fetchMarketTypes: func.isRequired,
    loading: bool,
    data: any,
  };

  static defaultProps = {
    data: [],
    loading: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      allColumns: this.getAllColumns(),
      columnsToShow: this.getDefaultColumns(),
      showTableSetting: false,
    };
    this.props.fetchEntityStatuses('Contest');
    this.handleClick = this.handleClick.bind(this);
  }

  getAllColumns = () => {
    const COLUMNS = [
      {
        Header: 'Name',
        accessor: 'name',
        show: true,
      },
      {
        Header: 'Entry Fee',
        accessor: 'entryFee',
        show: true,
      },
      {
        Header: 'Contest Status',
        accessor: 'contestStatusPk',
        show: true,
      },
      {
        Header: 'Contest Type',
        accessor: 'contestTypePk',
        show: true,
      },
      {
        Header: 'Start date',
        accessor: 'firstEventStartTimeStamp',
        show: true,
        Cell: row => <div>{<CustomCellTemplate {...row} />}</div>,
      },
      {
        Header: 'House cash prize', accessor: 'houseCashPrize', show: true,
      },
      {
        Header: 'Market type', accessor: 'marketTypeNames',
      },
      {
        Header: 'Number of entries', accessor: 'maxNrOfBetAccrossAllUsers',
      },
      {
        Header: 'Payout Structure', accessor: 'payoutStructurePk',
      },
      {
        Header: 'League', accessor: 'leagueName',
      },
      {
        Header: 'Number of computed entries', accessor: 'nrOfPlacedBetsComputed',
      },
      {
        Header: 'Actual payout structure', accessor: 'payoutStructureActualPk',
      },
      {
        Header: 'Currency', accessor: 'currencyPk',
      },
      {
        Header: 'Prize pool type', accessor: 'prizePoolTypePk',
      },
      // {
      //   Header: 'Open Bets time', accessor: 'betsOpenTimeStamp',
      // },
      {
        Header: 'Entry Cost', accessor: 'entryCostComputed',
      },
      // {
      //   Header: 'Next events time', accessor: 'nextEventStartTimeStamp',
      // },
      {
        Header: 'Total cash prize computed', accessor: 'totalCashPrizeComputed',
      },
      {
        Header: 'Commission', accessor: 'commission',
      },
      {
        Header: 'Actions',
        accessor: 'actions',
        sortable: false,
        show: true,
        Filter: ({ onChange }) => (
          <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
        ),
        Cell: row => <CustomCellTemplate {...row} />,
      },
    ];
    return COLUMNS;
  };

  getDefaultColumns = () => this.getAllColumns().filter(column => column.show);

  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn,
    }));
  }

  columnsChange = columnsToShow =>
    this.setState({ columnsToShow: columnsToShow.filter(column => column.show === true) });

  toggleTableSetting = () =>
    this.setState({ showTableSetting: !this.state.showTableSetting });

switchFilters = props => <ContestSwitchFilters {...props} />;

render() {
  const { data, loading } = this.props;
  const { columnsToShow } = this.state;
  return (
    <div className="events-container">
      <div className="filter-table-grid">
        <Button className="table-settings" onClick={this.toggleTableSetting}>
          <i className="material-icons">settings</i>
          <span>Settings</span>
        </Button>
        <Col className="filter-grid-container">
          <Row className={`filter-hidden-columns ${this.state.showTableSetting ? 'active' : ''}`}>
            <Col>
              <div className="panel panel-default">
                <div className="panel-heading">Column Chooser</div>
                <ColumnChooser
                  columns={this.state.allColumns}
                  onColumnsChange={this.columnsChange}
                />
              </div>
            </Col>
          </Row>
          {this.renderTable(
            columnsToShow,
            data,
            loading,
            this.switchFilters,
            'Contests',
          )}
        </Col>
      </div>
      <div className="add-event">
        <AddContest />
      </div>
    </div>
  );
}
}

const mapStateToProps = state => ({
  data: filteredContests(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchData: () => searchContests(),
  changeSorting: data => changeSorting(data),
  changePagination,
  fetchEntityStatuses,
  fetchSportTypes,
  fetchCurrencies,
  fetchMarketTypes,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(Contests);
