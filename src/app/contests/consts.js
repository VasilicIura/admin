import keyMirror from 'keymirror';

export const CONTEST_ACTIONS = keyMirror({
  FETCH_CONTESTS: null,
  SEARCH_EVENT_GROUP_CHAINS: null,
  ADD_CONTEST: null,
  FETCH_EVENT_GROUP_CHAIN_BY_ID: null,
  CONTEST_FETCH_EVENT_GROUP_BY_ID: null,
  SEARCH_CONTESTS: null,
  CHANGE_CONTESTS_PAGINATION: null,
  SET_CONTESTS_FILTERS: null,
  CHANGE_CONTESTS_SORTING: null,
  CANCEL_CONTEST: null,
  AUTOCOMPLETE_CONTESTS: null,
});

export const FILTER_ENTITY_ACTIONS = keyMirror({
  FETCH_ENTITY_STATUSES: null,
  FETCH_ENTITY_TYPES: null,
  FETCH_SPORT_TYPES: null,
  FETCH_CURRENCIES: null,
  FETCH_RANGE_VALUES: null,
});

export const CURRENT_ENTITY = 'Contest';
export const DEFAULT_STATUS = 'New';
export const REGULAR_CONTEST = 'Regular';

export const FILTER_TYPES = {
  IN: [
    'name',
    'contestStatusPk',
    'currencyPk',
    'marketTypeNames',
    'sportPk',
    'leagueNames',
    'contestTypePk',
    'payoutStructurePk',
    'prizePoolTypePk',
    'payoutStructureActualPk',
  ],
  RANGE: [
    'entryFee',
    'firstEventStartTimeStamp',
    'commission',
    'houseCashPrize',
    'maxNrOfBetAccrossAllUsers',
    'totalCashPrizeComputed',
    'nrOfUsersThatBettedComputed',
    'entryCostComputed',
    'betsOpenTimeStamp',
    'nextEventStartTimeStamp',
  ],
};

export const FILTER_FIELDS = {
  marketTypes: 'marketTypeNames',
  entryFee: 'entryFee',
  contestStatusPk: 'contestStatusPk',
  contestTypePk: 'contestTypePk',
  startDate: 'startDate',
  payoutStructurePk: 'payoutStructurePk',
  payoutStructureActualPk: 'payoutStructureActualPk',
  prizePoolTypePk: 'prizePoolTypePk',
  leagueNames: 'leagueNames',

  /* range-fields */

  commission: 'commission',
  maxNrOfBetAccrossAllUsers: 'maxNrOfBetAccrossAllUsers',
  entryCostComputed: 'entryCostComputed',
  houseCashPrize: 'houseCashPrize',
};

export const filterSize = {
  page: 0,
  size: 1000,
};
