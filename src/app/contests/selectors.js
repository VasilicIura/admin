import { createSelector } from 'reselect';
import { toSelectArray, toSelectArrayFromJSON, getEntryFeeValues } from 'fe-shared';

const contestsStateSelector = state => state.contests;
const leaguesStateSelector = state => state.leagues;

const contestSelectors = createSelector(
  contestsStateSelector,
  contests => contests.get('contests').toJS(),
);

const entityStatusesSelectors = createSelector(
  contestsStateSelector,
  entityStatuses => toSelectArray(entityStatuses.get('entity_statuses').toJS()),
);

const entityTypeSelectors = createSelector(
  contestsStateSelector,
  entityTypes => toSelectArray(entityTypes.get('entity_types').toJS()),
);

const leaguesSelector = createSelector(
  leaguesStateSelector,
  leagues => leagues.get('leagues').toJS().content,
);

const sportTypesSelector = createSelector(
  contestsStateSelector,
  sportTypes => toSelectArray(sportTypes.get('sport_types').toJS()),
);

const currenciesTypeSelector = createSelector(
  contestsStateSelector,
  currencies => toSelectArrayFromJSON(currencies.get('currencies').toJS(), 'code', 'name'),
);

const rangeValuesSelector = createSelector(
  contestsStateSelector,
  ranges => toSelectArray(getEntryFeeValues(
    ranges.get('ranges').toJS().sort((a, b) => a > b),
  )),
);

const eventGorupChainsSelector = createSelector(
  contestsStateSelector,
  eventGroupChains => eventGroupChains.get('eventGroupChains').toJS(),
);

const eventGroupChainById = createSelector(
  contestsStateSelector,
  eventGroupChains => eventGroupChains.get('eventGroupChain').toJS(),
);

const eventGroupById = createSelector(
  contestsStateSelector,
  eventGroupChains => eventGroupChains.get('eventGroup').toJS(),
);

const filteredContests = createSelector(
  contestsStateSelector,
  event => event.get('filteredContests').toJS(),
);

const filterCriteriasSelector = createSelector(
  contestsStateSelector,
  criterias => criterias.get('criteria').toJS(),
);

const contestsListSelector = createSelector(
  contestsStateSelector,
  (events) => {
    const list = events.get('filteredContests').toJS();
    return list && list.content;
  },
);

const contestsSortingSelector = createSelector(
  filterCriteriasSelector,
  ({ pageable }) => {
    if (pageable.hasOwnProperty('orders')) { //eslint-disable-line
      return [
        {
          columnName: pageable.orders[0].field,
          direction: pageable.orders[0].direction.toLowerCase(),
        },
      ];
    }

    return null;
  },
);

export {
  contestSelectors,
  entityStatusesSelectors,
  entityTypeSelectors,
  leaguesSelector,
  sportTypesSelector,
  currenciesTypeSelector,
  rangeValuesSelector,
  eventGorupChainsSelector,
  eventGroupChainById,
  eventGroupById,
  filteredContests,
  filterCriteriasSelector,
  contestsListSelector,
  contestsSortingSelector,
};
