import React from 'react';
import { Button } from 'react-bootstrap';
import propTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actions } from 'redux-router5';
import moment from 'moment';
import { DetailContests, EditContest } from '../contest';

const CustomCellTemplate = (props) => {
  switch (props.column.id) {
    case 'actions':
      return (
        <div>
          <DetailContests
            contest={props.original}
          />
          <EditContest
            contest={props.original}
          />
          <Button
            className="circle-button pull-left"
            style={{ display: 'inline-block', marginRight: '15px' }}
            onClick={() => props.navigateTo('contest-bets', { id: props.original.id })}
          >
            <i className="material-icons" >format_bold</i>
          </Button>
        </div>
      );
    case 'firstEventStartTimeStamp':
      return moment(props.value).format('YYYY-MM-DD');
    default:
      return props.value;
  }
};

CustomCellTemplate.propTypes = {
  column: propTypes.object,
  value: propTypes.any,
  original: propTypes.object,
};

CustomCellTemplate.defaultProps = {
  column: null,
  value: null,
  original: null,
};
const mapDispatchToProps = dispatch => (bindActionCreators({
  ...actions,
}, dispatch));

export default connect(null, mapDispatchToProps)(CustomCellTemplate);
