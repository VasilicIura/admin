import React from 'react';
import { func, object, array } from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { autocompleteLeagues } from 'app/leagues';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  SelectFilter,
  AutoCompleteFilter,
  DateRangeFilter,
} from 'fe-shared';
import { marketTypesSelector } from 'app/markets';
import { RangeFilters, TypeFilters } from './components';
import { autocompleteContests, fetchRangeValues, fetchEntityTypes } from '../../actions';
import { currenciesTypeSelector, entityStatusesSelectors } from '../../selectors';

class ContestSwitchFilters extends React.PureComponent {
  static defaultProps = {
    column: null,
    marketTypes: [],
    currencies: [],
    statuses: [],
  };

  static propTypes = {
    autocompleteContests: func.isRequired,
    autocompleteLeagues: func.isRequired,
    fetchRangeValues: func.isRequired,
    fetchEntityTypes: func.isRequired,
    marketTypes: array,
    currencies: array,
    statuses: array,
    column: object,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
      statuses,
    } = this.props;

    const statusesOptions = statuses;
    switch (name) {
      case 'name': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteContests}
          valueKey="name"
          {...this.props}
        />
      );
      case 'leagueNames': return (
        <FilterField
          name="leagueNames"
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteLeagues}
          {...this.props}
        />
      );
      case 'marketTypeNames': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={this.props.marketTypes}
          {...this.props}
        />
      );
      case 'currencyPk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={this.props.currencies}
          {...this.props}
        />
      );
      case 'contestTypePk': return (
        <FilterField
          name={name}
          typeAction={this.props.fetchEntityTypes}
          entity="Contest"
          component={TypeFilters}
          {...this.props}
        />
      );
      case 'payoutStructurePk': return (
        <FilterField
          name={name}
          typeAction={this.props.fetchEntityTypes}
          entity="Payout Structure"
          component={TypeFilters}
          {...this.props}
        />
      );
      case 'prizePoolTypePk': return (
        <FilterField
          name={name}
          typeAction={this.props.fetchEntityTypes}
          entity="Prize Pool"
          component={TypeFilters}
          {...this.props}
        />
      );
      case 'payoutStructureActualPk': return (
        <FilterField
          name={name}
          typeAction={this.props.fetchEntityTypes}
          entity="Payout Structure"
          component={TypeFilters}
          {...this.props}
        />
      );
      case 'entryFee': return (
        <FilterField
          name={name}
          rangeAction={this.props.fetchRangeValues}
          domain="Contest filters"
          groupName="Entry fee range boundary"
          component={RangeFilters}
          {...this.props}
        />
      );
      case 'commission': return (
        <FilterField
          name={name}
          rangeAction={this.props.fetchRangeValues}
          domain="Contest filters"
          groupName="Commission range boundary"
          component={RangeFilters}
          {...this.props}
        />
      );
      case 'maxNrOfBetAccrossAllUsers': return (
        <FilterField
          name={name}
          rangeAction={this.props.fetchRangeValues}
          domain="Contest filters"
          groupName="Max nr of users to bet range boundary"
          component={RangeFilters}
          {...this.props}
        />
      );
      case 'entryCostComputed': return (
        <FilterField
          name={name}
          rangeAction={this.props.fetchRangeValues}
          domain="Contest filters"
          groupName="Entry cost range boundary"
          component={RangeFilters}
          {...this.props}
        />
      );
      case 'houseCashPrize': return (
        <FilterField
          name={name}
          rangeAction={this.props.fetchRangeValues}
          domain="Contest filters"
          groupName="House cash prize range boundary"
          component={RangeFilters}
          {...this.props}
        />
      );
      case 'contestStatusPk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={statusesOptions && statuses}
          {...this.props}
        />
      );
      case 'firstEventStartTimeStamp': return (
        <FilterField
          name={name}
          component={DateRangeFilter}
          {...this.props}
        />
      );
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapStateToProps = state => ({
  marketTypes: marketTypesSelector(state),
  currencies: currenciesTypeSelector(state),
  statuses: entityStatusesSelectors(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  autocompleteContests,
  autocompleteLeagues,
  fetchRangeValues,
  fetchEntityTypes,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxFilter({
    filter: 'contestFilters',
  }),
)(ContestSwitchFilters);
