import React, { Component } from 'react';
import { array, bool, func, string } from 'prop-types';
import { SelectFilter, toSelectArray } from 'fe-shared';

class TypeFilter extends Component {
  static defaultProps = {
    options: [],
    multi: false,
  };

  static propTypes = {
    options: array,
    multi: bool,
    typeAction: func.isRequired,
    entity: string.isRequired,
  };

  state = {
    options: [],
  };

  componentDidMount() {
    const { entity } = this.props;
    if (this.props.typeAction) {
      this.props.typeAction(entity)
        .then(({ value }) => {
          this.setState({ options: toSelectArray(value) });
        });
    }
  }

  render() {
    return (
      <SelectFilter
        {...this.props}
        options={this.state.options}
        multi={this.props.multi}
      />
    );
  }
}

export default TypeFilter;
