import React, { Component } from 'react';
import { array, bool, func, string } from 'prop-types';
import { SelectFilter, toSelectArray, getEntryFeeValues } from 'fe-shared';

class RangeFilters extends Component {
  static defaultProps = {
    options: [],
    multi: false,
    action: null,
  };

  static propTypes = {
    options: array,
    multi: bool,
    rangeAction: func.isRequired,
    action: func,
    domain: string.isRequired,
    groupName: string.isRequired,
  };

  state = {
    options: [],
  };

  componentDidMount() {
    const { domain, groupName } = this.props;
    if (this.props.rangeAction) {
      this.props.rangeAction(domain, groupName)
        .then(({ value: { values } }) => {
          this.setState({
            options: toSelectArray(getEntryFeeValues(values).sort((a, b) => a > b)),
          });
        });
    }
  }

  render() {
    return (
      <SelectFilter
        {...this.props}
        options={this.state.options}
        multi={this.props.multi}
      />
    );
  }
}

export default RangeFilters;
