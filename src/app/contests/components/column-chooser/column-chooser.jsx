import React, { Component } from 'react';
import { array, func } from 'prop-types';

export class ColumnChooser extends Component {
    static propTypes = {
      onColumnsChange: func.isRequired,
      columns: array.isRequired,
    };

    changeColumns = (accessor) => {
      const columnsToShow = this.props.columns.map((column) => {
        if (column.accessor === accessor) {
          column.show = !column.show;
        }
        return column;
      });
      this.props.onColumnsChange(columnsToShow);
    };

    render() {
      return (
        <div>
          {this.props.columns.map((column, index) => (
            <div key={index} className="checkbox" style={{ marginLeft: '15px' }}>
              <label htmlFor="checkbox">
                <input type="checkbox" defaultChecked={column.show} onClick={() => this.changeColumns(column.accessor)} />
                {column.Header}
              </label>
            </div>
          ))}
        </div>
      );
    }
}

