import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { array, func } from 'prop-types';
import { Modal, Button } from 'react-bootstrap';
import { toSelectArrayFromJSON } from 'fe-shared';
import { fetchLeagues } from 'app/leagues';
import { fetchMarketTypes, marketTypesSelector } from 'app/markets';
import { NotificationManager } from 'react-notifications';
import { ContestsForm } from '../form';
import { leaguesSelector, sportTypesSelector, currenciesTypeSelector } from '../../../selectors';
import { addContest, searchContests, fetchSportTypes, fetchCurrencies } from '../../../actions';

class AddContest extends Component {
  static defaultProps = {
    leagues: [],
    currencies: [],
  };

  static propTypes = {
    fetchLeagues: func.isRequired,
    fetchMarketTypes: func.isRequired,
    fetchSportTypes: func.isRequired,
    addContest: func.isRequired,
    searchContests: func.isRequired,
    leagues: array,
    currencies: array,
    marketTypes: array.isRequired,
    sportTypes: array.isRequired,
    fetchCurrencies: func.isRequired,
  };

  state = {
    showModal: false,
  };

  componentDidMount() {
    this.props.fetchLeagues();
    this.props.fetchMarketTypes();
    this.props.fetchSportTypes();
    this.props.fetchCurrencies();
  }

  handleSubmit = (value) => {
    this.props.addContest(value)
      .then(() => {
        this.props.searchContests();
        this.showModal();
      })
      .catch((err) => {
        let error = 'Something went wrong. ';
        const data = err.response && err.response.data;
        if (data) {
          if (data.message) {
            error = data.message;
          } else {
            const { errors } = data;
            if (errors) {
              Object.keys(errors).forEach((key) => {
                error += errors[key];
              });
            }
          }
        }
        NotificationManager.error(error, 'Error', 3000);
      });
  };

  showModal = () => this.setState({ showModal: !this.state.showModal });

  render() {
    return (
      <div>
        <Button
          className="add-button"
          onClick={this.showModal}
        >
          <i className="mi mi-add" />
        </Button>

        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({ showModal: !this.state.showModal })}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Add Contest</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal && (
                <ContestsForm
                  onSubmit={this.handleSubmit}
                  leagues={toSelectArrayFromJSON(this.props.leagues, 'id', 'name')}
                  marketTypes={this.props.marketTypes}
                  sportTypes={this.props.sportTypes}
                  currencies={this.props.currencies}
                />
              )
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  leagues: leaguesSelector(state),
  marketTypes: marketTypesSelector(state),
  sportTypes: sportTypesSelector(state),
  currencies: currenciesTypeSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchLeagues,
  fetchMarketTypes,
  fetchSportTypes,
  addContest,
  searchContests,
  fetchCurrencies,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(AddContest);
