import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import propTypes from 'prop-types';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { ControlInput, ControlSelect, ControlAutoCompleteSelect, toQueryCriteria } from 'fe-shared';
import { autocompleteLeagues } from 'app/leagues';
import { searchEventGroups } from 'app/eventGroupChains';
import { SelectByStatus } from '../select-by-status';
import { SwitchContestsType } from '../switch-contest-type';
import { DEFAULT_STATUS, REGULAR_CONTEST } from '../../../consts';
import { searchEventGroupChains } from '../../../actions';
import { validate } from './validate';
import './form.scss';

export class ContestsForm extends Component {
  static defaultProps = {
    contest: null,
    periodFrom: null,
    periodTo: null,
    leagueId: null,
    contestStatusPk: null,
  };

  static propTypes = {
    handleSubmit: propTypes.func.isRequired,
    autocompleteLeagues: propTypes.func.isRequired,
    searchEventGroupChains: propTypes.func.isRequired,
    searchEventGroups: propTypes.func.isRequired,
    submitting: propTypes.any.isRequired,
    sportTypes: propTypes.array.isRequired,
    marketTypes: propTypes.array.isRequired,
    currencies: propTypes.array.isRequired,
    contestTypePk: propTypes.array.isRequired,
    prizePoolTypePk: propTypes.array.isRequired,
    contest: propTypes.object,
    periodFrom: propTypes.string,
    periodTo: propTypes.string,
    leagueId: propTypes.number,
    contestStatusPk: propTypes.string,
  };

  getIdsOfContestEntity = (entityArray, names) => {
    const listOfNames = entityArray.map(e => e.label);
    return names.map((e) => {
      const index = listOfNames.indexOf(e);
      return (index !== -1) && entityArray[index];
    });
  };

  showHouseCashPrize = (prizePoolTypePk) => {
    if (!prizePoolTypePk) {
      return 'none';
    }

    switch (prizePoolTypePk) {
      case 'Platform Guaranteed Prize  Default': return 'block';
      case 'Platform Guaranteed Prize Free to Enter': return 'block';
      case 'undefined': return 'none';
      default: return 'none';
    }
  };

  showEntryFeeAndCommission = (prizePoolTypePk) => {
    if (!prizePoolTypePk) {
      return 'none';
    }

    switch (prizePoolTypePk) {
      case 'Platform Free to Enter': return 'none';
      case 'User Free to Enter': return 'none';
      case 'Platform Guaranteed Prize Free to Enter': return 'none';
      default: return 'block';
    }
  };

  switchEventTypes = criterias =>
    ((this.props.contestTypePk !== REGULAR_CONTEST) ?
      (this.props.searchEventGroupChains({ criteria: [...criterias] })) :
      (this.props.searchEventGroups({ criteria: [...criterias] })));


  searchEventItems = ({ periodFrom, periodTo }) => {
    const creiterias = [
      toQueryCriteria('leagueId', 'EQ', this.props.leagueId.value),
      toQueryCriteria('startTimeStamp', 'RANGE', {
        from: periodFrom,
        to: periodTo,
      }),
    ];

    this.switchEventTypes(creiterias.filter(e => e.value || (e.from && e.to)));
  };

  checkForDisabled = () => this.props.contestStatusPk === 'Upcoming';

  render() {
    const {
      handleSubmit,
      submitting,
      sportTypes,
      marketTypes,
      currencies,
      contest,
      contestTypePk,
      prizePoolTypePk,
      periodFrom,
      periodTo,
      leagueId,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="name"
          placeholder="Name"
          type="text"
          component={ControlInput}
          label="Name:"
          currentValue={contest ? contest.name : ''}
          disabledField={this.checkForDisabled()}
        />
        <Field
          name="sportPk"
          placeholder="Sport"
          component={ControlSelect}
          options={sportTypes}
          label="Sport:"
          currentValue={contest ? contest.sportPk : ''}
          disabledField={this.checkForDisabled()}
        />
        <Field
          name="currencyPk"
          placeholder="Currency"
          component={ControlSelect}
          options={currencies}
          label="Currency:"
          currentValue={contest ? contest.currencyPk : ''}
          disabledField={this.checkForDisabled()}
        />
        <Field
          name="prizePoolTypePk"
          placeholder="Contest buy-in structure"
          entity="Prize Pool"
          component={SelectByStatus}
          flag="type"
          label="Contest buy-in structure:"
          currentValue={contest ? contest.prizePoolTypePk : ''}
          disabledField={this.checkForDisabled()}
        />
        <div style={{ display: this.showHouseCashPrize(prizePoolTypePk) }}>
          <Field
            name="houseCashPrize"
            placeholder="House cash prize"
            component={ControlInput}
            label="House cash prize:"
            currentValue={contest ? `${contest.houseCashPrize}` : ''}
            disabledField={this.checkForDisabled()}
          />
        </div>
        <div style={{ display: this.showEntryFeeAndCommission(prizePoolTypePk) }}>
          <Field
            name="entryFee"
            placeholder="Entry fees"
            component={ControlInput}
            label="Entry fees:"
            currentValue={contest ? `${contest.entryFee}` : ''}
            disabledField={this.checkForDisabled()}
          />
        </div>
        <div style={{ display: this.showEntryFeeAndCommission(prizePoolTypePk) }}>
          <Field
            name="commission"
            placeholder="Commission"
            component={ControlInput}
            label="Commission:"
            currentValue={contest ? `${contest.commission}` : ''}
            disabledField={this.checkForDisabled()}
          />
        </div>
        <Field
          name="payoutStructurePk"
          flag="type"
          entity="Payout Structure"
          placeholder="Payout structure"
          component={SelectByStatus}
          label="Payout structure:"
          currentValue={contest ? contest.payoutStructurePk : ''}
          disabledField={this.checkForDisabled()}
        />
        <Field
          name="maxNrOfBetAccrossAllUsers"
          placeholder="Max number of bet accross all users"
          component={ControlInput}
          label="Max Number Of Entries:"
          currentValue={contest ? `${contest.maxNrOfBetAccrossAllUsers}` : ''}
          disabledField={this.checkForDisabled()}
        />
        <Field
          multi
          joinValues
          delimiter=","
          name="marketTypeIds"
          placeholder="Market types"
          component={ControlSelect}
          options={marketTypes}
          label="Market types:"
          currentValue={contest ? this.getIdsOfContestEntity(marketTypes, contest.marketTypeNames) : ''}
          disabledField={this.checkForDisabled()}
        />
        <Field
          name="leagueId"
          placeholder="Leagues"
          component={ControlAutoCompleteSelect}
          autoCompleteAction={this.props.autocompleteLeagues}
          currentValue={contest ? contest.leagueName : ''}
          label="Leagues:"
          disabledField={this.checkForDisabled()}
        />
        <Field
          name="contestTypePk"
          flag="type"
          entity="Contest"
          placeholder="Contest type"
          component={SelectByStatus}
          label="Contest type:"
          currentValue={contest ? contest.contestTypePk : ''}
          disabledField={this.checkForDisabled()}
        />
        <SwitchContestsType
          contestTypePk={contestTypePk}
          periodFrom={periodFrom}
          periodTo={periodTo}
          leagueId={(leagueId && leagueId.value && leagueId.value) || leagueId}
          contest={contest}
          checkForDisabled={this.checkForDisabled}
          searchEventItems={this.searchEventItems}
        />
        <Field
          name="contestStatusPk"
          flag="status"
          entity="Contest"
          placeholder="Contest status"
          from={contest ? contest.contestStatusPk : DEFAULT_STATUS}
          component={SelectByStatus}
          label="Contest Status:"
          currentValue={contest ? contest.contestStatusPk : ''}
        />
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

const selector = formValueSelector('contestsForm');
const mapStateToProps = state => ({
  prizePoolTypePk: selector(state, 'prizePoolTypePk'),
  contestTypePk: selector(state, 'contestTypePk'),
  contestStatusPk: selector(state, 'contestStatusPk'),
  periodFrom: selector(state, 'from'),
  periodTo: selector(state, 'to'),
  leagueId: selector(state, 'leagueId'),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  searchEventGroupChains,
  searchEventGroups,
  autocompleteLeagues,
}, dispatch));

export default compose(
  reduxForm({
    form: 'contestsForm',
    validate,
  }),
  connect(mapStateToProps, mapDispatchToProps),
)(ContestsForm);
