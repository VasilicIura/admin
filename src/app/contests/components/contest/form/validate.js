export const validate = (values) => {
  const errors = {};
  const {
    name,
    sportPk,
    currencyPk,
    maxNrOfBetAccrossAllUsers,
    eventGroupId,
    prizePoolTypePk,
    contestTypePk,
    leagueId,
    contestStatusPk,
  } = values;

  if (!name) {
    errors.name = 'Name field is required';
  }

  if (!sportPk) {
    errors.sportPk = 'This field can be empty';
  }

  if (!currencyPk) {
    errors.currencyPk = 'This field can be empty';
  }

  if (!maxNrOfBetAccrossAllUsers) {
    errors.maxNrOfBetAccrossAllUsers = 'This field is required';
  }

  if (!eventGroupId) {
    errors.eventGroupId = 'This field is required';
  }

  if (!prizePoolTypePk) {
    errors.prizePoolTypePk = 'This field is required';
  }

  if (!leagueId) {
    errors.leagueId = 'This field is required';
  }

  if (!contestTypePk) {
    errors.contestTypePk = 'This field is required';
  }

  if (!contestStatusPk) {
    errors.contestStatusPk = 'This field is required';
  }

  return errors;
};

