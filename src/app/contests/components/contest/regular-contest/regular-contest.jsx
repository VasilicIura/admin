import React, { Component } from 'react';
import { connect } from 'react-redux';
import { func, any, array, number } from 'prop-types';
import { toSelectArrayFromJSON, ControlSelect, toQueryCriteria } from 'fe-shared';
import { bindActionCreators } from 'redux';
import { searchEventGroups, eventGroupsSelector } from 'app/eventGroupChains';
import { getEventGroupById } from '../../../actions';
import { eventGroupById } from '../../../selectors';

class RegularContest extends Component {
  static defaultProps = {
    name: null,
    value: null,
    eventGroups: [],
    leagueId: null,
  };

  static propTypes = {
    fetchEventGroups: func.isRequired,
    onHandleSelectChange: func.isRequired,
    searchEventGroups: func.isRequired,
    name: any,
    value: any,
    leagueId: number,
    eventGroups: array,
  };

  componentDidMount = () => {
    if (this.props.leagueId) {
      this.fetchOptions(this.props.leagueId);
    }
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.leagueId !== this.props.leagueId) {
      this.fetchOptions(nextProps.leagueId);
    }
  };

  fetchOptions = (leagueId) => {
    const model = toQueryCriteria('leagueId', 'EQ', leagueId);
    this.props.searchEventGroups({ criteria: [model] });
  };

  render = () => {
    const options = this.props.eventGroups.length &&
      toSelectArrayFromJSON(this.props.eventGroups, 'id', 'name');

    return (
      <ControlSelect
        options={options}
        {...this.props}
      />
    );
  };
}

const mapStateToProps = state => ({
  eventGroups: eventGroupsSelector(state),
  eventGroup: eventGroupById(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  searchEventGroups,
  getEventGroupById,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(RegularContest);
