import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { array, func, object } from 'prop-types';
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { toSelectArrayFromJSON } from 'fe-shared';
import { fetchLeagues } from 'app/leagues';
import { fetchMarketTypes, marketTypesSelector } from 'app/markets';
import { NotificationManager } from 'react-notifications';
import { ContestsForm } from '../form';
import { leaguesSelector, sportTypesSelector, currenciesTypeSelector } from '../../../selectors';
import { editContest, searchContests, fetchSportTypes, fetchCurrencies } from '../../../actions';

export class EditContest extends Component {
  static defaultProps = {
    leagues: [],
    currencies: [],
    contest: null,
  };

  static propTypes = {
    fetchLeagues: func.isRequired,
    fetchMarketTypes: func.isRequired,
    fetchSportTypes: func.isRequired,
    editContest: func.isRequired,
    searchContests: func.isRequired,
    leagues: array,
    contest: object,
    currencies: array,
    marketTypes: array.isRequired,
    sportTypes: array.isRequired,
    fetchCurrencies: func.isRequired,
  };

  state = {
    showModal: false,
  };

  toggleModal = () => {
    if (!this.state.showModal) {
      this.props.fetchLeagues();
      this.props.fetchMarketTypes();
      this.props.fetchSportTypes();
      this.props.fetchCurrencies();
    }
    this.setState({ showModal: !this.state.showModal });
  };
  handleSubmit = (value) => {
    this.props.editContest(Object.assign({ id: this.props.contest.id }, value))
      .then(() => {
        this.props.searchContests();
        this.toggleModal();
        NotificationManager.success('Contest updated');
      })
      .catch((err) => {
        let error = 'Something went wrong. ';
        const data = err.response && err.response.data;
        if (data) {
          if (data.message) {
            error = data.message;
          } else {
            const { errors } = data;
            if (errors) {
              Object.keys(errors).forEach((key) => {
                error += errors[key];
              });
            }
          }
        }
        NotificationManager.error(error, 'Error', 3000);
      });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Edit Contest</span>
      </Tooltip>
    );
    return (
      <div style={{ display: 'inline', float: 'left', marginRight: '15px' }}>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.toggleModal}
          >
            <i className="mi mi-edit" />
          </Button>
        </OverlayTrigger>

        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({ showModal: !this.state.showModal })}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Edit Contest</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal && this.props.contest && (
                <ContestsForm
                  onSubmit={this.handleSubmit}
                  leagues={toSelectArrayFromJSON(this.props.leagues, 'id', 'name')}
                  marketTypes={this.props.marketTypes}
                  sportTypes={this.props.sportTypes}
                  currencies={this.props.currencies}
                  contest={this.props.contest}
                />
              )
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  leagues: leaguesSelector(state),
  marketTypes: marketTypesSelector(state),
  sportTypes: sportTypesSelector(state),
  currencies: currenciesTypeSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchLeagues,
  fetchMarketTypes,
  fetchSportTypes,
  editContest,
  searchContests,
  fetchCurrencies,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(EditContest);
