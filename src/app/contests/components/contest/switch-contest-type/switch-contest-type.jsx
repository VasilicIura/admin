import React from 'react';
import propTypes from 'prop-types';
import { Field } from 'redux-form';
import { EventFilters } from '../eventFilters';
import { RecuringContests } from '../recuring-contests';
import { RegularContest } from '../regular-contest';
import { REGULAR_CONTEST } from '../../../consts';

const SwitchContestsType = (props) => {
  if (props.contestTypePk && (props.contestTypePk === REGULAR_CONTEST)) {
    return (
      <div>
        <div className="select-pull-left">
          <Field
            name="eventGroupId"
            placeholder="Contest event groups"
            component={RegularContest}
            leagueId={props.leagueId}
            label="Contest event groups:"
            currentValue={props.contest ? +props.contest.eventGroupId : ''}
            disabledField={props.checkForDisabled()}
          />
        </div>
        <div className="filter-container">
          <EventFilters
            periodFrom={props.periodFrom}
            periodTo={props.periodTo}
            searchEventItems={props.searchEventItems}
          />
        </div>
      </div>
    );
  } else if (props.contestTypePk && (props.contestTypePk !== REGULAR_CONTEST)) {
    return (
      <div>
        <div className="select-pull-left">
          <Field
            leagueId={props.leagueId}
            name="eventGroupChainId"
            placeholder="Contest event group chains"
            component={RecuringContests}
            label="Contest event group chains:"
            currentValue={props.contest ? props.contest.eventGroupChainId : ''}
            disabledField={props.checkForDisabled()}
          />
        </div>
        <div className="filter-container">
          <EventFilters
            periodFrom={props.periodFrom}
            periodTo={props.periodTo}
            searchEventItems={props.searchEventItems}
            disabledField={props.checkForDisabled()}
          />
        </div>
      </div>
    );
  }

  return null;
};

SwitchContestsType.defaultProps = {
  contestTypePk: null,
  contest: null,
  leagueId: null,
  periodFrom: null,
  periodTo: null,
};

SwitchContestsType.propTypes = {
  contestTypePk: propTypes.string,
  contest: propTypes.object,
  leagueId: propTypes.number,
  searchEventItems: propTypes.func.isRequired,
  periodFrom: propTypes.string,
  periodTo: propTypes.string,
  checkForDisabled: propTypes.func.isRequired,
};

export default SwitchContestsType;
