import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { string, func, any } from 'prop-types';
import { ControlSelect, toSelectArray } from 'fe-shared';
import { fetchAvailableStatuses } from 'app/eventGroupChains';
import { fetchEntityTypes, fetchEntityStatuses } from '../../../actions';

class SelectByStatus extends Component {
  static defaultProps = {
    flag: 'type',
    value: null,
    label: '',
    entity: '',
    name: '',
    from: '',
    multi: false,
  };

  static propTypes = {
    flag: string,
    entity: string,
    value: string,
    label: string,
    name: string,
    from: string,
    fetchEntityStatuses: func.isRequired,
    fetchEntityTypes: func.isRequired,
    onSelectChange: func.isRequired,
    multi: any,
    fetchAvailableStatuses: func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      options: [],
    };
  }

  componentDidMount() {
    const { entity, from } = this.props;
    if ((this.props.flag === 'status') && (from === '')) {
      this.props.fetchEntityStatuses(entity)
        .then(this.setOptions);
    } else if ((this.props.flag === 'status') && (from !== '')) {
      this.props.fetchAvailableStatuses(from, entity)
        .then(({ value }) => {
          value.unshift(from);
          this.setOptions({ value });
        });
    } else {
      this.props.fetchEntityTypes(entity)
        .then(this.setOptions);
    }
  }

  setOptions = ({ value }) => {
    this.setState({ options: toSelectArray(value) });
  };

  render = () => <ControlSelect options={this.state.options} {...this.props} />;
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchEntityTypes,
  fetchEntityStatuses,
  fetchAvailableStatuses,
}, dispatch));

export default connect(null, mapDispatchToProps)(SelectByStatus);
