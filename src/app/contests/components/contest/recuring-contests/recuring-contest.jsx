import React, { Component } from 'react';
import { connect } from 'react-redux';
import { func, any, array, number } from 'prop-types';
import { toSelectArrayFromJSON, ControlSelect, toQueryCriteria } from 'fe-shared';
import { bindActionCreators } from 'redux';
import { searchEventGroupChains, getEventGroupChainById } from '../../../actions';
import { eventGorupChainsSelector, eventGroupChainById } from '../../../selectors';

export class RecuringContests extends Component {
  static defaultProps = {
    name: null,
    value: null,
    eventGroupChains: [],
  };

  static propTypes = {
    onHandleSelectChange: func.isRequired,
    searchEventGroupChains: func.isRequired,
    eventGroupChains: array,
    name: any,
    value: any,
    leagueId: number.isRequired,
  };

  componentDidMount = () => {
    if (this.props.leagueId) {
      this.fetchOptions(this.props.leagueId);
    }
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.leagueId !== this.props.leagueId) {
      this.fetchOptions(nextProps.leagueId);
    }
  };

  fetchOptions = (leagueId) => {
    const model = toQueryCriteria('leagueId', 'EQ', leagueId);
    this.props.searchEventGroupChains({ criteria: [model] });
  };

  render = () => {
    const options = this.props.eventGroupChains.length &&
      toSelectArrayFromJSON(this.props.eventGroupChains, 'id', 'name');

    return (<ControlSelect options={options} {...this.props} />);
  };
}

const mapStateToProps = state => ({
  eventGroupChains: eventGorupChainsSelector(state),
  eventGroupChain: eventGroupChainById(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  getEventGroupChainById,
  searchEventGroupChains,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(RecuringContests);
