import React from 'react';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { object } from 'prop-types';
import DetailContestsTemplate from './detail-template';

export class DetailContests extends React.Component {
  static propTypes = {
    contest: object.isRequired,
  };

  state = {
    showModal: false,
  };

  showModal = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>View Contest</span>
      </Tooltip>
    );
    return (
      <div className="contest-detail-view">
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.showModal}
          >
            <i className="mi mi-visibility" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({ showModal: !this.state.showModal })}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">View Contest</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <DetailContestsTemplate contest={this.props.contest} />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default DetailContests;
