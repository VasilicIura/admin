import React, { PureComponent } from 'react';
import { object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ConfirmationComponent } from 'fe-shared';
import { Modal, ListGroup, ListGroupItem, Col, Row, Button } from 'react-bootstrap';
import { cancelContest, searchContests } from '../../../actions';

class DetailContestsTemplate extends PureComponent {
  static propTypes = {
    cancelContest: func.isRequired,
    searchContests: func.isRequired,
    contest: object.isRequired,
  };

  state = {
    showConfirmation: false,
    errors: null,
  };

  isConfirmed = (value) => {
    if (value) {
      this.props.cancelContest(this.props.contest.id)
        .then(() => {
          this.renderConfirmation();
          this.props.searchContests();
        })
        .catch(err => this.setState({ errors: err.response.data.message }));
    }
  };

  renderConfirmation = () => {
    this.setState({ showConfirmation: !this.state.showConfirmation });
  };

  render = () => {
    const { contest } = this.props;

    return (
      <Modal.Body>
        <div className="detail-contests-template">
          <ListGroup>
            <strong>Name:</strong> {contest.name}
          </ListGroup>
          <ListGroup>
            <strong>Sport:</strong> {contest.sportPk}
          </ListGroup>
          <ListGroup>
            <strong>Currency:</strong> {contest.currencyPk}
          </ListGroup>

          <ListGroup>
            <strong>Contest Status:</strong>
            <span className="status-label label label-default">{contest.contestStatusPk}</span>
          </ListGroup>

          <ListGroup>
            <strong>Entry fees:</strong> {contest.entryFee}
          </ListGroup>

          <ListGroup>
            <strong>Commission:</strong> {contest.commission}
          </ListGroup>

          <ListGroup>
            <strong>Payout structure:</strong> {contest.payoutStructureActualPk}
          </ListGroup>

          <ListGroup>
            <strong>Max Number Of Entries:</strong> {contest.maxNrOfBetAccrossAllUsers}
          </ListGroup>

          <ListGroup>
            <strong>Market types:</strong> {contest.marketTypeNames}
          </ListGroup>

          <h3>Market types:</h3>
          <ListGroup>
            {
              contest.marketTypeNames
                .map(e => (<ListGroupItem key={e.id}>{e}</ListGroupItem>))
            }
          </ListGroup>

          <h3>Leagues:</h3>
          <ListGroup>
            {
              contest.leagueName ?
                (<ListGroupItem >{contest.leagueName}</ListGroupItem>) :
                'No leagues assigned'
            }
          </ListGroup>

          <Row>
            <Col md="12">
              <div className="submit-modal-btn">
                <Button bsStyle="danger" onClick={this.renderConfirmation}>Cancel Contest</Button>
              </div>
            </Col>
          </Row>
        </div>
        {
          this.state.showConfirmation && (
            <ConfirmationComponent
              isConfirmedAction={this.isConfirmed}
              showConfirmation={this.state.showConfirmation}
              toggleConfirmation={this.renderConfirmation}
              errors={this.state.errors}
            />
          )
        }
      </Modal.Body>
    );
  };
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  cancelContest,
  searchContests,
}, dispatch));

export default connect(null, mapDispatchToProps)(DetailContestsTemplate);
