import React, { Component } from 'react';
import { func, string } from 'prop-types';
import { Modal, Button, Col, Row } from 'react-bootstrap';
import { Field } from 'redux-form';
import { ControlDatePicker } from 'fe-shared';

class EventFilters extends Component {
  static defaultProps = {
    periodFrom: null,
    periodTo: null,
  };

  static propTypes = {
    searchEventItems: func.isRequired,
    periodFrom: string,
    periodTo: string,
  };

  state = {
    openModal: false,
  };

  toggleFilter = () => this.setState({ openModal: !this.state.openModal });

  filterEventGroups = () => {
    const { periodFrom, periodTo } = this.props;

    this.props.searchEventItems({
      periodFrom,
      periodTo,
    });

    this.toggleFilter();
  };

  render = () =>
    (
      <div>
        <Button
          className="events-filter"
          onClick={this.toggleFilter}
        >
          <i className="mi mi-filter-list" />
        </Button>

        <Modal show={this.state.openModal} onHide={this.toggleFilter}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Filter event by selected period</Modal.Title>
          </Modal.Header>
          { this.state.openModal &&
            (
              <Modal.Body>
                <Row>
                  <Col md={12} >
                    <Col md={6}>
                      <Field
                        name="from"
                        placeholder="Chose start period"
                        component={ControlDatePicker}
                        from
                        label="Choose start period"
                      />
                    </Col>
                    <Col md={6}>
                      <Field
                        name="to"
                        placeholder="Choose end period"
                        component={ControlDatePicker}
                        to
                        label="Choose end period"
                      />
                    </Col>
                    <div className="submit-modal-btn">
                      <Button
                        type="button"
                        bsStyle="success"
                        onClick={this.filterEventGroups}
                      >
                        Filter
                      </Button>
                    </div>
                  </Col>
                </Row>
              </Modal.Body>
            )
          }
        </Modal>
      </div>
    );
}

export default EventFilters;
