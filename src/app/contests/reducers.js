import { handleActions } from 'redux-actions';
import { Map, List } from 'immutable';
import { CONTEST_ACTIONS, FILTER_ENTITY_ACTIONS } from './consts';

const initialState = Map({
  contests: List([]),
  entity_statuses: List([]),
  entity_types: List([]),
  sport_types: List([]),
  currencies: List([]),
  ranges: List([]),
  eventGroupChains: List([]),
  eventGroupChain: Map({}),
  eventGroup: Map({}),
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  filteredContests: Map({}),
});

export const reducer = handleActions({
  [`${CONTEST_ACTIONS.FETCH_CONTESTS}_FULFILLED`]:
    (state, action) => state.set('contests', List(action.payload.content)),
  [`${FILTER_ENTITY_ACTIONS.FETCH_ENTITY_STATUSES}_FULFILLED`]:
    (state, action) => state.set('entity_statuses', List(action.payload)),
  [`${FILTER_ENTITY_ACTIONS.FETCH_ENTITY_TYPES}_FULFILLED`]:
    (state, action) => state.set('entity_types', List(action.payload)),
  [`${FILTER_ENTITY_ACTIONS.FETCH_SPORT_TYPES}_FULFILLED`]:
    (state, action) => state.set('sport_types', List(action.payload)),
  [`${FILTER_ENTITY_ACTIONS.FETCH_CURRENCIES}_FULFILLED`]:
    (state, action) => state.set('currencies', List(action.payload)),
  [`${CONTEST_ACTIONS.SEARCH_EVENT_GROUP_CHAINS}_FULFILLED`]:
    (state, action) => state.set('eventGroupChains', List(action.payload.content)),
  [`${CONTEST_ACTIONS.FETCH_EVENT_GROUP_CHAIN_BY_ID}_FULFILLED`]:
    (state, action) => state.set('eventGroupChain', Map(action.payload)),
  [`${CONTEST_ACTIONS.CONTEST_FETCH_EVENT_GROUP_BY_ID}_FULFILLED`]:
    (state, action) => state.set('eventGroup', Map(action.payload)),
  [`${CONTEST_ACTIONS.SEARCH_CONTESTS}_FULFILLED`]:
    (state, action) => state.set('filteredContests', Map(action.payload)),
  [CONTEST_ACTIONS.CHANGE_CONTESTS_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [CONTEST_ACTIONS.SET_CONTESTS_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
  [CONTEST_ACTIONS.CHANGE_CONTESTS_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
}, initialState);
