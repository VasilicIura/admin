import { createAction } from 'redux-actions';
import { filterSelector, queryFilters } from 'fe-shared';
import { CONTEST_ACTIONS, FILTER_ENTITY_ACTIONS, REGULAR_CONTEST, FILTER_TYPES } from './consts';
import { filterCriteriasSelector } from './selectors';
import { tradingApi, commonApi, bettingApi } from '../api';

const cancelContest = createAction(
  CONTEST_ACTIONS.CANCEL_CONTEST,
  contestId => bettingApi.post(`/contest/cancel/${contestId}`, {}),
);

const fetchContests = createAction(
  CONTEST_ACTIONS.FETCH_CONTESTS,
  (criteria = []) => tradingApi.post('contests/search', { criteria }),
);

const addContest = createAction(
  CONTEST_ACTIONS.ADD_CONTEST,
  ({
    marketTypeIds,
    leagueId,
    entryFee,
    sportPk,
    currencyPk,
    ...contest
  }) => {
    const { contestTypePk } = contest;
    if (contestTypePk === REGULAR_CONTEST) {
      delete contest.eventGroupChainId;
    } else {
      delete contest.eventGroupId;
    }

    return tradingApi.post('contests', {
      ...contest,
      sportPk: sportPk.value ? sportPk.value : sportPk,
      currencyPk: currencyPk.value ? currencyPk.value : currencyPk,
      marketTypeIds: Array.isArray(marketTypeIds) ?
        marketTypeIds.map(e => e.value) : [marketTypeIds.value],
      leagueIds: [leagueId.value || leagueId],
      entryFees: entryFee ? entryFee.split(',') : [0],
      commission: contest.commission || 0,
    });
  },
);

const editContest = createAction(
  CONTEST_ACTIONS.ADD_CONTEST,
  ({
    id,
    marketTypeIds,
    leagueId,
    entryFees,
    sportPk,
    currencyPk,
    ...contest
  }) => {
    const { contestTypePk } = contest;
    if (contestTypePk === REGULAR_CONTEST) {
      delete contest.eventGroupChainId;
    } else {
      delete contest.eventGroupId;
    }

    return tradingApi.patch(`contests/${id}`, {
      ...contest,
      id,
      leagueId: leagueId.value || leagueId,
      sportPk: sportPk.value ? sportPk.value : sportPk,
      currencyPk: currencyPk.value ? currencyPk.value : currencyPk,
      marketTypeIds: Array.isArray(marketTypeIds) ?
        marketTypeIds.map(e => e.value) : [marketTypeIds.value],
    });
  },
);

const fetchSportTypes = createAction(
  FILTER_ENTITY_ACTIONS.FETCH_SPORT_TYPES,
  () => commonApi.get('enums/sports'),
);

const fetchCurrencies = createAction(
  FILTER_ENTITY_ACTIONS.FETCH_CURRENCIES,
  () => commonApi.get('enums/currencies'),
);

const fetchRangeValues = createAction(
  FILTER_ENTITY_ACTIONS.FETCH_RANGE_VALUES,
  (domain, groupName) => commonApi.get(`config/domain/${domain}/groupname/${groupName}`),
);

const fetchEntityStatuses = createAction(
  FILTER_ENTITY_ACTIONS.FETCH_ENTITY_STATUSES,
  entity => commonApi.get(`enums/statuses/entity/${entity}`),
);

const fetchEntityTypes = createAction(
  FILTER_ENTITY_ACTIONS.FETCH_ENTITY_TYPES,
  entity => commonApi.get(`enums/types/entity/${entity}`),
);

const searchEventGroupChains = createAction(
  CONTEST_ACTIONS.SEARCH_EVENT_GROUP_CHAINS,
  (criteria = []) => tradingApi.post('event-group-chains/search', {
    ...criteria,
    pageable: {
      page: 0,
      size: 100,
      orders: [{
        field: 'startTimeStamp',
        direction: 'DESC',
      }],
    },
  }),
);

const getEventGroupChainById = createAction(
  CONTEST_ACTIONS.FETCH_EVENT_GROUP_CHAIN_BY_ID,
  id => tradingApi.get(`event-group-chains/${id}`),
);

const getEventGroupById = createAction(
  CONTEST_ACTIONS.CONTEST_FETCH_EVENT_GROUP_BY_ID,
  id => tradingApi.get(`event-groups/${id}`),
);

const filterContests = createAction(
  CONTEST_ACTIONS.SEARCH_CONTESTS,
  (criteria = []) => tradingApi.post('contests/search', { ...criteria }),
);

const changePagination = createAction(
  CONTEST_ACTIONS.CHANGE_CONTESTS_PAGINATION,
  (pagination = {}) => pagination,
);

const setFilterCriterias = createAction(
  CONTEST_ACTIONS.SET_CONTESTS_FILTERS,
  values => values,
);

const changeSorting = createAction(
  CONTEST_ACTIONS.CHANGE_CONTESTS_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

const autocompleteContests = createAction(
  CONTEST_ACTIONS.AUTOCOMPLETE_CONTESTS,
  (criteria = []) => tradingApi.post('contests/search', { ...criteria }),
);

const searchContests = () =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'contestFilters' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    dispatch(filterContests(filterCriteriasSelector(newState)));
  };

export {
  editContest,
  fetchContests,
  fetchEntityStatuses,
  fetchEntityTypes,
  addContest,
  fetchSportTypes,
  fetchCurrencies,
  fetchRangeValues,
  searchEventGroupChains,
  getEventGroupChainById,
  getEventGroupById,
  filterContests,
  changePagination,
  setFilterCriterias,
  changeSorting,
  searchContests,
  autocompleteContests,
  cancelContest,
};
