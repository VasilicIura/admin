import React, { Component } from 'react';
import { func } from 'prop-types';
import ReactTable from 'react-table';
import { PageSize } from 'fe-shared';
import { Pagination } from 'react-bootstrap';

class Table extends Component {
  static propTypes = {
    fetchData: func.isRequired,
    changePagination: func.isRequired,
    changeSorting: func.isRequired,
  };

  state = {
    activePage: 0,
    pageSize: 10,
    sorted: [],
  };

  getData = (
    activePage = this.state.activePage,
    pageSize = this.state.pageSize,
    sorted = this.state.sorted,
  ) => {
    if (sorted) {
      this.setState({ activePage, pageSize, sorted });
    }
    this.props.changePagination({
      page: activePage,
      size: pageSize,
    });
    if (sorted.length) {
      this.props.changeSorting(sorted);
    }
    this.props.fetchData();
  };

  renderTable(
    columns,
    data,
    loading,
    switchFilters,
    tableName,
  ) {
    return (
      <div>
        <h3 style={{ float: 'left' }}>{tableName}</h3>
        <div>
          <PageSize
            onChangePageSizeSelect={({ value }) => this.getData(this.state.activePage, value)}
            value={this.state.pageSize}
          />
        </div>
        <ReactTable
          columns={columns}
          manual
          data={data.content}
          pages={data.totalPages}
          loading={loading}
          filterable
          FilterComponent={switchFilters}
          minRows={3}
          defaultPageSize={10}
          pageSizeOptions={[this.state.pageSize]}
          pageSize={this.state.pageSize}
          showPagination={false}
          onFetchData={state => this.getData(state.page, state.pageSize, state.sorted)}
        />
        <Pagination
          bsSize="medium"
          maxButtons={20}
          items={data.totalPages}
          activePage={data.number + 1}
          onSelect={value => this.getData(value - 1)}
          ellipsis
          prev
          next
        />
      </div>
    );
  }
}

export default Table;
