import React from 'react';
import moment from 'moment';
import ReactTable from 'react-table';
import { Pagination } from 'react-bootstrap';

const CustomCell = (props) => {
  switch (props.column.id) {
    case 'amount':
      return <div><b>{props.value}</b> {props.original.currency}</div>;
    case 'timeStamp':
      return <div>{props.value && moment(props.value).format('lll')}</div>;
    default:
      return <div>{props.value}</div>;
  }
};

const generateTableCell = grid => <CustomCell {...grid} />;

const columns = [
  {
    Header: 'Amount',
    accessor: 'amount',
    sortable: false,
    Cell: row => generateTableCell(row),
  },
  {
    Header: 'Date',
    accessor: 'timeStamp',
    sortable: false,
    Cell: row => generateTableCell(row),
  },
];

class BalanceRecordsTable extends React.Component {
  render() {
    const { data } = this.props;

    return (
      <div className="gridContainer">
        <ReactTable
          columns={columns}
          manual
          data={data.content}
          pages={data.totalPages}
          defaultPageSize={10}
          showPagination={false}
          onFetchData={(state) => {
            this.props.onPageChange(state.page, state.pageSize);
          }}
        />
        <Pagination
          bsSize="medium"
          maxButtons={20}
          items={data.totalPages}
          activePage={data.number + 1}
          onSelect={value => this.props.onPageChange(value - 1)}
          ellipsis
          prev
          next
        />
      </div>
    );
  }
}

export default BalanceRecordsTable;
