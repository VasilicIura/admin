import React, { Component } from 'react';
import ReactQuill from 'react-quill';
import { Button } from 'react-bootstrap';
import { PropTypes } from 'prop-types';
import { EDITOR_MODULES, EDITOR_FORMATS } from './../../../static-pages/components/page-editor/consts';
import './page-editor.scss';

class PageEditor extends Component {
  static propTypes = {
    content: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    subject: PropTypes.string.isRequired,
    variables: PropTypes.object.isRequired,
    onSave: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      subject: props.subject || '',
      content: props.content || '',
      variables: props.variables || '',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.content !== this.state.content) {
      this.setState({ content: nextProps.content });
    }
    if (nextProps.subject !== this.state.subject) {
      this.setState({ subject: nextProps.subject });
    }
    if (nextProps.variables !== this.state.variables) {
      this.setState({ variables: nextProps.variables });
    }
  }

  handleChangeSubject = (event) => {
    this.setState({ subject: event.target.value });
  };

  handleChangeContent = (value) => {
    this.setState({ content: value });
  };

  render() {
    const variableItems = Object.keys(this.state.variables);
    return (
      <div className="page-editor">
        <h1>{this.props.title}</h1>

        <input
          type="text"
          style={{ marginBottom: '15px' }}
          className="form-control"
          placeholder="Subject"
          value={this.state.subject}
          onChange={this.handleChangeSubject}
        />

        <ReactQuill
          className="bg-white"
          placeholder="Content"
          value={this.state.content}
          onChange={this.handleChangeContent}
          modules={EDITOR_MODULES}
          formats={EDITOR_FORMATS}
        />

        <code className="preview-code">
          <ul>
            {
              variableItems.map(item =>
                (<li>{item}: {this.state.variables[item]}</li>))
            }
          </ul>
        </code>

        <Button
          onClick={() => this.props.onSave(this.state.subject, this.state.content)}
        >
          Save
        </Button>
      </div>
    );
  }
}

export default PageEditor;
