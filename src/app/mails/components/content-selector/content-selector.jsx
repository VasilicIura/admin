import React from 'react';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import { string } from 'prop-types';

const ContentSelector = (props) => {
  const keys = Object.keys(props.data).map(key => (
    <MenuItem
      eventKey={key}
      key={key}
      active={key === props.selectedKey}
      onSelect={props.onSelect}
    >
      {key}
    </MenuItem>
  ));

  return (
    <DropdownButton
      bsStyle="primary"
      title={props.selectedKey ? props.selectedKey : 'Select mail to edit'}
      id="mail-select"
    >
      {keys}
    </DropdownButton>
  );
};

ContentSelector.propTypes = {
  selectedKey: string,
};

ContentSelector.defaultProps = {
  selectedKey: null,
};


export default ContentSelector;
