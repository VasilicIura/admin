import { Map } from 'immutable';
import { handleActions } from 'redux-actions';
import { MAIL_ACTIONS } from './consts';

const initialState = Map({
  mailVariables: Map({}),
  mailTemplate: Map({}),
});

export const reducer = handleActions({
  [`${MAIL_ACTIONS.FETCH_EMAILS_VARIABLES}_FULFILLED`]: (state, action) =>
    state.set('mailVariables', Map(action.payload)),
  [`${MAIL_ACTIONS.FETCH_MAIL_TEMPLATE}_FULFILLED`]: (state, action) =>
    state.set('mailTemplate', Map(action.payload)),
}, initialState);
