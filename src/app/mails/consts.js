import keyMirror from 'keymirror';

export const MAIL_ACTIONS = keyMirror({
  FETCH_EMAILS_VARIABLES: null,
  FETCH_MAIL_TEMPLATE: null,
  FETCH_CONTENT: null,
  SAVE_PAGE: null,
});
