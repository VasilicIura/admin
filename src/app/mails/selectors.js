import { createSelector } from 'reselect';

const mailsStateSelector = state => state.mail;

const mailTemplateVariables = createSelector(
  mailsStateSelector,
  mails => mails.get('mailVariables').toJS(),
);

const mailTemplateSelector = createSelector(
  mailsStateSelector,
  mails => mails.get('mailTemplate').toJS(),
);

export {
  mailTemplateVariables,
  mailTemplateSelector,
};

