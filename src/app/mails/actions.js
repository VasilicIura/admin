import { createAction } from 'redux-actions';
import { commonApi } from '../api';
import { MAIL_ACTIONS } from './consts';


const fetchMails = createAction(
  MAIL_ACTIONS.FETCH_EMAILS_VARIABLES,
  (domain, groupname) => commonApi.get(`/config/domain/${domain}/groupname/${groupname}`),
);

const fetchMailTemplate = createAction(
  MAIL_ACTIONS.FETCH_MAIL_TEMPLATE,
  (domain, groupname) => commonApi.get(`/config/domain/${domain}/groupname/${groupname}`),
);

const savePageContent = createAction(
  MAIL_ACTIONS.SAVE_PAGE,
  (domain, groupname, text) => commonApi.patch('config/', {
    domain,
    groupname,
    values: [JSON.stringify(text)],
  }),
);

export {
  fetchMails,
  fetchMailTemplate,
  savePageContent,
};
