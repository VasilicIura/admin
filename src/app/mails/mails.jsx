import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { object, func } from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { NotificationManager } from 'react-notifications';
import { mailTemplateVariables, mailTemplateSelector } from './selectors';
import { fetchMails, fetchMailTemplate, savePageContent } from './actions';
import { ContentSelector, PageEditor } from './components';

class Mails extends Component {
  static propTypes = {
    fetchMails: func.isRequired,
    savePageContent: func.isRequired,
    mailVariables: object.isRequired,
    mailTemplate: object.isRequired,
    fetchMailTemplate: object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      subject: null,
      content: null,
      selectedKey: null,
      variables: {},
    };
  }

  componentDidMount() {
    this.props.fetchMails('Mail templates', 'Variable fields');
  }

  getContentByKey = (key) => {
    this.setState({ selectedKey: key });
    this.props.fetchMailTemplate('Mail templates', key).then(
      () => {
        const data = JSON.parse(this.props.mailTemplate.values);
        const { mailVariables: { values } } = this.props;
        const str = values && values.length && values[0];
        const arr = JSON.parse(str);
        const variables = arr[key];
        this.setState({ variables });
        this.setState({
          content: data.content,
          subject: data.subject,
        });
      },
    );
  };

  save = (subject, content) => {
    this.props.savePageContent('Mail templates', this.state.selectedKey, { subject, content })
      .then(() => NotificationManager.success('Content saved.', 'Done.', 3000))
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render() {
    const { mailVariables: { values } } = this.props;
    const str = values && values.length && values[0];
    const props = str ? JSON.parse(str) : {};
    return (
      <Row>
        <Col md={12}>
          <h3>Mails page</h3>
          <ContentSelector
            onSelect={this.getContentByKey}
            selectedKey={this.state.selectedKey}
            data={props}
          />

          {this.state.selectedKey && this.state.content
          && this.state.subject && this.state.variables &&
            <PageEditor
              subject={this.state.subject}
              content={this.state.content}
              variables={this.state.variables}
              title={this.state.selectedKey}
              onSave={this.save}
            />
          }
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  mailVariables: mailTemplateVariables(state),
  mailTemplate: mailTemplateSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchMails,
  fetchMailTemplate,
  savePageContent,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(Mails);
