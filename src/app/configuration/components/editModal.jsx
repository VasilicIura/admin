/* eslint-disable react/no-typos */
import React from 'react';
import { Modal } from 'react-bootstrap';
import propTypes from 'prop-types';
import { EditForm } from './editForm';

export const EditModal = props => (
  <Modal show={props.show} onHide={props.onClose} className="betduel-modal no-side-padding">
    <Modal.Header closeButton>
      <Modal.Title>{props.config && props.config.groupname}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <EditForm onSubmit={props.onSubmit} initialValues={props.config} />
    </Modal.Body>
  </Modal>
);

EditModal.defaultProps = {
  config: null,
  groupname: null,
};

EditModal.propTypes = {
  onSubmit: propTypes.func.isRequired,
  onClose: propTypes.func.isRequired,
  config: propTypes.any,
  groupname: propTypes.any,
  show: propTypes.any.isRequired,
};
