import React from 'react';
import propTypes from 'prop-types';
import { Panel, Glyphicon, Button } from 'react-bootstrap';

export const Group = props => (
  <Panel header={props.group.groupname} key={1} collapsible>
    {props.group.restartRequired &&
      <div style={{ color: '#e4564e' }}><Glyphicon glyph="warning-sign" /> Restart Required</div>
    }

    <div><b>Values:</b></div>
    <div>
      <ul>
        {props.group.values.map(value => <li key={value}>{value}</li>)}
      </ul>
    </div>
    <Button bsStyle="success" onClick={() => props.onEdit(props.group)}><Glyphicon glyph="pencil" /> Edit</Button>
  </Panel>
);

Group.propTypes = {
  group: propTypes.object.isRequired,
  onEdit: propTypes.func.isRequired,
};
