import React from 'react';
import propTypes from 'prop-types';
import { Panel } from 'react-bootstrap';
import { Group } from './group';

export const Domain = props => (
  <Panel header={props.domain} key={1} collapsible>
    {props.values.map((group, index) => (
      <Group group={group} onEdit={props.onEdit} key={index} />
    ))}
  </Panel>
);

Domain.defaultProps = {
  values: null,
  domain: null,
};

Domain.propTypes = {
  onEdit: propTypes.func.isRequired,
  values: propTypes.any,
  domain: propTypes.any,
};
