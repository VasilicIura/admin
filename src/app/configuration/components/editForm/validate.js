export const validate = (data) => {
  const errors = {};

  if (!data.values || !data.values.length) {
    errors.values = { _error: 'At least on value is required.' };
  } else {
    const valuesErrors = [];
    data.values.forEach((value, index) => {
      if (!value) valuesErrors[index] = 'Required.';
    });

    if (valuesErrors.length) errors.values = valuesErrors;
  }

  return errors;
};
