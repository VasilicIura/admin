/* eslint-disable react/no-typos */
import React from 'react';
import { FieldArray, Field, reduxForm } from 'redux-form';
import { Form, Button, FormGroup, FormControl, HelpBlock, Alert } from 'react-bootstrap';
import propTypes from 'prop-types';
import { validate } from './validate';

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error },
}) => (
  <div>
    <FormGroup validationState={(touched && (error ? 'error' : 'success')) || null}>
      <FormControl {...input} type={type} placeholder={label} />
      <HelpBlock>
        {touched && error && error}
      </HelpBlock>
    </FormGroup>
  </div>
);

renderField.propTypes = {
  input: propTypes.any.isRequired,
  label: propTypes.any.isRequired,
  type: propTypes.any.isRequired,
  meta: propTypes.any.isRequired,
};

const RenderValues = ({ fields, meta: { error, submitFailed } }) => (
  <div>
    {fields.map((value, index) => (
      <Field
        name={`${value}`}
        type="text"
        component={renderField}
        label="Value"
        key={index} //eslint-disable-line
      />
    ))}

    {submitFailed && error && <Alert bsStyle="danger">{error}</Alert>}
  </div>
);

RenderValues.propTypes = {
  fields: propTypes.any.isRequired,
  meta: propTypes.any.isRequired,
};

const EditConfigForm = props => (
  <Form onSubmit={props.handleSubmit}>
    <FieldArray name="values" component={RenderValues} />

    {props.submitFailed && props.error && <Alert bsStyle="danger">{props.error}</Alert>}

    <Button type="submit" disabled={props.submitting} bsStyle="success">
      Save
    </Button>
  </Form>
);

EditConfigForm.propTypes = {
  handleSubmit: propTypes.func.isRequired,
  submitFailed: propTypes.any.isRequired,
  submitting: propTypes.any.isRequired,
  error: propTypes.any.isRequired,
};

export default reduxForm({
  form: 'editConfigForm',
  validate,
})(EditConfigForm);
