/* eslint-disable */
import React from 'react';
import { SubmissionError } from 'redux-form';
import { Map } from 'immutable';
import { commonApi } from 'app/api';
import { Domain, EditModal } from './components';

class Configuration extends React.Component {
  state = { domains: [], selectedGroup: null };

  componentWillMount = () => {
    // ToDo: get request when commonApi will be changed.
    commonApi
      .get("config?page=0&size=1000")
      .then(result => this.setDomains(result.content));
  }

  onSubmit = config => {
    // ToDo: Update domains with the result received from server & error handling.
    return commonApi
      .patch("config", config)
      .then(() => this.updateDomains(config))
      .catch(() =>
        Promise.reject(new SubmissionError({ _error: "Failed to save config" }))
      );
  };

  setDomains = (configs) => {
    const domains = this.groupConfigsByDomain(configs);
    this.setState({ domains });
  }

  groupConfigsByDomain = (configs) => {
    const obj = {};

    configs.forEach(config => {
      if (!obj[config.domain]) obj[config.domain] = [];
      obj[config.domain].push(config);
    });

    return Object.keys(obj).map(k => ({ domain: k, values: obj[k] }));
  }

  /* update domains with the updated config and close the modal  */
  updateDomains = config => {
    this.setState(prevState => {

      var domains = prevState.domains.map(
        domain =>
          domain.domain === config.domain
            ? this.getUpdatedDomain(domain, config)
            : domain
      );

      return { domains, selectedGroup: null };
    });
  };

  getUpdatedDomain = (domain, newConfig) => {
    return Map(domain)
      .set(
        "values",
        domain.values.map(
          config =>
            config.groupname === newConfig.groupname ? newConfig : config
        )
      )
      .toObject();
  }

  editGroup = group => this.setState({ selectedGroup: group });

  render = () => {
    return (
      <div>
        <h3>Configuration</h3>

        {this.state.domains.map(config => (
          <Domain {...config} onEdit={this.editGroup} key={config.domain} />
        ))}

        <EditModal
          show={!!this.state.selectedGroup}
          config={this.state.selectedGroup}
          onSubmit={this.onSubmit}
          onClose={() => this.setState({ selectedGroup: null })}
        />
      </div>
    );
  }
}

export default Configuration;
