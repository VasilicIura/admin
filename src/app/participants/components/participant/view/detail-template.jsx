import React from 'react';
import { Modal, ListGroup, ListGroupItem } from 'react-bootstrap';
import { object } from 'prop-types';

const DetailParticipantTemplate = ({ team }) => (
  <Modal.Body>
    <div className="detail-contests-template">
      <ListGroupItem>
        <ListGroup>
          <strong>Participant Name:</strong> {team.name}
        </ListGroup>
        <ListGroup>
          <strong>Sport Type:</strong>
          <span className="status-label label label-default">{team.sportPk}</span>
        </ListGroup>
      </ListGroupItem>
    </div>
  </Modal.Body>
);

DetailParticipantTemplate.defaultProps = {
  team: null,
};

DetailParticipantTemplate.propTypes = {
  team: object,
};

export default DetailParticipantTemplate;
