import React, { Component } from 'react';
import { object } from 'prop-types';
import { Modal, Button, Tooltip, OverlayTrigger } from 'react-bootstrap';
import DetailParticipantTemplate from './detail-template';

class ViewParticipant extends Component {
  static propTypes = {
    subParticipant: object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }
  openModal = () => {
    this.setState({ showModal: true });
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>View team participant</span>
      </Tooltip>
    );

    return (
      <div className="action-button">
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="add-sub-participant"
            onClick={this.openModal}
          >
            <i className="mi mi-visibility" />
          </Button>
        </OverlayTrigger>

        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">View team participant</Modal.Title>
          </Modal.Header>
          <DetailParticipantTemplate team={this.props.subParticipant} />
        </Modal>
      </div>
    );
  }
}

export default ViewParticipant;
