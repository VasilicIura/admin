import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import { array, any, func, object } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { ControlInput, ControlSelect } from 'fe-shared';
import { validate } from './validate';

class TeamParticipantsForm extends Component {
  static defaultProps = {
    participant: null,
  };

  static propTypes = {
    handleSubmit: func.isRequired,
    submitting: any.isRequired,
    sports: array.isRequired,
    participant: object,
  };

  render() {
    const {
      handleSubmit,
      submitting,
      sports,
      participant,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="name"
          placeholder="Name"
          type="text"
          component={ControlInput}
          label="Name"
          currentValue={participant ? participant.name : ''}
        />
        <Field
          name="sportPk"
          placeholder="Sport type"
          component={ControlSelect}
          options={sports}
          label="Sport type:"
          currentValue={participant ? participant.sportPk : ''}
        />
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'teamParticipants',
  validate,
})(TeamParticipantsForm);
