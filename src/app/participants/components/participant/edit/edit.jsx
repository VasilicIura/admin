import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { object, func } from 'prop-types';
import { toSelectArray } from 'fe-shared';
import { Modal, Button, Tooltip, OverlayTrigger } from 'react-bootstrap';
import { NotificationManager } from 'react-notifications';
import { TeamParticipantsForm } from '../form';
import { searchParticipants, updateParticipant } from '../../../actions';
import { SPORT_TYPES, PARTICIPANTS_TYPE } from '../../../consts';
import './edit.scss';

class EditParticipant extends Component {
  static propTypes = {
    participant: object.isRequired,
    updateParticipant: func.isRequired,
    searchParticipants: func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  closeModal = () => {
    this.setState({ showModal: false });
  };

  openModal = () => {
    this.setState({ showModal: true });
  };

  handleSubmit = (value) => {
    const model = Object.assign({}, this.props.participant, value);

    this.props.updateParticipant({ ...model, subparticipants: [] })
      .then(() => {
        this.props.searchParticipants();
        this.closeModal();
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Edit Team</span>
      </Tooltip>
    );
    return (
      <div className="action-button">
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.openModal}
          >
            <i className="mi mi-create" />
          </Button>
        </OverlayTrigger>
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Edit Team</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TeamParticipantsForm
              onSubmit={this.handleSubmit}
              participantTypes={toSelectArray(PARTICIPANTS_TYPE)}
              sports={toSelectArray(SPORT_TYPES)}
              participant={this.props.participant}
            />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  updateParticipant,
  searchParticipants,
}, dispatch));

export default connect(null, mapDispatchToProps)(EditParticipant);
