import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func } from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { Modal, Button } from 'react-bootstrap';
import { toSelectArray } from 'fe-shared';
import { TeamParticipantsForm } from '../form';
import { SPORT_TYPES } from '../../../consts';
import { addParticipant, searchParticipants } from '../../../actions';

class AddParticipant extends Component {
  static propTypes = {
    searchParticipants: func.isRequired,
    addParticipant: func.isRequired,
  };

  state = {
    showModal: false,
  };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = (value) => {
    value.participantTypePk = 'Team';
    this.props.addParticipant(value)
      .then(() => {
        NotificationManager.success(`Team ${value.name} was successful created`);
        return this.props.searchParticipants();
      })
      .then(this.toggleModal)
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render() {
    return (
      <div>
        <Button
          className="add-button"
          onClick={this.toggleModal}
        >
          <i className="mi mi-add" />
        </Button>

        <Modal show={this.state.showModal} onHide={this.toggleModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Add team</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TeamParticipantsForm
              onSubmit={this.handleSubmit}
              sports={toSelectArray(SPORT_TYPES)}
            />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  searchParticipants,
  addParticipant,
}, dispatch));

export default connect(null, mapDispatchToProps)(AddParticipant);
