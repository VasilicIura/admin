import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { object, array, func, number } from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { toSelectArray, toSelectArrayFromJSON } from 'fe-shared';
import { Modal, Button, Tooltip, OverlayTrigger } from 'react-bootstrap';
import { TeamSubParticipantsForm } from '../form';
import { searchParticipants, updateSubParticipant, changeParticipantTeam } from '../../../actions';
import { SPORT_TYPES, PARTICIPANTS_TYPE } from '../../../consts';
import './edit.scss';

class EditSubParticipant extends Component {
  static propTypes = {
    subParticipant: object.isRequired,
    updateSubParticipant: func.isRequired,
    searchParticipants: func.isRequired,
    currentTeamId: number.isRequired,
    changeParticipantTeam: func.isRequired,
    teams: array.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  closeModal = () => {
    this.setState({ showModal: false });
  };

  openModal = () => {
    this.setState({ showModal: true });
  };

  handleSubmit = (value) => {
    const { parentParticipantId, ...model } = value;
    /* parentParticipant -> team JSON id */
    if (this.props.currentTeamId === parentParticipantId.value) {
      this.props.updateSubParticipant(Object.assign({}, this.props.subParticipant, model))
        .then(() => {
          this.props.searchParticipants();
          this.closeModal();
        })
        .catch((err) => {
          const response = err.response.data;
          NotificationManager.error(response.message, `Status ${response.status}`, 3000);
        });
    } else {
      const { currentTeamId } = this.props;
      const { id } = this.props.subParticipant;
      this.props.changeParticipantTeam(currentTeamId, parentParticipantId, id)
        .then(() => {
          this.props.searchParticipants();
          this.closeModal();
        })
        .catch((err) => {
          const response = err.response.data;
          NotificationManager.error(response.message, `Status ${response.status}`, 3000);
        });
    }
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Edit team participant</span>
      </Tooltip>
    );
    return (
      <div className="action-button">
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.openModal}
          >
            <i className="mi mi-create" />
          </Button>
        </OverlayTrigger>
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Edit team participant</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TeamSubParticipantsForm
              onSubmit={this.handleSubmit}
              subparticipant={this.props.subParticipant}
              currentTeamId={this.props.currentTeamId}
              participantTypes={toSelectArray(PARTICIPANTS_TYPE)}
              sports={toSelectArray(SPORT_TYPES)}
              teams={toSelectArrayFromJSON(this.props.teams, 'id', 'name')}
            />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  updateSubParticipant,
  searchParticipants,
  changeParticipantTeam,
}, dispatch));

export default connect(null, mapDispatchToProps)(EditSubParticipant);
