import React, { Component } from 'react';
import { object } from 'prop-types';
import { Modal, Button, Tooltip, OverlayTrigger } from 'react-bootstrap';
import DetailParticipantsTemplate from './detail-template';
import './view.scss';


class ViewSubParticipants extends Component {
  static propTypes = {
    participant: object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }
  openModal = () => {
    this.setState({ showModal: true });
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>View team members</span>
      </Tooltip>
    );

    return (
      <div className="action-button">
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="add-sub-participant"
            onClick={this.openModal}
          >
            <i className="mi mi-visibility" />
          </Button>
        </OverlayTrigger>

        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">View team members</Modal.Title>
          </Modal.Header>
          <DetailParticipantsTemplate participant={this.props.participant} />
        </Modal>
      </div>
    );
  }
}


export default ViewSubParticipants;
