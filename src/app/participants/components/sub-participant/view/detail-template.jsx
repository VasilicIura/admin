import React from 'react';
import { Modal, ListGroup, ListGroupItem } from 'react-bootstrap';
import { object } from 'prop-types';

const DetailParticipantsTemplate = ({ participant }) => (
  <Modal.Body>
    <div className="team-members-list">
      {participant.subparticipants ? (
        <ListGroup>
          {
            participant.subparticipants
              .map(team => (
                <ListGroupItem key={team.id}>
                  <span className="team-item">{team.name}</span>
                  <span className="status-label label label-default pull-right">{team.sportPk}</span>
                </ListGroupItem>),
              )
          }
        </ListGroup>
      ) : (
        <ListGroup>
          <ListGroupItem>In this team we don't have participants</ListGroupItem>
        </ListGroup>
      )}

    </div>
  </Modal.Body>
);

DetailParticipantsTemplate.defaultProps = {
  participant: null,
};

DetailParticipantsTemplate.propTypes = {
  participant: object,
};

export default DetailParticipantsTemplate;
