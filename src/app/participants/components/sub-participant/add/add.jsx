import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, object } from 'prop-types';
import { Modal, Button, Tooltip, OverlayTrigger } from 'react-bootstrap';
import { NotificationManager } from 'react-notifications';
import { toSelectArray } from 'fe-shared';
import { TeamSubParticipantsForm } from '../form';
import { SPORT_TYPES } from '../../../consts';
import { searchParticipants, addParticipant } from '../../../actions';
import './add.scss';

class AddSubparticipant extends Component {
  static propTypes = {
    searchParticipants: func.isRequired,
    addParticipant: func.isRequired,
    participant: object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  setSubParticipants = (subparticipant) => {
    const { participant } = this.props;

    return {
      ...participant,
      subparticipants: participant.subparticipants !== null ? [
        subparticipant,
      ] : [subparticipant],
    };
  };

  handleSubmit = (value) => {
    value.participantTypePk = 'Team Member';
    const model = this.setSubParticipants(value);

    this.props.addParticipant(model)
      .then(() => {
        this.props.searchParticipants()
          .then(() => {
            NotificationManager.success(`Participant ${value.name} was successfully added to ${this.props.participant.name}`);
            this.closeModal();
          });
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  openModal = () => {
    this.setState({ showModal: true });
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Add team member</span>
      </Tooltip>
    );

    return (
      <div className="action-button">
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.openModal}
          >
            <i className="mi mi-add" />
          </Button>
        </OverlayTrigger>
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Add team member</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TeamSubParticipantsForm
              onSubmit={this.handleSubmit}
              participant={this.props.participant}
              sports={toSelectArray(SPORT_TYPES)}
            />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  searchParticipants,
  addParticipant,
}, dispatch));

export default connect(null, mapDispatchToProps)(AddSubparticipant);
