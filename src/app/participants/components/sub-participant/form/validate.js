export const validate = (values) => {
  const errors = {};
  const {
    name, participantTypePk, sportPk,
  } = values;

  if (!name) {
    errors.name = 'Name field is required';
  }

  if (!participantTypePk) {
    errors.participantTypePk = 'Participant type field is required';
  }

  if (!sportPk) {
    errors.sportPk = 'Sport type field is required';
  }

  return errors;
};
