import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import { Form, Button } from 'react-bootstrap';
import { array, any, func, object, number } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { ControlInput, ControlSelect, ControlAutoCompleteSelect } from 'fe-shared';
import { autocompleteParticipants } from '../../../actions';
import { validate } from './validate';

class TeamSubParticipantsForm extends Component {
  static defaultProps = {
    teams: [],
  };

  static propTypes = {
    handleSubmit: func.isRequired,
    autocompleteParticipants: func.isRequired,
    submitting: any.isRequired,
    sports: array.isRequired,
    subparticipant: object.isRequired,
    currentTeamId: number.isRequired,
    teams: array,
  };

  getCurrentTeamName = id =>
    this.props.teams.find(e => e.value === id);

  render() {
    const {
      handleSubmit,
      submitting,
      sports,
      subparticipant,
      currentTeamId,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="name"
          placeholder="Name"
          type="text"
          component={ControlInput}
          label="Name"
          currentValue={subparticipant ? subparticipant.name : ''}
        />
        <Field
          name="sportPk"
          placeholder="Sport type"
          component={ControlSelect}
          options={sports}
          label="Sport type:"
          currentValue={subparticipant ? subparticipant.sportPk : ''}
        />
        {
          subparticipant ? (
            <Field
              name="parentParticipantId"
              placeholder="Team"
              component={ControlAutoCompleteSelect}
              autoCompleteAction={this.props.autocompleteParticipants}
              label="Team:"
              currentValue={subparticipant ? this.getCurrentTeamName(currentTeamId).label : ''}
            />
            ) : null
        }
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  autocompleteParticipants,
}, dispatch));

export default compose(
  connect(null, mapDispatchToProps),
  reduxForm({
    form: 'teamSubParticipants',
    validate,
  }),
)(TeamSubParticipantsForm);
