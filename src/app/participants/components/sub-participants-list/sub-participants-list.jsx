import React, { Component } from 'react';
import { object, array } from 'prop-types';
import { Col, ListGroupItem } from 'react-bootstrap';
import { EditSubParticipant } from '../sub-participant';

class SubParticipantsList extends Component {
  static defaultProps = {
    participant: {},
    teams: [],
  };

  static propTypes = {
    participant: object,
    teams: array,
  };

  render() {
    const { subparticipants, id } = this.props.participant;

    return (
      <Col>
        {
          subparticipants.map(teamParticipant => ((
            <ListGroupItem
              className="list-item sub-participant"
              key={teamParticipant.id}
            >
              <span>
                <strong> Name: </strong>
                {teamParticipant.name}
              </span>
              <div className="sub-participant-activity">
                <EditSubParticipant
                  subParticipant={teamParticipant}
                  currentTeamId={id}
                  teams={this.props.teams}
                />
              </div>
            </ListGroupItem>
          )))
        }
      </Col>
    );
  }
}

export default SubParticipantsList;
