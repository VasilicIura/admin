import React from 'react';
import { PropTypes } from 'prop-types';
import { AddSubParticipant } from '../sub-participant/add';
import { ViewSubParticipants } from '../sub-participant/view';
import { EditParticipant } from '../participant/edit';

const HeaderRow = (props) => {
  const row = props.original;
  switch (props.column.id) {
    case 'actions':
      return (
        <div className="participants-team-btn">
          <AddSubParticipant participant={row} />
          <EditParticipant participant={row} />
          <ViewSubParticipants participant={row} />
        </div>
      );
    default:
      return (
        <div className="participants-team">
          <div className="participants-team-info">
            <span>Name: </span>
            <strong>{props.value}</strong>
          </div>
        </div>
      );
  }
};

HeaderRow.defaultProps = {
  value: null,
};

HeaderRow.propTypes = {
  value: PropTypes.string,
  original: PropTypes.object.isRequired,
  column: PropTypes.object.isRequired,
};

export default HeaderRow;
