import React from 'react';
import { PropTypes } from 'prop-types';
import { EditSubParticipant } from '../sub-participant';
import { ViewParticipant } from '../participant';

const DetailRow = props => (
  Array.isArray(props.row.subparticipants) ? (
    <div className="participants-wrapper">
      <div className="participants-list">
        <div className="participant-label">Participants</div>{
        props.row.subparticipants.map(subparticipant => (

          <div className="list-item" key={subparticipant.id}>
            <div className="sub-participant">
              <span>{subparticipant.name}</span>
              <div className="participants-team-btn">
                <EditSubParticipant
                  subParticipant={subparticipant}
                  currentTeamId={props.row.id}
                  teams={props.teams}
                />
                <ViewParticipant
                  subParticipant={subparticipant}
                />
              </div>
            </div>
          </div>
        ))
      }
      </div>
    </div>
  ) : (
    <div className="participants-list">
      <div className="participant-label">This team has no participants</div>
    </div>
  )
);

DetailRow.propTypes = {
  teams: PropTypes.array.isRequired,
  row: PropTypes.object.isRequired,
};

export default DetailRow;
