import React, { Component } from 'react';
import { func, object } from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  AutoCompleteFilter,
} from 'fe-shared';
import { autocompleteParticipants } from '../../actions';

class ParticipantsSwitchFilterCell extends Component {
  static defaultProps = {
    column: null,
  };

  static propTypes = {
    autocompleteParticipants: func.isRequired,
    column: object,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
    } = this.props;

    switch (name) {
      case 'name': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteParticipants}
          valueKey="name"
          {...this.props}
        />
      );
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  autocompleteParticipants,
}, dispatch));

export default compose(
  connect(null, mapDispatchToProps),
  reduxFilter({
    filter: 'eventGroupsFilter',
  }),
)(ParticipantsSwitchFilterCell);
