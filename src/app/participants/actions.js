import { createAction } from 'redux-actions';
import { filterSelector, queryFilters } from 'fe-shared';
import { tradingApi } from '../api';
import { filterCriteriasSelector } from './selectors';
import { CONST_ACTIONS, PARTICIPANT_TYPES, FILTER_TYPES } from './consts';

const fetchParticipants = createAction(
  CONST_ACTIONS.FETCH_PARTICIPANTS,
  (paginationQueries = '') => tradingApi.get(`participants/type/${PARTICIPANT_TYPES.team}${paginationQueries}`),
);

const addParticipant = createAction(
  CONST_ACTIONS.CREATE_PARTICIPANT,
  (participant) => {
    if (participant.subparticipnats) {
      return tradingApi.post('participants', { ...participant, subparticipnats: [] });
    }
    return tradingApi.post('participants', participant);
  },
);

const updateParticipant = createAction(
  CONST_ACTIONS.UPDATE_PARTICIPANT,
  ({ id, ...participant }) => tradingApi.patch(`participants/${id}`, { ...participant, id }),
);

const updateSubParticipant = createAction(
  CONST_ACTIONS.UPDATE_SUBPARTICIPANT,
  ({ id, ...subparticipant }) => tradingApi.patch(`participants/${id}`, { ...subparticipant, id }),
);

const changeParticipantTeam = createAction(
  CONST_ACTIONS.CHANGE_PARTICIPANT_TEAM,
  (currentTeamId, newTeamId, subParticipantId) => {
    const newTeam = newTeamId.value || newTeamId;
    return tradingApi.patch(`participants/change-team/participant/${subParticipantId}/old-team/${currentTeamId}/new-team/${newTeam}`, {});
  },
);

const filterParticipants = createAction(
  CONST_ACTIONS.FILTER_PARTICIPANTS,
  (criteria = []) => tradingApi.post('participants/search', { ...criteria }),
);

const changePagination = createAction(
  CONST_ACTIONS.CHANGE_PARTICIPANTS_PAGINATION,
  (pagination = {}) => pagination,
);

const setFilterCriterias = createAction(
  CONST_ACTIONS.SET_PARTICIPANTS_FILTERS,
  values => values,
);

const autocompleteParticipants = createAction(
  CONST_ACTIONS.AUTOCOMPLETE_PARTICIPANTS_FILTER,
  (criteria = []) => tradingApi.post('participants/search', { ...criteria }),
);

const searchParticipants = () =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'eventGroupsFilter' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    return dispatch(filterParticipants(filterCriteriasSelector(newState)));
  };

const changeSorting = createAction(
  CONST_ACTIONS.CHANGE_PARTICIPANTS_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

export {
  fetchParticipants,
  updateParticipant,
  addParticipant,
  updateSubParticipant,
  changeParticipantTeam,
  changePagination,
  searchParticipants,
  autocompleteParticipants,
  changeSorting,
};
