import keyMirror from 'keymirror';

export const CONST_ACTIONS = keyMirror({
  FETCH_PARTICIPANTS: null,
  UPDATE_PARTICIPANT: null,
  CREATE_PARTICIPANT: null,
  CREATE_SUBPARTICIPANT: null,
  UPDATE_SUBPARTICIPANT: null,
  CHANGE_PARTICIPANT_TEAM: null,
  FILTER_PARTICIPANTS: null,
  CHANGE_PARTICIPANTS_PAGINATION: null,
  CHANGE_PARTICIPANTS_SORTING: null,
  AUTOCOMPLETE_PARTICIPANTS_FILTER: null,
  SET_PARTICIPANTS_FILTERS: null,
});

export const SPORT_TYPES = [
  'Soccer',
];

export const PARTICIPANTS_TYPE = [
  'Team Member',
  'Team',
];

export const PARTICIPANT_TYPES = {
  team: 'Team',
  teamMember: 'Team Member',
};

export const FILTER_TYPES = {
  LIKE: [
    'name',
  ],
};

