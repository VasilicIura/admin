import { createSelector } from 'reselect';

const participantsStateSelector = state => state.participants;

const participantsSelector = createSelector(
  participantsStateSelector,
  participantsList => participantsList.get('participants').toJS(),
);

const teamsSelector = createSelector(
  participantsSelector,
  participantsList =>
    participantsList.content &&
    participantsList.content.length &&
    participantsList.content.map(({ id, name }) => ({ id, name })),
);

const filteredParticipants = createSelector(
  participantsStateSelector,
  participants => participants.get('filteredParticipants').toJS(),
);

const participantsListSelector = createSelector(
  participantsStateSelector,
  (participants) => {
    const list = participants.get('filteredParticipants').toJS();
    return list && list.content;
  },
);

const filterCriteriasSelector = createSelector(
  participantsStateSelector,
  criterias => criterias.get('criteria').toJS(),
);

const participantsSortingSelector = createSelector(
  filterCriteriasSelector,
  ({ pageable }) => {
    if (pageable.hasOwnProperty('orders')) { //eslint-disable-line
      return [
        {
          columnName: pageable.orders[0].field,
          direction: pageable.orders[0].direction.toLowerCase(),
        },
      ];
    }

    return null;
  },
);

export {
  participantsSelector,
  teamsSelector,
  filterCriteriasSelector,
  filteredParticipants,
  participantsListSelector,
  participantsSortingSelector,
};
