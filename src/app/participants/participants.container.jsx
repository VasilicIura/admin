import React, { Component } from 'react';
import { func, object } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Col, Row, Pagination } from 'react-bootstrap';
import ReactTable from 'react-table';
import { PageSize } from 'fe-shared';
import {
  fetchParticipants,
  searchParticipants,
  changePagination,
  changeSorting,
} from './actions';
import { filteredParticipants } from './selectors';
import {
  AddParticipant,
  HeaderRow,
  DetailRow,
  ParticipantsSwitchFilterCell,
} from './components';
import './participant.container.scss';

class Participants extends Component {
  static propTypes = {
    changePagination: func.isRequired,
    changeSorting: func.isRequired,
    searchParticipants: func.isRequired,
    participants: object.isRequired,
  };

  state = {
    activePage: 0,
    pageSize: 10,
    sorted: [],
  };

  getParticipants = (
    activePage = this.state.activePage,
    pageSize = this.state.pageSize,
    sorted = this.state.sorted,
  ) => {
    if (sorted) {
      this.setState({ activePage, pageSize, sorted });
    }
    this.props.changePagination({
      page: activePage,
      size: pageSize,
    });

    if (sorted.length) {
      this.props.changeSorting(sorted);
    }

    this.props.searchParticipants();
  };

  getCustomCell = cell => <HeaderRow {...cell} />;
  switchFilters = grid => <ParticipantsSwitchFilterCell {...grid} />;
  handleClick = () => {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn,
    }));
  };

  COLUMNS = [{
    accessor: 'name',
    Header: 'Team Name',
    Cell: row => this.getCustomCell(row),
  },
  {
    Header: '',
    accessor: 'actions',
    sortable: false,
    Filter: ({ onChange }) => (
      <button onClick={() => onChange()} style={{ margin: '6px', float: 'right' }}>Filter</button>
    ),
    Cell: row => this.getCustomCell(row),
  }];

  render() {
    const { participants } = this.props;

    return (
      <Row>
        <Col lg={10} lgOffset={1} md={12}>
          <h3 style={{ float: 'left' }}>Participants</h3>
          <div>
            <PageSize
              onChangePageSizeSelect={
                ({ value }) => this.getParticipants(this.state.activePage, value)
              }
              value={this.state.pageSize}
            />
          </div>
          <div>
            <ReactTable
              columns={this.COLUMNS}
              manual
              data={participants.content}
              pages={participants.totalPages}
              minRows={0}
              defaultPageSize={10}
              pageSizeOptions={[this.state.pageSize]}
              pageSize={this.state.pageSize}
              showPagination={false}
              onFetchData={state => this.getParticipants(state.page, state.pageSize, state.sorted)}
              filterable
              FilterComponent={this.switchFilters}
              SubComponent={row => (
                <DetailRow
                  row={row.original}
                  teams={participants && participants.content}
                />
              )}
            />
            <Pagination
              bsSize="medium"
              maxButtons={20}
              items={participants.totalPages}
              activePage={participants.number + 1}
              onSelect={value => this.getParticipants(value - 1)}
              ellipsis
              prev
              next
            />
          </div>
        </Col>
        <div className="add-participant">
          <AddParticipant />
        </div>
      </Row>
    );
  }
}

const mapStateToProp = state => ({
  participants: filteredParticipants(state),
});

const mapDispatchToProp = dispatch => (bindActionCreators({
  fetchParticipants,
  searchParticipants,
  changePagination,
  changeSorting,
}, dispatch));


export default connect(mapStateToProp, mapDispatchToProp)(Participants);
