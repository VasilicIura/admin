import { handleActions } from 'redux-actions';
import { Map, List } from 'immutable';
import { CONST_ACTIONS } from './consts';

const initialState = Map({
  participants: Map({}),
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  filteredParticipants: Map({}),
});

export const reducer = handleActions({
  [`${CONST_ACTIONS.FETCH_PARTICIPANTS}_FULFILLED`]:
    (state, action) => state.set('participants', Map(action.payload)),
  [`${CONST_ACTIONS.FILTER_PARTICIPANTS}_FULFILLED`]:
    (state, action) => state.set('filteredParticipants', Map(action.payload)),
  [CONST_ACTIONS.CHANGE_PARTICIPANTS_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [CONST_ACTIONS.CHANGE_PARTICIPANTS_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
  [CONST_ACTIONS.SET_PARTICIPANTS_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
}, initialState);
