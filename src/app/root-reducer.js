import { combineReducers } from 'redux';
import { router5Reducer as router } from 'redux-router5';
import { formsReducer, filterReducer } from 'fe-shared';
import { reducer as formReducer } from 'redux-form';
import { reducer as auth } from './auth/reducers';
import { reducer as participants } from './participants';
import { reducer as leagues } from './leagues';
import { reducer as markets } from './markets';
import { reducer as events } from './events';
import { reducer as eventGroup } from './event-groups';
import { reducer as outcomes } from './outcomes';
import { reducer as contests } from './contests';
import { reducer as chainsOfEventGroups } from './eventGroupChains';
import { reducer as staticPages } from './static-pages';
import { reducer as loader } from './loader';
import { reducer as mail } from './mails';
import { reducer as selections } from './selections';
import { reducer as userManagement } from './userManagement';
import { reducer as chainsWorkflow } from './eventGroupChainsWorkflow';
import transactions from './transactions';
import { reducer as bets } from './user-bets';
import { reducer as roles } from './roles';
import balance from './balance';
import reports from './reports';
import enums from './enums';

const rootReducer = combineReducers({
  router,
  auth,
  participants,
  leagues,
  mail,
  markets,
  contests,
  eventGroup,
  chainsOfEventGroups,
  events,
  outcomes,
  staticPages,
  chainsWorkflow,
  forms: formsReducer,
  reduxFilters: filterReducer,
  loader,
  selections,
  userManagement,
  roles,
  bets,
  balance: balance.reducer,
  reports: reports.reducer,
  enums: enums.reducer,
  form: formReducer,
  transactions: transactions.reducer,
});

export default rootReducer;
