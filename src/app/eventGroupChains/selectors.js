import { createSelector } from 'reselect';
import { toSelectArrayFromJSON, toSelectArray } from 'fe-shared';
import { DEFAULT_NEW_STATUS } from './consts';

const eventGroupChainsStateSelector = state => state.chainsOfEventGroups;

const eventGroupChainsSelector = createSelector(
  eventGroupChainsStateSelector,
  eventGroupChains => eventGroupChains.get('event_group_chains').toJS(),
);

const eventGroupsSelector = createSelector(
  eventGroupChainsStateSelector,
  eventGroups => eventGroups.get('eventGroups').toJS(),
);

const availableStatusesSelector = createSelector(
  eventGroupChainsStateSelector,
  (state, currentStatus) => currentStatus || null,
  (eventGroupStatuses, currentStatus) => {
    const appendStatus = currentStatus || DEFAULT_NEW_STATUS;
    const eventGroupCurrentStatuses = eventGroupStatuses.get('availableStatuses').toJS();
    eventGroupCurrentStatuses.unshift(appendStatus);

    return toSelectArray(eventGroupCurrentStatuses);
  },
);

const eventGroupChainStatusesSelector = createSelector(
  eventGroupChainsStateSelector,
  statuses => toSelectArray(statuses.get('eventGroupChainStatuses').toJS()),
);

const eventGroupChainsListSelector = createSelector(
  eventGroupChainsStateSelector,
  (eventGroups) => {
    const list = eventGroups.get('filteredEventGroupChains').toJS();
    return list && list.content;
  },
);

const leaguesSelector = createSelector(
  eventGroupChainsStateSelector,
  leagues => toSelectArrayFromJSON(leagues.get('leagues').toJS(), 'id', 'name'),
);

const filteredEventGroupChains = createSelector(
  eventGroupChainsStateSelector,
  eventGroups => eventGroups.get('filteredEventGroupChains').toJS(),
);

const filterCriteriasSelector = createSelector(
  eventGroupChainsStateSelector,
  criterias => criterias.get('criteria').toJS(),
);

const leagueByIdSelector = createSelector(
  eventGroupChainsStateSelector,
  leagues => leagues.get('league').toJS(),
);

const eventsSortingSelector = createSelector(
  filterCriteriasSelector,
  ({ pageable }) => {
    if (pageable.hasOwnProperty('orders')) { //eslint-disable-line
      return [
        {
          columnName: pageable.orders[0].field,
          direction: pageable.orders[0].direction.toLowerCase(),
        },
      ];
    }

    return null;
  },
);

export {
  eventGroupChainsSelector,
  eventGroupsSelector,
  availableStatusesSelector,
  eventGroupChainStatusesSelector,
  leaguesSelector,
  filteredEventGroupChains,
  filterCriteriasSelector,
  eventGroupChainsListSelector,
  leagueByIdSelector,
  eventsSortingSelector,
};
