import { handleActions } from 'redux-actions';
import { Map, List } from 'immutable';
import { ACTIONS } from './consts';

const initialState = Map({
  event_group_chains: Map({}),
  availableStatuses: List([]),
  eventGroupChainStatuses: List([]),
  eventGroups: List([]),
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  filteredEventGroupChains: Map({}),
  leagues: List([]),
  league: Map({}),
});

export const reducer = handleActions({
  [`${ACTIONS.FETCH_EVENT_GROUP_CHAINS}_FULFILLED`]:
    (state, action) => state.set('event_group_chains', Map(action.payload)),
  [`${ACTIONS.FETCH_AVAILABLE_STATUSES}_FULFILLED`]:
    (state, action) => state.set('availableStatuses', List(action.payload)),
  [`${ACTIONS.SEARCH_EVENT_GROUP_CHAINS}_FULFILLED`]:
    (state, action) => state.set('filteredEventGroupChains', Map(action.payload)),
  [ACTIONS.CHANGE_EVENT_GROUP_CHAINS_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [ACTIONS.SET_EVENT_GROUP_CHAINS_FILTERS]:
    (state, action) => state.setIn(['criteria', 'criteria'], List(action.payload)),
  [`${ACTIONS.SEARCH_EVENT_GROUPS}_FULFILLED`]:
    (state, action) => state.set('eventGroups', List(action.payload.content)),
  [`${ACTIONS.FETCH_ALL_EVENTGROUPCHAIN_STATUSES}_FULFILLED`]:
    (state, action) => state.set('eventGroupChainStatuses', List(action.payload)),
  [`${ACTIONS.EVENT_GROUP_CHAINS_LEAGUES}_FULFILLED`]: (state, action) =>
    state.set('leagues', List(action.payload.content)),
  [ACTIONS.CHANGE_EVENT_GROUP_CHAINS_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
  [`${ACTIONS.FETCH_EVENT_GROUP_CHAIN_LEAGUE_BY_ID}_FULFILLED`]:
    (state, action) => state.set('league', Map(action.payload)),
}, initialState);
