import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, object } from 'prop-types';
import { SubmissionError } from 'redux-form';
import {
  ShowLastCreatedNotification,
} from 'fe-shared';
import Table from '../common/table';
import {
  AddEventGroupChain,
  EventGroupChainDetailTemplate,
  CustomCellTemplate,
  EventGroupChainsSwitchFilterCell,
} from './components';
import { filteredEventGroupChains } from './selectors';
import {
  searchEventGroupChains,
  fetchEventGroupChainStatuses,
  changePagination,
  changeSorting,
  addEventGroupChain,
} from './actions';
import { COLUMNS } from './consts';

class EventGroupChains extends Table {
  static defaultProps = {
    eventGroupChains: null,
  };

  static propTypes = {
    addEventGroupChain: func.isRequired,
    changePagination: func.isRequired,
    fetchData: func.isRequired,
    eventGroupChains: object,
  };

  state = {
    eventGroupChain: {},
    showLastCreated: true,
    showModal: false,
  };
  componentDidMount = () => this.props.fetchEventGroupChainStatuses();

  getEventGroupChains = () => {
    this.props.fetchData();
  };

  showLastCreated = () => this.setState({ showLastCreated: !this.state.showLastCreated });
  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = values =>
    this.props.addEventGroupChain(values)
      .then(({ value }) => {
        this.setState({ eventGroupChain: value });
        return this.getEventGroupChains();
      })
      .then(() => {
        ShowLastCreatedNotification(this.showLastCreated);
        this.toggleModal();
      })
      .catch((err) => {
        const response = err.response.data;
        let message = '';
        if (typeof response.message === 'string') {
          ({ message } = response);
        } else {
          message = JSON.stringify(response.message);
        }
        return Promise.reject(new SubmissionError({ _error: message }));
      });

  switchFilters = grid => <EventGroupChainsSwitchFilterCell {...grid} />;
  getCustomCell = cell => <CustomCellTemplate {...cell} />;

  render() {
    return (
      <div className="events-container">
        <EventGroupChainDetailTemplate
          eventGroupChain={this.state.eventGroupChain}
          showCreated={this.state.showLastCreated}
        />
        <div>
          {this.renderTable(
            COLUMNS(this.getCustomCell),
            this.props.eventGroupChains,
            false,
            this.switchFilters,
            'Event Group Chains',
          )}
          <div className="add-btn-container">
            <AddEventGroupChain
              toggleModal={this.toggleModal}
              showModal={this.state.showModal}
              showLastCreated={this.state.showLastCreated}
              handleSubmit={this.handleSubmit}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  eventGroupChains: filteredEventGroupChains(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchData: () => searchEventGroupChains(),
  fetchEventGroupChainStatuses,
  changePagination,
  changeSorting,
  addEventGroupChain,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(EventGroupChains);
