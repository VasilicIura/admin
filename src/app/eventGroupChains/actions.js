import { createAction } from 'redux-actions';
import { queryFilters, filterSelector } from 'fe-shared';
import { ACTIONS, CURRENT_ENTITY, FILTER_TYPES } from './consts';
import { filterCriteriasSelector } from './selectors';
import { tradingApi, commonApi } from '../api';

const fetchChainsOfEventsGroup = createAction(
  ACTIONS.FETCH_EVENT_GROUP_CHAINS,
  (paginationQueries = '') => tradingApi.get(`event-group-chains${paginationQueries}`),
);

const fetchChainsOfEventsGroupById = createAction(
  ACTIONS.FETCH_EVENT_GROUP_CHAINS_BY_ID,
  id => tradingApi.get(`event-group-chains/${id}`),
);

const addEventGroupChain = createAction(
  ACTIONS.ADD_EVENT_GROUP_CHAINS,
  ({ eventGroups, eventGroupLeague, ...eventGroupChain }) => tradingApi.post('event-group-chains', {
    ...eventGroupChain,
    leagueId: eventGroupLeague.value || eventGroupLeague,
    eventGroups: eventGroups.length ? eventGroups.map(e => e.value) : [...eventGroups],
  }),
);

const updateEventGroupChain = createAction(
  ACTIONS.UPDATE_GROUP_CHAIN,
  ({
    id,
    eventGroups,
    eventGroupLeague,
    ...rest
  }) => (tradingApi.patch(`event-group-chains/${id}`, {
    ...rest,
    leagueId: eventGroupLeague.value || eventGroupLeague,
    eventGroups: eventGroups.map(item => item.value || item),
  })),
);

const fetchAvailableStatuses = createAction(
  ACTIONS.FETCH_AVAILABLE_STATUSES,
  (currentStatus, entity) => commonApi.get(`enums/statuses/entity/${entity}/from-status/${currentStatus}`),
);

const fetchEventGroupChainStatuses = createAction(
  ACTIONS.FETCH_ALL_EVENTGROUPCHAIN_STATUSES,
  () => commonApi.get(`enums/statuses/entity/${CURRENT_ENTITY}`),
);

const searchEventGroups = createAction(
  ACTIONS.SEARCH_EVENT_GROUPS,
  (criteria = []) => tradingApi.post(
    'event-groups/search',
    {
      ...criteria,
      pageable: {
        page: 0,
        size: 1000,
        orders: [{
          field: 'startTimeStamp',
          direction: 'DESC',
        }],
      },
    },
  ),
);

const filterEventGroups = createAction(
  ACTIONS.SEARCH_EVENT_GROUP_CHAINS,
  (criteria = []) => tradingApi.post('event-group-chains/search', { ...criteria }),
);

const fetchEventGroupChainsLeagues = createAction(
  ACTIONS.EVENT_GROUP_CHAINS_LEAGUES,
  (paginationQueries = '') => tradingApi.get(`leagues${paginationQueries}`),
);

const changePagination = createAction(
  ACTIONS.CHANGE_EVENT_GROUP_CHAINS_PAGINATION,
  (pagination = {}) => pagination,
);

const setFilterCriterias = createAction(
  ACTIONS.SET_EVENT_GROUP_CHAINS_FILTERS,
  values => values,
);

const autocompleteEventGroupChains = createAction(
  ACTIONS.AUTOCOMPLETE_EVENT_GROUP_CHAINS,
  (criteria = []) => tradingApi.post('event-group-chains/search', { ...criteria }),
);

const fetchLeagueById = createAction(
  ACTIONS.FETCH_EVENT_GROUP_CHAIN_LEAGUE_BY_ID,
  id => tradingApi.get(`leagues/${id}`),
);

const searchEventGroupChains = () =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'eventGroupChainsFilter' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    dispatch(filterEventGroups(filterCriteriasSelector(newState)));
  };

const changeSorting = createAction(
  ACTIONS.CHANGE_EVENT_GROUP_CHAINS_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

export {
  fetchChainsOfEventsGroup,
  fetchChainsOfEventsGroupById,
  fetchAvailableStatuses,
  addEventGroupChain,
  updateEventGroupChain,
  fetchEventGroupChainStatuses,
  searchEventGroups,
  fetchEventGroupChainsLeagues,
  searchEventGroupChains,
  changePagination,
  autocompleteEventGroupChains,
  fetchLeagueById,
  changeSorting,
};
