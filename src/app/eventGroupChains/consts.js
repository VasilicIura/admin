import React from 'react';
import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  FETCH_EVENT_GROUP_CHAINS: null,
  FETCH_EVENT_GROUP_CHAINS_BY_ID: null,
  UPDATE_GROUP_CHAIN: null,
  ADD_EVENT_GROUP_CHAINS: null,
  FETCH_AVAILABLE_STATUSES: null,
  FETCH_ALL_EVENTGROUPCHAIN_STATUSES: null,
  EVENT_GROUP_CHAINS_LEAGUES: null,
  SEARCH_EVENT_GROUPS: null,
  SEARCH_EVENT_GROUP_CHAINS: null,
  CHANGE_EVENT_GROUP_CHAINS_PAGINATION: null,
  SET_EVENT_GROUP_CHAINS_FILTERS: null,
  AUTOCOMPLETE_EVENT_GROUP_CHAINS: null,
  CHANGE_EVENT_GROUP_CHAINS_SORTING: null,
  FETCH_EVENT_GROUP_CHAIN_LEAGUE_BY_ID: null,
});

export const DEFAULT_NEW_STATUS = 'New';
export const CURRENT_ENTITY = 'Event Group Chain';

export const FILTER_TYPES = {
  IN: [
    'eventGroupChainStatusPk',
    'eventGroupId',
  ],
  LIKE: [
    'name',
    'description',
  ],
};

export const COLUMNS = getCustomCell => [
  {
    Header: 'Name',
    accessor: 'name',
  },
  {
    Header: 'Description',
    accessor: 'description',
  },
  {
    Header: 'Status',
    accessor: 'eventGroupChainStatusPk',
  },
  {
    Header: 'Assigned Event Groups',
    accessor: 'eventGroups',
    Cell: cell => getCustomCell(cell),
  },
  {
    Header: '',
    sortable: false,
    width: 130,
    accessor: 'actions',
    Filter: ({ onChange }) => (
      <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
    ),
    Cell: cell => getCustomCell(cell),
  },
];
