import React from 'react';
import propTypes from 'prop-types';
import { EventGroupChainDetailTemplate } from '../detail';
import { EditEventGroupChain } from '../edit';

const CustomCellTemplate = (props) => {
  switch (props.column.id) {
    case 'eventGroups':
      return Array.isArray(props.value)
        ? `Event groups assigned ${props.value.length}`
        : 'No Event Groups Added';
    case 'actions':
      return (
        <div>
          <EventGroupChainDetailTemplate id={props.original.id} />
          <EditEventGroupChain
            eventGroupChain={props.original}
          />
        </div>
      );
    default: return props.value;
  }
};

CustomCellTemplate.defaultProps = {
  value: null,
};

CustomCellTemplate.propTypes = {
  column: propTypes.object.isRequired,
  original: propTypes.object.isRequired,
  value: propTypes.any,
};

export default CustomCellTemplate;
