import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Form, Button, Alert } from 'react-bootstrap';
import { array, any, func, object, string, number } from 'prop-types';
import { compose } from 'recompose';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { ControlInput, ControlSelect, toSelectArrayFromJSON, ControlAutoCompleteSelect } from 'fe-shared';
import { autocompleteLeagues } from 'app/leagues';
import { EventGroupFilters } from '../eventGroupFilters';
import { searchEventGroups, fetchAvailableStatuses, fetchLeagueById } from '../../../actions';
import { eventGroupsSelector, availableStatusesSelector, leagueByIdSelector } from '../../../selectors';
import { filterSize, CURRENT_ENTITY, DEFAULT_NEW_STATUS } from '../../../consts';
import { validate } from './validate';

class EventGroupChainsForm extends Component {
  static defaultProps = {
    statuses: [],
    eventGroupChain: null,
    eventGroups: [],
    periodTo: null,
    periodFrom: null,
    eventGroupLeague: null,
    error: null,
  };

  static propTypes = {
    handleSubmit: func.isRequired,
    searchEventGroups: func.isRequired,
    fetchLeagueById: func.isRequired,
    submitting: any.isRequired,
    autocompleteLeagues: func.isRequired,
    fetchAvailableStatuses: func.isRequired,
    statuses: array,
    eventGroupChain: object,
    eventGroups: array,
    periodTo: string,
    periodFrom: string,
    eventGroupLeague: number,
    error: string,
    league: object.isRequired,
  };


  constructor(props) {
    super(props);
    const { eventGroupChain } = props;
    this.state = {
      criterias: {
        status: {
          field: 'eventGroupStatusPk',
          type: 'NOT_IN',
          values: ['New'],
        },
      },
      currentStatus: (eventGroupChain && eventGroupChain.eventGroupChainStatusPk) ||
      DEFAULT_NEW_STATUS,
    };
  }

  componentDidMount = () => {
    const { currentStatus } = this.state;
    this.fetchEventGroups();
    this.props.fetchAvailableStatuses(currentStatus, CURRENT_ENTITY);
    this.props.eventGroupChain && this.props.fetchLeagueById(this.props.eventGroupChain.leagueId); // eslint-disable-line
  };

  componentWillReceiveProps = ({ eventGroupLeague }) => (
    (eventGroupLeague !== this.props.eventGroupLeague) &&
    this.setLeagueFilter(eventGroupLeague));

  setPeriodFilter = ({ periodFrom, periodTo }) => {
    const model = {
      field: 'startTimeStamp',
      type: 'RANGE',
      from: periodFrom,
      to: periodTo,
    };

    this.setCriterias('period', model);
  };

  setLeagueFilter = league => this.setCriterias('league', {
    field: 'leagueId',
    type: 'EQ',
    value: league.value,
  });

  setCriterias = (name, model) => this.setState({
    criterias: {
      ...this.state.criterias,
      [name]: model,
    },
  }, () => {
    this.props.searchEventGroups({
      criteria: Object.keys(this.state.criterias)
        .map(criteria => this.state.criterias[criteria]),
      pageable: filterSize,
    });
  });

  fetchEventGroups = () => {
    const { eventGroupChain } = this.props;

    return ((eventGroupChain && eventGroupChain.leagueId) ?
      this.setLeagueFilter(eventGroupChain.leagueId) :
      this.props.searchEventGroups({
        criteria: [{
          field: 'eventGroupStatusPk',
          type: 'NOT_IN',
          values: ['New'],
        }],
      }));
  };

  render() {
    const {
      handleSubmit,
      submitting,
      eventGroups,
      periodTo,
      periodFrom,
      eventGroupLeague,
      eventGroupChain,
      error,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="name"
          placeholder="Name"
          type="text"
          component={ControlInput}
          label="Name"
          currentValue={eventGroupChain ? eventGroupChain.name : ''}
        />
        <Field
          name="description"
          placeholder="Description"
          type="text"
          component={ControlInput}
          label="Description:"
          currentValue={eventGroupChain ? eventGroupChain.description : ''}
        />
        <Field
          name="eventGroupChainStatusPk"
          placeholder="Event group chains statuses"
          component={ControlSelect}
          options={this.props.statuses}
          label="Event group chain statuses:"
          currentValue={eventGroupChain ? eventGroupChain.eventGroupChainStatusPk : ''}
        />
        <Field
          name="eventGroupLeague"
          placeholder="League"
          component={ControlAutoCompleteSelect}
          autoCompleteAction={this.props.autocompleteLeagues}
          label="Leagues"
          currentValue={eventGroupChain ? this.props.league.name : ''}
        />
        <div className="clearfix">
          <div className="select-pull-left">
            <Field
              multi
              name="eventGroups"
              placeholder="Event groups"
              component={ControlSelect}
              options={toSelectArrayFromJSON(eventGroups, 'id', 'name')}
              label="Event groups:"
              currentValue={(eventGroupChain && Array.isArray(eventGroupChain.eventGroups)) ? toSelectArrayFromJSON(eventGroupChain.eventGroups, 'id', 'name') : ''}
            />
          </div>
          <div className="filter-container">
            <EventGroupFilters
              league={eventGroupLeague}
              periodFrom={periodFrom}
              periodTo={periodTo}
              setPeriodFilter={this.setPeriodFilter}
            />
          </div>
        </div>
        {error && <Alert bsStyle="danger">{error}</Alert>}
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

const selector = formValueSelector('eventGroupChains');
const mapStateToProps = (state, props) => ({
  periodFrom: selector(state, 'from'),
  periodTo: selector(state, 'to'),
  eventGroupLeague: selector(state, 'eventGroupLeague'),
  eventGroups: eventGroupsSelector(state),
  statuses: availableStatusesSelector(state, props.eventGroupChain &&
  props.eventGroupChain.eventGroupChainStatusPk),
  league: leagueByIdSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  searchEventGroups,
  fetchAvailableStatuses,
  autocompleteLeagues,
  fetchLeagueById,
}, dispatch));

export default compose(
  reduxForm({
    form: 'eventGroupChains',
    validate,
  }),
  connect(mapStateToProps, mapDispatchToProps),
)(EventGroupChainsForm);
