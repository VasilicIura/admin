export const validate = (values) => {
  const errors = {};
  const {
    name, description, eventGroupChainStatusPk, eventGroups, eventGroupLeague,
  } = values;

  if (!name) {
    errors.name = 'Name field is required';
  }

  if (!description) {
    errors.description = 'Description field is required';
  }

  if (!eventGroupChainStatusPk) {
    errors.eventGroupChainStatusPk = 'Status field is required';
  }

  if (!eventGroupLeague) {
    errors.eventGroupLeague = 'Legue field is required';
  }

  if (!eventGroups || !eventGroups.length) {
    errors.eventGroups = 'Status field is required';
  }

  return errors;
};
