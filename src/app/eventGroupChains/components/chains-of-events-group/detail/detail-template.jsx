import React from 'react';
import { object } from 'prop-types';
import { Modal, Label, ListGroup, ListGroupItem } from 'react-bootstrap';

const EventGroupChainDetailTemplate = ({ eventGroupChain }) => (
  <Modal.Body>
    <div>
      <div className="detail-view">
        <h5 style={{ lineHeight: '44px' }}>Name:  {eventGroupChain.name}</h5>
        <h5 style={{ float: 'right' }}>
          Current status:
          <Label className="status-label">{eventGroupChain.eventGroupChainStatusPk}</Label>
        </h5>
      </div>
      <div>
        <h5>Description:</h5><br />
        {eventGroupChain.description}
      </div>
      <div style={{ marginTop: '3em' }}>
        <h3>Assigned event groups</h3>
        <ListGroup>
          {
            eventGroupChain.eventGroups
              .map(e => (<ListGroupItem key={e.id}>{e.name}</ListGroupItem>))
          }
        </ListGroup>
      </div>
    </div>
  </Modal.Body>
);

EventGroupChainDetailTemplate.defaultProps = {
  eventGroupChain: null,
};

EventGroupChainDetailTemplate.propTypes = {
  eventGroupChain: object,
};

export default EventGroupChainDetailTemplate;
