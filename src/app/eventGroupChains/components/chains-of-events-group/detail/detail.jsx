import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { number, func, bool, object } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { fetchChainsOfEventsGroupById } from '../../../actions';
import EventGroupDetailTemplate from './detail-template';

export class EventGroupChainsDetail extends Component {
  static defaultProps = {
    id: null,
    showCreated: true,
    eventGroupChain: null,
  };

  static propTypes = {
    id: number,
    fetchChainsOfEventsGroupById: func.isRequired,
    showCreated: bool,
    eventGroupChain: object,
  };

  state = {
    showModal: false,
  };

  componentWillReceiveProps(newProps) {
    if (newProps.showCreated !== this.props.showCreated) {
      this.showModal();
    }
  }

  showModal = () => {
    if (this.props.id) {
      this.props.fetchChainsOfEventsGroupById(this.props.id)
        .then(({ value }) => this.setState({
          eventGroupChain: value,
          showModal: !this.state.showModal,
        }));
    } else {
      this.setState({
        eventGroupChain: this.props.eventGroupChain,
        showModal: !this.state.showModal,
      });
    }
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>View Event Group Chains</span>
      </Tooltip>
    );
    return (
      <div>
        {
          this.props.id ? (
            <OverlayTrigger placement="top" overlay={tooltip}>
              <Button
                className="circle-button"
                onClick={this.showModal}
                style={{ float: 'left', marginRight: '15px' }}
              >
                <i className="mi mi-visibility" />
              </Button>
            </OverlayTrigger>
          ) : null
        }
        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({ showModal: !this.state.showModal })}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Event Group Chains</Modal.Title>
          </Modal.Header>
          <EventGroupDetailTemplate eventGroupChain={this.state.eventGroupChain} />
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchChainsOfEventsGroupById,
}, dispatch));

export default connect(null, mapDispatchToProps)(EventGroupChainsDetail);
