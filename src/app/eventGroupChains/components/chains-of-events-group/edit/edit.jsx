import React, { Component } from 'react';
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { func, object } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  fetchAvailableStatuses,
  updateEventGroupChain,
  searchEventGroupChains,
} from 'app/eventGroupChains';
import { toSelectArray } from 'fe-shared';
import { NotificationManager } from 'react-notifications';
import { EventGroupChainsForm } from '../form';
import { CURRENT_ENTITY } from '../../../consts';

export class EditEventGroupChain extends Component {
  static propTypes = {
    searchEventGroupChains: func.isRequired,
    updateEventGroupChain: func.isRequired,
    eventGroupChain: object.isRequired,
    fetchAvailableStatuses: func.isRequired,
  };

  state = {
    showModal: false,
  };

  handleSubmit = (value) => {
    const { id } = this.props.eventGroupChain;
    this.props.updateEventGroupChain({
      ...value,
      id,
    })
      .then(() => this.props.searchEventGroupChains())
      .then(() => {
        NotificationManager.info('Updated', `Event group chain was updated ${value.name}`);
        this.setState({ showModal: !this.state.showModal });
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  toggleModal = () => {
    const { eventGroupChainStatusPk } = this.props.eventGroupChain;

    this.props.fetchAvailableStatuses(eventGroupChainStatusPk, CURRENT_ENTITY)
      .then(({ value }) => {
        value.unshift(eventGroupChainStatusPk);
        this.setState({ statuses: toSelectArray(value) });
      })
      .then(this.setState({ showModal: !this.state.showModal }));
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Edit Event Group Chains</span>
      </Tooltip>
    );
    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.toggleModal}
            style={{ float: 'left', marginRight: '15px' }}
          >
            <i className="mi mi-edit" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({ showModal: !this.state.showModal })}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Edit Event Group Chains</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal && (
                <EventGroupChainsForm
                  onSubmit={this.handleSubmit}
                  eventGroupChainStatuses={this.state.statuses}
                  eventGroupChain={this.props.eventGroupChain}
                />
              )
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  updateEventGroupChain,
  fetchAvailableStatuses,
  searchEventGroupChains,
}, dispatch));

export default connect(null, mapDispatchToProps)(EditEventGroupChain);
