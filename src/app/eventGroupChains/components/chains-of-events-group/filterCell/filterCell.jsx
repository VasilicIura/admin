import React, { Component } from 'react';
import propTypes from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  SelectFilter,
  AutoCompleteFilter,
} from 'fe-shared';
import { autoCompleteEventGroups } from 'app/event-groups';
import { eventGroupChainStatusesSelector } from '../../../selectors';
import { autocompleteEventGroupChains } from '../../../actions';

class EventGroupChainsSwitchFilterCell extends Component {
  static defaultProps = {
    statuses: [],
    eventsGroups: [],
    column: null,
  };

  static propTypes = {
    autocompleteEventGroupChains: propTypes.func.isRequired,
    autoCompleteEventGroups: propTypes.func.isRequired,
    statuses: propTypes.array,
    eventsGroups: propTypes.array,
    column: propTypes.object,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
      statuses,
    } = this.props;

    switch (name) {
      case 'name': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteEventGroupChains}
          valueKey="name"
          {...this.props}
        />
      );

      case 'eventGroups': return (
        <FilterField
          name="eventGroupId"
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autoCompleteEventGroups}
          {...this.props}
        />
      );
      case 'eventGroupChainStatusPk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={statuses}
          {...this.props}
        />
      );
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapStateToProps = state => ({
  statuses: eventGroupChainStatusesSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  autocompleteEventGroupChains,
  autoCompleteEventGroups,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxFilter({
    filter: 'eventGroupChainsFilter',
  }),
)(EventGroupChainsSwitchFilterCell);
