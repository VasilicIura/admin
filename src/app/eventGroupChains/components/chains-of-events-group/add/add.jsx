import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import propTypes from 'prop-types';
import { EventGroupChainsForm } from '../form';

const AddEventGroupChain = props => (
  <div>
    <Button
      className="add-button"
      onClick={props.toggleModal}
    >
      <i className="mi mi-add" />
    </Button>

    <Modal show={props.showModal} onHide={props.toggleModal}>
      <Modal.Header closeButton>
        <Modal.Title className="modal-title">Add Event Group Chain</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <EventGroupChainsForm onSubmit={props.handleSubmit} />
      </Modal.Body>
    </Modal>
  </div>
);

AddEventGroupChain.propTypes = {
  toggleModal: propTypes.func.isRequired,
  showModal: propTypes.bool.isRequired,
  handleSubmit: propTypes.func.isRequired,
};

export default AddEventGroupChain;
