import React from 'react';
import { tradingApiUrl, accountsApiUrl, commonApiUrl, bettingApiUrl, walletApiUrl } from '../../api';
import './apiList.scss';

export const ApiList = () => (
  <div className="apiList">
    <div className="apiListTitle">API list:</div>
    <div className="apiListLinks">
      <div>
        <span>Trading:</span>
        <a href={`${tradingApiUrl}/swagger-ui.html`} target="_blank">
          {tradingApiUrl}
        </a>
      </div>
      <div>
        <span>Accounts:</span>
        <a href={`${accountsApiUrl}/swagger-ui.html`} target="_blank">
          {accountsApiUrl}
        </a>
      </div>
      <div>
        <span>Common:</span>
        <a href={`${commonApiUrl}/swagger-ui.html`} target="_blank">
          {commonApiUrl}
        </a>
      </div>
      <div>
        <span>Betting:</span>
        <a href={`${bettingApiUrl}/swagger-ui.html`} target="_blank">
          {bettingApiUrl}
        </a>
      </div>
      <div>
        <span>Wallet:</span>
        <a href={`${walletApiUrl}/swagger-ui.html`} target="_blank">
          {walletApiUrl}
        </a>
      </div>
    </div>
  </div>
);
