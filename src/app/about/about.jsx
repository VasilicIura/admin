import React, { Component } from 'react';
import { AppVersion } from './version';
import { ApiList } from './apiList';

class About extends Component {
  render() {
    return (
      <div>
        <h3>About</h3>
        <AppVersion />
        <ApiList />
      </div>
    );
  }
}

export default About;
