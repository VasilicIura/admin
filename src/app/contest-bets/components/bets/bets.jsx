import React from 'react';
import { func, object } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { CustomCell, FilterCell } from './components/bets-grid/components';
import { COLUMNS } from '../../../contest-bets/consts';
import { betsSortingSelector, filteredBets } from '../../selectors';
import Table from '../../../common/table';
import { searchBets, changePagination, searchUserBets, changeSorting, fetchBetsStatuses } from '../../actions';


class Bets extends Table {
  static defaultProps = {
    bets: null,
  };

  static propTypes = {
    changePagination: func.isRequired,
    searchBets: func.isRequired,
    route: object.isRequired,
    bets: object,
  };


  componentDidMount = () => {
    this.props.fetchBetsStatuses();
  };


  switchFilters = grid => (<FilterCell
    userId={this.props.route.params.id}
    {...grid}
  />);

  useCustomCell = props => <CustomCell {...props} />;

  render = () => {
    const contestName = this.props.bets
      && this.props.bets.content
      && this.props.bets.content.length
      && this.props.bets.content[0].contestName;
    return (
      <div className="events-container">
        {this.renderTable(
            COLUMNS(this.useCustomCell),
            this.props.bets,
            false,
            this.switchFilters,
            `Contest Bets ${contestName}`,
        )}
      </div>
    );
  };
}

const mapStateToProp = state => ({
  bets: filteredBets(state),
  sorting: betsSortingSelector(state),
});

const mapDispatchToProp = (dispatch, props) => (bindActionCreators({
  fetchData: () => searchBets(props.route.params.id),
  searchBets,
  changePagination,
  searchUserBets,
  changeSorting,
  fetchBetsStatuses,
}, dispatch));
export default connect(mapStateToProp, mapDispatchToProp)(Bets);
