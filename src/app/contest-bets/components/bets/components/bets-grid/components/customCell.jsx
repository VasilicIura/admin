import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';
import { BetDetail } from './detail';

const CustomCell = (props) => {
  switch (props.column.id) {
    case 'selections': return (
      <div>
        {
          Array.isArray(props.value) ? `For this bet assigned (${props.value.length}) selections` :
            'No selections for this bet'
        }
      </div>
    );
    case 'actions':
      return (
        <div>
          <BetDetail bet={props.original} />
        </div>
      );
    case 'placedTimeStamp':
      return <div>{props.value && moment(props.value).format('lll')}</div>;
    default:
      return <div>{props.value}</div>;
  }
};

CustomCell.defaultProps = {
  column: null,
  original: null,
  value: null,
};

CustomCell.propTypes = {
  column: propTypes.object,
  original: propTypes.object,
  value: propTypes.any,
};

export default CustomCell;
