import React, { Component } from 'react';
import { object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { ConfirmationComponent } from 'fe-shared';
import { Modal, Label, ListGroup, ListGroupItem, Col, Button } from 'react-bootstrap';
import { cancelBet } from '../../../../../../actions';

class BetDetailTemplate extends Component {
  static propTypes = {
    cancelBet: func.isRequired,
  };

  state = {
    showConfirmation: false,
    errors: null,
  };

  isConfirmed = (value) => {
    if (value) {
      this.props.cancelBet(this.props.bet.id)
        .then(() => this.renderConfirmation)
        .catch(err => this.setState({ errors: err.response.data.message }));
    }
  };

  renderConfirmation = () => {
    this.setState({ showConfirmation: !this.state.showConfirmation });
  };

  render = () => {
    const { bet } = this.props;
    return (
      <Modal.Body>
        <div>
          <div className="detail-view">
            <h5 style={{ lineHeight: '32px' }}>Bet value:  {bet.betValue}</h5>
            <h5 style={{ float: 'right' }}>
              Current status:
              <Label className="status-label">{bet.betStatusPk}</Label>
            </h5>
          </div>
          <div className="detail-view">
            <h5 style={{ lineHeight: '20px' }}>Username:  {bet.userName}</h5>
            <br />
            <h5 style={{ lineHeight: '20px' }}>Potential points:  {bet.potentialPointsWhenPlaced}</h5>
            <br />
            <h5 style={{ lineHeight: '20px' }}>Current points :  {bet.currentPointsComputed}</h5>
            <br />
            <h5 style={{ lineHeight: '20px' }}>Final Points :  {bet.finalPointsWhenResulted}</h5>
            <br />
            <h5 style={{ lineHeight: '20px' }}>
              Placed at: <em>{moment(bet.placedTimeStamp).format('MMM DD, YYYY, h:mm A')}</em>
            </h5>
          </div>
          <Col md="12">
            <div className="submit-modal-btn">
              <Button bsStyle="danger" onClick={this.renderConfirmation}>Cancel Bet</Button>
            </div>
          </Col>
          <div style={{ marginTop: '3em' }}>
            <h3>Selections</h3>
            <ListGroup>
              {
                bet.selections.length && bet.selections.map(selection => (
                  <ListGroupItem>
                    <h5 style={{ lineHeight: '23px' }}>Applyes to:  {selection.appliesToCdn}</h5>
                    <h5 style={{ lineHeight: '23px' }}>Odd type:  {selection.selectionTypePk}</h5>
                    <h5 style={{ lineHeight: '23px' }}>Odd: {selection.oddsNumerator}/{selection.oddsDenominator}</h5>
                  </ListGroupItem>
                ))
              }
            </ListGroup>
          </div>
        </div>
        {
          this.state.showConfirmation && (
            <ConfirmationComponent
              isConfirmedAction={this.isConfirmed}
              showConfirmation={this.state.showConfirmation}
              toggleConfirmation={this.renderConfirmation}
              errors={this.state.errors}
            />
          )
        }
      </Modal.Body>
    );
  };
}

BetDetailTemplate.defaultProps = {
  bet: null,
};

BetDetailTemplate.propTypes = {
  bet: object,
};

const mapDispatchToProps = dispatch => (bindActionCreators({
  cancelBet,
}, dispatch));

export default connect(null, mapDispatchToProps)(BetDetailTemplate);
