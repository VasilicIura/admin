import React from 'react';
import { connect } from 'react-redux';
import { object, array, number } from 'prop-types';
import { compose } from 'recompose';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  SelectFilter,
  getEntryFeeValues,
  toSelectArray,
} from 'fe-shared';
import { betStatusesSelector } from '../../../../../selectors';
import { RANGE_VALUES } from '../../../../../consts';

class FilterCell extends React.PureComponent {
  static defaultProps = {
    betStatuses: [],
    column: null,
  };

  static propTypes = {
    betStatuses: array,
    column: object,
    userId: number.isRequired,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
    } = this.props;

    switch (name) {
      case 'betStatusPk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={this.props.betStatuses}
          {...this.props}
        />
      );
      case 'finalPointsWhenResulted': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={toSelectArray(getEntryFeeValues(RANGE_VALUES))}
          {...this.props}
        />
      );
      case 'currentPointsComputed': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={toSelectArray(getEntryFeeValues(RANGE_VALUES))}
          {...this.props}
        />
      );
      case 'potentialPointsWhenPlaced': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={toSelectArray(getEntryFeeValues(RANGE_VALUES))}
          {...this.props}
        />
      );
      case 'betValue': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={toSelectArray(getEntryFeeValues(RANGE_VALUES))}
          {...this.props}
        />
      );
      case 'selections': return (<div />);
      case 'contestName': return <div />;
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapStateToProps = state => ({
  betStatuses: betStatusesSelector(state),
});

export default compose(
  connect(mapStateToProps),
  reduxFilter({
    filter: 'betFilters',
  }),
)(FilterCell);
