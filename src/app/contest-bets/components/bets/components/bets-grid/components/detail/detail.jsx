import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { object } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import BetDetailTemplate from './detail-template';
import { cancelBet } from '../../../../../../actions';

export class BetDetail extends Component {
  static defaultProps = {
    bet: null,
  };

  static propTypes = {
    bet: object,
  };

  state = {
    showModal: false,
  };

  toggleModal = () => {
    this.setState({ showModal: !this.state.showModal });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>View Bet details</span>
      </Tooltip>
    );
    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.toggleModal}
            style={{ float: 'left', marginRight: '15px' }}
          >
            <i className="mi mi-visibility" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggleModal}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Bet Details</Modal.Title>
          </Modal.Header>
          <BetDetailTemplate bet={this.props.bet} />
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  cancelBet,
}, dispatch));

export default connect(null, mapDispatchToProps)(BetDetail);
