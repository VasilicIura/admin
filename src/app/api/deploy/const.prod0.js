const accountsApiUrl = 'https://api.betduel.com/services/accounts';
const bettingApiUrl = 'https://api.betduel.com/services/betting';
const commonApiUrl = 'https://api.betduel.com/services/commons';
const tradingApiUrl = 'https://api.betduel.com/services/trading';
const walletApiUrl = 'https://api.betduel.com/services/finance';

export {
  accountsApiUrl,
  commonApiUrl,
  bettingApiUrl,
  tradingApiUrl,
  walletApiUrl,
};
