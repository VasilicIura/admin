const accountsApiUrl = '/services/accounts';
const bettingApiUrl = '/services/betting';
const commonApiUrl = '/services/common';
const tradingApiUrl = '/services/trading';
const walletApiUrl = '/services/finance';

export {
  accountsApiUrl,
  commonApiUrl,
  bettingApiUrl,
  tradingApiUrl,
  walletApiUrl,
};
