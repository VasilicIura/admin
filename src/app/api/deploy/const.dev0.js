const accountsApiUrl = 'http://10.102.0.76:8091';
const bettingApiUrl = 'http://10.102.0.76:8111';
const commonApiUrl = 'http://10.102.0.76:8101';
const tradingApiUrl = 'http://10.102.0.76:8081';
const walletApiUrl = 'http://10.102.0.76:8121';

export {
  accountsApiUrl,
  commonApiUrl,
  bettingApiUrl,
  tradingApiUrl,
  walletApiUrl,
};
