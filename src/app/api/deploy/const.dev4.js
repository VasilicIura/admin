const accountsApiUrl = 'https://api-dev4.betduel.com/services/accounts';
const bettingApiUrl = 'https://api-dev4.betduel.com/services/betting';
const commonApiUrl = 'https://api-dev4.betduel.com/services/commons';
const tradingApiUrl = 'https://api-dev4.betduel.com/services/trading';
const walletApiUrl = 'https://api-dev4.betduel.com/services/finance';

export {
  accountsApiUrl,
  commonApiUrl,
  bettingApiUrl,
  tradingApiUrl,
  walletApiUrl,
};
