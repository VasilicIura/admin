/* eslint-disable no-unused-vars */
import axios from 'axios';
import { setAsyncErrors } from 'fe-shared';
import store from '../store';
import generateApi from './generate-api';
import { tradingApiUrl, accountsApiUrl, commonApiUrl, walletApiUrl, bettingApiUrl } from './const';

const tradingApi = generateApi(tradingApiUrl);
const accountApi = generateApi(accountsApiUrl);
const commonApi = generateApi(commonApiUrl);
const walletApi = generateApi(walletApiUrl);
const bettingApi = generateApi(bettingApiUrl);

export {
  tradingApi,
  accountApi,
  commonApi,
  walletApi,
  bettingApi,
};
