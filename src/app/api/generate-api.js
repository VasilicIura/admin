/* eslint-disable no-unused-vars */
import axios from 'axios';
import { setAsyncErrors } from 'fe-shared';
import { store } from '../store';
import { commonApiUrl } from './const';
import { logoutUser } from '../auth';

function generateApi(baseUrl) {
  const api = axios.create({
    baseURL: baseUrl,
    headers: {
      'Content-Type': 'application/json',
    },
    responseType: 'json',
  });

  api.interceptors.response.use(response =>
    Promise.resolve(response.data || response));

  api.interceptors.response.use(null, ({ config, request, response }) => {
    if (response && response.status === 401) store.dispatch(logoutUser());

    return Promise.reject({ config, request, response }); // eslint-disable-line
  });

  api.interceptors.request.use((config) => {
    const token = localStorage.getItem('token');
    config.headers['x-auth-token'] = token || '';
    return config;
  },
  );

  return api;
}

export default generateApi;
