const accountsApiUrl = 'http://10.102.0.76:8092';
const bettingApiUrl = 'http://10.102.0.76:8112';
const commonApiUrl = 'http://10.102.0.76:8102';
const tradingApiUrl = 'http://10.102.0.76:8082';
const walletApiUrl = 'http://10.102.0.76:8122';

export {
  accountsApiUrl,
  commonApiUrl,
  bettingApiUrl,
  tradingApiUrl,
  walletApiUrl,
};
