import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { func, object, array } from 'prop-types';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  SelectFilter,
  AutoCompleteFilter,
} from 'fe-shared';
import { autoCompleteOutcomes } from '../../../actions';

class OutcomesSwitchFilterCell extends React.PureComponent {
  static defaultProps = {
    outcomeTypes: [],
    column: null,
  };

  static propTypes = {
    autoCompleteOutcomes: func.isRequired,
    outcomeTypes: array,
    column: object,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
      outcomeTypes,
    } = this.props;

    switch (name) {
      case 'outcome': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autoCompleteOutcomes}
          autocompleteKey="outcome"
          valueKey="outcome"
          labelKey="outcome"
          {...this.props}
        />
      );

      case 'outcomeTypePk': return (
        <FilterField
          name={name}
          component={SelectFilter}
          options={outcomeTypes}
          {...this.props}
        />
      );
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  autoCompleteOutcomes,
}, dispatch));

export default compose(
  connect(null, mapDispatchToProps),
  reduxFilter({
    filter: 'outcomesFilter',
  }),
)(OutcomesSwitchFilterCell);
