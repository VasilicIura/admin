import React from 'react';
import propTypes from 'prop-types';
import { EditOutcome } from '../../outcome';
import { DetailOutcomes } from '../../outcome/detail/detail';

const CustomCellTemplate = (props) => {
  switch (props.column.id) {
    case 'actions':
      return (
        <div>
          {
                props.outcomeTypes.length ? (
                  <EditOutcome
                    outcome={props.original}
                    outcomeTypes={props.outcomeTypes}
                  />
                 )
                : null
          }
          <DetailOutcomes outcome={props.original} />
        </div>
      );
    default:
      return <div>{props.value}</div>;
  }
};

CustomCellTemplate.defaultProps = {
  outcomeTypes: [],
  original: null,
  value: null,
};

CustomCellTemplate.propTypes = {
  column: propTypes.object.isRequired,
  value: propTypes.any,
  outcomeTypes: propTypes.array,
  original: propTypes.object,
};

export default CustomCellTemplate;
