
export const COLUMNS = customCell => [
  {
    Header: 'Odds assigned',
    accessor: 'outcome',
  },
  {
    Header: 'Odds type:',
    accessor: 'outcomeTypePk',
    Cell: row => customCell(row),
  },
  {
    Header: 'Actions',
    width: 130,
    accessor: 'actions',
    Cell: row => customCell(row),
  },
];
