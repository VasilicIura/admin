import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NotificationManager } from 'react-notifications';
import { func, array, object } from 'prop-types';
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { OutcomeForm } from '../form';
import { fetchOutComes, editOutcome } from '../../../actions';
import './edit.scss';

class EditOutcome extends Component {
  static propTypes = {
    outcome: object.isRequired,
    outcomeTypes: array.isRequired,
    editOutcome: func.isRequired,
    fetchOutComes: func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  closeModal = () => {
    this.setState({ showModal: false });
  };

  openModal = () => {
    this.setState({ showModal: true });
  };

  handleSubmit = (value) => {
    this.props.editOutcome(Object.assign({}, this.props.outcome, value))
      .then(() => {
        this.props.fetchOutComes();
        this.closeModal();
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Edit Outcome</span>
      </Tooltip>
    );
    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button edit-outcome"
            onClick={this.openModal}
          >
            <i className="mi mi-create" />
          </Button>
        </OverlayTrigger>
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Edit Outcome</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <OutcomeForm
              onSubmit={this.handleSubmit}
              outcomeTypes={this.props.outcomeTypes}
              outcome={this.props.outcome}
            />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  editOutcome,
  fetchOutComes,
}, dispatch));

export default connect(null, mapDispatchToProps)(EditOutcome);
