import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, array } from 'prop-types';
import { Modal, Button } from 'react-bootstrap';
import { NotificationManager } from 'react-notifications';
import { OutcomeForm } from '../form';
import { searchOutcomes, addOutCome } from '../../../actions';
import './add.scss';

class AddOutcome extends Component {
  static propTypes = {
    addOutCome: func.isRequired,
    searchOutcomes: func.isRequired,
    outcomeTypes: array.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  closeModal = () => {
    this.setState({ showModal: false });
  };

  openModal = () => {
    this.setState({ showModal: true });
  };

  handleSubmit = (value) => {
    this.props.addOutCome(value)
      .then(() => {
        this.props.searchOutcomes();
        this.closeModal();
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render() {
    return (
      <div>
        <Button
          className="add-button"
          onClick={this.openModal}
        >
          <i className="mi mi-add" />
        </Button>

        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Add Outcome</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <OutcomeForm
              onSubmit={this.handleSubmit}
              outcomeTypes={this.props.outcomeTypes}
            />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}


const mapDispatchToProps = dispatch => (bindActionCreators({
  addOutCome,
  searchOutcomes,
}, dispatch));

export default connect(null, mapDispatchToProps)(AddOutcome);
