import React from 'react';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { object } from 'prop-types';
import DetailOutcomesTemplate from './detail-template';

export class DetailOutcomes extends React.Component {
  static propTypes = {
    outcome: object.isRequired,
  };

  state = {
    showModal: false,
  };

  showModal = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>View Outcome</span>
      </Tooltip>
    );
    return (
      <div className="outcome-view">
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.showModal}
          >
            <i className="mi mi-visibility" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({ showModal: !this.state.showModal })}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">View Outcome</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <DetailOutcomesTemplate outcome={this.props.outcome} />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default DetailOutcomes;
