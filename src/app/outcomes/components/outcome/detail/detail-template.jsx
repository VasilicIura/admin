import React from 'react';
import { Modal, ListGroup } from 'react-bootstrap';
import { object } from 'prop-types';

const DetailOutcomesTemplate = ({ outcome }) => (
  <Modal.Body>
    <div className="detail-outcome-template">
      <ListGroup>
        <strong>Outcome Type:</strong> {outcome.outcomeTypePk}
      </ListGroup>

      <ListGroup>
        <strong>Odds assigned: </strong> {outcome.outcome}
      </ListGroup>

    </div>
  </Modal.Body>
);

DetailOutcomesTemplate.defaultProps = {
  outcome: null,
};

DetailOutcomesTemplate.propTypes = {
  outcome: object,
};

export default DetailOutcomesTemplate;
