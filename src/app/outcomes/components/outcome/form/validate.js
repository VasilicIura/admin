export const validate = (values) => {
  const errors = {};
  const {
    outcomeTypePk, numenator, denominator,
  } = values;

  if (!outcomeTypePk) {
    errors.outcomeTypePk = 'Outcome type field is required';
  }

  if (!numenator) {
    errors.numenator = 'Numenator field is required';
  }

  if (!denominator) {
    errors.denominator = 'Denominator type field is required';
  }

  return errors;
};
