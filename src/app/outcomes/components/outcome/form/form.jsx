import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import { array, any, func, object } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { ControlInput, ControlSelect } from 'fe-shared';
import { validate } from './validate';

class OutcomeForm extends Component {
  static defaultProps = {
    outcome: null,
  };

  static propTypes = {
    handleSubmit: func.isRequired,
    submitting: any.isRequired,
    outcome: object,
    outcomeTypes: array.isRequired,
  };

  render() {
    const {
      handleSubmit,
      submitting,
      outcomeTypes,
      outcome,
    } = this.props;

    const outcomeValues = outcome ? outcome.outcome.split('-') : [];

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="outcomeTypePk"
          placeholder="Outcome Type"
          component={ControlSelect}
          options={outcomeTypes}
          label="Outcome Type:"
          currentValue={outcome ? outcome.outcomeTypePk : ''}
        />
        <Field
          name="numenator"
          placeholder="Numerator"
          type="text"
          component={ControlInput}
          label="Numerator:"
          currentValue={outcome ? outcomeValues[0] : ''}
        />
        <Field
          name="denominator"
          placeholder="Denominator"
          type="text"
          component={ControlInput}
          label="Denominator:"
          currentValue={outcome ? outcomeValues[1] : ''}
        />
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'outcome',
  validate,
})(OutcomeForm);
