import { createAction } from 'redux-actions';
import { queryFilters, filterSelector } from 'fe-shared';
import { ACTIONS, FILTER_TYPES } from './consts';
import { filterCriteriasSelector } from './selectors';
import { tradingApi } from '../api';

const fetchOutComes = createAction(
  ACTIONS.FETCH_OUTCOMES,
  (paginationQueries = '') => tradingApi.get(`outcomes${paginationQueries}`),
);

const addOutCome = createAction(
  ACTIONS.ADD_OUTCOMES,
  (outcome) => {
    const { denominator, numenator, outcomeTypePk } = outcome;

    return tradingApi.post('outcomes', {
      outcome: `${denominator}-${numenator}`,
      outcomeTypePk,
    });
  },
);

const editOutcome = createAction(
  ACTIONS.UPDATE_OUTCOME,
  ({ id, ...outcome }) => {
    const { denominator, numenator, outcomeTypePk } = outcome;

    return tradingApi.patch(`outcomes/${id}`, {
      id,
      outcome: `${denominator}-${numenator}`,
      outcomeTypePk,
    });
  },
);

const filterOutcomes = createAction(
  ACTIONS.FILTER_OUTCOMES,
  (criteria = []) => tradingApi.post('outcomes/search', { ...criteria }),
);

const changePagination = createAction(
  ACTIONS.CHANGE_OUTCOMES_PAGINATION,
  (pagination = {}) => pagination,
);

const setFilterCriterias = createAction(
  ACTIONS.SET_OUTCOMES_FILTERS,
  values => values,
);

const searchOutcomes = () =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'outcomesFilter' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    dispatch(filterOutcomes(filterCriteriasSelector(newState)));
  };

const autoCompleteOutcomes = createAction(
  ACTIONS.AUTOCOMPLETE_OUTCOMES,
  (criteria = []) => tradingApi.post('outcomes/search', { ...criteria }),
);

const changeSorting = createAction(
  ACTIONS.CHANGE_OUTCOMES_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

export {
  fetchOutComes,
  addOutCome,
  editOutcome,
  searchOutcomes,
  changePagination,
  autoCompleteOutcomes,
  changeSorting,
};
