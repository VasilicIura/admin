import { createSelector } from 'reselect';

const outcomesStateSelector = state => state.outcomes;

const outcomesSelector = createSelector(
  outcomesStateSelector,
  outcomes => outcomes.get('outcomes').toJS(),
);

const filteredOutcomes = createSelector(
  outcomesStateSelector,
  event => event.get('filteredOutcomes').toJS(),
);

const filterCriteriasSelector = createSelector(
  outcomesStateSelector,
  criterias => criterias.get('criteria').toJS(),
);

const outcomesListSelector = createSelector(
  outcomesStateSelector,
  (outcomes) => {
    const list = outcomes.get('filteredOutcomes').toJS();
    return list && list.content;
  },
);

const outcomesSortingSelector = createSelector(
  filterCriteriasSelector,
  ({ pageable }) => {
    if (pageable.hasOwnProperty('orders')) { //eslint-disable-line
      return [
        {
          columnName: pageable.orders[0].field,
          direction: pageable.orders[0].direction.toLowerCase(),
        },
      ];
    }

    return null;
  },
);

export {
  outcomesSelector,
  filterCriteriasSelector,
  outcomesListSelector,
  filteredOutcomes,
  outcomesSortingSelector,
};
