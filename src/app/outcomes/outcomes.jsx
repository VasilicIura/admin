import React from 'react';
import { connect } from 'react-redux';
import { object, func } from 'prop-types';
import { bindActionCreators } from 'redux';
import { Row, Col } from 'react-bootstrap';
import { fetchEntityTypes, entityTypeSelectors } from 'app/contests';
import { filteredOutcomes } from './selectors';
import { fetchOutComes, changePagination, searchOutcomes, changeSorting } from './actions';
import { AddOutcome } from './components';
import { COLUMNS, CURRENT_ENTITY } from './consts';
import Table from '../common/table';
import { OutcomesSwitchFilterCell } from './components/outcomes-list/filterCell';
import { CustomCellTemplate } from './components/outcomes-list/custom-cell-templates';

class OutComes extends Table {
    static propTypes = {
      outcomes: object.isRequired,
      changePagination: func.isRequired,
      searchOutcomes: func.isRequired,
      fetchEntityTypes: func.isRequired,
      fetchData: func.isRequired,
    };

    static defaultProps = {
      outcomes: [],
    };


    componentDidMount() {
      this.props.fetchEntityTypes(CURRENT_ENTITY);
    }

    switchFilters = grid => (<OutcomesSwitchFilterCell
      outcomeTypes={this.props.outcomeTypes}
      {...grid}
    />);
  useCustomCell = props => <CustomCellTemplate {...props} outcomeTypes={this.props.outcomeTypes} />;


  render() {
    const { outcomes } = this.props;

    return (
      <div className="outcomes-container">
        <Row>
          <Col lg={10} lgOffset={1} md={12}>
            {this.renderTable(
              COLUMNS(this.useCustomCell),
              outcomes,
              false,
              this.switchFilters,
              'Outcomes',
            )}
          </Col>
        </Row>
        <div className="add-btn-container">
          <AddOutcome
            outcomeTypes={this.props.outcomeTypes}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  outcomes: filteredOutcomes(state),
  outcomeTypes: entityTypeSelectors(state),
});


const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchData: () => searchOutcomes(),
  searchOutcomes,
  fetchOutComes,
  fetchEntityTypes,
  changePagination,
  changeSorting,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(OutComes);
