import React from 'react';
import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  FETCH_OUTCOMES: null,
  ADD_OUTCOMES: null,
  UPDATE_OUTCOME: null,
  OUTCOME_TYPES: null,
  FILTER_OUTCOMES: null,
  SET_OUTCOMES_FILTERS: null,
  CHANGE_OUTCOMES_PAGINATION: null,
  CHANGE_OUTCOMES_SORTING: null,
  AUTOCOMPLETE_OUTCOMES: null,
});

export const FILTER_TYPES = {
  IN: [
    'outcomeTypePk',
    'outcome',
  ],
};


export const COLUMNS = customCell => [
  {
    Header: 'Odds assigned',
    accessor: 'outcome',
  },
  {
    Header: 'Odds type',
    accessor: 'outcomeTypePk',
    Cell: row => customCell(row),
  },
  {
    Header: '',
    width: 130,
    sortable: false,
    accessor: 'actions',
    Filter: ({ onChange }) => (
      <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
    ),
    Cell: row => customCell(row),
  },
];

export const CURRENT_ENTITY = 'Outcome';
