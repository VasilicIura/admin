import { handleActions } from 'redux-actions';
import { Map, List } from 'immutable';
import { ACTIONS } from './consts';

const initialState = Map({
  outcomes: Map({
    content: List(),
  }),
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  filteredOutcomes: Map({}),
});

export const reducer = handleActions({
  [`${ACTIONS.FETCH_OUTCOMES}_FULFILLED`]:
    (state, action) => state.set('outcomes', Map(action.payload)),
  [`${ACTIONS.FILTER_OUTCOMES}_FULFILLED`]:
    (state, action) => state.set('filteredOutcomes', Map(action.payload)),
  [ACTIONS.CHANGE_OUTCOMES_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [ACTIONS.CHANGE_OUTCOMES_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
  [ACTIONS.SET_OUTCOMES_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
}, initialState);
