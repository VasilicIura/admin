import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { betsSortingSelector, filteredBets } from '../../selectors';
import { searchBets, changePagination, searchUserBets, changeSorting, fetchBetsStatuses } from '../../actions';
import { COLUMNS } from '../../../user-bets/consts';
import Table from '../../../common/table';
import CustomCell from './components/bets-grid/components/customCell';
import { FilterCell } from './components/bets-grid/components';

class Bets extends Table {
  static defaultProps = {
    bets: [],
    sorting: [],
  };

  static propTypes = {
    changeSorting: propTypes.func.isRequired,
    fetchBetsStatuses: propTypes.func.isRequired,
    searchBets: propTypes.func.isRequired,
    sorting: propTypes.array,
  };

  componentDidMount = () => {
    this.props.fetchBetsStatuses();
  };

  switchFilters = grid => (<FilterCell
    userId={this.props.route.params.id}
    {...grid}
  />);

  useCustomCell = props => <CustomCell {...props} />;

  render = () => (
    <div className="events-container">
      {this.renderTable(
          COLUMNS(this.useCustomCell),
          this.props.bets,
          false,
          this.switchFilters,
          'User Bets',
      )}
    </div>
  );
}
const mapStateToProp = state => ({
  bets: filteredBets(state),
  sorting: betsSortingSelector(state),
});

const mapDispatchToProp = (dispatch, props) => (bindActionCreators({
  fetchData: () => searchBets(props.route.params.id),
  searchBets,
  changePagination,
  searchUserBets,
  changeSorting,
  fetchBetsStatuses,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(Bets);
