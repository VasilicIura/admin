import React from 'react';

export const RANGE_VALUES = [0, 5, 10, 20, 50, 100, 500, 1000, 10000];


export const COLUMNS = customCell => [
  {
    Header: 'Contest',
    accessor: 'contestName',
    Cell: row => customCell(row),
  },
  {
    Header: 'Bet status',
    accessor: 'betStatusPk',
    Cell: row => customCell(row),
  },
  {
    Header: 'Bet value',
    accessor: 'betValue',
    Cell: row => customCell(row),
  },
  {
    Header: 'Current points',
    accessor: 'currentPointsComputed',
    Cell: row => customCell(row),
  },
  {
    Header: 'Potential points',
    accessor: 'potentialPointsWhenPlaced',
    Cell: row => customCell(row),
  },
  {
    Header: 'Final points',
    accessor: 'finalPointsWhenResulted',
  },
  {
    Header: 'Bet Selections',
    accessor: 'selections',
    Cell: row => customCell(row),
  },
  {
    Header: 'Placed time',
    accessor: 'placedTimeStamp',
    Cell: row => customCell(row),
  },
  {
    Header: 'Actions',
    width: 130,
    accessor: 'actions',
    Filter: ({ onChange }) => (
      <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
    ),
    Cell: row => customCell(row),
  },
];

export const FILTER_TYPES = {
  IN: [
    'betStatusPk',
  ],
  RANGE: [
    'finalPointsWhenResulted',
    'currentPointsComputed',
    'potentialPointsWhenPlaced',
    'betValue',
  ],
  EQ: ['userId'],
};
