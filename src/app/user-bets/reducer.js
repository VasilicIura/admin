import { Map, List } from 'immutable';
import { handleActions } from 'redux-actions';
import { ACTIONS } from './actionTypes';

const initialState = Map({
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  filteredBets: Map({}),
  betsStatuses: List([]),
});

export const reducer = handleActions({
  [`${ACTIONS.SEARCH_BETS}_FULFILLED`]:
    (state, action) => state.set('filteredBets', Map(action.payload)),
  [ACTIONS.CHANGE_BETS_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [ACTIONS.SET_BETS_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
  [ACTIONS.CHANGE_BETS_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
  [`${ACTIONS.FETCH_BETS_STATUSES}_FULFILLED`]: (state, action) =>
    state.set('betsStatuses', List(action.payload)),
}, initialState);
