import { createAction } from 'redux-actions';
import { queryFilters, filterSelector } from 'fe-shared';
import { bettingApi, commonApi } from 'app/api';
import { filterCriteriasSelector } from './selectors';
import { ACTIONS } from './actionTypes';
import { FILTER_TYPES } from './consts';

const cancelBet = createAction(
  ACTIONS.CANCEL_BET,
  betId => bettingApi.post(`/bets/cancel/${betId}`, {}),
);

const fetchBetsStatuses = createAction(
  ACTIONS.FETCH_BETS_STATUSES,
  () => commonApi.get('enums/statuses/entity/Bet'),
);

const searchUserBets = createAction(
  ACTIONS.SEARCH_BETS,
  (criteria = []) => bettingApi.post('bets/search', { ...criteria }),
);

const changePagination = createAction(
  ACTIONS.CHANGE_BETS_PAGINATION,
  (pagination = {}) => pagination,
);

const setFilterCriterias = createAction(
  ACTIONS.SET_BETS_FILTERS,
  values => values,
);

const changeSorting = createAction(
  ACTIONS.CHANGE_BETS_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

const searchBets = userId =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'betFilters' });

    dispatch(setFilterCriterias(queryFilters({
      ...filterValues,
      userId: {
        value: userId,
      },
    }, FILTER_TYPES)));
    const newState = getState();
    dispatch(searchUserBets(filterCriteriasSelector(newState)));
  };

export {
  fetchBetsStatuses,
  searchUserBets,
  changePagination,
  changeSorting,
  searchBets,
  cancelBet,
};
