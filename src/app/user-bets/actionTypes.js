import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  SEARCH_BETS: null,
  SET_BETS_FILTERS: null,
  CHANGE_BETS_PAGINATION: null,
  FETCH_BETS_STATUSES: null,
  CHANGE_BETS_SORTING: null,
  CANCEL_BET: null,
});
