import { createSelector } from 'reselect';
import { toSelectArray } from 'fe-shared';

const betsStateSelector = state => state.bets;

const betStatusesSelector = createSelector(
  betsStateSelector,
  bets => toSelectArray(bets.get('betsStatuses').toJS()),
);

const filteredBets = createSelector(
  betsStateSelector,
  bets => bets.get('filteredBets').toJS(),
);

const filterCriteriasSelector = createSelector(
  betsStateSelector,
  criterias => criterias.get('criteria').toJS(),
);

const betsListSelector = createSelector(
  betsStateSelector,
  (bets) => {
    const list = bets.get('filteredBets').toJS();
    return list && list.content;
  },
);

const betsSortingSelector = createSelector(
  filterCriteriasSelector,
  ({ pageable }) => {
    if (pageable.hasOwnProperty('orders')) { //eslint-disable-line
      return [
        {
          columnName: pageable.orders[0].field,
          direction: pageable.orders[0].direction.toLowerCase(),
        },
      ];
    }

    return null;
  },
);

export {
  filteredBets,
  betsSortingSelector,
  betsListSelector,
  filterCriteriasSelector,
  betStatusesSelector,
};
