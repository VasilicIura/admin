import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { array, func } from 'prop-types';
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import { fetchMarketTypes, marketTypesSelector } from '../markets';

class MarketTypes extends Component {
  static propTypes = {
    fetchMarketTypes: func.isRequired,
    marketTypes: array.isRequired,
  };

  componentDidMount() {
    this.props.fetchMarketTypes();
  }

  render() {
    return (
      <div>
        <h3>Market Types</h3>
        <ListGroup>
          {this.props.marketTypes.map(market =>
            <ListGroupItem key={market.value}>{market.label}</ListGroupItem>)}
        </ListGroup>
      </div>
    );
  }
}

const mapStateToProp = state => ({
  marketTypes: marketTypesSelector(state),
});

const mapDispatchToProp = dispatch => (bindActionCreators({
  fetchMarketTypes,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(MarketTypes);
