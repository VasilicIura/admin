import { createSelector } from 'reselect';

const rolesStateSelector = state => state.roles;

const rolesSelectors = createSelector(
  rolesStateSelector,
  roles => roles.get('roles').toJS(),
);

const rolePermissionsSelector = createSelector(
  rolesStateSelector,
  rolePermissions => rolePermissions.get('rolePermissions').toJS(),
);

const permissionsListSelector = createSelector(
  rolesStateSelector,
  rolePermissions => rolePermissions.get('permissions').toJS(),
);

const restrictionsListSelector = createSelector(
  rolesStateSelector,
  roleRestrictions => roleRestrictions.get('restrictions').toJS(),
);

const roleRestrictionsSelector = createSelector(
  rolesStateSelector,
  rolePermissions => rolePermissions.get('roleRestrictions').toJS(),
);

const childRolesListSelector = createSelector(
  rolesStateSelector,
  childRoles => childRoles.get('childRoles').toJS(),
);

const childRolesSelector = createSelector(
  childRolesListSelector,
  (state, roleName) => roleName,
  (childRoles, roleName) => childRoles[roleName] || [],
);

export {
  rolesSelectors,
  rolePermissionsSelector,
  permissionsListSelector,
  restrictionsListSelector,
  roleRestrictionsSelector,
  childRolesSelector,
};
