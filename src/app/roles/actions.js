import { createAction } from 'redux-actions';
import { ROLE_ACTIONS } from './consts';
import { accountApi } from '../api';

const fetchRoleList = createAction(
  ROLE_ACTIONS.FETCH_ROLE_LIST,
  () => accountApi.get('roles'),
);

const addRole = createAction(
  ROLE_ACTIONS.ADD_ROLE,
  name => accountApi.post('roles', { name }),
);

const updateRole = createAction(
  ROLE_ACTIONS.EDIT_ROLE,
  (currentName, name) => accountApi.patch(`roles/${currentName}`, { name }),
);

const getPermissionsByRoleName = createAction(
  ROLE_ACTIONS.GET_PERMISSIONS_BY_ROLE_NAME,
  roleName => accountApi.get(`role-permissions/role-name/${roleName}`),
);

const addRolePermissions = createAction(
  ROLE_ACTIONS.ADD_ROLE_PERMISSIONS,
  (name, rolePk) => accountApi.post('role-permissions', { permissions: [{ name }], rolePk }),
);

const fetchPermissions = createAction(
  ROLE_ACTIONS.FETCH_PERMISSIONS_LIST,
  () => accountApi.get('permissions'),
);

const deleteRolePermissions = createAction(
  ROLE_ACTIONS.DELETE_ROLE_PERMISSIONS,
  (name, rolePk) => accountApi.delete('role-permissions', {
    data: {
      permissions: [{ name }], rolePk,
    },
  }),
);

const getChildRoles = createAction(
  ROLE_ACTIONS.FETCH_CHILD_ROLES,
  roleName => accountApi.get(`role-roles/${roleName}/children`)
    .then(childRoles => Promise.resolve({ roleName, childRoles })),
);

const addChildRole = createAction(
  ROLE_ACTIONS.ADD_CHILD_ROLE,
  (rolePk, childRolePk) => accountApi.post('role-roles', {
    rolePk,
    childRolePk,
  }),
);

const deleteChildRole = createAction(
  ROLE_ACTIONS.DELETE_CHILD_ROLES,
  (rolePk, childRolePk) => accountApi.delete(`role-roles/${rolePk}/child/${childRolePk}`),
);

const fetchRestrictions = createAction(
  ROLE_ACTIONS.FETCH_RESTRICTIONS_LIST,
  () => accountApi.get('restrictions'),
);

const addRoleRestriction = createAction(
  ROLE_ACTIONS.ADD_ROLE_RESTRICTIONS,
  (name, rolePk) => accountApi.post('role-restrictions', { restrictions: [{ name }], rolePk }),
);

const deleteRoleRestrictions = createAction(
  ROLE_ACTIONS.DELETE_ROLE_RESTRICTION,
  (name, rolePk) => accountApi.delete('role-restrictions', {
    data: {
      restrictions: [{ name }], rolePk,
    },
  }),
);

const getRestrictionByRoleName = createAction(
  ROLE_ACTIONS.GET_RESTRICTIONS_BY_ROLE_NAME,
  roleName => accountApi.get(`role-restrictions/role-name/${roleName}`),
);

const deleteRole = createAction(
  ROLE_ACTIONS.DELETE_ROLE,
  roleName => accountApi.delete(`roles/${roleName}`),
);

export {
  fetchRoleList,
  getPermissionsByRoleName,
  addRole,
  addRolePermissions,
  fetchPermissions,
  addRoleRestriction,
  fetchRestrictions,
  getRestrictionByRoleName,
  deleteRole,
  deleteRolePermissions,
  deleteRoleRestrictions,
  getChildRoles,
  addChildRole,
  deleteChildRole,
  updateRole,
};
