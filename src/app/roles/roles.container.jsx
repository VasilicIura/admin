import React, { Component } from 'react';
import ReactTable from 'react-table';
import { func, any, bool } from 'prop-types';
import { Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AddRole, HeaderRow, DetailRow } from './components';
import { fetchRoleList, getChildRoles } from './actions';
import { rolesSelectors } from './selectors';

class Roles extends Component {
  static propTypes = {
    fetchRoleList: func.isRequired,
    getChildRoles: func.isRequired,
    data: any,
    loading: bool,
  };
  static defaultProps = {
    data: [],
    loading: false,
  };

  componentDidMount() {
    this.props.fetchRoleList();
    this.props.getChildRoles();
  }

  getCustomCell = row => <HeaderRow {...row} />;

  expandDetailRow = name => <div style={{ padding: '20px' }}><DetailRow name={name} /></div>;

  COLUMNS = [
    {
      Header: () => 'Role',
      accessor: 'name',
      sortable: false,
      Cell: row => this.getCustomCell(row),
    },
    {
      Header: '',
      accessor: 'actions',
      sortable: false,
      Cell: row => this.getCustomCell(row),
    },
  ];

  subComponent = ({ original }) => this.expandDetailRow(original.name);

  render() {
    const { loading, data } = this.props;
    return (
      <Row>
        <h3>Roles</h3>
        <div style={{ marginBottom: '140px' }} >
          <ReactTable
            columns={this.COLUMNS}
            minRows={3}
            manual
            collapseOnDataChange={false}
            pageSize={data.length}
            showPagination={false}
            data={data}
            loading={loading}
            SubComponent={this.subComponent}
          />
        </div>
        <div className="add-btn-container">
          <AddRole />
        </div>
      </Row>
    );
  }
}

const mapStateToProps = state => ({ data: rolesSelectors(state) });
const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchRoleList,
  getChildRoles,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(Roles);
