import React, { Component } from 'react';
import { connect } from 'react-redux';
import { array } from 'prop-types';
import { UserChildRolesModal, DeleteChildRole } from './child-roles';
import { childRolesSelector } from '../../selectors';

class DetailRow extends Component {
  static defaultProps = {
    childRoles: [],
  };

  static propTypes = {
    childRoles: array,
  };

  render = () => {
    const { name, childRoles } = this.props;

    return (
      childRoles.length > 0 ? (
        <div colSpan="3" className="active">
          <div className="participants-list">
            <div className="participant-label">Roles</div>
            {
              childRoles.map(childRole => (
                <div className="list-item" key={childRole}>
                  <div className="sub-participant">
                    <span>{childRole}</span>
                    <div className="participants-team-btn">
                      <DeleteChildRole
                        childRole={childRole}
                        roleName={name}
                      />
                    </div>
                  </div>
                </div>
              ))
            }
            <UserChildRolesModal role={name} />
          </div>
        </div>
      ) : (
        <div colSpan="3">
          <div className="participants-list">
            <div className="participant-label">In this role we don't have childRoles</div>
            <UserChildRolesModal role={name} />
          </div>
        </div>
      )
    );
  };
}

const mapStateToProps = (state, props) => ({
  childRoles: childRolesSelector(state, props.name),
});

export default connect(mapStateToProps, null)(DetailRow);
