import React, { Component } from 'react';
import { connect } from 'react-redux';
import { func, array, any } from 'prop-types';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { Field, reduxForm } from 'redux-form';
import { Form, Button } from 'react-bootstrap';
import { toSelectArray, ControlSelect } from 'fe-shared';
import { fetchRoleList } from './../../../actions';
import { rolesSelectors, childRolesSelector } from './../../../selectors';

class UserChildRolesTemplate extends Component {
  static defaultProps = {
    childRoles: [],
    rolesList: [],
  };

  static propTypes = {
    fetchRoleList: func.isRequired,
    handleSubmit: func.isRequired,
    rolesList: array,
    childRoles: array,
    submitting: any.isRequired,
  };

  componentDidMount = () => {
    this.props.fetchRoleList();
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.childRoles !== this.props.childRoles) {
      this.listOfRolesOptions();
    }
  };

  listOfRolesOptions = () => {
    const filtered = this.props.rolesList
      .filter(e => this.props.childRoles.indexOf(e.name) === -1);

    return toSelectArray(filtered.map(e => e.name || e));
  };

  render = () => {
    const { rolesList } = this.props;
    return (
      <div>
        <Form onSubmit={this.props.handleSubmit}>
          <Field
            name="childRolePk"
            placeholder="Child roles"
            component={ControlSelect}
            options={
              (!!rolesList.length && this.listOfRolesOptions()) || []
            }
            label="Child roles"
          />
          <div className="submit-modal-btn">
            <Button
              type="submit"
              disabled={this.props.submitting}
              bsStyle="success"
            >
              Submit
            </Button>
          </div>
        </Form>
      </div>
    );
  };
}

const mapStateToProps = (state, props) => ({
  rolesList: rolesSelectors(state),
  childRoles: childRolesSelector(state, props.roleName),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchRoleList,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'user-child-roles',
  }),
)(UserChildRolesTemplate);
