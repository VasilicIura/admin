import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { getChildRoles, deleteChildRole } from './../../../../actions';

const DeleteChildRole = (props) => {
  const tooltip = (
    <Tooltip id="tooltip">
      <span>Delete user permission</span>
    </Tooltip>
  );

  const deleteRole = () => {
    const { roleName, childRole } = props;
    props.deleteChildRole(roleName, childRole)
      .then(() => props.getChildRoles(roleName));
  };

  return (
    <div style={{ float: 'left' }}>
      <OverlayTrigger placement="top" overlay={tooltip}>
        <Button
          className="circle-button"
          onClick={deleteRole}
        >
          <i className="mi mi-delete" />
        </Button>
      </OverlayTrigger>
    </div>
  );
};

DeleteChildRole.defaultProps = {
  roleName: null,
  childRole: null,
};

DeleteChildRole.propTypes = {
  deleteChildRole: propTypes.func.isRequired,
  roleName: propTypes.string,
  childRole: propTypes.string,
};

const mapDispatchToProps = dispatch => (bindActionCreators({
  getChildRoles,
  deleteChildRole,
}, dispatch));

export default connect(null, mapDispatchToProps)(DeleteChildRole);
