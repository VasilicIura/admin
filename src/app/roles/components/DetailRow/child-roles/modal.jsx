import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, string } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { addChildRole, getChildRoles } from '../../../actions';
import UserChildRolesTemplate from './child-roles-template';

export class UserChildRolesModal extends Component {
  static defaultProps = {
    role: null,
  };

  static propTypes = {
    addChildRole: func.isRequired,
    getChildRoles: func.isRequired,
    role: string,
  };

  state = {
    showModal: false,
  };

  toggle = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = ({ childRolePk }) => {
    this.props.addChildRole(this.props.role, childRolePk)
      .then(() => {
        this.props.getChildRoles(this.props.role);
      });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Add child roles</span>
      </Tooltip>
    );

    return (
      <div className="pull-right">
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button -big"
            onClick={this.toggle}
          >
            <i className="mi mi-add" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggle}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Add Child Role</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.showModal &&
              <UserChildRolesTemplate
                onSubmit={this.handleSubmit}
                roleName={this.props.role}
              />
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  addChildRole,
  getChildRoles,
}, dispatch));


export default connect(null, mapDispatchToProps)(UserChildRolesModal);
