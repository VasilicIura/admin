import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { getRestrictionByRoleName, deleteRoleRestrictions } from '../../../actions';

const DeleteRoleRestrictions = (props) => {
  const tooltip = (
    <Tooltip id="tooltip">
      <span>Delete Role Permission</span>
    </Tooltip>
  );

  const deleteUserRoleRestriction = () => {
    const { name, roleName } = props;
    props.deleteRoleRestrictions(name, roleName)
      .then(() => props.getRestrictionByRoleName(roleName));
  };

  return (
    <div style={{ float: 'left' }}>
      <OverlayTrigger placement="top" overlay={tooltip}>
        <Button
          className="circle-button"
          onClick={deleteUserRoleRestriction}
        >
          <i className="mi mi-delete" />
        </Button>
      </OverlayTrigger>
    </div>
  );
};

DeleteRoleRestrictions.defaultProps = {
  roleName: null,
  name: null,
};

DeleteRoleRestrictions.propTypes = {
  deleteRoleRestrictions: propTypes.func.isRequired,
  roleName: propTypes.string,
  name: propTypes.string,
};

const mapDispatchToProps = dispatch => (bindActionCreators({
  deleteRoleRestrictions,
  getRestrictionByRoleName,
}, dispatch));

export default connect(null, mapDispatchToProps)(DeleteRoleRestrictions);
