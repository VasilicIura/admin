import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, string } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import RolePermissionTemplate from './restrictions-template';
import { addRoleRestriction, getRestrictionByRoleName } from '../../../actions';

export class RoleRestrictionsModal extends Component {
  static defaultProps = {
    roleName: null,
  };

  static propTypes = {
    addRoleRestriction: func.isRequired,
    getRestrictionByRoleName: func.isRequired,
    roleName: string,
  };

  state = {
    showModal: false,
  };

  toggle = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = ({ name }) => {
    this.props.addRoleRestriction(name, this.props.roleName)
      .then(() => {
        this.props.getRestrictionByRoleName(this.props.roleName);
      });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Role Restrictions</span>
      </Tooltip>
    );

    return (
      <div style={{ float: 'left', marginRight: '10px' }}>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.toggle}
          >
            <i className="mi mi-block" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggle}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Role restrictions</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal &&
              <RolePermissionTemplate
                onSubmit={this.handleSubmit}
                roleName={this.props.roleName}
              />
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  addRoleRestriction,
  getRestrictionByRoleName,
}, dispatch));

export default connect(null, mapDispatchToProps)(RoleRestrictionsModal);
