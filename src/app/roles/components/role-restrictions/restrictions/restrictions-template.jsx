import React from 'react';
import { connect } from 'react-redux';
import { func, array, string, any } from 'prop-types';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { Field, reduxForm } from 'redux-form';
import { ListGroup, ListGroupItem, Form, Button } from 'react-bootstrap';
import { toSelectArray, ControlSelect, toSelectArrayFromJSON } from 'fe-shared';
import { DeleteRoleRestrictions } from '../delete';
import { getRestrictionByRoleName, fetchRestrictions } from '../../../actions';
import { restrictionsListSelector, roleRestrictionsSelector } from '../../../selectors';

class RolePermissionTemplate extends React.PureComponent {
  static defaultProps = {
    roleName: null,
    restrictions: [],
    roleRestrictionsList: [],
  };

  static propTypes = {
    getRestrictionByRoleName: func.isRequired,
    fetchRestrictions: func.isRequired,
    handleSubmit: func.isRequired,
    restrictions: array,
    roleRestrictionsList: array,
    roleName: string,
    submitting: any.isRequired,
  };

  componentDidMount = () => {
    this.props.getRestrictionByRoleName(this.props.roleName);
    this.props.fetchRestrictions();
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.restrictions !== this.props.restrictions) {
      this.listOfPermissionOptions();
    }
  };

  listOfPermissionOptions = () => {
    if (!this.props.roleRestrictionsList.length) {
      return toSelectArrayFromJSON(this.props.restrictions, 'name', 'name');
    }

    const assignedPermissions = this.props.roleRestrictionsList.map(e => e.name);

    const filtered = this.props.restrictions
      .filter(e => assignedPermissions.indexOf(e.name) === -1);

    return toSelectArray(filtered.map(e => e.name || e));
  };

  render = () => {
    const { restrictions } = this.props;
    return (
      <div>
        <Form onSubmit={this.props.handleSubmit}>
          <Field
            name="name"
            placeholder="Restriction name"
            component={ControlSelect}
            options={
              (!!restrictions.length && this.listOfPermissionOptions()) || []
            }
            label="Restriction name"
          />
          <div className="submit-modal-btn">
            <Button
              type="submit"
              disabled={this.props.submitting}
              bsStyle="success"
            >
              Submit
            </Button>
          </div>
        </Form>
        <ListGroup>
          {
            this.props.roleRestrictionsList.length ? (
                this.props.roleRestrictionsList
                  .map((roleRestrictions, index) => (
                    <ListGroupItem key={index} className="list-permissions">
                      <span className="permission-list-info">{roleRestrictions.name}</span>
                      <DeleteRoleRestrictions
                        name={roleRestrictions.name}
                        roleName={this.props.roleName}
                      />
                    </ListGroupItem>
                  ))
              ) : (
                'No Restrictions for this role...'
              )
          }
        </ListGroup>
      </div>
    );
  };
}

const mapStateToProps = state => ({
  restrictions: restrictionsListSelector(state),
  roleRestrictionsList: roleRestrictionsSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  getRestrictionByRoleName,
  fetchRestrictions,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'role-restrictions',
  }),
)(RolePermissionTemplate);
