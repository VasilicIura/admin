import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { getPermissionsByRoleName, deleteRolePermissions } from '../../../actions';

const DeleteRolePermission = (props) => {
  const tooltip = (
    <Tooltip id="tooltip">
      <span>Delete Role Permission</span>
    </Tooltip>
  );

  const deleteUserRolePermission = () => {
    const { name, roleName } = props;
    props.deleteRolePermissions(name, roleName)
      .then(() => props.getPermissionsByRoleName(roleName));
  };

  return (
    <div style={{ float: 'left' }}>
      <OverlayTrigger placement="top" overlay={tooltip}>
        <Button
          className="circle-button"
          onClick={deleteUserRolePermission}
        >
          <i className="mi mi-delete" />
        </Button>
      </OverlayTrigger>
    </div>
  );
};

DeleteRolePermission.defaultProps = {
  roleName: null,
  name: null,
};

DeleteRolePermission.propTypes = {
  deleteRolePermissions: propTypes.func.isRequired,
  roleName: propTypes.string,
  name: propTypes.string,
};

const mapDispatchToProps = dispatch => (bindActionCreators({
  deleteRolePermissions,
  getPermissionsByRoleName,
}, dispatch));

export default connect(null, mapDispatchToProps)(DeleteRolePermission);
