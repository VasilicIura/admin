import React from 'react';
import { connect } from 'react-redux';
import { func, array, string, any } from 'prop-types';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { Field, reduxForm } from 'redux-form';
import { ListGroup, ListGroupItem, Form, Button } from 'react-bootstrap';
import { toSelectArray, ControlSelect, toSelectArrayFromJSON } from 'fe-shared';
import { DeleteRolePermission } from '../delete';
import { getPermissionsByRoleName, fetchPermissions } from '../../../actions';
import { permissionsListSelector, rolePermissionsSelector } from '../../../selectors';

class RolePermissionTemplate extends React.PureComponent {
  static defaultProps = {
    roleName: null,
    permissions: [],
    rolePermissionsList: [],
  };

  static propTypes = {
    getPermissionsByRoleName: func.isRequired,
    fetchPermissions: func.isRequired,
    handleSubmit: func.isRequired,
    permissions: array,
    rolePermissionsList: array,
    roleName: string,
    submitting: any.isRequired,
  };

  componentDidMount = () => {
    this.props.getPermissionsByRoleName(this.props.roleName);
    this.props.fetchPermissions();
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.permissions !== this.props.permissions) {
      this.listOfPermissionOptions();
    }
  };

  listOfPermissionOptions = () => {
    if (!this.props.rolePermissionsList.length) {
      return toSelectArrayFromJSON(this.props.permissions, 'name', 'name');
    }

    const assignedPermissions = this.props.rolePermissionsList.map(e => e.name);

    const filtered = this.props.permissions
      .filter(e => assignedPermissions.indexOf(e.name) === -1);

    return toSelectArray(filtered.map(e => e.name || e));
  };

  render = () => {
    const { permissions } = this.props;

    return (
      <div>
        <Form onSubmit={this.props.handleSubmit} className="clear-fix">
          <Field
            name="name"
            placeholder="Permission Name"
            component={ControlSelect}
            options={
              (!!permissions.length && this.listOfPermissionOptions()) || []
            }
            label="Permission Name"
          />
          <div className="submit-modal-btn">
            <Button
              type="submit"
              disabled={this.props.submitting}
              bsStyle="success"
            >
              Submit
            </Button>
          </div>
        </Form>
        <ListGroup>
          {
            this.props.rolePermissionsList.length ? (
                this.props.rolePermissionsList
                  .map(rolePermission => (
                    <ListGroupItem key={rolePermission.name} className="list-permissions">
                      <span className="permission-list-info">{rolePermission.name}</span>
                      <DeleteRolePermission
                        name={rolePermission.name}
                        roleName={this.props.roleName}
                      />
                    </ListGroupItem>
                  ))
              ) : (
                'No Permissions for this role...'
              )
          }
        </ListGroup>
      </div>
    );
  };
}

const mapStateToProps = state => ({
  permissions: permissionsListSelector(state),
  rolePermissionsList: rolePermissionsSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  getPermissionsByRoleName,
  fetchPermissions,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'role-permissions',
  }),
)(RolePermissionTemplate);
