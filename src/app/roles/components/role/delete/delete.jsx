import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { deleteRole, fetchRoleList } from '../../../actions';

const DeleteRole = (props) => {
  const tooltip = (
    <Tooltip id="tooltip">
      <span>Delete Role</span>
    </Tooltip>
  );

  const deleteUserRole = () =>
    props.deleteRole(props.roleName)
      .then(() => props.fetchRoleList());

  return (
    <div style={{ float: 'left', marginRight: '10px' }}>
      <OverlayTrigger placement="top" overlay={tooltip}>
        <Button
          className="circle-button"
          onClick={deleteUserRole}
        >
          <i className="mi mi-delete" />
        </Button>
      </OverlayTrigger>
    </div>
  );
};

DeleteRole.defaultProps = {
  roleName: null,
};

DeleteRole.propTypes = {
  deleteRole: propTypes.func.isRequired,
  roleName: propTypes.string,
};

const mapDispatchToProps = dispatch => (bindActionCreators({
  deleteRole,
  fetchRoleList,
}, dispatch));

export default connect(null, mapDispatchToProps)(DeleteRole);
