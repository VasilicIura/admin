import React, { Component } from 'react';
import { object } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import RolePermissionsDetail from './detail-template';

export class RoleDetail extends Component {
  static defaultProps = {
    row: null,
  };

  static propTypes = {
    row: object,
  };

  state = {
    showModal: false,
  };

  toggle = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Role Permissions</span>
      </Tooltip>
    );

    return (
      <div style={{ float: 'left', marginRight: '10px' }}>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.toggle}
            style={{ float: 'left', marginRight: '15px' }}
          >
            <i className="mi mi-visibility" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggle}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Role permissions</Modal.Title>
          </Modal.Header>
          {this.state.showModal && <RolePermissionsDetail roleName={this.props.row.name} />}
        </Modal>
      </div>
    );
  }
}

export default RoleDetail;
