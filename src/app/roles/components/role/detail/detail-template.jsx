import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { func, array, any } from 'prop-types';
import { Modal, ListGroup, ListGroupItem } from 'react-bootstrap';
import { rolePermissionsSelector } from '../../../selectors';
import { getPermissionsByRoleName } from '../../../actions';

class RolePermissionsDetail extends Component {
  static defaultProps = {
    roleName: null,
    rolePermissions: [],
  };

  static propTypes = {
    getPermissionsByRoleName: func.isRequired,
    roleName: any,
    rolePermissions: array,
  };

  componentDidMount = () => this.props.getPermissionsByRoleName(this.props.roleName);

  render = () => (
    <Modal.Body>
      <div>
        <div style={{ marginTop: '0.5em' }}>
          <h3>Role Permissions</h3>
          <ListGroup>
            {
              this.props.rolePermissions
                .map((rolePermission, index) =>
                  <ListGroupItem key={index}>{rolePermission.name}</ListGroupItem>)
            }
          </ListGroup>
        </div>
      </div>
    </Modal.Body>
  );
}


const mapStateToProps = state => ({
  rolePermissions: rolePermissionsSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  getPermissionsByRoleName,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(RolePermissionsDetail);
