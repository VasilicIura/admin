export const validate = (values) => {
  const errors = {};
  const { name } = values;

  if (!name) {
    errors.name = 'Role name is required';
  }

  return errors;
};
