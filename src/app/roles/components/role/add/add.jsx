import React, { Component } from 'react';
import { func, any } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Form, Modal, Button } from 'react-bootstrap';
import { compose } from 'recompose';
import { Field, reduxForm } from 'redux-form';
import { NotificationManager } from 'react-notifications';
import { ControlInput } from 'fe-shared';
import { addRole, getPermissionsByRoleName, fetchRoleList } from '../../../actions';
import { validate } from '../edit/validate';

class AddRole extends Component {
  static defaultProps = {
    handleSubmit: null,
    submitting: null,
  };

  static propTypes = {
    addRole: func.isRequired,
    getPermissionsByRoleName: func.isRequired,
    handleSubmit: any,
    submitting: any,
    fetchRoleList: func.isRequired,
  };

  state = {
    showModal: false,
  };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = ({ name }) => {
    this.props.addRole(name)
      .then(() => {
        NotificationManager.success(`Role ${name} was successful created`);
        this.props.getPermissionsByRoleName(name);
      })
      .then(this.props.fetchRoleList)
      .then(this.toggleModal)
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render = () => {
    const {
      handleSubmit,
      submitting,
    } = this.props;

    return (
      <div>
        <Button
          className="add-button"
          onClick={this.toggleModal}
        >
          <i className="mi mi-add" />
        </Button>

        <Modal show={this.state.showModal} onHide={this.toggleModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Add Role</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={handleSubmit(this.handleSubmit)}>
              <Field
                name="name"
                placeholder="Role name"
                type="text"
                component={ControlInput}
                label="Role Name:"
              />
              <div className="submit-modal-btn">
                <Button
                  type="submit"
                  disabled={submitting}
                  bsStyle="success"
                >
                  Submit
                </Button>
              </div>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    );
  };
}


const mapDispatchToProp = dispatch => (bindActionCreators({
  addRole,
  getPermissionsByRoleName,
  fetchRoleList,
}, dispatch));

export default compose(
  connect(null, mapDispatchToProp),
  reduxForm({
    form: 'role',
    validate,
  }),
)(AddRole);
