import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, Button, OverlayTrigger, Tooltip, Form } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { ControlInput } from 'fe-shared';
import { compose } from 'recompose';
import { func, object, any } from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { updateRole, fetchRoleList } from '../../../actions';
import { validate } from './validate';

class EditRole extends Component {
  static defaultProps = {
    role: null,
    handleSubmit: null,
    submitting: null,
  };

  static propTypes = {
    role: object,
    handleSubmit: any,
    submitting: any,
    updateRole: func.isRequired,
    fetchRoleList: func.isRequired,
  };

  state = {
    showModal: false,
  };

  handleSubmit = ({ name }) => {
    this.props.updateRole(this.props.role.name, name)
      .then(() => {
        NotificationManager.info(`Role ${name} was successful updated`);
        this.props.fetchRoleList(name);
      })
      .then(this.toggleModal)
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Update role</span>
      </Tooltip>
    );

    const {
      handleSubmit,
      submitting,
      role,
    } = this.props;

    return (
      <div style={{ float: 'left', marginRight: '10px' }}>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            style={{ display: 'inline-block' }}
            onClick={this.toggleModal}
          >
            <i className="mi mi-create" />
          </Button>
        </OverlayTrigger>

        <Modal
          show={this.state.showModal}
          onHide={this.toggleModal}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Update role name</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={handleSubmit(this.handleSubmit)}>
              <Field
                name="name"
                placeholder="Role name"
                type="text"
                component={ControlInput}
                label="Role Name:"
                currentValue={role ? role.name : ''}
              />
              <div className="submit-modal-btn">
                <Button
                  type="submit"
                  disabled={submitting}
                  bsStyle="success"
                >
                  Submit
                </Button>
              </div>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProp = dispatch => (bindActionCreators({
  updateRole,
  fetchRoleList,
}, dispatch));

export default compose(
  connect(null, mapDispatchToProp),
  reduxForm({
    form: 'update-role',
    validate,
  }),
)(EditRole);
