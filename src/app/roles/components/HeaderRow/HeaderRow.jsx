import React from 'react';
import propTypes from 'prop-types';
import { RoleDetail, DeleteRole, EditRole } from './../role';
import { RolePermissionsModal } from './../role-permission';
import { RoleRestrictionsModal } from './../role-restrictions';

const HeaderRow = (props) => {
  switch (props.column.id) {
    case 'actions':
      return (
        <div >
          <div style={{ float: 'right' }}>
            <RoleDetail
              {...props}
            />
            <EditRole role={props.original} />
            <RolePermissionsModal
              roleName={props.original.name}
            />
            <RoleRestrictionsModal
              roleName={props.original.name}
            />
            <DeleteRole roleName={props.original.name} />
          </div>
        </div>
      );
    default:
      return (
        <div>
          <div className="participants-team">
            <div className="participants-team-info">
              <span>Role: </span>
              <strong>{props.value}1</strong>
            </div>
          </div>
        </div>
      );
  }
};

HeaderRow.defaultProps = {
  value: null,
  original: null,
};

HeaderRow.propTypes = {
  value: propTypes.string,
  original: propTypes.object,
  column: propTypes.object.isRequired,
};

export default HeaderRow;
