import { handleActions } from 'redux-actions';
import { Map, List } from 'immutable';
import { ROLE_ACTIONS } from './consts';

const initialState = Map({
  roles: List([]),
  rolePermissions: List([]),
  roleRestrictions: List([]),
  permissions: List([]),
  restrictions: List([]),
  childRoles: Map({}),
});

export const reducer = handleActions({
  [`${ROLE_ACTIONS.FETCH_ROLE_LIST}_FULFILLED`]:
    (state, action) => state.set('roles', List(action.payload)),
  [`${ROLE_ACTIONS.GET_PERMISSIONS_BY_ROLE_NAME}_FULFILLED`]:
    (state, action) => state.set('rolePermissions', List(action.payload.permissions)),
  [`${ROLE_ACTIONS.FETCH_PERMISSIONS_LIST}_FULFILLED`]:
    (state, action) => state.set('permissions', List(action.payload)),
  [`${ROLE_ACTIONS.FETCH_RESTRICTIONS_LIST}_FULFILLED`]:
    (state, action) => state.set('restrictions', List(action.payload)),
  [`${ROLE_ACTIONS.FETCH_CHILD_ROLES}_FULFILLED`]:
    (state, action) => state.setIn(['childRoles', [action.payload.roleName]], List(action.payload.childRoles)),
  [`${ROLE_ACTIONS.GET_RESTRICTIONS_BY_ROLE_NAME}_FULFILLED`]:
    (state, action) => state.set('roleRestrictions', List(action.payload.restrictions)),
}, initialState);
