import React from 'react';
import { func, bool, any } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { CustomCellTemplate, UsersSwitchFilterCell } from './components/users/components';
import Table from '../common/table';
import users from './components/users';

class UserManagement extends Table {
  static propTypes = {
    fetchData: func.isRequired,
    changePagination: func.isRequired,
    changeSorting: func.isRequired,
    loading: bool,
    data: any,
  };

  static defaultProps = {
    data: [],
    loading: false,
  };

  COLUMNS = [
    {
      Header: 'First Name',
      accessor: 'firstName',
    },
    {
      Header: 'Last Name',
      accessor: 'lastName',
    },
    { Header: 'Email', accessor: 'email' },
    {
      Header: 'Username',
      accessor: 'username',
    },
    {
      Header: 'Date of Birth',
      maxWidth: 100,
      accessor: 'dateOfBirth',
    },
    {
      Header: 'Current Balance',
      sortable: false,
      accessor: 'walletBalance',
      Cell: row => <div>{this.useCustomCell(row)}</div>,
    },
    {
      Header: 'Transactions',
      sortable: false,
      maxWidth: 110,
      accessor: 'transactions',
      Cell: row => <div>{this.useCustomCell(row)}</div>,
    },
    {
      Header: '',
      accessor: 'actions',
      sortable: false,
      Filter: ({ onChange }) => (
        <button onClick={() => onChange()} style={{ margin: '6px' }}>Filter</button>
      ),
      Cell: row => <div>{this.useCustomCell(row)}</div>,
    },
  ];

  switchFilters = grid => <UsersSwitchFilterCell {...grid} />;
  useCustomCell = props => <CustomCellTemplate {...props} />;

  render() {
    const { data, loading } = this.props;
    return (
      <div>
        {this.renderTable(this.COLUMNS, data, loading, this.switchFilters, 'Users')}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  data: users.selectors.filteredUsers(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchData: () => users.actions.searchUsers(),
  changePagination: data => users.actions.changePagination(data),
  changeSorting: data => users.actions.changeSorting(data),
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(UserManagement);
