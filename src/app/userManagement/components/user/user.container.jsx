import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './actions';
import * as selectors from './selectors';
import User from './user';
import users from '../users';

class UserContainer extends Component {
  onBack = () => {
    if (this.props.selectedOption) {
      this.props.selectOption(null);
      return;
    }

    this.props.selectUser(null);
  };

  render = () => (
    <User
      {...this.props}
      onBack={this.onBack}
    />
  );
}

const mapStateToProp = state => ({
  selectedOption: selectors.getSelectedOption(state),
});

const mapDispatchToProp = dispatch => (bindActionCreators({
  selectOption: actions.setOption,
  selectUser: users.actions.selectUser,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(UserContainer);
