import { createAction } from 'redux-actions';
import * as ACTIONS from './actionTypes';

export const setOption = createAction(
  ACTIONS.SELECT_OPTION,
  option => option,
);
