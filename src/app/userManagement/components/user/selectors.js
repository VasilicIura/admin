import { createSelector } from 'reselect';

const userStateSelector = state => state.userManagement.user;

export const getSelectedOption = createSelector(
  userStateSelector,
  state => state.get('selectedOption'),
);
