import React from 'react';
import { Row, Col, Button, Glyphicon } from 'react-bootstrap';
import { UserMenu } from './components';
import { Transactions } from '../../../transactions';
import { menuOption } from './consts';
import './user.scss';

const renderSelectedComponent = (selectedOption, user) => {
  switch (selectedOption) {
    case menuOption.transactions:
      return <Transactions user={user} />;
    default:
      return <UserMenu user={user} />;
  }
};

const User = ({ user, selectedOption, onBack }) => (
  <Row>
    <Col>
      <h3>User {user.username}</h3>
      <Button bsStyle="primary" style={{ marginBottom: '10px' }} onClick={onBack}>
        <Glyphicon glyph="chevron-left" /> Back
      </Button>

      { renderSelectedComponent(selectedOption, user) }

    </Col>
  </Row>
);

export default User;
