import React from 'react';
import propTypes from 'prop-types';
import { menuOption } from '../consts';
import { MenuLink } from './';

export const UserMenu = ({ user }) => (
  <div>
    <div>
      <p>Username: {user.username}</p>
      <p>First Name: {user.firstName}</p>
      <p>Last Name: {user.lastName}</p>
      <div className="user-management-menu" >
        <MenuLink
          label="Permissions"
          glyph="eye-close"
          option={menuOption.transactions}
        />
        <MenuLink
          label="Transactions"
          glyph="transfer"
          option={menuOption.transactions}
        />
        <MenuLink
          label="Option"
          glyph="asterisk"
          option={menuOption.transactions}
        />
        <MenuLink
          label="Option"
          glyph="asterisk"
          option={menuOption.transactions}
        />
        <MenuLink
          label="Option"
          glyph="asterisk"
          option={menuOption.transactions}
        />
      </div>
    </div>
  </div>
);

UserMenu.defaultProps = {
  user: null,
};

UserMenu.propTypes = {
  user: propTypes.object,
};
