import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Glyphicon } from 'react-bootstrap';
import * as actions from '../actions';

const MenuLink = props => (
  <a onClick={() => props.selectOption(props.option)}>
    <Glyphicon glyph={props.glyph} /> {props.label}
  </a>
);

const mapDispatchToProp = dispatch => (bindActionCreators({
  selectOption: actions.setOption,
}, dispatch));

export default connect(null, mapDispatchToProp)(MenuLink);
