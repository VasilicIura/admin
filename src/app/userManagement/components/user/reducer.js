import { Map } from 'immutable';
import { handleActions } from 'redux-actions';
import * as ACTIONS from './actionTypes';
import users from '../users';

const initialState = Map({
  selectedOption: null,
});

export const reducer = handleActions({
  [ACTIONS.SELECT_OPTION]: (state, action) =>
    state.set('selectedOption', action.payload),
  [users.actionTypes.SELECT_USER]: () => initialState,
}, initialState);
