import { Map, List } from 'immutable';
import { handleActions } from 'redux-actions';
import * as ACTIONS from './actionTypes';

const initialState = Map({
  users: List([]),
  selectedUser: null,
  userDetails: Map({}),
  permissions: List([]),
  restrictions: List([]),
  roles: List([]),
  criteria: Map({
    criteria: List([]),
    pageable: Map({}),
  }),
  filteredUsers: Map({}),
  userPermissions: List([]),
  userRestrictions: List([]),
  userRoles: List([]),
  unpackedPermissionsList: List([]),
});

export const reducer = handleActions({
  [ACTIONS.SET_USERS_FILTERS]: (state, action) =>
    state.setIn(['criteria', 'criteria'], List(action.payload)),
  [ACTIONS.CHANGE_USERS_PAGINATION]: (state, action) =>
    state.setIn(['criteria', 'pageable'], Map(action.payload)),
  [ACTIONS.CHANGE_USERS_SORTING]: (state, action) =>
    state.setIn(['criteria', 'pageable', 'orders'], List(action.payload)),
  [`${ACTIONS.SEARCH_USERS}_FULFILLED`]:
    (state, action) => state.set('filteredUsers', Map(action.payload)),
  [ACTIONS.SELECT_USER]: (state, action) =>
    state.set('selectedUser', action.payload),
  [`${ACTIONS.FETCH_USER_DY_ID}_FULFILLED`]: (state, action) =>
    state.set('userDetails', Map(action.payload)),
  [`${ACTIONS.FETCH_PERMISSIONS}_FULFILLED`]: (state, action) =>
    state.set('permissions', List(action.payload)),
  [`${ACTIONS.FETCH_RESTRICTIONS}_FULFILLED`]: (state, action) =>
    state.set('restrictions', List(action.payload)),
  [`${ACTIONS.FETCH_USER_PERMISSIONS}_FULFILLED`]: (state, action) =>
    state.set('userPermissions', List(action.payload)),
  [`${ACTIONS.FETCH_USER_RESTRICTIONS}_FULFILLED`]: (state, action) =>
    state.set('userRestrictions', List(action.payload)),
  [`${ACTIONS.UNPACKED_PARMISSIONS_LIST}_FULFILLED`]: (state, action) =>
    state.set('unpackedPermissionsList', List(action.payload)),
  [`${ACTIONS.FETCH_USER_ROLES}_FULFILLED`]: (state, action) =>
    state.set('userRoles', List(action.payload)),
  [`${ACTIONS.FETCH_ROLES}_FULFILLED`]: (state, action) =>
    state.set('roles', List(action.payload)),
}, initialState);

// ACTIONS.UNPACKED_PARMISSIONS_LIST,
