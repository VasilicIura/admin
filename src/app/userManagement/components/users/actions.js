import { createAction } from 'redux-actions';
import { accountApi, walletApi } from 'app/api';
import { filterSelector, queryFilters } from 'fe-shared';
import { filterCriteriasSelector } from './selectors';
import { FILTER_TYPES } from './consts';
import * as ACTIONS from './actionTypes';

const fetchUsers = createAction(
  ACTIONS.FETCH_USERS,
  () => accountApi.get('users'),
);

const selectUser = createAction(
  ACTIONS.SELECT_USER,
  user => user,
);

const updateUser = createAction(
  ACTIONS.UPDATE_USER,
  ({ id, ...user }) => accountApi.patch(`users/${id}`, { id, ...user }),
);

const fetchPermissions = createAction(
  ACTIONS.FETCH_PERMISSIONS,
  () => accountApi.get('permissions'),
);

const addUserPermission = createAction(
  ACTIONS.ADD_USER_PERMISSION,
  (userId, permissionPk) => accountApi.post('user-permissions', {
    permissionPk,
    userId,
  }),
);

const fetchAllUnpackedPermissions = createAction(
  ACTIONS.UNPACKED_PARMISSIONS_LIST,
  userId => accountApi.get(`unpacked-authorities/${userId}`),
);

const fetchRestrictions = createAction(
  ACTIONS.FETCH_RESTRICTIONS,
  () => accountApi.get('restrictions'),
);

const fetchUserRestrictions = createAction(
  ACTIONS.FETCH_USER_RESTRICTIONS,
  userId => accountApi.get(`user-restrictions/${userId}`),
);

const addUserRestriction = createAction(
  ACTIONS.ADD_USER_RESTRICTION,
  (userId, restrictionPk) => accountApi.post('user-restrictions', {
    restrictionPk,
    userId,
  }),
);

const deleteUserRestriction = createAction(
  ACTIONS.DELETE_USER_RESTRICTION,
  (userId, restrictionName) =>
    accountApi.delete(`user-restrictions/${userId}/restriction/${restrictionName}`),
);

const fetchRoles = createAction(
  ACTIONS.FETCH_ROLES,
  () => accountApi.get('roles'),
);

const fetchUserRoles = createAction(
  ACTIONS.FETCH_USER_ROLES,
  userId => accountApi.get(`/user-roles/${userId}`),
);

const addRole = createAction(
  ACTIONS.ADD_ROLE,
  name => accountApi.post('roles', { name }),
);

const addUserRole = createAction(
  ACTIONS.ADD_USER_ROLE,
  (userId, rolePk) => accountApi.post('user-roles', {
    rolePk,
    userId,
  }),
);

const deleteUserRole = createAction(
  ACTIONS.DELETE_USER_ROLE,
  (userId, rolePk) => accountApi.delete(`user-roles/${userId}/role/${rolePk}`),
);

const addRolePermissions = createAction(
  ACTIONS.ADD_USER_ROLE_PERMISSIONS,
  (permissions, rolePk) => accountApi.post('role-permissions', { permissions, rolePk }),
);

const deleteUserPermission = createAction(
  ACTIONS.DELETE_USER_PERMISSION,
  (userId, permissionName) => accountApi.delete(`user-permissions/${userId}/permission/${permissionName}`),
);

const fetchUserById = createAction(
  ACTIONS.FETCH_USER_DY_ID,
  id => accountApi.get(`users/${id}`),
);

const fetchUserPermissions = createAction(
  ACTIONS.FETCH_USER_PERMISSIONS,
  id => accountApi.get(`user-permissions/${id}`),
);

function transferFunds(userId, data) {
  return () => walletApi.post(`admin/transfer/${userId}`, data);
}

function fetchBalanceRecords(user, requestedPage, pageSize) {
  const page = requestedPage || 0;
  const size = pageSize || 10;
  return () => walletApi.post(`report/balanceRecords/${user.id}`, {
    pageable: {
      size,
      page,
    },
  });
}

const changePagination = createAction(
  ACTIONS.CHANGE_USERS_PAGINATION,
  (pagination = {}) => pagination,
);

const changeSorting = createAction(
  ACTIONS.CHANGE_USERS_SORTING,
  ([sorting]) => ([
    {
      field: sorting.id,
      direction: sorting.desc ? 'DESC' : 'ASC',
    },
  ]),
);

const autocompleteUsers = createAction(
  ACTIONS.AUTOCOMPLETE_USERS_FILTER,
  (criteria = []) => accountApi.post('users/search', { ...criteria }),
);

const setFilterCriterias = createAction(
  ACTIONS.SET_USERS_FILTERS,
  values => values,
);

const filterUsers = createAction(
  ACTIONS.SEARCH_USERS,
  (criteria = []) => accountApi.post('users/search', { ...criteria }),
);

const searchUsers = () =>
  (dispatch, getState) => {
    const state = getState();
    const filterValues = filterSelector(state, { filter: 'usersFilter' });
    dispatch(setFilterCriterias(queryFilters(filterValues, FILTER_TYPES)));
    const newState = getState();
    dispatch(filterUsers(filterCriteriasSelector(newState)));
  };

export {
  fetchUsers,
  selectUser,
  updateUser,
  fetchUserById,
  fetchPermissions,
  fetchUserPermissions,
  fetchUserRestrictions,
  fetchUserRoles,
  addUserPermission,
  addUserRestriction,
  fetchRestrictions,
  fetchRoles,
  addRole,
  addRolePermissions,
  addUserRole,
  deleteUserPermission,
  deleteUserRestriction,
  deleteUserRole,
  transferFunds,
  fetchBalanceRecords,
  fetchAllUnpackedPermissions,
  searchUsers,
  changePagination,
  setFilterCriterias,
  autocompleteUsers,
  changeSorting,
};
