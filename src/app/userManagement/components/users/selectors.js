import { createSelector } from 'reselect';

const usersStateSelector = state => state.userManagement.users;

export const usersSelector = createSelector(
  usersStateSelector,
  users => users.get('users').toJS(),
);

export const selectedUser = createSelector(
  usersStateSelector,
  users => users.get('selectedUser') && users.get('selectedUser'),
);

export const userDetails = createSelector(
  usersStateSelector,
  users => users.get('userDetails').toJS(),
);

export const permissions = createSelector(
  usersStateSelector,
  users => users.get('permissions').toJS(),
);

export const restrictions = createSelector(
  usersStateSelector,
  users => users.get('restrictions').toJS(),
);

export const userPermissions = createSelector(
  usersStateSelector,
  users => users.get('userPermissions').toJS(),
);

export const userRestrictions = createSelector(
  usersStateSelector,
  users => users.get('userRestrictions').toJS(),
);

export const userRoles = createSelector(
  usersStateSelector,
  users => users.get('userRoles').toJS(),
);

export const unpackedUserPermissions = createSelector(
  usersStateSelector,
  users => users.get('unpackedPermissionsList').toJS(),
);

export const roles = createSelector(
  usersStateSelector,
  users => users.get('roles').toJS(),
);

export const filterCriteriasSelector = createSelector(
  usersStateSelector,
  criterias => criterias.get('criteria').toJS(),
);

export const usersListSelector = createSelector(
  usersStateSelector,
  (users) => {
    const list = users.get('filteredUsers').toJS();
    return list && list.content;
  },
);

export const usersSortingSelector = createSelector(
  filterCriteriasSelector,
  ({ pageable }) => {
    if (pageable.hasOwnProperty('orders')) { //eslint-disable-line
      return [
        {
          columnName: pageable.orders[0].field,
          direction: pageable.orders[0].direction.toLowerCase(),
        },
      ];
    }

    return null;
  },
);

export const filteredUsers = createSelector(
  usersStateSelector,
  users => users.get('filteredUsers').toJS(),
);
