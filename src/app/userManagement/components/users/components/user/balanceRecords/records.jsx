import React from 'react';
import { func, object } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BalanceRecordsTable from 'app/common/balanceRecordsTable';
import { fetchBalanceRecords } from '../../../actions';
import { CURRENCIES } from '../../../../../../consts';


class Records extends React.Component {
  static propTypes = {
    fetchBalanceRecords: func,
    user: object,
  };

  static defaultProps = {
    fetchBalanceRecords: null,
    user: null,
  };


  state = {
    balanceRecords: null,
  };

  componentDidMount = () => {
    this.props.fetchBalanceRecords(this.props.user)
      .then(response => this.setState({ balanceRecords: response }));
  };

  onPageChange = (page, pageSize) => {
    this.props.fetchBalanceRecords(this.props.user, page, pageSize)
      .then(response => this.setState({ balanceRecords: response }));
  };

  render = () => {
    const data = this.state.balanceRecords;
    const hasValues = data
      && data.content
      && data.content.length;
    return (
      <div>
        {this.props.user.walletBalance &&
          <div>
            <b>Current Balance: </b>
            {CURRENCIES[this.props.user.walletBalance.currency]}
            {this.props.user.walletBalance.balance.toFixed(2)}
          </div>
        }
        {hasValues ?
          <BalanceRecordsTable
            data={data}
            onPageChange={this.onPageChange}
          />
        : <div>This user has no balance records.</div>
        }
      </div>
    );
  };
}

const mapDispatchToProp = dispatch => (bindActionCreators({
  fetchBalanceRecords,
}, dispatch));

export default connect(null, mapDispatchToProp)(Records);
