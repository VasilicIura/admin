import React, { Component } from 'react';
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { object } from 'prop-types';
import Records from './records';

class BalanceRecords extends Component {
  static propTypes = {
    user: object.isRequired,
  };

  state = {
    showModal: false,
  };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Balance Records</span>
      </Tooltip>
    );

    return (
      <div style={{ float: 'left', marginRight: '10px' }}>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            style={{ display: 'inline-block', float: 'left' }}
            onClick={this.toggleModal}
          >
            <i className="mi mi-account-balance" />
          </Button>
        </OverlayTrigger>

        <Modal
          show={this.state.showModal}
          onHide={this.toggleModal}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Balance Records</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Records user={this.props.user} />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default BalanceRecords;
