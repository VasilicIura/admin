import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { object, func, number } from 'prop-types';
import moment from 'moment';
import { Modal, ListGroup, ListGroupItem } from 'react-bootstrap';
import { userDetails } from '../../../selectors';
import { fetchUserById } from '../../../actions';

class UserDetailTemplate extends Component {
  static defaultProps = {
    user: null,
    id: null,
  };

  static propTypes = {
    user: object,
    fetchUserById: func.isRequired,
    id: number,
  };

  componentDidMount = () => this.props.fetchUserById(this.props.id);

  render = () => {
    const { user } = this.props;
    return (
      <Modal.Body>
        <div>

          <div style={{ marginTop: '0.5em' }}>
            <h3>User information</h3>
            <ListGroup>
              <ListGroupItem>
                <span style={{ fontWeight: 'bold' }}>Username:</span> {user.username}
              </ListGroupItem>
              <ListGroupItem>
                <span style={{ fontWeight: 'bold' }}>First name:</span> {user.firstName}
              </ListGroupItem>
              <ListGroupItem>
                <span style={{ fontWeight: 'bold' }}>Last name:</span> {user.lastName}
              </ListGroupItem>
              <ListGroupItem>
                <span style={{ fontWeight: 'bold' }}>Date of birth:</span> {moment(user.dateOfBirth).format('YYYY-MM-DD')}
              </ListGroupItem>
              <ListGroupItem>
                <span style={{ fontWeight: 'bold' }}>Phone number:</span> +{user.phoneCountryExtPk} {user.phoneNumber}
              </ListGroupItem>
              <ListGroupItem>
                <span style={{ fontWeight: 'bold' }}>Email:</span> {user.email}
              </ListGroupItem>
              <ListGroupItem>
                <span style={{ fontWeight: 'bold' }}>Currency:</span> {user.userDetails ? user.userDetails.currency : 'No details'}
                <br />
                <span style={{ fontWeight: 'bold' }}>Wallet id:</span> {user.userDetails ? user.userDetails.walletId : 'No details'}
              </ListGroupItem>
              <ListGroupItem>
                {user.userDetails && user.userDetails.address
                  ? (
                    <div>
                      <span style={{ fontWeight: 'bold' }}>Country:</span> {user.userDetails.address.countryPk}
                      <br />
                      <span style={{ fontWeight: 'bold' }}>City:</span> {user.userDetails.address.city}
                      <br />
                      <span style={{ fontWeight: 'bold' }}>County:</span> {user.userDetails.address.county}
                      <br />
                      <span style={{ fontWeight: 'bold' }}>Street:</span> {user.userDetails.address.street}
                      <br />
                      <span style={{ fontWeight: 'bold' }}>Zip Code:</span> {user.userDetails.address.zipCode}
                    </div>
                  )
                  : <span style={{ fontWeight: 'bold' }}>No Address Information.</span>}
              </ListGroupItem>
            </ListGroup>
          </div>
        </div>
      </Modal.Body>
    );
  };
}


const mapStateToProps = state => ({
  user: userDetails(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchUserById,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(UserDetailTemplate);
