import React, { Component } from 'react';
import { number } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import UserDetailTemplate from './detail-template';

export class UserDetail extends Component {
  static defaultProps = {
    id: null,
  };

  static propTypes = {
    id: number,
  };

  state = {
    showModal: false,
  };

  toggle = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>View User information</span>
      </Tooltip>
    );

    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.toggle}
            style={{ float: 'left', marginRight: '10px' }}
          >
            <i className="mi mi-visibility" />
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggle}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">User information</Modal.Title>
          </Modal.Header>
          {this.state.showModal && <UserDetailTemplate id={this.props.id} />}
        </Modal>
      </div>
    );
  }
}

export default UserDetail;
