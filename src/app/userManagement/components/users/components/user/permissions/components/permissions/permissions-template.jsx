import React from 'react';
import { connect } from 'react-redux';
import { func, array, number, any } from 'prop-types';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { Field, reduxForm } from 'redux-form';
import { ListGroup, ListGroupItem, Form, Button } from 'react-bootstrap';
import { toSelectArray, toSelectArrayFromJSON, ControlSelect } from 'fe-shared';
import { DeleteUserRolePermission } from './delete';
import { fetchUserPermissions, fetchPermissions } from '../../../../../actions';
import { userPermissions, permissions } from '../../../../../selectors';

class PermissionTemplate extends React.PureComponent {
  static defaultProps = {
    userId: null,
    permissions: [],
    permissionsList: [],
  };

  static propTypes = {
    fetchUserPermissions: func.isRequired,
    fetchPermissions: func.isRequired,
    handleSubmit: func.isRequired,
    permissions: array,
    permissionsList: array,
    userId: number,
    submitting: any.isRequired,
  };

  componentDidMount = () => {
    this.props.fetchUserPermissions(this.props.userId);
    this.props.fetchPermissions();
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.permissions !== this.props.permissions) {
      this.listOfPermissionOptions();
    }
  };

  listOfPermissionOptions = () => {
    if (!this.props.permissionsList.length) {
      return toSelectArrayFromJSON(this.props.permissionsList, 'name', 'name');
    }

    const assignedPermissions = this.props.permissions.map(e => e.permissionPk);

    const filtered = this.props.permissionsList
      .filter(e => assignedPermissions.indexOf(e.name) === -1);

    return toSelectArray(filtered.map(e => e.name || e));
  };

  render = () => (
    <div>
      <Form onSubmit={this.props.handleSubmit}>
        <Field
          name="permissionPk"
          placeholder="Permission"
          component={ControlSelect}
          options={
            (!!this.props.permissionsList.length && this.listOfPermissionOptions()) || []
          }
          label="Permission"
        />
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={this.props.submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
      <ListGroup>
        {
          this.props.permissions
            .map((permission, index) => (
              <ListGroupItem
                key={index}
                className="list-permissions"
              >
                <span className="permission-list-info">{permission.permissionPk}</span>
                <DeleteUserRolePermission
                  name={permission.permissionPk}
                  userId={this.props.userId}
                />
              </ListGroupItem>
            ))
        }
      </ListGroup>
    </div>
  );
}


const mapStateToProps = state => ({
  permissions: userPermissions(state),
  permissionsList: permissions(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchUserPermissions,
  fetchPermissions,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'user-permissions',
  }),
)(PermissionTemplate);
