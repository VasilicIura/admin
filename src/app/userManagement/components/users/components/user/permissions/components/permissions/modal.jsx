import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { number } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import PermissionTemplate from './permissions-template';
import { addUserPermission, fetchUserPermissions } from '../../../../../actions';

export class UserPermissionsModal extends Component {
  static defaultProps = {
    userId: null,
  };

  static propTypes = {
    userId: number,
  };

  state = {
    showModal: false,
  };

  toggle = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Permissions</span>
      </Tooltip>
    );

    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            bsStyle="primary"
            onClick={this.toggle}
          >
            Permissions
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggle}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">User permissions</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal &&
              <PermissionTemplate userId={this.props.userId} />
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  addUserPermission,
  fetchUserPermissions,
}, dispatch));

export default connect(null, mapDispatchToProps)(UserPermissionsModal);
