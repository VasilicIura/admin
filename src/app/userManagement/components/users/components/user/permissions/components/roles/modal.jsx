import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, number } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { addUserRole, fetchUserRoles } from '../../../../../actions';
import RoleTemplate from './roles-template';


export class UserRolesModal extends Component {
  static defaultProps = {
    userId: null,
  };

  static propTypes = {
    addUserRole: func.isRequired,
    fetchUserRoles: func.isRequired,
    userId: number,
  };

  state = {
    showModal: false,
  };

  toggle = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = ({ rolePk }) => {
    this.props.addUserRole(this.props.userId, rolePk)
      .then(() => {
        this.props.fetchUserRoles(this.props.userId);
      });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Roles</span>
      </Tooltip>
    );

    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            bsStyle="primary"
            onClick={this.toggle}
          >
            Roles
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggle}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Roles assigned</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.showModal &&
              <RoleTemplate
                userId={this.props.userId}
                onSubmit={this.handleSubmit}
              />
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}


const mapDispatchToProps = dispatch => (bindActionCreators({
  addUserRole,
  fetchUserRoles,
}, dispatch));


export default connect(null, mapDispatchToProps)(UserRolesModal);
