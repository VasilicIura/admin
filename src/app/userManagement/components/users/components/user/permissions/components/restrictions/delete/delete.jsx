import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { fetchUserRestrictions, deleteUserRestriction } from '../../../../../../actions';

const DeleteUserRoleRestriction = (props) => {
  const tooltip = (
    <Tooltip id="tooltip">
      <span>Delete user Restriction</span>
    </Tooltip>
  );

  const deleteUserRolePermission = () => {
    const { userId, name } = props;
    props.deleteUserRestriction(userId, name)
      .then(() => props.fetchUserRestrictions(userId));
  };

  return (
    <div>
      <OverlayTrigger placement="top" overlay={tooltip}>
        <Button
          className="circle-button"
          onClick={deleteUserRolePermission}
        >
          <i className="mi mi-delete" />
        </Button>
      </OverlayTrigger>
    </div>
  );
};

DeleteUserRoleRestriction.defaultProps = {
  userId: null,
  name: null,
};

DeleteUserRoleRestriction.propTypes = {
  deleteUserRestriction: propTypes.func.isRequired,
  userId: propTypes.number,
  name: propTypes.string,
};

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchUserRestrictions,
  deleteUserRestriction,
}, dispatch));

export default connect(null, mapDispatchToProps)(DeleteUserRoleRestriction);
