import React, { Component } from 'react';
import { connect } from 'react-redux';
import { func, array, number, any } from 'prop-types';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { Field, reduxForm } from 'redux-form';
import { Label, Form, Button, ListGroupItem, ListGroup } from 'react-bootstrap';
import { toSelectArray, ControlSelect } from 'fe-shared';
import { DeleteUserRole } from './delete';
import { fetchUserRoles, fetchRoles } from '../../../../../actions';
import { userRoles, roles } from '../../../../../selectors';

class UserRolesModal extends Component {
  static defaultProps = {
    userId: null,
    roles: [],
    rolesList: [],
  };

  static propTypes = {
    fetchUserRoles: func.isRequired,
    fetchRoles: func.isRequired,
    handleSubmit: func.isRequired,
    roles: array,
    rolesList: array,
    userId: number,
    submitting: any.isRequired,
  };

  componentDidMount = () => {
    this.props.fetchUserRoles(this.props.userId);
    this.props.fetchRoles();
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.roles !== this.props.roles) {
      this.listOfRolesOptions();
    }
  };

  listOfRolesOptions = () => {
    const filtered = this.props.rolesList
      .filter(e => this.props.roles.indexOf(e.name) === -1);

    return toSelectArray(filtered.map(e => e.name || e));
  };

  render = () => {
    const { rolesList } = this.props;
    return (
      <div>
        <Form onSubmit={this.props.handleSubmit}>
          <Field
            name="rolePk"
            placeholder="Roles"
            component={ControlSelect}
            options={
              (!!rolesList.length && this.listOfRolesOptions()) || []
            }
            label="Roles"
          />
          <div className="submit-modal-btn">
            <Button
              type="submit"
              disabled={this.props.submitting}
              bsStyle="success"
            >
              Submit
            </Button>
          </div>
        </Form>
        <ListGroup>
          {
            this.props.roles
              .map(role => (
                <ListGroupItem key={role}>
                  <Label className="status-label roles" styles={{ float: 'left' }}>{role}</Label>
                  <DeleteUserRole
                    name={role}
                    userId={this.props.userId}
                  />
                </ListGroupItem>
              ))
          }
        </ListGroup>
      </div>
    );
  };
}

const mapStateToProps = state => ({
  roles: userRoles(state),
  rolesList: roles(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchUserRoles,
  fetchRoles,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'user-roles',
  }),
)(UserRolesModal);
