import React, { Component } from 'react';
import { func, array, number, any } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { Field, reduxForm } from 'redux-form';
import { ListGroup, ListGroupItem, Form, Button } from 'react-bootstrap';
import { toSelectArray, ControlSelect } from 'fe-shared';
import { DeleteUserRoleRestriction } from './delete';
import { fetchUserRestrictions, fetchRestrictions } from '../../../../../actions';
import { userRestrictions, restrictions } from '../../../../../selectors';

class UserRestrictionModal extends Component {
  static defaultProps = {
    userId: null,
    restrictions: [],
    restrictionsList: [],
  };

  static propTypes = {
    fetchUserRestrictions: func.isRequired,
    fetchRestrictions: func.isRequired,
    handleSubmit: func.isRequired,
    restrictions: array,
    restrictionsList: array,
    userId: number,
    submitting: any.isRequired,
  };

  componentDidMount = () => {
    this.props.fetchUserRestrictions(this.props.userId);
    this.props.fetchRestrictions();
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.restrictions !== this.props.restrictions) {
      this.listOfRestrictionOptions();
    }
  };

  listOfRestrictionOptions = () => {
    const filtered = this.props.restrictionsList
      .filter(e => this.props.restrictions.indexOf(e.name) === -1);

    return toSelectArray(filtered.map(e => e.name || e));
  };

  render = () => {
    const { restrictionsList } = this.props;

    return (
      <div>
        <Form onSubmit={this.props.handleSubmit}>
          <Field
            name="restrictionPk"
            placeholder="Restriction"
            component={ControlSelect}
            options={
              (!!restrictionsList.length && this.listOfRestrictionOptions()) || []
            }
            label="Restriction"
          />
          <div className="submit-modal-btn">
            <Button
              type="submit"
              disabled={this.props.submitting}
              bsStyle="success"
            >
              Submit
            </Button>
          </div>
        </Form>
        <ListGroup>
          {
            this.props.restrictions
              .map((restriction, index) => (
                <ListGroupItem
                  key={index}
                  className="list-permissions"
                >
                  <span className="permission-list-info">{restriction}</span>
                  <DeleteUserRoleRestriction
                    name={restriction}
                    userId={this.props.userId}
                    onSubmit={this.handleSubmit}
                  />
                </ListGroupItem>
              ))
          }
        </ListGroup>
      </div>
    );
  };
}

const mapStateToProps = state => ({
  restrictions: userRestrictions(state),
  restrictionsList: restrictions(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchUserRestrictions,
  fetchRestrictions,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'user-restrictions',
  }),
)(UserRestrictionModal);
