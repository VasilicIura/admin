import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, number } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { addUserRestriction, fetchUserRestrictions } from '../../../../../actions';
import RestrictionsTemplate from './restriction-template';

export class UserRestrictionModal extends Component {
  static defaultProps = {
    userId: null,
  };

  static propTypes = {
    addUserRestriction: func.isRequired,
    fetchUserRestrictions: func.isRequired,
    userId: number,
  };

  state = {
    showModal: false,
  };

  toggle = () => this.setState({ showModal: !this.state.showModal });

  handleSubmit = ({ restrictionPk }) => {
    this.props.addUserRestriction(this.props.userId, restrictionPk)
      .then(() => {
        this.props.fetchUserRestrictions(this.props.userId);
      });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Restrictions</span>
      </Tooltip>
    );

    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            bsStyle="primary"
            onClick={this.toggle}
          >
            Restrictions
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggle}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">User restrictions</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.showModal &&
              <RestrictionsTemplate
                userId={this.props.userId}
                onSubmit={this.handleSubmit}
              />
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  addUserRestriction,
  fetchUserRestrictions,
}, dispatch));

export default connect(null, mapDispatchToProps)(UserRestrictionModal);
