import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { fetchUserPermissions, deleteUserPermission } from '../../../../../../actions';

const DeleteUserRolePermission = (props) => {
  const tooltip = (
    <Tooltip id="tooltip">
      <span>Delete user permission</span>
    </Tooltip>
  );

  const deleteUserRolePermission = () => {
    const { userId, name } = props;
    props.deleteUserPermission(userId, name)
      .then(() => props.fetchUserPermissions(userId));
  };

  return (
    <div style={{ float: 'left' }}>
      <OverlayTrigger placement="top" overlay={tooltip}>
        <Button
          className="circle-button"
          onClick={deleteUserRolePermission}
        >
          <i className="mi mi-delete" />
        </Button>
      </OverlayTrigger>
    </div>
  );
};

DeleteUserRolePermission.defaultProps = {
  userId: null,
  name: null,
};

DeleteUserRolePermission.propTypes = {
  deleteUserPermission: propTypes.func.isRequired,
  userId: propTypes.number,
  name: propTypes.string,
};

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchUserPermissions,
  deleteUserPermission,
}, dispatch));

export default connect(null, mapDispatchToProps)(DeleteUserRolePermission);
