import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { number } from 'prop-types';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import ActualPermissions from './actual-permissions';
import { addUserPermission, fetchUserPermissions } from '../../../../../actions';

export class ActualUserPermissionsModal extends Component {
  static defaultProps = {
    userId: null,
  };

  static propTypes = {
    userId: number,
  };

  state = {
    showModal: false,
  };

  toggle = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>actual permissions</span>
      </Tooltip>
    );

    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            bsStyle="success"
            onClick={this.toggle}
          >
            Actual permissions
          </Button>
        </OverlayTrigger>
        <Modal
          show={this.state.showModal}
          onHide={this.toggle}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Actual user permissions</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal &&
              <ActualPermissions userId={this.props.userId} />
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  addUserPermission,
  fetchUserPermissions,
}, dispatch));

export default connect(null, mapDispatchToProps)(ActualUserPermissionsModal);
