import React from 'react';
import { connect } from 'react-redux';
import { array, number, func } from 'prop-types';
import { bindActionCreators } from 'redux';
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import { fetchAllUnpackedPermissions } from '../../../../../actions';
import { unpackedUserPermissions } from '../../../../../selectors';

class ActualPermissions extends React.PureComponent {
  static defaultProps = {
    userId: null,
    actualPermissionsList: [],
  };

  static propTypes = {
    actualPermissionsList: array,
    fetchAllUnpackedPermissions: func.isRequired,
    userId: number,
  };

  componentDidMount = () => this.props.fetchAllUnpackedPermissions(this.props.userId);

  render = () => (
    <div>
      <ListGroup>
        {
          this.props.actualPermissionsList
            .map((permission, index) => (
              <ListGroupItem
                key={index}
                className="list-permissions"
              >
                <span className="permission-list-info">{permission}</span>
              </ListGroupItem>
            ))
        }
      </ListGroup>
    </div>
  );
}


const mapStateToProps = state => ({
  actualPermissionsList: unpackedUserPermissions(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchAllUnpackedPermissions,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(ActualPermissions);
