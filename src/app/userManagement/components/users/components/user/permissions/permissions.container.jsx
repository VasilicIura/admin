import React, { Component } from 'react';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import {
  UserPermissionsModal,
  UserRestrictionModal,
  UserRolesModal,
  ActualUserPermissionsModal,
} from './components';
import './permissions.scss';

export class UserPermissions extends Component {
  state = {
    showModal: false,
  };

  toggle = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>User permissions details</span>
      </Tooltip>
    );

    return (
      <div>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            onClick={this.toggle}
            style={{ float: 'left', marginRight: '10px' }}
          >
            <i className="mi mi-visibility-off" />
          </Button>
        </OverlayTrigger>
        <Modal
          className="userPermissionsModal"
          show={this.state.showModal}
          onHide={this.toggle}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">User permissions details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="clearfix">
              <div style={{ marginBottom: '5px' }}>
                <UserPermissionsModal userId={this.props.id} />
              </div>
              <div style={{ marginBottom: '5px' }}>
                <UserRestrictionModal userId={this.props.id} />
              </div>
              <div>
                <UserRolesModal userId={this.props.id} />
              </div>
              <div style={{ marginTop: '20px' }}>
                <ActualUserPermissionsModal userId={this.props.id} />
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default UserPermissions;
