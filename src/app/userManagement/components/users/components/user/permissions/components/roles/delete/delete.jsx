import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { fetchUserRoles, deleteUserRole } from '../../../../../../actions';

const DeleteUserRole = (props) => {
  const tooltip = (
    <Tooltip id="tooltip">
      <span>Delete user permission</span>
    </Tooltip>
  );

  const deleteRole = () => {
    const { userId, name } = props;
    props.deleteUserRole(userId, name)
      .then(() => props.fetchUserRoles(userId));
  };

  return (
    <div style={{ float: 'left' }}>
      <OverlayTrigger placement="top" overlay={tooltip}>
        <Button
          className="circle-button"
          onClick={deleteRole}
        >
          <i className="mi mi-delete" />
        </Button>
      </OverlayTrigger>
    </div>
  );
};

DeleteUserRole.defaultProps = {
  userId: null,
  name: null,
};

DeleteUserRole.propTypes = {
  deleteUserRole: propTypes.func.isRequired,
  userId: propTypes.number,
  name: propTypes.string,
};

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchUserRoles,
  deleteUserRole,
}, dispatch));

export default connect(null, mapDispatchToProps)(DeleteUserRole);
