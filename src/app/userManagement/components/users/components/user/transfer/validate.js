export const validate = (values) => {
  const errors = {};

  if (!values.amount) {
    errors.amount = 'Required';
  } else {
    const amount = +values.amount;
    if (!amount) errors.amount = 'Invalid amount given';
  }

  if (!values.direction) errors.direction = 'Required';

  return errors;
};
