import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, Button, OverlayTrigger, Tooltip, Glyphicon } from 'react-bootstrap';
import { func, object } from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { SubmissionError } from 'redux-form';
import { transferFunds } from '../../../actions';
import TransferForm from './form';

class EditUser extends Component {
  static defaultProps = {
    user: null,
  };

  static propTypes = {
    user: object,
    transferFunds: func.isRequired,
  };

  state = {
    showModal: false,
  };

  handleSubmit = values =>
    this.props.transferFunds(this.props.user.id, values)
      .then(() => {
        NotificationManager.info('Success', 'Transfer was successful');
        this.toggleModal();
      })
      .catch((err) => {
        let _error = 'Transfer failed';
        if (err.response && err.response.data && err.response.data.message) {
          _error = err.response.data.message;
        }

        return Promise.reject(new SubmissionError({ _error }));
      });

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Transfer</span>
      </Tooltip>
    );

    return (
      <div style={{ float: 'left', marginRight: '10px' }}>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            style={{ display: 'inline-block' }}
            onClick={this.toggleModal}
          >
            <Glyphicon glyph="transfer" />
          </Button>
        </OverlayTrigger>

        <Modal
          show={this.state.showModal}
          onHide={this.toggleModal}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Transfer</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal && this.props.user && (
                <TransferForm
                  onSubmit={this.handleSubmit}
                  user={this.props.user}
                />
              )
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProp = dispatch => (bindActionCreators({
  transferFunds,
}, dispatch));

export default connect(null, mapDispatchToProp)(EditUser);
