import React, { Component } from 'react';
import { Alert, Form, Button } from 'react-bootstrap';
import { any, func } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { ControlInput, ControlSelect } from 'fe-shared';
import { validate } from './validate';

class DepositForm extends Component {
  static propTypes = {
    handleSubmit: func.isRequired,
    submitting: any.isRequired,
  };

  render() {
    const {
      handleSubmit,
      submitting,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="direction"
          placeholder=""
          component={ControlSelect}
          options={[
            { value: 'TO_USER', label: 'To User' },
            { value: 'FROM_USER', label: 'From User' },
          ]}
          label="Transaction Type:"
          currentValue={null}
        />
        <Field
          name="amount"
          placeholder="Amount"
          component={ControlInput}
          label="Amount:"
          doValidate
        />
        {this.props.error && <Alert bsStyle="danger">{this.props.error}</Alert>}
        <Button
          type="submit"
          disabled={submitting}
          bsStyle="success"
        >
          Deposit
        </Button>
      </Form>
    );
  }
}

export default
reduxForm({
  form: 'Transfer',
  validate,
})(DepositForm);
