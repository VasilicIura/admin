import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { func, object } from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { updateUser, searchUsers } from '../../../actions';
import { UsersForm } from '../form';

class EditUser extends Component {
  static defaultProps = {
    user: null,
  };

  static propTypes = {
    user: object,
    updateUser: func.isRequired,
    searchUsers: func.isRequired,
  };

  state = {
    showModal: false,
  };

  handleSubmit = (values) => {
    const model = Object.assign({}, this.props.user, values);
    this.props.updateUser(model)
      .then(() => this.props.searchUsers())
      .then(() => {
        NotificationManager.info('Updated', `Event was updated ${values.username}`);
        this.toggleModal();
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <span>Edit user</span>
      </Tooltip>
    );

    return (
      <div style={{ float: 'left', marginRight: '10px' }}>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Button
            className="circle-button"
            style={{ display: 'inline-block' }}
            onClick={this.toggleModal}
          >
            <i className="mi mi-create" />
          </Button>
        </OverlayTrigger>

        <Modal
          show={this.state.showModal}
          onHide={this.toggleModal}
        >
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Edit user</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal && this.props.user && (
                <UsersForm
                  onSubmit={this.handleSubmit}
                  user={this.props.user}
                />
              )
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProp = dispatch => (bindActionCreators({
  updateUser,
  searchUsers,
}, dispatch));

export default connect(null, mapDispatchToProp)(EditUser);
