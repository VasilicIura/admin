export const validate = (values) => {
  const errors = {};
  const {
    username,
    firstName,
    lastName,
    phoneCountryExtPk,
    phoneNumber,
  } = values;

  if (!username) {
    errors.username = 'User name field is required';
  }

  if (!firstName) {
    errors.firstName = 'First name field is required';
  }

  if (!lastName) {
    errors.lastName = 'Last name field is required';
  }

  if (!phoneCountryExtPk) {
    errors.phoneCountryExtPk = 'Phone country Ext. field is required';
  }

  if (!phoneNumber) {
    errors.phoneNumber = 'Phone number field is required';
  }

  return errors;
};
