import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { Form, Button } from 'react-bootstrap';
import { any, func, object } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { ControlInput, ControlSelect, toSelectArray } from 'fe-shared';
import enums from 'app/enums';
import { validate } from './validate';

class UsersForm extends Component {
    static defaultProps = {
      user: null,
    };
    static propTypes = {
      handleSubmit: func.isRequired,
      submitting: any.isRequired,
      user: object,
    };

  componentDidMount = () => {
    this.props.fetchPhonePrefixes();
  };

  render() {
    const {
      handleSubmit,
      submitting,
      user,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="username"
          placeholder="Username"
          type="text"
          component={ControlInput}
          label="Username"
          currentValue={user ? user.username : ''}
        />
        <Field
          name="firstName"
          placeholder="First Name"
          type="text"
          component={ControlInput}
          label="First Name"
          currentValue={user ? user.firstName : ''}
        />
        <Field
          name="lastName"
          placeholder="Last Name"
          type="text"
          component={ControlInput}
          label="Last Name:"
          currentValue={user ? user.lastName : ''}
        />
        <Field
          name="phoneCountryExtPk"
          placeholder="Phone Country"
          type="number"
          component={ControlSelect}
          label="Prefix:"
          options={toSelectArray(this.props.phonePrefixes)}
          currentValue={user ? user.phoneCountryExtPk : ''}
        />
        <Field
          name="phoneNumber"
          placeholder="Phone Number"
          type="number"
          component={ControlInput}
          label="Phone Number"
          currentValue={user ? user.phoneNumber : ''}
        />
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  phonePrefixes: enums.selectors.getPhonePrefixes(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchPhonePrefixes: enums.actions.fetchPhonePrefixes,
}, dispatch));

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'user',
    validate,
  }),
)(UsersForm);
