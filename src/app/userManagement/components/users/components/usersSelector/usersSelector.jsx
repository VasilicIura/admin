import React from 'react';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import { object, arrayOf } from 'prop-types';

const UsersSelector = (props) => {
  const keys = props.users.map(user => (
    <MenuItem
      eventKey={user.id}
      key={user.id}
      active={user.id === props.selectedUser && props.selectedUser.id}
      onClick={() => props.onSelect(user)}
    >
      {user.username}
    </MenuItem>
  ));

  return (
    <DropdownButton
      bsStyle="primary"
      title={props.selectedUser ? props.selectedUser.username : 'Select an user'}
      id="content-select"
    >
      {keys}
    </DropdownButton>
  );
};

UsersSelector.propTypes = {
  selectedUser: object,
  users: arrayOf(object).isRequired,
};

UsersSelector.defaultProps = {
  selectedUser: object,
};

export default UsersSelector;
