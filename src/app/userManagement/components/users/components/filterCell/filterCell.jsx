import React from 'react';
import { func, object } from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxFilter,
  FilterField,
  FilterInputControl,
  AutoCompleteFilter,
} from 'fe-shared';
import { autocompleteUsers } from '../../actions';

class UsersSwitchFilterCell extends React.PureComponent {
  static defaultProps = {
    column: null,
  };

  static propTypes = {
    autocompleteUsers: func.isRequired,
    column: object,
  };

  switchTemplates = () => {
    const {
      column: {
        id: name,
      },
    } = this.props;

    switch (name) {
      case 'firstName': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteUsers}
          autocompleteKey="firstName"
          labelKey="firstName"
          valueKey="firstName"
          {...this.props}
        />
      );
      case 'lastName': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteUsers}
          autocompleteKey="lastName"
          labelKey="lastName"
          valueKey="lastName"
          {...this.props}
        />
      );
      case 'email': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteUsers}
          autocompleteKey="email"
          labelKey="email"
          valueKey="email"
          {...this.props}
        />
      );
      case 'username': return (
        <FilterField
          name={name}
          component={AutoCompleteFilter}
          autoCompleteAction={this.props.autocompleteUsers}
          autocompleteKey="username"
          labelKey="username"
          valueKey="username"
          {...this.props}
        />
      );
      case 'transactions':
      case 'walletBalance': return (
        <div />
      );
      default: return (
        <FilterField
          name={name}
          component={FilterInputControl}
          {...this.props}
        />
      );
    }
  };

  render = () => this.switchTemplates();
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  autocompleteUsers,
}, dispatch));

export default compose(
  connect(null, mapDispatchToProps),
  reduxFilter({
    filter: 'usersFilter',
  }),
)(UsersSwitchFilterCell);
