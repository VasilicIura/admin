import React from 'react';
import propTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actions } from 'redux-router5';
import { CURRENCIES } from 'app/consts';
import { EditUser, UserDetail, UserPermissions, Transfer, BalanceRecords } from '../user';

const CustomCellTemplate = (props) => {
  switch (props.column.id) {
    case 'actions': return (
      <div>
        <EditUser
          user={props.original}
          style={{ float: 'left' }}
        />
        <UserDetail
          id={props.original.id}
          style={{ float: 'left' }}
        />
        <UserPermissions
          style={{ float: 'left' }}
          id={props.original.id}
        />
        <Transfer
          user={props.original}
          style={{ float: 'left' }}
        />
        <BalanceRecords
          user={props.original}
          style={{ float: 'left' }}
        />
        <Button
          className="circle-button"
          style={{ display: 'inline-block', float: 'left', marginRight: '10px' }}
          onClick={() => props.navigateTo('userBets', { id: props.original.id })}
        >
          <i className="material-icons" >format_bold</i>
        </Button>
      </div>
    );
    case 'transactions': return (
      <Button
        className="circle-button"
        style={{ display: 'inline-block' }}
        onClick={() => props.navigateTo('userTransactions', { id: props.original.id })}
      >
        <i className="material-icons" >title</i>
      </Button>
    );
    case 'walletBalance': return (
      <div>
        {props.original.walletBalance &&
          <div>
            {CURRENCIES[props.original.walletBalance.currency]}
            {props.original.walletBalance.balance.toFixed(2)}
          </div>
        }
      </div>
    );
    default: return (<div>{props.value}</div>);
  }
};

CustomCellTemplate.defaultProps = {
  value: null,
  original: null,
};

CustomCellTemplate.propTypes = {
  column: propTypes.object.isRequired,
  value: propTypes.any,
  original: propTypes.object,
};

const mapDispatchToProps = dispatch => (bindActionCreators({
  ...actions,
}, dispatch));

export default connect(null, mapDispatchToProps)(CustomCellTemplate);
