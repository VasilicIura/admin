import { combineReducers } from 'redux';
import user from './components/user';
import users from './components/users';
import transactions from '../transactions';

export const reducer = combineReducers({
  users: users.reducer,
  user: user.reducer,
  transactions: transactions.reducer,
});
