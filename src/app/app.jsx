import React, { Component } from 'react';
import { PageLayout } from './page';
import { Loader } from './loader';

export default class App extends Component {
  render() {
    return (
      <div>
        <PageLayout {...this.props} />
        <Loader />
      </div>
    );
  }
}

