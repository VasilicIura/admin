import { createAction } from 'redux-actions';
import { commonApi } from 'app/api';
import { getCountries, getPhonePrefixes } from './selectors';
import * as ACTIONS from './actionTypes';

const setCountries = createAction(
  ACTIONS.SET_COUNTRIES,
  countries => countries,
);

const setPhonePrefixes = createAction(
  ACTIONS.SET_PHONE_PREFIXES,
  prefixes => prefixes,
);

export function fetchCountries() {
  return async (dispatch, getState) => {
    const existingCountries = getCountries(getState());

    if (existingCountries && existingCountries.length) return existingCountries;

    const countries = await commonApi.get('enums/countries');
    dispatch(setCountries(countries));
    return countries;
  };
}

export function fetchPhonePrefixes() {
  return async (dispatch, getState) => {
    const phonePrefixes = getPhonePrefixes(getState());

    if (phonePrefixes && phonePrefixes.length) return phonePrefixes;

    const prefixes = await commonApi.get('enums/phone-country-exts');
    dispatch(setPhonePrefixes(prefixes));
    return prefixes;
  };
}
