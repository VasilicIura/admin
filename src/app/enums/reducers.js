import { handleActions } from 'redux-actions';
import { Map } from 'immutable';
import * as ACTIONS from './actionTypes';

const initialState = Map({
  countries: [],
  phonePrefixes: [],
});

export const reducer = handleActions({
  [ACTIONS.SET_COUNTRIES]: (state, action) => state
    .set('countries', action.payload),
  [ACTIONS.SET_PHONE_PREFIXES]: (state, action) =>
    state.set('phonePrefixes', action.payload),
}, initialState);
