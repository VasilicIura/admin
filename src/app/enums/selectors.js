import { createSelector } from 'reselect';

const enumsStateSelector = state => state.enums;

export const getCountries = createSelector(
  enumsStateSelector,
  enums => enums.get('countries'),
);

export const getPhonePrefixes = createSelector(
  enumsStateSelector,
  enums => enums.get('phonePrefixes'),
);
