import * as actions from './actions';
import * as actionTypes from './actionTypes';
import { reducer } from './reducers';
import * as selectors from './selectors';

export default {
  actions,
  actionTypes,
  reducer,
  selectors,
};
