import { createAction } from 'redux-actions';
import { toQueryCriteria, getMarketTypeById } from 'fe-shared';
import { marketTypesSelector } from './selectors';
import { MARKETS_ACTIONS } from './consts';
import { tradingApi, commonApi } from '../api';

const fetchMarkets = createAction(
  MARKETS_ACTIONS.FETCH_MARKETS,
  event => tradingApi
    .get(`markets/event-id/${event.id}`)
    .then(markets => Promise.resolve({ event, markets })),
);

const addMarket = createAction(
  MARKETS_ACTIONS.ADD_MARKET,
  market => tradingApi.post('markets', market),
);

const updateMarket = createAction(
  MARKETS_ACTIONS.ADD_MARKET,
  market => tradingApi.patch(`markets/${market.id}`, market),
);

const fetchSelections = createAction(
  MARKETS_ACTIONS.FETCH_SELECTIONS,
  async (market) => {
    const { content: selections } = await tradingApi.get(`selections/market/${market.id}?size=2000`);
    return { market, selections };
  },
);

const currentSelectionEventKey = active => ({
  type: MARKETS_ACTIONS.SELECTION_EVENT,
  payload: active,
});

const currentMarketKey = active => ({
  type: MARKETS_ACTIONS.SELECTION_MARKET,
  payload: active,
});

const addSelections = createAction(
  MARKETS_ACTIONS.ADD_SELECTIONS,
  selections => tradingApi.put('selections', selections),
);

const fetchEnums = createAction(
  MARKETS_ACTIONS.FETCH_ENUMS,
  type => commonApi.get(`enums/${type}`),
);

const fetchMarketTypes = createAction(
  MARKETS_ACTIONS.MARKET_TYPES,
  () => commonApi.get('enums/market-types'),
);

const fetchEvents = createAction(
  MARKETS_ACTIONS.FETCH_EVENTS,
  request => tradingApi.post('events/search', request),
);

const fetchSelectionParticipants = createAction(
  MARKETS_ACTIONS.FETCH_PARTICIPANT_BY_ID,
  participants => Promise.all(participants.map(participant => tradingApi.get(`participants/${participant.id}`))),
);

const fetchOddsHistory = createAction(
  MARKETS_ACTIONS.FETCH_ODDS_HISTORY,
  (selectionId, query) => tradingApi.get(`selections/${selectionId}/odds-history${query || ''}`),
);

const fetchOutcomesByType = createAction(
  MARKETS_ACTIONS.FETCH_OUTCOMES_BY_TYPE,
  outcomeType => tradingApi.get(`outcomes/type/${outcomeType}`),
);

const searchOutcomesByType = createAction(
  MARKETS_ACTIONS.SEARCH_OUTCOMES_BY_TYPE,
  outcomeType =>
    tradingApi.post('outcomes/search', {
      criteria: [toQueryCriteria('outcomeTypePk', 'IN', outcomeType)],
      pageable: {
        page: 0,
        size: 1000,
      },
    })
      .then(outcomes => Promise.resolve({ type: outcomeType, outcomes })),
);

const setTypeForOutcomes = marketTypeId =>
  (dispatch, getState) => {
    const state = getState();
    const marketTypes = marketTypesSelector(state);
    const currentMarket = getMarketTypeById(marketTypeId, marketTypes);
    dispatch(searchOutcomesByType(currentMarket.label));
  };

export {
  fetchMarkets,
  fetchSelections,
  addMarket,
  fetchEnums,
  addSelections,
  fetchMarketTypes,
  fetchSelectionParticipants,
  fetchOddsHistory,
  fetchEvents,
  fetchOutcomesByType,
  currentSelectionEventKey,
  currentMarketKey,
  searchOutcomesByType,
  setTypeForOutcomes,
  updateMarket,
};
