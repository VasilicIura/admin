import keyMirror from 'keymirror';

export const MARKETS_ACTIONS = keyMirror({
  FETCH_MARKETS: null,
  ADD_MARKET: null,
  FETCH_SELECTIONS: null,
  ADD_SELECTIONS: null,
  FETCH_ENUMS: null,
  FETCH_EVENTS: null,
  MARKET_TYPES: null,
  FETCH_OUTCOMES_BY_TYPE: null,
  FETCH_PARTICIPANT_BY_ID: null,
  FETCH_ODDS_HISTORY: null,
  SELECTION_EVENT: null,
  SELECTION_MARKET: null,
  SEARCH_OUTCOMES_BY_TYPE: null,
  UPDATE_MARKET: null,
});

export const ODDS_GRID = getCustomCell => [
  {
    Header: 'Odds',
    accessor: 'odds',
  },
  {
    Header: 'Market Type',
    accessor: 'marketTypePk',
  },
  {
    Header: 'Target',
    accessor: 'selectionTarget',
  },
  {
    Header: 'Selection type',
    accessor: 'selectionTypePk',
  },
  {
    Header: 'Created',
    accessor: 'timeStamp',
    Cell: cell => getCustomCell(cell),
  },
];

export const SELECTIONS_GRID = [
  { name: 'appliesToCdn', title: 'Selection Target' },
  { name: 'odds', title: 'Odds' },
  { name: 'selectionTypePk', title: 'Selection Types' },
  { name: 'oddsHistoryLink', title: 'Odds Histories' },
];

export const SELECTION_TYPES = {
  BY_PARTICIPANTS: 'By Participant',
  BY_OUTCOMES: 'By Outcome',
};
