import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { array, func, object } from 'prop-types';
import { PanelContainer, PanelElements } from 'fe-shared';
import { AddSelections } from '../selection';
import { fetchMarkets, currentMarketKey } from '../../actions';
import { eventMarketsSelector, currentSelectedMarket } from '../../selectors';
import { Selections } from '../market-selections';
import './event-markets.scss';

class EventMarkets extends Component {
  static defaultProps = {
    marketTypes: [],
    markets: [],
  };

  static propTypes = {
    fetchMarkets: func.isRequired,
    marketTypes: array,
    markets: array,
    event: object.isRequired,
  };

  componentDidMount = () => this.props.fetchMarkets(this.props.event);

  getMarketTypeName = marketTypeId =>
    this.props.marketTypes
      .find(({ value }) => value === marketTypeId);

  panelHeader = ({ market, event }) => (
    <h3>
      <div className="circle-button add-selections">
        <AddSelections
          market={market}
          event={event}
        />
      </div>
    </h3>
  );

  headerTemplate = props => (
    <h3
      onClick={props.handleClick}
      className="selections-panel-header"
    >
      Selections: Market {' '}
      <strong>
        {
          this.getMarketTypeName(props.market.marketTypeId).label ?
            this.getMarketTypeName(props.market.marketTypeId).label : null
        }
      </strong>
    </h3>
  );

  render() {
    const { markets } = this.props;

    return (
      <div>
        {
          this.props.event && markets.map(market => (
            <PanelContainer key={market.id}>
              <PanelElements.Title
                header={props => this.headerTemplate({ ...props, market })}
              >
                {this.panelHeader({ market, event: this.props.event })}
              </PanelElements.Title>
              <PanelElements.Body>
                <Selections market={market} />
              </PanelElements.Body>
            </PanelContainer>
          ))
        }
        {!markets.length && <div>No markets for this event.</div>}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  markets: eventMarketsSelector(state, props),
  currentSelected: currentSelectedMarket(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchMarkets,
  currentMarketKey,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(EventMarkets);
