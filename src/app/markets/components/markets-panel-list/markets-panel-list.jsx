import React from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { eventsListSelector } from 'app/events';
import { PanelContainer, PanelElements, Accordion } from 'fe-shared';
import { EventMarkets } from '../event-markets';
import { AddMarket } from '../market';

const AccordionPanels = (props) => {
  const panelHeaderTemplate = event => (
    <div>
      <div className="add-markets circle-button">
        <AddMarket event={event} />
      </div>
    </div>
  );

  const headerTemplate = headerProps => (
    <span
      role="button"
      className="panel-header-button"
      onClick={headerProps.handleClick}
      style={{ textDecoration: 'underline' }}
    >
      {headerProps.name}
    </span>
  );

  return props.events && props.events.length && (
    <Accordion defaultExpanded="0">
      {
        props.events.map(event => (
          <PanelContainer key={event.id}>
            <PanelElements.Title
              header={headerProps => headerTemplate({ ...headerProps, name: event.name })}
            >
              {panelHeaderTemplate(event)}
            </PanelElements.Title>
            <PanelElements.Body>
              <EventMarkets
                event={event}
                marketTypes={props.marketTypes}
              />
            </PanelElements.Body>
          </PanelContainer>
        ))
      }
    </Accordion>
  );
};


AccordionPanels.defaultProps = {
  events: [],
};

AccordionPanels.propTypes = {
  events: propTypes.array,
};

const mapStateToProps = state => ({
  events: eventsListSelector(state),
});

export default connect(mapStateToProps)(AccordionPanels);
