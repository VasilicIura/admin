import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import { array, any, func } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { ControlSelect } from 'fe-shared';
import { validate } from './validate';

class MarketsForm extends Component {
  static propTypes = {
    handleSubmit: func.isRequired,
    submitting: any.isRequired,
    marketTypes: array.isRequired,
  };

  render() {
    const {
      handleSubmit,
      submitting,
      marketTypes,
    } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field
          name="marketTypeId"
          placeholder="Market type"
          component={ControlSelect}
          options={marketTypes}
          label="Market Type:"
        />
        <div className="submit-modal-btn">
          <Button
            type="submit"
            disabled={submitting}
            bsStyle="success"
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

export default
reduxForm({
  form: 'markets',
  validate,
})(MarketsForm);
