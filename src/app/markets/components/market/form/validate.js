export const validate = (values) => {
  const errors = {};
  const {
    marketTypeId,
  } = values;

  if (!marketTypeId) {
    errors.marketTypeId = 'Market Type field is required';
  }

  return errors;
};
