import React, { Component } from 'react';
import { object } from 'prop-types';
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { FormContainer } from './formContainer';
import './add.scss';

class AddMarket extends Component {
  static propTypes = {
    event: object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  closeModal = () => {
    this.setState({ showModal: false });
  };

  openModal = () => {
    this.setState({ showModal: true });
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        Add Market
      </Tooltip>
    );
    return (
      <div>
        <OverlayTrigger placement="left" overlay={tooltip}>
          <Button
            className="add-market"
            onClick={this.openModal}
          >
            <i className="mi mi-add" />
          </Button>
        </OverlayTrigger>
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Add Market</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormContainer onClose={this.closeModal} event={this.props.event} />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default AddMarket;

