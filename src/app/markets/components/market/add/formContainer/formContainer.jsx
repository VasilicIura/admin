import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, object, array } from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { fetchMarkets, addMarket } from '../../../../actions';
import { MarketsForm } from '../../form';
import { eventMarketsSelector, marketTypesSelector } from '../../../../selectors';


class FormContainer extends Component {
  static propTypes = {
    fetchMarkets: func.isRequired,
    marketTypes: array.isRequired,
    event: object.isRequired,
    onClose: func.isRequired,
    markets: object.isRequired,
    addMarket: object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      unusedMarkets: [],
    };
  }

  componentDidMount() {
    this.props.fetchMarkets(this.props.event).then(() => this.getUnusedMarkets());
  }

  componentWillReceiveProps(marketTypes) {
    if (marketTypes.length !== this.props.marketTypes.length) {
      this.getMarketTypeName();
    }
  }

  getUnusedMarkets = () => {
    if (this.props.markets === null) {
      this.setState({ unusedMarkets: this.props.marketTypes });
      return null;
    }
    const marketTypesNames = this.props.markets.map(
      ({ marketTypeId }) => this.getMarketTypeName(marketTypeId));
    const usedMarkets = marketTypesNames.map(e => e.label);
    const result = this.props.marketTypes.filter(marketType =>
      !usedMarkets.find(marketTypeName => marketTypeName === marketType.label));

    this.setState({ unusedMarkets: result });

    return result;
  };

  getMarketTypeName = marketTypeId =>
    this.props.marketTypes
      .find(({ value }) => value === marketTypeId);

  handleSubmit = (value) => {
    const { id } = this.props.event;
    this.props.addMarket({ ...value, eventId: id })
      .then(() => {
        this.props.fetchMarkets(this.props.event);
        this.props.onClose();
      })
      .catch((err) => {
        const response = err.response.data;
        NotificationManager.error(response.message, `Status ${response.status}`, 3000);
      });
  };

  render() {
    return (
      <MarketsForm
        onSubmit={this.handleSubmit}
        marketTypes={this.state.unusedMarkets}
      />
    );
  }
}

const mapStateToProps = (state, props) => ({
  markets: eventMarketsSelector(state, props),
  marketTypes: marketTypesSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchMarkets,
  addMarket,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(FormContainer);

