import React from 'react';
import propTypes from 'prop-types';
import { AddMarket } from '../market';

const PanelHeaderTemplate = ({ event }) => (
  <div>
    <span style={{ padding: '10px 5px' }}>Event: {event.name}</span>
    <div className="add-markets">
      <AddMarket event={event} />
    </div>
  </div>
);

PanelHeaderTemplate.propTypes = {
  event: propTypes.object.isRequired,
};

export default PanelHeaderTemplate;
