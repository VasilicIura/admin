import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { array, func, object } from 'prop-types';
import { Table } from 'react-bootstrap';
import { RouteLink } from 'fe-shared';
import { SELECTIONS_GRID } from '../../consts';
import { fetchSelections } from '../../actions';
import { marketSelections } from '../../selectors';
import './market-selections.scss';

class Selections extends React.PureComponent {
  static propTypes = {
    fetchSelections: func.isRequired,
    selections: array.isRequired,
    market: object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      columns: SELECTIONS_GRID,
    };
  }

  componentDidMount = () =>
    this.props.fetchSelections(this.props.market);

  renderBody = () => {
    const selectionsArray = this.props.selections
      ? Object.keys(this.props.selections).map(k => this.props.selections[k])
      : [];
    return (
      selectionsArray.map(selection => (
        <tr>
          <th>{selection.appliesToCdn}</th>
          <th>{selection.oddsNumerator} / {selection.oddsDenominator}</th>
          <th>{selection.selectionTypePk}</th>
          <th><RouteLink value="Odds History" href="odds-history" params={{ id: selection.id }} /></th>
        </tr>
      ))
    );
  };

  render() {
    const { columns } = this.state;
    return (
      <Table responsive>
        <thead>
          <tr>
            {columns.map(column => <th>{column.title}</th>)}
          </tr>
        </thead>
        <tbody>
          {this.renderBody()}
        </tbody>
      </Table>
    );
  }
}

const mapStateToProps = (state, props) => ({
  selections: marketSelections(state, props),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchSelections,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(Selections);
