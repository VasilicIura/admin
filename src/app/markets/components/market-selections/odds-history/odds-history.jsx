import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactTable from 'react-table';
import moment from 'moment';
import { any, func } from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { ODDS_GRID } from 'app/markets';
import { oddsHistorySelector } from '../../../selectors';
import { fetchOddsHistory } from '../../../actions';

class OddsHistory extends Component {
  static propTypes = {
    fetchOddsHistory: func.isRequired,
    id: any.isRequired,
  };

  state = {
    oddsHistories: [],
    totalPages: 0,
  };

  getData = (tableState) => {
    const { id } = this.props.route.params; // eslint-disable-line
    const query = `?size=${tableState.pageSize}&page=${tableState.page}`;
    this.props.fetchOddsHistory(+id, query)
      .then(({ value }) => {
        const oddsHistories = value.content.map((oddsHistory) => {
          const { oddsDenominator, oddsNumerator, ...rest } = oddsHistory;
          return {
            odds: ` ${oddsNumerator} / ${oddsDenominator} `,
            ...rest,
          };
        });

        this.setState({
          totalPages: value.totalPages,
          oddsHistories,
        });
      });
  };

  getCustomCell = (props) => {
    if (props.column.id === 'timeStamp') {
      return (<em>{moment(props.value).format('LLL')}</em>);
    }

    return props.value;
  };

  render() {
    return (
      <div className="outcomes-container">
        <Row>
          <Col className="wrap-element">
            <h3 className="outcomes-header">Odds History</h3>
            <ReactTable
              columns={ODDS_GRID(this.getCustomCell)}
              data={this.state.oddsHistories}
              pages={this.state.totalPages}
              minRows={3}
              manual
              defaultPageSize={10}
              sortable={false}
              onFetchData={this.getData}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  oddsHistory: oddsHistorySelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchOddsHistory,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(OddsHistory);
