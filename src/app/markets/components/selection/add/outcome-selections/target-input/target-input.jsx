import React, { Component } from 'react';
import { AppForm, withFormId } from 'fe-shared';
import { any, func, string } from 'prop-types';

const Form = withFormId('target')(AppForm);

class TargetInput extends Component {
  static defaultProps = {
    value: null,
  };

  static propTypes = {
    outcomeId: any.isRequired,
    onChangeTarget: func.isRequired,
    value: string,
  };

  constructor(props) {
    super(props);

    this.state = {
      checked: true,
    };
  }

  checkboxToggle = () => {
    this.setState({ checked: !this.state.checked });
  };

  handleChange = (name, value) => {
    const { outcomeId } = this.props;
    this.props.onChangeTarget(outcomeId, name, value);
  };

  render() {
    return (
      <Form.Form onSubmit={e => e.preventDefault}>
        <div className="target-input-container">
          <div className="target-input-switcher">
            <input
              className="switcher"
              type="checkbox"
              onChange={this.checkboxToggle}
              value={this.state.checked}
              id={`checkbox-${this.props.outcomeId}`}
            />
            <label htmlFor={`checkbox-${this.props.outcomeId}`}>
              <span>{this.props.outcomeId}</span>
            </label>
          </div>
          <div className={`input-container target-input ${!this.state.checked ? 'disabled' : ''}`}>
            <Form.Control
              validations="notEmpty"
              type="text"
              name="appliesToCdn"
              value={this.props.value}
              onChange={this.handleChange}
            />
          </div>
        </div>
      </Form.Form>
    );
  }
}

export default TargetInput;
