import React, { Component } from 'react';
import { Table, Button, Form, HelpBlock, Alert } from 'react-bootstrap';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { FormField, AsyncCheckbox } from 'fe-shared';
import { array, func, object } from 'prop-types';
import { Field, FormSection, reduxForm } from 'redux-form';
import {
  addSelections,
  fetchSelections,
  fetchOutcomesByType,
  setTypeForOutcomes,
  updateMarket,
  fetchMarkets,
} from '../../../../actions';
import { marketSelections, marketTypesSelector, outcomeByType } from '../../../../selectors';
import { SELECTION_TYPES } from '../../../../consts';
import './outcome-selections.scss';

class SelectionCorrectScore extends Component {
  static propTypes = {
    outcomes: array.isRequired,
    setTypeForOutcomes: func.isRequired,
    handleSubmit: func.isRequired,
    addSelections: func.isRequired,
    fetchSelections: func.isRequired,
    fetchMarkets: func.isRequired,
    event: func.isRequired,
    initialize: func.isRequired,
    onSave: func.isRequired,
    market: object.isRequired,
    selections: array.isRequired,
    updateMarket: func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = { error: null };
  }
  componentDidMount() {
    const { market } = this.props;
    this.props.fetchSelections(market);
    this.props.setTypeForOutcomes(market.marketTypeId);
  }

  setBatchModel = (diffKeys, objectDiffers, values) => objectDiffers.map((e) => {
    if (Array.isArray(diffKeys[e])) {
      return {
        ...values[e],
        oddsDenominator: +values[e].oddsDenominator,
        oddsNumerator: +values[e].oddsNumerator,
      };
    }

    return {
      appliesToCdn: this.props.outcomes.find(outcome => outcome.id === +e).outcome,
      marketId: this.props.market.id,
      oddsDenominator: +values[e].oddsDenominator,
      oddsNumerator: +values[e].oddsNumerator,
      selectionTypePk: SELECTION_TYPES.BY_OUTCOMES,
      targetId: +e,
    };
  });

  updateSelections = (values) => {
    const arrayOfvalues = values;
    const arrayOfSelections = this.props.selections;

    const diffKeys = this.difference(arrayOfvalues, arrayOfSelections);
    const objectDiffers = Object.keys(diffKeys);

    if (objectDiffers.length) {
      const batchModel = this.setBatchModel(diffKeys, objectDiffers, values);
      this.props.addSelections(batchModel)
        .then(() => {
          this.props.fetchSelections(this.props.market);
          this.props.onSave();
        });
    }
  };

  difference = (obj, base) =>
    _.transform(obj, (result, value, key) => {
      if (!_.isEqual(value, base[key])) {
        result[key] = _.isObject(value) &&
        _.isObject(base[key]) ? _.difference(value, base[key]) : value;
      }
    });

  updateMarketWithSelections = (outcomeId) => {
    const winningSelectionId = this.props.selections[outcomeId.toString()].id;
    return this.props.updateMarket({ ...this.props.market, winningSelectionId })
      .then(() => {
        this.props.fetchMarkets(this.props.event);
        this.setState({ error: null });
      })
      .then(() => this.props.fetchSelections(this.props.market))
      .then(() => this.props.initialize(this.props.selections))
      .catch((err) => {
        this.setState({ error: err.response.data.message });
      });
  };

  renderWinningSelection = (outcomeId) => {
    const convertToObject = Object.keys(this.props.selections)
      .map(e => this.props.selections[e]);
    const isSelectionForThisId = convertToObject
      .find(selection => selection.targetId === outcomeId);

    if (isSelectionForThisId) {
      return (
        <Field
          name="isWinningSelection"
          component={AsyncCheckbox}
          updateData={this.updateMarketWithSelections}
        />
      );
    }

    return null;
  };

  render = () => (
    <div>
      {
        (
          <Form onSubmit={this.props.handleSubmit(this.updateSelections)}>
            <Table>
              <thead className="selections-participants-header">
                <tr>
                  <th>#Target</th>
                  <th>Odds Numerator</th>
                  <th>Odds Denominator</th>
                  <th>Winning Selection</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.props.outcomes.length && this.props.outcomes.map(outcome => (
                    <FormSection name={outcome.id.toString()}>
                      <tr key={outcome.id} className="selections-participants-body">
                        <td>{outcome.outcome}</td>
                        <td>
                          <div className="odds">
                            <Field
                              className="input-container odds"
                              name="oddsNumerator"
                              component={FormField}
                            />
                          </div>
                        </td>
                        <td>
                          <div className="odds numenator">
                            <Field
                              className="input-container"
                              name="oddsDenominator"
                              component={FormField}
                            />
                          </div>
                        </td>
                        <td>
                          {
                            Object.keys(this.props.selections) &&
                            this.renderWinningSelection(outcome.id)
                          }
                        </td>
                      </tr>
                    </FormSection>
                  ))
                }
              </tbody>
            </Table>
            <div className="submit-btn-container">
              <HelpBlock>
                {this.state.error && <Alert bsStyle="danger">Error: {this.state.error}</Alert>}
              </HelpBlock>
              <Button
                type="submit"
                bsStyle="success"
                className="submit-league"
              >
                Add Selections
              </Button>
            </div>
          </Form>
        )
      }
    </div>
  );
}

const mapStateToProps = (state, props) => ({
  outcomes: outcomeByType(state, props),
  selections: marketSelections(state, props),
  marketTypes: marketTypesSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  addSelections,
  fetchSelections,
  fetchOutcomesByType,
  setTypeForOutcomes,
  updateMarket,
  fetchMarkets,
}, dispatch));

export default compose(
  reduxForm({
    form: 'outcomeSelections',
    enableReinitialize: true,
  }),
  connect(mapStateToProps, mapDispatchToProps),
)(SelectionCorrectScore);
