import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, Button, Tooltip, OverlayTrigger } from 'react-bootstrap';
import { object } from 'prop-types';
import { fetchSelections } from '../../../actions';
import { marketSelections } from '../../../selectors';
import { SwitchSelectionType } from './switch-selection-types';
import './add.scss';

class AddSelections extends Component {
  static propTypes = {
    event: object.isRequired,
    market: object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  closeModal = () => {
    this.setState({ showModal: false });
  };

  openModal = () => {
    this.setState({ showModal: true }, () => this.props.fetchSelections(this.props.market));
  };

  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        Edit Selections
      </Tooltip>
    );

    return (
      <div>
        <OverlayTrigger placement="left" overlay={tooltip}>
          <Button
            className="add-market"
            onClick={this.openModal}
          >
            <i className="mi mi-create" />
          </Button>
        </OverlayTrigger>

        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title className="modal-title">Edit Selections</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.showModal ? (
                <SwitchSelectionType
                  market={this.props.market}
                  onSave={this.closeModal}
                  initialValues={this.props.selections}
                  event={this.props.event}
                />
              ) : null
            }
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  selections: marketSelections(state, props),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchSelections,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(AddSelections);
