import React from 'react';
import { object } from 'prop-types';
import { TeamParticipantsSelections } from '../team-participants';
import { SelectionCorrectScore } from '../outcome-selections';

export const SwitchSelectionType = (props) => {
  SwitchSelectionType.propTypes = {
    market: object.isRequired,
    event: object.isRequired,
  };

  const { marketTypeId } = props.market;

  switch (marketTypeId) {
    case 2: return (<SelectionCorrectScore {...props} />);
    case 3: return (<TeamParticipantsSelections {...props} />);
    default: return (<SelectionCorrectScore {...props} />);
  }
};
