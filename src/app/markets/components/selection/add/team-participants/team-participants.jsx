import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import _ from 'lodash';
import { Table, Button, Form, HelpBlock, Alert } from 'react-bootstrap';
import { func, object, array } from 'prop-types';
import { reduxForm, FormSection, Field } from 'redux-form';
import { FormField, AsyncCheckbox } from 'fe-shared';
import { participantSelector, listOfPlayersSelector, marketSelections } from '../../../../selectors';
import { SELECTION_TYPES } from '../../../../consts';
import { fetchSelectionParticipants, addSelections, fetchSelections, updateMarket, fetchMarkets } from '../../../../actions';
import './team-participants.scss';

class TeamParticipantsSelections extends Component {
  static defaultProps = {
    subparticipants: [],
    market: {},
    initialValues: {},
    selections: {},
  };

  static propTypes = {
    fetchSelectionParticipants: func.isRequired,
    fetchSelections: func.isRequired,
    handleSubmit: func.isRequired,
    onSave: func.isRequired,
    event: object.isRequired,
    fetchMarkets: func.isRequired,
    updateMarket: func.isRequired,
    initialize: func.isRequired,
    addSelections: func.isRequired,
    participants: array.isRequired,
    subparticipants: array,
    market: object,
    initialValues: object,
    selections: array,
  };

  constructor(props) {
    super(props);
    this.state = { error: null };
  }

  componentDidMount() {
    const { participants } = this.props.event;
    this.props.fetchSelectionParticipants(participants);
  }

  setBatchModel = (diffKeys, objectDiffers, values) => objectDiffers.map((e) => {
    if (Array.isArray(diffKeys[e])) {
      return {
        ...values[e],
        oddsDenominator: +values[e].oddsDenominator,
        oddsNumerator: +values[e].oddsNumerator,
      };
    }

    return {
      appliesToCdn: this.props.subparticipants
        .find(subparticipant => subparticipant.id === +e).name,
      marketId: this.props.market.id,
      oddsDenominator: +values[e].oddsDenominator,
      oddsNumerator: +values[e].oddsNumerator,
      selectionTypePk: SELECTION_TYPES.BY_PARTICIPANTS,
      targetId: +e,
    };
  });

  updateSelections = (values) => {
    const arrayOfvalues = values;
    const arrayOfSelections = this.props.initialValues;
    const diffKeys = this.difference(arrayOfvalues, arrayOfSelections);

    const objectDiffers = Object.keys(diffKeys);
    if (objectDiffers.length) {
      const batchModel = this.setBatchModel(diffKeys, objectDiffers, values);
      this.props.addSelections(batchModel)
        .then(() => {
          this.props.fetchSelections(this.props.market);
          this.props.onSave();
        });
    }
  };

  difference = (obj, base) =>
    _.transform(obj, (result, value, key) => {
      if (!_.isEqual(value, base[key])) {
        result[key] = _.isObject(value) &&
        _.isObject(base[key]) ? _.difference(value, base[key]) : value;
      }
    });

  updateMarketWithSelections = (participantId) => {
    const winningSelectionId = this.props.selections[participantId.toString()].id;
    return this.props.updateMarket({ ...this.props.market, winningSelectionId })
      .then(() => {
        this.props.fetchMarkets(this.props.event);
        this.setState({ error: null });
      })
      .then(() => this.props.fetchSelections(this.props.market))
      .then(() => this.props.initialize(this.props.selections))
      .catch((err) => {
        this.setState({ error: err.response.data.message });
      });
  };

  renderWinningSelection = (participantId) => {
    const convertToObject = Object.keys(this.props.selections)
      .map(e => this.props.selections[e]);
    const isSelectionForThisId = convertToObject
      .find(selection => selection.targetId === participantId);

    if (isSelectionForThisId) {
      return (
        <Field
          name="isWinningSelection"
          updateData={this.updateMarketWithSelections}
          component={AsyncCheckbox}
        />
      );
    }

    return null;
  };

  render() {
    return (
      <div>
        <Form onSubmit={this.props.handleSubmit(this.updateSelections)}>
          {
            this.props.participants.map(teamParticipant => (
              <div>
                <h3 className="team-participants-selections-header">{teamParticipant.name}</h3>
                <Table>
                  <thead className="selections-participants-header">
                    <tr>
                      <th>name</th>
                      <th>sport</th>
                      <th>odds numerator</th>
                      <th>odds denominator</th>
                      <th>Winning Selection</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      teamParticipant.subparticipants.map(subParticipant => (
                        <FormSection name={subParticipant.id.toString()}>
                          <tr key={subParticipant.id} className="selections-participants-body">
                            <td>{subParticipant.name}</td>
                            <td>{subParticipant.sportPk}</td>
                            <td>
                              <div className="odds">
                                <Field
                                  className="input-container odds"
                                  type="number"
                                  name="oddsNumerator"
                                  component={FormField}
                                />
                              </div>
                            </td>
                            <td>
                              <div className="odds numenator">
                                <Field
                                  className="input-container"
                                  type="number"
                                  name="oddsDenominator"
                                  component={FormField}
                                />
                              </div>
                            </td>
                            <td>
                              {
                                !!Object.keys(this.props.selections).length &&
                                this.renderWinningSelection(subParticipant.id)
                              }
                            </td>
                          </tr>
                        </FormSection>
                      ))
                    }
                  </tbody>
                </Table>
              </div>
            ))
          }
          <div className="submit-btn-container">
            <HelpBlock>
              {this.state.error && <Alert bsStyle="danger">Error: {this.state.error}</Alert>}
            </HelpBlock>
            <Button
              type="submit"
              bsStyle="success"
              className="submit-league"
            >
              Add Selections
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  participants: participantSelector(state),
  subparticipants: listOfPlayersSelector(state),
  selections: marketSelections(state, props),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchSelectionParticipants,
  addSelections,
  fetchSelections,
  updateMarket,
  fetchMarkets,
}, dispatch));

export default compose(
  reduxForm({
    form: 'participantSelections',
    enableReinitialize: true,
  }),
  connect(mapStateToProps, mapDispatchToProps),
)(TeamParticipantsSelections);
