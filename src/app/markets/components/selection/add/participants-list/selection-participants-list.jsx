import React, { Component } from 'react';
import { Table, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { object, func, array } from 'prop-types';
import { AppForm, withFormId } from 'fe-shared';
import { participantSelector } from '../../../../selectors';
import { fetchSelectionParticipants, fetchSelections, addSelections } from '../../../../actions';
import { SELECTION_TYPES } from '../../../../consts';
import './selection-participants-list.scss';

const Form = withFormId('addSelections')(AppForm);

class SelectionParticipantsList extends Component {
  static propTypes = {
    participants: array.isRequired,
    event: object.isRequired,
    fetchSelectionParticipants: func.isRequired,
    addSelections: func.isRequired,
    fetchSelections: func.isRequired,
    onSave: func.isRequired,
    market: object.isRequired,
  };

  state = {
    model: {},
  };

  componentWillMount() {
    this.setTeamParticipantsModel(this.props.participants);
  }

  componentDidMount() {
    const { participants } = this.props.event;
    this.props.fetchSelectionParticipants(participants);
  }

  componentWillReceiveProps({ participants }) {
    if (participants.length !== this.props.participants.length) {
      this.setTeamParticipantsModel(participants);
    }
  }

  setTeamParticipantsModel = (participants) => {
    const { model } = this.state;

    participants.forEach((team) => {
      model[team.id] = {
        appliesToCdn: team.name,
        targetId: team.id,
        marketId: this.props.market.id,
        selectionTypePk: SELECTION_TYPES.BY_PARTICIPANTS,
      };
    });

    this.setState(model);
  };

  handleChange = (id, name, value) => {
    const { model } = this.state;
    const selection = { ...model[id], [name]: value };
    this.setState({ model: { ...model, [id]: selection } });
  };

  handleSubmit = (unlock, formId) => {
    this.props.addSelections({ ...this.state.model, formId })
      .then(() => {
        this.props.fetchSelections(this.props.market);
        this.props.onSave();
      })
      .catch(() => unlock());
  };

  render() {
    return (
      <div>
        <Form.Form onSubmit={this.handleSubmit}>
          <Table>
            <thead className="selections-participants-header">
              <tr>
                <th>Name</th>
                <th>Sport</th>
                <th>Odds Numerator</th>
                <th>Odds Denominator</th>
              </tr>
            </thead>
            <tbody>
              {
                this.props.participants.map(participant => (
                  <tr key={participant.id} className="selections-participants-body">
                    <td>{participant.name}</td>
                    <td>{participant.sportPk}</td>
                    <td>
                      <div className="odds">
                        <Form.Control
                          className="input-container odds"
                          type="number"
                          name="oddsNumerator"
                          validations="notEmpty"
                          value={this.state.model[participant.id].oddsNumerator}
                          onChange={(...rest) => this.handleChange(participant.id, ...rest)}
                        />
                      </div>
                    </td>
                    <td>
                      <div className="odds numenator">
                        <Form.Control
                          className="input-container"
                          type="number"
                          name="oddsDenominator"
                          validations="notEmpty"
                          value={this.state.model[participant.id].oddsDenominator}
                          onChange={(...rest) => this.handleChange(participant.id, ...rest)}
                        />
                      </div>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </Table>
          <div className="submit-btn-container">
            <Button
              type="submit"
              bsStyle="success"
              className="submit-league"
            >
              Add Selections
            </Button>
          </div>
        </Form.Form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  participants: participantSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  fetchSelectionParticipants,
  fetchSelections,
  addSelections,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(SelectionParticipantsList);
