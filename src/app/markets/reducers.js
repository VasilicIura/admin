import { Map, List } from 'immutable';
import { handleActions } from 'redux-actions';
import { transformToObjectKey } from 'fe-shared';
import { MARKETS_ACTIONS } from './consts';

const initialState = Map({
  /**
   * markets: {
   *  [eventId]: <Array>: Markets
   * }
   * */
  markets: Map(),
  selections: Map(),
  enums: Map(),
  market_types: List(),
  selection_participants: List(),
  odds_history: Map(),
  events: List([]),
  outcomes: List([]),
  currentSelectedEvent: null,
  currentSelectedMarket: null,
  outcomesByType: Map({}),
});

export const reducer = handleActions({
  [`${MARKETS_ACTIONS.FETCH_MARKETS}_FULFILLED`]: (state, action) =>
    state.setIn(['markets', action.payload.event.id.toString()], List(action.payload.markets)),
  [`${MARKETS_ACTIONS.FETCH_SELECTIONS}_FULFILLED`]: (state, action) => {
    const { selections } = action.payload;
    const mappedSelections = selections.reduce((obj, selection) => {
      obj[selection.targetId] = selection; //eslint-disable-line
      return obj;
    }, {});

    return state.setIn(['selections', action.payload.market.id.toString()], Map(mappedSelections));
  },
  [`${MARKETS_ACTIONS.SEARCH_OUTCOMES_BY_TYPE}_FULFILLED`]: (state, action) =>
    state.setIn(['outcomesByType', transformToObjectKey(action.payload.type)], List(action.payload.outcomes.content)),
  [`${MARKETS_ACTIONS.FETCH_ENUMS}_FULFILLED`]: (state, action) =>
    state.set('enums', List(action.payload)),
  [`${MARKETS_ACTIONS.MARKET_TYPES}_FULFILLED`]: (state, action) =>
    state.set('market_types', List(action.payload)),
  [`${MARKETS_ACTIONS.FETCH_PARTICIPANT_BY_ID}_FULFILLED`]: (state, action) =>
    state.set('selection_participants', List(action.payload)),
  [`${MARKETS_ACTIONS.FETCH_ODDS_HISTORY}_FULFILLED`]: (state, action) =>
    state.set('odds_history', Map(action.payload)),
  [`${MARKETS_ACTIONS.FETCH_EVENTS}_FULFILLED`]: (state, action) =>
    state.set('events', List(action.payload.content)),
  [`${MARKETS_ACTIONS.FETCH_OUTCOMES_BY_TYPE}_FULFILLED`]: (state, action) =>
    state.set('outcomes', List(action.payload)),
  [MARKETS_ACTIONS.SELECTION_EVENT]: (state, action) =>
    state.set('currentSelectedEvent', action.payload),
  [MARKETS_ACTIONS.SELECTION_MARKET]: (state, action) =>
    state.set('currentSelectedMarket', action.payload),
}, initialState);
