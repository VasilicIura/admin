import { createSelector } from 'reselect';
import _ from 'lodash';
import { toSelectArrayFromJSON, getMarketTypeById, transformToObjectKey } from 'fe-shared';

const marketsStateSelector = state => state.markets;

const marketsSelector = createSelector(
  marketsStateSelector,
  markets => markets.get('markets').toJS(),
);

const eventMarketsSelector = createSelector(
  marketsSelector,
  (state, props) => props.event,
  (markets, event) => markets[event.id] || [],
);

const selectionsSelector = createSelector(
  marketsStateSelector,
  selections => selections.get('selections').toJS(),
);

const marketSelections = createSelector(
  selectionsSelector,
  (state, props) => props.market,
  (selections, market) => {
    const selectionsByMarket = selections[market.id.toString()];

    if (!_.isEmpty(selectionsByMarket)) {
      const transformToObject = Object.keys(selectionsByMarket).map((e) => {
        if (selectionsByMarket[e].id === market.winningSelectionId) {
          selectionsByMarket[e].isWinningSelection = true;
          return selectionsByMarket[e];
        }

        selectionsByMarket[e].isWinningSelection = false;
        return selectionsByMarket[e];
      });

      return transformToObject.reduce((obj, selection) => {
        obj[selection.targetId] = selection; //eslint-disable-line
        return obj;
      }, {});
    }

    return selections[market.id] || [];
  },
);

const selectionTypesSelector = createSelector(
  marketsStateSelector,
  selectionTypes => selectionTypes.get('enums').toJS(),
);

const marketTypesSelector = createSelector(
  marketsStateSelector,
  marketTypes =>
    toSelectArrayFromJSON(marketTypes.get('market_types').toJS(), 'id', 'name'),
);

const participantSelector = createSelector(
  marketsStateSelector,
  participants => participants.get('selection_participants').toJS(),
);

const oddsHistorySelector = createSelector(
  marketsStateSelector,
  oddsHistory => oddsHistory.get('odds_history').toJS(),
);

const eventsSelector = createSelector(
  marketsStateSelector,
  events => events.get('events').toJS(),
);

const outcomesByTypeSelector = createSelector(
  marketsStateSelector,
  outcomes => outcomes.get('outcomes').toJS(),
);

const currentSelectedEvent = createSelector(
  marketsStateSelector,
  selectedEvent => selectedEvent.get('currentSelectedEvent'),
);
const currentSelectedMarket = createSelector(
  marketsStateSelector,
  selectedEvent => selectedEvent.get('currentSelectedEvent'),
);

const outcomesByTypesSelector = createSelector(
  marketsStateSelector,
  marketsState => marketsState.get('outcomesByType').toJS(),
);

const listOfPlayersSelector = createSelector(
  participantSelector,
  (participants) => {
    let selectionParticipants = []; //eslint-disable-line
    participants.length && participants //eslint-disable-line
      .forEach((participant) => {
        participant.subparticipants.forEach(e => selectionParticipants.push(e));
      });

    return selectionParticipants;
  },
);

const outcomeByType = createSelector(
  outcomesByTypesSelector,
  marketTypesSelector,
  (state, props) => props,
  (outcomeTypesState, markets, props) => {
    const currentMarketType = getMarketTypeById(props.market.marketTypeId, markets);
    const stateKey = transformToObjectKey(currentMarketType.label);
    return outcomeTypesState[stateKey] || [];
  },
);

export {
  eventMarketsSelector,
  marketSelections,
  selectionTypesSelector,
  marketTypesSelector,
  participantSelector,
  oddsHistorySelector,
  eventsSelector,
  outcomesByTypeSelector,
  currentSelectedEvent,
  currentSelectedMarket,
  outcomeByType,
  listOfPlayersSelector,
};
