import React, { Component } from 'react';
import { connect } from 'react-redux';
import { array, func, object } from 'prop-types';
import { bindActionCreators } from 'redux';
import { BasePagination } from 'fe-shared';
import { filteredEvents, searchEvents, changePagination } from 'app/events';
import { AccordionPanels } from './components';
import { fetchMarketTypes, fetchEvents, currentSelectionEventKey } from './actions';
import { marketTypesSelector, currentSelectedEvent } from './selectors';
import './markets.scss';

class Markets extends Component {
  static propTypes = {
    events: object.isRequired,
    searchEvents: func.isRequired,
    changePagination: func.isRequired,
    fetchMarketTypes: func.isRequired,
    marketTypes: array.isRequired,
  };

  state = {
    pageSize: 10,
    activePage: 0,
  };

  componentDidMount = () => {
    this.props.fetchMarketTypes();
    this.getEvents();
  };

  getEvents = () => {
    this.props.changePagination({
      page: this.state.activePage,
      size: this.state.pageSize,
    });

    this.props.searchEvents();
  };

  getSizeOfPages = (total, maxOnPage) => Math.ceil(total / maxOnPage);


  changeItemsPerPage = ({ value }) => {
    this.setState({ pageSize: value }, () => this.getEvents());
  };

  handleChangePage = (eventKey) => {
    this.setState({ activePage: eventKey }, () => this.getEvents());
  };

  renderPagination = () => (
    <BasePagination
      pageSize={this.getSizeOfPages(this.props.events.totalElements, this.state.pageSize)}
      changeCurrentPage={this.handleChangePage}
      activePage={this.state.activePage}
    />
  );

  render = () => (
    <div>
      <h3> Markets Workflow </h3>
      {this.renderPagination()}
      <div className="markets-list">
        <div>
          {
            <AccordionPanels
              marketTypes={this.props.marketTypes}
            />
          }
        </div>
      </div>
      {this.renderPagination()}
    </div>
  );
}

const mapStateToProp = state => ({
  events: filteredEvents(state),
  marketTypes: marketTypesSelector(state),
  currentSelected: currentSelectedEvent(state),
});

const mapDispatchToProp = dispatch => (bindActionCreators({
  fetchEvents,
  searchEvents,
  fetchMarketTypes,
  changePagination,
  currentSelectionEventKey,
}, dispatch));

export default connect(mapStateToProp, mapDispatchToProp)(Markets);
