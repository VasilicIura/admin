import React, { Component } from 'react';
import { bool } from 'prop-types';
import { connect } from 'react-redux';
import { Grid, Col, Row } from 'react-bootstrap';
import { NotificationContainer } from 'react-notifications';
import { bindActionCreators } from 'redux';
import { HeaderComponent } from 'fe-shared';
import { logoutUser } from '../auth/actions';
import { isAuthenticatedSelector } from '../auth/selectors';
import './page.layout.scss';

class PageLayout extends Component {
  static defaultProps = {
    isAuthenticated: false,
  };

  static propTypes = {
    isAuthenticated: bool,
  };

  render() {
    const { isAuthenticated } = this.props;
    return (
      <Grid fluid>
        <div className="main-container">
          <Row>
            {isAuthenticated &&
              <HeaderComponent
                onLogOut={this.props.logoutUser}
              />
            }
          </Row>
          <Row className="row-eq-height">
            <Col md={12} className="base-content">{ this.props.children }</Col>
          </Row>
        </div>
        <div
          onClick={e => e.stopPropagation()}
          role="button"
          tabIndex="0"
        >
          <NotificationContainer />
        </div>
      </Grid>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: isAuthenticatedSelector(state),
});

const mapDispatchToProps = dispatch => (bindActionCreators({
  logoutUser,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(PageLayout);
